EnvX.Resources = {Ids={},Names={},Data={},Interactions={}}

function EnvX.RegisterResource(name,data)
	local index = table.Count(EnvX.Resources.Ids) + 1
	EnvX.Resources.Ids[name] = index
	EnvX.Resources.Names[index] = data.DisplayName
	EnvX.Resources.Data[index] = data
	
	print("Registered Resource.. "..data.DisplayName)
end

function EnvX.GetDisplayName(name)
	local ID = EnvX.Resources.Ids[name]
	
	if ID then
		return EnvX.Resources.Names[ID]
	else
		return name
	end
end

function EnvX.GetResourceData(name)
	local ID = EnvX.Resources.Ids[name]

	if ID then
		return EnvX.Resources.Data[ID]
	end
end

function EnvX.RegisterInteraction(res1,res2,value)
	local Id1,Id2 = EnvX.Resources.Ids[res1], EnvX.Resources.Ids[res2]
	
	if not Id1 or not Id2 then print("You can't register interactions for resources before you register the resources themselfs! "..res1.."|"..res2) return end
	
	EnvX.Resources.Interactions[Id1] = EnvX.Resources.Interactions[Id1] or {}
	EnvX.Resources.Interactions[Id1][Id2] = value

	EnvX.Resources.Interactions[Id2] = EnvX.Resources.Interactions[Id2] or {}
	EnvX.Resources.Interactions[Id2][Id1] = value
end

EnvX.RegisterResource("energy",{
	MUnit = " kJ", --Measurement Unit.
	DisplayName = "Energy", --Display Name for clientside.
	StorePerUnit = 3600, --How much of the resource can be stored in a singular "storage unit".
	Volatality = 0, --How Explosive a resource is.
	MaintainCost = {} --Resource costs to maintain a storage of this resource.
})

----------Base Resources--------------
--Not Volatile
EnvX.RegisterResource("water",{MUnit = " L", DisplayName = "Water", StorePerUnit = 360, Volatality = -1, MaintainCost = {}})

--Gases
EnvX.RegisterResource("nitrogen",{MUnit = " L", DisplayName = "Nitrogen", StorePerUnit = 2300, Volatality = 0, MaintainCost = {}})
EnvX.RegisterResource("steam",{MUnit = " L", DisplayName = "Steam", StorePerUnit = 720, Volatality = -0.2, MaintainCost = {}})
EnvX.RegisterResource("carbon dioxide",{MUnit = " L", DisplayName = "CO2", StorePerUnit = 2300, Volatality = -0.2, MaintainCost = {}})

--Volatile
EnvX.RegisterResource("hydrogen",{MUnit = " L", DisplayName = "Hydrogen", StorePerUnit = 2300, Volatality = 1, MaintainCost = {}})
EnvX.RegisterResource("oxygen",{MUnit = " L", DisplayName = "Oxygen", StorePerUnit = 2300, Volatality = 0.2, MaintainCost = {}})

----------Mining Resources--------------
--Not Volatile
EnvX.RegisterResource("Raw Ore",{MUnit = " kg", DisplayName = "Raw Ore", StorePerUnit = 1000, Volatality = 0, MaintainCost = {}})
EnvX.RegisterResource("Refined Ore",{MUnit = " kg", DisplayName = "Refined Ore", StorePerUnit = 800, Volatality = 0, MaintainCost = {}})
EnvX.RegisterResource("Hardened Ore",{MUnit = " kg", DisplayName = "Hardened Ore", StorePerUnit = 600, Volatality = 0, MaintainCost = {}})
EnvX.RegisterResource("Carbon",{MUnit = " kg", DisplayName = "Carbon", StorePerUnit = 900, Volatality = 0, MaintainCost = {}})

--Volatile
EnvX.RegisterResource("Crystalized Polylodarium",{MUnit = " kg", DisplayName = "Crystalized Polylodarium", StorePerUnit = 700, Volatality = 4, MaintainCost = {}})
EnvX.RegisterResource("Liquid Polylodarium",{MUnit = " L", DisplayName = "Liquid Polylodarium", StorePerUnit = 800, Volatality = 6, MaintainCost = {}})
EnvX.RegisterResource("AntiMatter",{MUnit = " g", DisplayName = "AntiMatter", StorePerUnit = 500, Volatality = 80, MaintainCost = {energy={1,100}}})

----------Ammounition Resources--------------
--Not Volatile
EnvX.RegisterResource("Casings",{MUnit = "", DisplayName = "Casings", StorePerUnit = 800, Volatality = 0, MaintainCost = {}})
EnvX.RegisterResource("Missile Parts",{MUnit = "", DisplayName = "Missile Parts", StorePerUnit = 400, Volatality = 0, MaintainCost = {}})

--Volatile
EnvX.RegisterResource("Basic Rounds",{MUnit = "", DisplayName = "Basic Rounds", StorePerUnit = 600, Volatality = 1, MaintainCost = {}})

EnvX.RegisterResource("Basic Shells",{MUnit = "", DisplayName = "Basic Shells", StorePerUnit = 400, Volatality = 4, MaintainCost = {}})
EnvX.RegisterResource("Heavy Shells",{MUnit = "", DisplayName = "Heavy Shells", StorePerUnit = 200, Volatality = 8, MaintainCost = {}})

EnvX.RegisterResource("Plasma",{MUnit = " L", DisplayName = "Plasma", StorePerUnit = 6000, Volatality = 10, MaintainCost = {}})
EnvX.RegisterResource("BlackHolium",{MUnit = "", DisplayName = "BlackHolium", StorePerUnit = 20, Volatality = -900, MaintainCost = {}})

----------What is this here....--------------
EnvX.RegisterResource("Potatoes",{MUnit = "", DisplayName = "Potatoes", StorePerUnit = 100, Volatality = 0, MaintainCost = {}})


















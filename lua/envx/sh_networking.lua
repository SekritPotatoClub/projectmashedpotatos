local EnvX = EnvX --Localise the global table for speed.
local Utl = EnvX.Utl --Makes it easier to read the code.

Utl.NetMan = Utl.NetMan or {} --Where we store the queued up data to sync.

local NDat = Utl.NetMan --Ease link to the netdata table.
NDat.Data = NDat.Data or {} -- The actual table we store data in.
NDat.Hookers = NDat.Hookers or {} --The table we store net hooks in.

---------Config Options for networking, Make sure you know what your doing before changing!

local ComplexMults = {string=0.2,player=1,entity=1,weapon=1,number=1,vector=1,angle=1,boolean=1,table=1}--The complexity multipliers for
local ServerMaxComplexity = 300 --The maximum complexity total we can send in a single network cycle, per player on the server.
local ClientMaxComplexity = 100 --The maximum complexity a client can send to the server per network cycle.
local MaxBatchSize = 50 --The maximum complexity a single segment can contain.

local NetCycleDelay = 0.1 --How long in seconds between each network cycle.


-----------------Exposed Functions.----------------

function Debug(MSG) 
	--print(MSG) 
end

function EnvX:HookNet(MSG,Func) NDat.Hookers[MSG] = Func end --Create a function to hook into the net library.
function EnvX:NetworkData(Name,Data,Ply) --Sends data out
	local MSG = {}--Create the message table.
	--[[Data={Name="example",Val=1,Dat={VName="example"}}]]		
	MSG.Name = Name
	
	local Complexity = NDat.DataComplexity(Data) Debug("Message: "..Name.." Segment Complexity: "..Complexity)
	if Complexity > MaxBatchSize and type(Data) == "table" then 
		return NDat.SegmentSend(Name,Data,Ply)
	end
	MSG.Val = Complexity 
	
	MSG.Dat = Data
	
	if SERVER then
		if Ply ~= nil then--Is there a variable for ply?
			if IsValid(Ply) then--Check if the client is valid!
				return NDat.AddData(MSG,Ply)--Special telegraph!
			else
				--Invalid player entity?
			end
		else
			return NDat.AddDataAll(MSG)--Send the data to all clients!
		end
	else
		return NDat.AddData(MSG)--Sending stuff to the server eh?
	end
end

--Bulk Data Test. lua_run local Data = {} for I=1,500 do table.insert(Data,{"SPAM"}) end EnvX:NetworkData("Test",Data)
--Large Data Test. lua_run local Data = {} for I=1,5 do local T = {} for X=1,I*10 do table.insert(T,{"SPAM"}) end table.insert(Data,T) end EnvX:NetworkData("Test",Data)
EnvX:HookNet("Test",function(Dat) PrintTable(Dat) end)

--[[------------------------------------------------
			Internal Functions Below
------------------------------------------------]]--

function NDat.SegmentSend(Name,Data,Ply)
	local Msgs = {}
	
	--The id we will use for this batch of segmented data. (Todo: Use a better method to get the ids.)
	local ID = tostring(math.random(1,1000))..Name 
	
	--Lets prepare the data so we can smartly segment it for transmission.
	local Segments = {}
	for tab, dat in pairs( Data ) do
		table.insert(Segments,{Complexity = NDat.DataComplexity(dat),Key=tab,Data = dat})
	end
	
	local Processing = true
	while Processing == true do --While we are processing the net message run.
		local MsgComplexity = 0
		local Message = {}
		for tab, dat in pairs( Segments ) do --Lets try combining segments to save on the amount of netmessages we have to make.
			if MsgComplexity == 0 or MsgComplexity+dat.Complexity < MaxBatchSize then --Todo: Make tables above the size, trigger their own segmented send call.
				MsgComplexity=MsgComplexity+dat.Complexity
				
				Message[dat.Key]=dat.Data--Combine the segmented data.
				
				Segments[tab]=nil --Remove the data from the table.
			end
		end
		
		table.insert(Msgs,{Name=Name,ID=ID,Data=Message,Total=0})--Add the message to our list to send.
		
		if table.Count(Segments)<=0 then Processing = false end --Hope this doesnt cause a infinite loop, otherwise ill have to get crazy.
	end
	
	--table.insert(Msgs,{Name=Name,ID=ID,Data=dat,Total=table.Count(Data)})
	
	local SegmentTotal = table.Count(Msgs)
	for tab, dat in pairs( Msgs ) do --Create the segments.
		dat.Total = SegmentTotal
	end
	
	local SuccessAll = true
	for x, Dat in pairs( Msgs ) do
		local MSG = {}
		
		MSG.Name = "Spehs_SegmentedData"
		
		local Complexity = NDat.DataComplexity(Dat)
		MSG.Val = Complexity Debug("Message: "..Name.." Segment Complexity: "..Complexity)
		
		MSG.Dat = Dat	
	
		if SERVER then
			if Ply ~= nil then--Is there a variable for ply?
				if IsValid(Ply) then--Check if the client is valid!
					if NDat.AddData(MSG,Ply)==false then --Special telegraph!
						SuccessAll = false
					end
				else
					--Invalid player entity?
				end
			else
				if NDat.AddDataAll(MSG) == false then--Send the data to all clients!
					SuccessAll = false
				end
			end
		else
			if NDat.AddData(MSG) == false then--Sending stuff to the server eh?
				SuccessAll = false
			end
		end
	end
	
	return SuccessAll
end

local RecievedSegments = {}
function NDat.SegmentRecieve(Dat,ply) 
	local Meta = {}
	
	if SERVER then --If the server recieves segment data isolate it per player.
		if RecievedSegments[ply:Nick()] == nil then RecievedSegments[ply:Nick()] = {} end
		if RecievedSegments[ply:Nick()][Dat.ID] == nil then RecievedSegments[ply:Nick()][Dat.ID] = {Segments={},Total=Dat.Total} end
		Meta = RecievedSegments[ply:Nick()][Dat.ID]	
	else --If the client recieves segmented data use a simplier path.
		if RecievedSegments[Dat.ID] == nil then RecievedSegments[Dat.ID] = {Segments={},Total=Dat.Total} end
		Meta = RecievedSegments[Dat.ID]
	end
			
	table.insert(Meta.Segments,Dat.Data)--Add the recieved segments data.
	
	local CountSegments = table.Count(Meta.Segments)
	if CountSegments == Dat.Total then
		local Data = {}
		
		--Time to reconstruct the data.
		for seg, dat in pairs( Meta.Segments ) do
			for tab, var in pairs( dat ) do
				Data[tab]=var
			end
		end
		
		Debug("Recieved all segments!")
		
		NDat:InNetF(Dat.Name,Data,ply) --Call recieved netmessage function with our reconstructed data set.
		
		--Clear the segment data incase a new message gets sent with the same name.
		if SERVER then
			
		else
			RecievedSegments[Dat.ID] = nil
		end
	else
		Debug("Recieved: "..CountSegments.." out of "..Dat.Total.." segments.")
	end
end
EnvX:HookNet("Spehs_SegmentedData",NDat.SegmentRecieve)

local ComplexFuncs = { -- Contains functions that help is figure out the complexity of data.
	string=function(d) return string.len(d) end,
	player=function(d) return 1 end,
	entity=function(d) return 0.1 end,
	number=function(d) return 2 end,
	vector=function(d) return 6 end,
	angle=function(d) return 3 end,
	boolean=function(d) return 0.05 end,
	table=function(d) return NDat.DataComplexity(d) end,
	weapon=function(d) return 0.1 end
}

--Returns the estimated complexity of data inputed.
function NDat.DataComplexity(Data)
	local Complexity = 0
	
	local Type = string.lower(type(Data)) --Get the type of the data were checking.
	if ComplexFuncs[Type]==nil then print("I Dont understand this datatype: "..Type) end
	
	if Type == "table" then --If its a table we want to check all its children.
		for tab, dat in pairs( Data ) do
			local dType = string.lower(type(dat))
			if ComplexFuncs[dType]==nil then print("I Dont understand this datatype: "..dType) end
			Complexity = Complexity+ComplexFuncs[dType](dat)*ComplexMults[dType]
		end
	else
		Complexity = Complexity+ComplexFuncs[Type](Data)*ComplexMults[Type] --Add the detected complexity to our total.
	end
	
	--print(Type.." Complexity: "..Complexity)
	
	return Complexity 
end

--Massive data tables, with the purpose of allowing us to automagically transmit data.
local NumBool = function(V) if V then return 1 else return 0 end end --Bool to number.
local BoolNum = function(V) if V>0 then return true else return false end end --Number to bool.
NDat.NetDTWrite = {S=net.WriteString,E=function(V) net.WriteFloat(V:EntIndex()) end,F=net.WriteFloat,V=net.WriteVector,A=net.WriteAngle,B=function(V) net.WriteFloat(NumBool(V)) end}
NDat.NetDTRead = {S=net.ReadString,E=function(V) return Entity(net.ReadFloat()) end,F=net.ReadFloat,V=net.ReadVector,A=net.ReadAngle,B=function() return BoolNum(net.ReadFloat()) end}
NDat.Types = {string="S",player="E",entity="E",number="F",vector="V",angle="A",boolean="B",table="T"}

--Since converting to json doesnt seem to work 100% of the time anymore made a new set of functions to handle them better.
NDat.NetDTWrite["T"] = function(V) 
	net.WriteFloat(table.Count(V)) --How Many variables are we sending?
	
	for I, S in pairs( V ) do NDat.WriteData(I,S) end --Transmit our variables.
end

NDat.NetDTRead["T"] = function() 
	local Data = {} --Empty Data Table to put our data into.
	
	local Count = net.ReadFloat()
	for I=1,Count do --Read all the variables.
		local isNumber = net.ReadBool()
		local k,v = net.ReadString(),net.ReadString()
		if isNumber then k = tonumber(k) end
		Data[k]=NDat.NetDTRead[v]()
	end
	
	return Data
end

function NDat.WriteData(Name,Value)
	local Type = NDat.Types[string.lower(type(Value))]
	if Type then
		--print("Sending Data "..Type.." : "..tostring(Value))
		net.WriteBool(type(Name) == "number") --Write if Name is Number
		net.WriteString(Name)--Write the variable name.
		net.WriteString(Type)--Write the variables type.
		NDat.NetDTWrite[Type](Value)
	else
		print("Unknown Type Entered! "..Type)
	end
end

--Internal function dealing with the transmission of data.
function NDat.SendData(Data,Name,ply)
	--print("Sending Data: "..Name)
	if not Data.Dat then error("Netcode Failure! Missing Dat table. ["..tostring(Name).."]") return false end --Damn Missing Dat table....
	net.Start("envx_basenetmessage") --Start the netmessage, using the base string indentifier for easy recieving.
	
		net.WriteString(Name) --Write the name of the hook to call, when recieved.
		net.WriteFloat(table.Count(Data.Dat)) --Write how much data we will be sending.
		
		for I, S in pairs( Data.Dat ) do NDat.WriteData(I,S) end --Loop all the variables.
		
	if SERVER then--Are we the server or a client?
		net.Send(ply)
	else
		net.SendToServer()
	end
	return true
end

function NDat:InNetF(MSG,Data,ply)
	if NDat.Hookers[MSG] then
		xpcall(function() --Always wear protection!
			NDat.Hookers[MSG](Data,ply)
		end,ErrorNoHalt)
		--print("Recieved Message: "..MSG)
	else 
		print("Unhandled message... "..MSG) 
	end 
end

--Function that receives the netmessage.
net.Receive( "envx_basenetmessage", function( length, ply )
	local Name = net.ReadString() --Gets the name of the message.
	local Count = net.ReadFloat() --Get the amount of variables were recieving.

	local D = {}
	for I=1,Count do --Read all the variables.
		local isNumber = net.ReadBool()
		local VN = net.ReadString()
		local Ty = net.ReadString()
		if isNumber then VN = tonumber(VN) end
		if VN == nil or Ty == nil or NDat.NetDTRead[Ty] == nil then print(tostring(VN).." "..tostring(Ty)) end
		D[VN]=NDat.NetDTRead[Ty]()
	end
	NDat:InNetF(Name,D,ply)	
end)


if(SERVER)then	
	--[[----------------------------------------------------
	Serverside Networking Handling.
	----------------------------------------------------]]--
	util.AddNetworkString( "envx_basenetmessage" )
	
	function NDat.AddDataAll(Data)
		Utl:LoopValidPlayers(function(ply) NDat.AddData(Data,ply) end)
	end
	
	--Creates the table we will use for each player.
	function NDat.AddPlay(ply)
		NDat.Data[ply:Nick()] = {Data={},Ent=ply}
	end
	Utl:HookHook("PlayerInitialSpawn","NetDatHook",NDat.AddPlay,1)
end

--[[ Data={Name="example",Val=1,Dat={VName="example"}} ]]	
function NDat.AddData(Data,ply)
	if not Data.Dat then error("Netcode Failure! Missing Dat table. ["..tostring(Data.Name).."]") return false end --Damn Missing Dat table....
	
	if SERVER then
		local T=NDat.Data[ply:Nick()]
		if not T then NDat.AddPlay(ply) NDat.AddData(Data,ply) return false end
		table.insert(T.Data,Data)
	else
		table.insert(NDat.Data,Data)
	end

	return true
end

--Setup the think to power the networking.
local NextThink = 0
hook.Add("Think","EnvxNetCode",function()
	xpcall(function()
		if CurTime()>NextThink then NextThink = CurTime()+NetCycleDelay
			if SERVER then--Are we running server or clientside?
				for nick, pdat in pairs( NDat.Data ) do	--Loop all the players to send their data
					local Max = ServerMaxComplexity	--The maximum amount of data we can send per player.
					for id, Data in pairs( pdat.Data ) do
						if Max<=0 then break end--We reached the maximum amount of data for this player.
						Max=Max-Data.Val --Subtract the complexity of the data from our max.
						NDat.SendData(Data,Data.Name,pdat.Ent) --
						table.remove(pdat.Data,id)
					end
				end			
			else
				local Max = ClientMaxComplexity --The maximum amount of data we will be sending at once.
				for id, Data in pairs( NDat.Data ) do
					if Max<=0 then break end--We reached the maximum amount of data we can send this cycle.
					Max=Max-Data.Val
					-- PrintTable(Data)
					NDat.SendData(Data,Data.Name)
					table.remove(NDat.Data,id)
				end			
			end
		end
	end,ErrorNoHalt)
end)













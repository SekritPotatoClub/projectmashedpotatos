
local function AddToolTab()
	-- Add Tab
	local logo;
	--if(file.Exists("..logo")) then logo = "logo" end;
	spawnmenu.AddToolTab("EnvX","EnvX",logo)
	-- Add Config Category
	spawnmenu.AddToolCategory("EnvX","Config"," Config");
	-- Add the admin menu
	spawnmenu.AddToolMenuOption("EnvX","Config","Admin Tools","Admin Tools","","",Environments.AdminMenu,{});
end
hook.Add("AddToolMenuTabs", "EnvironmentsAddTabs", AddToolTab);

hook.Add( "PopulateMenuBar", "EnvironmentsAddMenubar", function( menubar )
    local m = menubar:AddOrGetMenu( "EnvX" )
	
	--local sub = m:AddSubMenu( "Admin Options")
	
	m:AddOption("Reload EnvX", function() RunConsoleCommand("env_server_reload") end)
	m:AddOption("Fully Reload EnvX", function() RunConsoleCommand("env_server_full_reload") end)
	
    m:AddSpacer()
    
    m:AddSpacer()
	
	m:AddOption("Reload HUD", function() RunConsoleCommand("env_reload_hud") end)
end )

local function Bool2Num(b)
	if b == true then
		return "1"
	else
		return "0"
	end
end

local Menu = {}
local PlanetData = {}
local function GetData(msg)
	PlanetData["name"] = msg:ReadString()
	PlanetData["gravity"] = msg:ReadFloat()
	PlanetData["unstable"] = msg:ReadBool()
	PlanetData["sunburn"] = msg:ReadBool()
	PlanetData["temperature"] = msg:ReadFloat()
	PlanetData["suntemperature"] = msg:ReadFloat()
	PlanetData["safezone"] = msg:ReadFloat()
	
	Menu.List:Clear()
	for k,v in pairs(PlanetData) do
		Menu.List:AddLine(k,tostring(v))
	end
end
usermessage.Hook("env_planet_data", GetData)

function Environments.AdminMenu(Panel)
	Panel:ClearControls()
	if LocalPlayer():IsAdmin() then
		Panel:Button("Reset EnvX", "env_server_reload")
		
		Panel:Help("Enable Noclip For Everyone?")
		local box = Panel:AddControl("CheckBox", {Label = "Enable Noclip?", Command = ""} )
		box:SetValue(tobool(GetConVarNumber("env_noclip")))
		box.Button.Toggle = function()
			if box.Button:GetChecked() == nil or not box.Button:GetChecked() then 
				box.Button:SetValue( true ) 
			else 
				box.Button:SetValue( false ) 
			end 
			RunConsoleCommand("environments_admin", "noclip", Bool2Num(box.Button:GetChecked()))
		end
		
		local planetmod =  vgui.Create("DCollapsibleCategory", DermaPanel)
		planetmod:SetSize( 100, 50 ) -- Keep the second number at 50
		planetmod:SetExpanded( 0 ) -- Expanded when popped up
		planetmod:SetLabel( "Planet Modification" )
		
		local p = vgui.Create( "DPanelList" )
		p:SetAutoSize( true )
		p:SetSpacing( 5 )
		p:EnableHorizontal( false )
		p:EnableVerticalScrollbar( true )
		 
		planetmod:SetContents( p )
		
		local List = vgui.Create("DListView")
		List:SetSize(100, 100)
		List:SetMultiSelect(false)
		List:AddColumn("Setting")
		List:AddColumn("Value")
		Menu.List = List
		
		for k,v in pairs(PlanetData) do
			List:AddLine(k,v)
		end
		
		List.OnRowSelected = function(self, line)
			line = self:GetLine(line)
			local setting = line:GetValue(1)
			local value = line:GetValue(2)
			Menu.Entry.line = line
			Menu.Entry:SetValue(value)
		end	

		local entry = vgui.Create( "DTextEntry" )
		entry:SetTall( 20 )
		entry:SetWide( 160 )
		entry:SetEnterAllowed( false )
		entry:SetMultiline(false)
		entry.OnTextChanged = function(self) -- Passes a single argument, the text entry object.
			if self.line then
				self.line:SetValue(2, self:GetValue())
			end
		end
		Menu.Entry = entry

		local send = vgui.Create( "DButton" )
		send:SetSize( 100, 30 )
		send:SetText( "Set Value" )
		send.DoClick = function( self )
			if Menu.Entry.line then
				RunConsoleCommand("environments_admin", "planetconfig", Menu.Entry.line:GetValue(1), Menu.Entry.line:GetValue(2))
			else
				LocalPlayer():ChatPrint("Select a value to set first!")
			end
		end
		 
		local get = vgui.Create( "DButton" )
		get:SetSize( 100, 30 )
		get:SetText( "Get Planet Info" )
		get.DoClick = function( self )
			RunConsoleCommand("request_planet_data")
		end
		
		p:AddItem(List)
		p:AddItem(entry)
		p:AddItem(send)
		p:AddItem(get)
		Panel:AddPanel(planetmod)
		
		Panel:Help("WARNING: Resets Saved Data!"):SetTextColor(Color(255,0,0,255))
		Panel:Button("Reload Environments From Map", "env_server_full_reload")
	else
		Panel:Help("You are not an admin!")
	end
end

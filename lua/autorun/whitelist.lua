if SERVER then
	local Whitelist = {

	}

	local function WhitelistSave()
		local Data = util.TableToJSON(Whitelist)
		file.Write("whitelist.txt", Data)
	end

	local function WhitelistLoad()
		local Data = file.Read( "whitelist.txt", "DATA" )
		Whitelist = util.JSONToTable(Data or "") or {}
	end

	concommand.Add("whitelist", function( ply, cmd, args )
	    if args[1] == nil or args[2] == nil then
	    	print("Invalid Arguments")
	    	return
	    end
	    local Player
	    local Players = player.GetAll()
	    for i=1,#Players do
	    	if string.find(Players[i]:Nick():lower(),args[2]:lower()) then
	    		Player = Players[i]
	    		break
	    	end
	    end
	    
	    if Player == nil or not Player:isValid() then 
	    	print("Invalid Player")
	    	return 
	    end

	    if args[1] == "add" then
	    	Whitelist[util.SteamIDTo64(Player:SteamID())] = true
	    elseif args[1]:sub(1,3) == "rem" then
	    	Whitelist[util.SteamIDTo64(Player:SteamID())] = nil
	    end

	    WhitelistSave()
	end)

	hook.Add( "CheckPassword", "whitelist", function( steamID64, ipAddress, svPassword, clPassword, Name )
		if svPassword == clPassword or Whitelist[ steamID64 ] then
			return true
		else
			return false, #GameUI_ServerRejectBadPassword
		end
	end )
	print("Whitelist Running.")
	WhitelistLoad()
end
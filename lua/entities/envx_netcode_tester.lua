AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_env_base"

ENT.PrintName	= "Netcode Tester"
ENT.Author		= "Ludsoe"
ENT.Purpose		= "Tests the Netcode"
ENT.Instructions	= ""

ENT.Spawnable = true
ENT.AdminOnly = true
ENT.Category = "EnvX"


--Shared Space
EnvXAdvNet = EnvXAdvNet or {Ents={}}

if(SERVER)then
    local Letters = {"A","B","C","D","E","F","G","H","I","J","K","L","O","M","N","P","Q","R","S","T","U","V","W","X","Y","Z"}
    function EnvXAdvNet.GenKey() local Key = "" for I=1,3 do Key=Key..Letters[math.random(1,#Letters)] end return Key end
    
    function EnvXAdvNet.RandKey()
        local Key = EnvXAdvNet.GenKey()..tostring(math.random(1,999))
        if EnvXAdvNet.Ents[Key] then return EnvXAdvNet.RandKey() end
        return Key
    end

    function ENT:Initialize()
        self:SetModel("models/props_lab/filecabinet02.mdl")
		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
        self:SetUseType( SIMPLE_USE )

        print("Netcode Tester Spawned!")

        self.AdvNetKey = EnvXAdvNet.RandKey()

        EnvX:NetworkData("Envx_Advanced_Sync",{
            Ent=self.AdvNetKey,
            Model=self:GetModel(),
            Pos=self:GetPos()
        })
    end

    --Disable gmods built in netcode
    function ENT:UpdateTransmitState() return TRANSMIT_NEVER end


    function ENT:AdvNetworkSend()

    end

	function ENT:OnRemove()
		EnvXAdvNet.Ents[self.AdvNetKey] = nil
    end
    
    function ENT:Think()

		
		self:NextThink(CurTime()+1)
    end
    
else

    EnvX:HookNet("Envx_Advanced_Sync",function(Data)
        local ent = Data.Ent
        


        PrintTable(Data)

    end)

end
AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
--include('entities/base_wire_entity/init.lua')
include( 'shared.lua' )

function ENT:Initialize()
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_NONE )
	self:SetSolid( SOLID_VPHYSICS )
	self:DrawShadow(false)
	
	self.LDEHealth = 500
	
	--self:SetNotSolid( true )
	
	--self.InfHealth = true --Give us infinite health damage system wise.
end

function ENT:OnLDEDamage(dmg,attacker,inflictor)
	--Put damage calculation code here.
end	
		
function ENT:CanTool()
	return false
end
 
function ENT:GravGunPunt()
	return false
end

function ENT:GravGunPickupAllowed()
	return false
end


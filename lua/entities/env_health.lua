AddCSLuaFile()

ENT.Type 		= "anim"
ENT.Base 		= "base_env_entity"
ENT.PrintName 	= "Medicinal Dispenser"
ENT.Author		= "Ludsoe"

ENT.OverlayText = {HasOOO = true ,resnames = {"energy","oxygen","water"}, genresnames={"carbon dioxide"}}

ENT.Spawnable		= false
ENT.AdminSpawnable	= false

if(SERVER)then
    function ENT:Initialize()
        self.BaseClass.Initialize(self)
        self.Active = 0
        self.overdrive = 0
        self.damaged = 0
        self.lastused = 0
        self.Mute = 0
        self.Locked = false
        if not (WireAddon == nil) then
            self.WireDebugName = self.PrintName
            self.Inputs = Wire_CreateInputs(self.Entity, { "Lock" })
        end
    end
    
    function ENT:TriggerInput(iname, value)
        if (iname == "Lock") then
            if (value > 0) then
                self.Locked = true
            else
                self.Locked = false
            end	
        end
    end

    function ENT:AcceptInput(name,activator,caller)
        --No gui thing :u
    end
        
    --Below Here Deals With Healing--
    function ENT:Use( activator, caller )
        local water = self:GetResourceAmount("water")
        local oxygen = self:GetResourceAmount("oxygen")
        local energy = self:GetResourceAmount("energy")
        if(self.Locked)then return end

        if ( activator:IsPlayer() and water >10 and oxygen >10 and energy >30 ) then
            local health = activator:Health()
            local armor = activator:Armor() 

            if health < 250 then
                activator:SetHealth( health + 1 )
                self:ConsumeResource("oxygen",5)
                self:ConsumeResource("water",5)
                self:ConsumeResource("energy",15)
                self:SupplyResource("carbon dioxide",10)
                self:SetOOO(1)
            end
            if armor < 250 then
                activator:SetArmor( armor + 1 )
                self:ConsumeResource("oxygen",5)
                self:ConsumeResource("water",5)
                self:ConsumeResource("energy",15)
                self:SupplyResource("carbon dioxide",10)
                self:SetOOO(1)
            end
        else 
            self:SetOOO(0)
        end
    end

else

end
ENT.Type				= "anim"  
ENT.Base				= "base_anim"     
ENT.PrintName		= "shield"
ENT.Author			= "Mechanos"
ENT.Contact			= "http://steamcommunity.com/id/Mechanos/"
ENT.Purpose			= "Temporary Spawn Method"
ENT.Instructions		= "herpderpWUT"
ENT.Category		= "Temporary Spawn Method"
 
ENT.Spawnable		= true
ENT.AdminOnly	= true
--ENT.WhoMadeMe					= nil
ENT.WInfo								= ""
ENT.RenderGroup 					= RENDERGROUP_BOTH
ENT.AutomaticFrameAdvance	= true

function ENT:SetupDataTables()
	self:NetworkVar( "Vector", 0 , "DoScale" )
	self:NetworkVar( "Entity", 1 , "Dummy" )
	self:NetworkVar( "Vector", 2 , "DummyScale" )
	--if SERVER then	self:NetworkVarNotify( "DoScale", self.Entity.ScaleMe ) end
	--if CLIENT then
		--self:NetworkVarNotify( "DoScale", self.Entity.ClientScaleMe )
	--end
end



if CLIENT then
	function ENT:Draw()
		self.Entity:DrawModel()
		Wire_Render(self.Entity)
	end
	function ENT:Initialize()
		self.WInfo = ""
		--self.Entity:SetModelScale( 10,0 )
	end
	
	function ENT:ClientScaleMe()
		local vec = self.Entity:GetDoScale()
		if vec == Vector(0,0,0) then return end
		self:SetDoScale(Vector(0,0,0))
		print("ClientScaleMe Running")
		--if self.Entity and ent:IsValid() then
			--ent:SetModelScale(scale, 0)

			--[[local mat = Matrix()
			mat:Scale( vec )
			self.Entity:EnableMatrix( "RenderMultiply", mat )]]
			
			local phys = self.Entity:GetPhysicsObject()
			if phys and phys:IsValid() then
				--[[local mesh = phys:GetMesh()
				
				for k,v in pairs(mesh) do
					v.pos.x = v.pos.x*vec.x
					v.pos.y = v.pos.y*vec.y
					v.pos.z = v.pos.z*vec.z
				end
				
				self.Entity:PhysicsFromMesh(mesh)]]
				self.Entity:EnableCustomCollisions()
			end
			--print(ent:OBBMins(), ent:OBBMaxs())
			--ent:SetCollisionBounds(ent:OBBMins()*scale, ent:OBBMaxs()*scale)
			self.Entity.DrawEntityOutline = function() end--fixes it breaking the clientside scale
			
			local dummy = self.Entity:GetDummy()
			if not IsValid(dummy) then return end
			
			local shieldscale = self:GetDummyScale()
			--[[local mat = Matrix()
			mat:Scale( shieldscale )
			dummy:EnableMatrix( "RenderMultiply", mat )]]
			
			local phys = dummy:GetPhysicsObject()
			if phys and phys:IsValid() then
				--[[local mesh = phys:GetMesh()
				
				for k,v in pairs(mesh) do
					v.pos.x = v.pos.x*shieldscale.x
					v.pos.y = v.pos.y*shieldscale.y
					v.pos.z = v.pos.z*shieldscale.z
				end
				
				dummy:PhysicsFromMesh(mesh)]]
				dummy:EnableCustomCollisions()
			end
			--print(ent:OBBMins(), ent:OBBMaxs())
			--ent:SetCollisionBounds(ent:OBBMins()*scale, ent:OBBMaxs()*scale)
			dummy.DrawEntityOutline = function() end--fixes it breaking the clientside scale
		--else
		--else
			--shrinked[entID] = scale--this isnt working
		--end
	end
	
	
	local ThinkTimer=CurTime()
	function ENT:Think()
		if ThinkTimer <= CurTime() then
			local vec = self:GetDoScale()
			if vec ~= Vector(0,0,0) then
				self.Entity:ClientScaleMe()
			end
			ThinkTimer = CurTime()+2
		end
	end
	
end

if SERVER then

AddCSLuaFile()
util.PrecacheSound("ambient/energy/ion_cannon_shot1.wav")

--Initialize----------------------------------------------------------------------------

function ENT:ScaleMe( vec )
	if not vec then return false
	elseif vec == Vector(0,0,0) then return false
	end
	
	--local mat = Matrix()
	--mat:Scale( vec )
	--self.Entity:EnableMatrix( "RenderMultiply", mat )
	
	local angles = self.Entity:GetAngles()
	
	local phys = self.Entity:GetPhysicsObject()
	if not phys:IsValid() then return false end
	local mesh = phys:GetMesh()
	
	for k,v in pairs(mesh) do
		v.pos.x = v.pos.x*vec.x
		v.pos.y = v.pos.y*vec.y
		v.pos.z = v.pos.z*vec.z
	end
	
	self.Entity:PhysicsFromMesh(mesh)
	self.Entity:EnableCustomCollisions()
	self.Entity:SetAngles(angles)
	return true
end


function ENT:Initialize()
	----------Declare Shared Variables------
	self.ThinkTimer = CurTime()
	self.SetOnce = 0
	self.Delay = CurTime()+2
	self.DelayRun = false
	self.Color = self.Color or Color(255,255,255,255)
	self.Alpha = self.Alpha or 20
	self.Material = self.Material or "prop_freeze"
	self.Damaged = false
	self.Cleanup = false
	self.CleanupTimer = CurTime()
	self.HaxCheck = CurTime()
	self.EffectTimer = CurTime()
	self.RealScale = self.RealScale or Vector(3,3,7)
	self.Scale = self.Scale or (self.RealScale.x+self.RealScale.y+self.RealScale.z)/3
	self:SetDummyScale(Vector(1.8,1.8,1.8)+(self.RealScale*Vector(1,1,1)))
	----------------------------------------------
	----------Initial Setup----------------------
	local model = self.Entity:GetModel()
	if model == "" or model == nil or model == "models/error.mdl" then model = "models/hunter/misc/shell2x2.mdl" end
	print(model)
	self.Entity:SetModel(model)
	--self.Entity:SetModelScale( self.Scale,0 )
		
	--self.Entity:ScaleMe(self.RealScale)		--Scale Server Side
	self:CreateDummy()							--Create better looking Dummy Shield
	self:SetDoScale(self.RealScale)			--Scale both Client Side
	
	self.Entity:PhysicsInit(SOLID_VPHYSICS)
	self.Entity:SetMoveType(MOVETYPE_VPHYSICS)
	self.Entity:SetSolid(SOLID_VPHYSICS)
	self.Entity:SetUseType(SIMPLE_USE)
	self.Entity:SetRenderMode(4)
	self.Entity:SetMaterial("models/wireframe")
	self.Entity:SetColor(Color(0,200,0,100))
	-----------------------------------------------
	----------Get Things Moving----------------
	local phys = self.Entity:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:Wake()
		phys:EnableGravity(false)
		phys:EnableDrag(false)
		phys:EnableCollisions(true)
		phys:EnableMotion(false)
	end
	-----------------------------------------------
	----------Wiremod Inputs and Outputs-----
	if WireAddon then
		--self.Inputs = WireLib.CreateInputs(self, {"Fire", "Reload"})
		--self.Outputs = WireLib.CreateOutputs(self, {"Shots Left", "Heat %"})
	end
	------------------------------------------------
	self.Entity:ScaleMe(self.RealScale)
end
------------------------------------------------------------------------------------------

--SENT Spawn Menu-----------------------------------
function ENT:SpawnFunction(ply,tr)
	if not tr.Hit then return end
	local SpawnPos = tr.HitPos + tr.HitNormal * 400
	local ent = ents.Create("shield")
	ent:SetPos(SpawnPos)
	ent:Spawn()
	ent:Activate()
	local phys = ent:GetPhysicsObject()
	if phys:IsValid() then ent:GetPhysicsObject():EnableMotion(false) end
	--ent:SetColor(Color(255,255,255,255))
	ent.WhoMadeMe = ply
	return ent
end
-----------------------------------------------------------

--Wiremod Functionality------------------------------------------------------------------------------------
function ENT:TriggerInput(iname, value)		
	--if (iname == "Fire" and value > 0) then self.Attack = true else self.Attack = false end
	--if (iname == "Reload" and value > 0) then self.Reload = true else self.Reload = false end
	--if (iname == "InputValue") then self.InputValue = value end
end

function ENT:UpdateWireOutput()
	--Wire_TriggerOutput(self.Entity, "Shots Left", self.ShotsLeft)
end
----------------------------------------------------------------------------------------------------------------

--Required funcs we can leave empty 'till needed--
function ENT:PhysicsUpdate()

end

function ENT:PhysicsCollide(data,physobj)
--[[
HitNormal	=	-0.000000 -0.000000 -1.000000
DeltaTime	=	1
HitPos	=	-11955.686523 -14676.476563 9737.250000
TheirOldVelocity	=	0.000000 0.000000 -0.000000
OurOldVelocity	=	60.659130 -22.685530 -699.022766
HitObject	=	PhysObject
HitEntity	=	Entity [0][worldspawn]
Speed	=	698.82739257813
]]--
	--PrintTable(data)
	if not data.HitEntity:IsWorld() then
		phys = data.HitEntity:GetPhysicsObject()
		if phys:IsValid() then
			if phys:IsMotionEnabled() then
				local isplayer = false
				if data.HitEntity:IsPlayer() or data.HitEntity:IsNPC() then
					--data.HitEntity:SetMoveType(5)
					isplayer = true
				end
				local vel = math.min(phys:GetVelocity():Length(),100)
				local pos = phys:GetPos()
				local pos2 = physobj:GetPos()
				local dir = pos-pos2
				dir:Normalize()
				if isplayer then
					local newdir = Vector(math.random(-1,1),math.random(-1,1),math.random(0,1))
					newdir:Normalize()
					data.HitEntity:SetVelocity(newdir*750)
				else
					phys:SetPos(pos+dir*2)
					phys:SetVelocityInstantaneous(dir*vel*5)
				end
				
				if IsValid(self.DummyShield) then
					local myColor = self.Entity:GetColor()
					local amount = 40
					if myColor.a <= 255-amount then
						myColor.a = myColor.a + amount
						self.DummyShield:SetColor(myColor)
					end
				end
				
				if self.EffectTimer < CurTime() then
					local vPoint = data.HitPos
					local effectdata = EffectData()
					effectdata:SetStart( vPoint ) -- not sure if ( we need a start and origin ( endpoint ) for this effect, but whatever
					effectdata:SetOrigin( vPoint )
					effectdata:SetScale( self.Scale )
					util.Effect( "cball_bounce", effectdata )
					self.EffectTimer = CurTime()+0.1
				end
				
				self.Damaged = true
				return false
			end
		end
	end
end

function ENT:OnTakeDamage(dmginfo)
	if not self.Damaged then
		if IsValid(self.DummyShield) then
			local myColor = self.DummyShield:GetColor()
			local amount = 40
			if myColor.a <= 255-amount then
				myColor.a = myColor.a + amount
				self.DummyShield:SetColor(myColor)
			end
		end
		
		if self.EffectTimer < CurTime() then
			local vPoint = dmginfo:GetDamagePosition()
			local effectdata = EffectData()
			effectdata:SetStart( vPoint ) -- not sure if ( we need a start and origin ( endpoint ) for this effect, but whatever
			effectdata:SetOrigin( vPoint )
			effectdata:SetScale( self.Scale )
			util.Effect( "inflator_magic", effectdata )
			self.EffectTimer = CurTime()+0.1
		end
		
		self.Damaged = true
	end
end

function ENT:Use(activator,caller)

end
-----------------------------------------------------------
--Custom Functions--------------------------------------------------------------------------------------
	-----------------------------------------------
	----------Dummy Shield--------------------
function ENT:CreateDummy()
	self.DummyShield = self.DummyShield or ents.Create("prop_physics")
	self.DummyShield:SetModel("models/holograms/hq_sphere.mdl")
	--self.DummyShield:SetModelScale( 2+self.Scale*0.7,0 )
	
	local vec = self:GetDummyScale()
			
	--local mat = Matrix()
	--mat:Scale( shieldscale )
	--self.DummyShield:EnableMatrix( "RenderMultiply", mat )
	
	self.DummyShield:SetPos(self.Entity:GetPos())
	self.DummyShield:PhysicsInit(0)
	self.DummyShield:SetMoveType(0)
	--self.DummyShield:Activate()
	self.DummyShield:SetRenderMode(4)
	self.DummyShield:SetMaterial(self.Material)
	self.DummyShield:SetColor(Color(255,255,255,255))
	local phys = self.DummyShield:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:EnableGravity(false)
		phys:EnableDrag(false)
		phys:EnableCollisions(false)
		phys:EnableMotion(false)
	end
	local phys = self.DummyShield:GetPhysicsObject()
	if phys and phys:IsValid() then
		local mesh = phys:GetMesh()
				
		for k,v in pairs(mesh) do
			v.pos.x = v.pos.x*vec.x
			v.pos.y = v.pos.y*vec.y
			v.pos.z = v.pos.z*vec.z
		end
				
		self.DummyShield:PhysicsFromMesh(mesh)
		--self.DummyShield:EnableCustomCollisions()
	end
	self.DummyShield.DrawEntityOutline = function() end--fixes it breaking the clientside scale
	self.DummyShield:SetParent(self.Entity)
	if IsValid(self.DummyShield) then print("Dummy Created") end
	self:SetDummy(self.DummyShield)
end
--[[
local function ShieldPickup1( ply, ent )
	if ent:GetClass() == "shield" then
		return false
	end
end
hook.Add( "PhysgunPickup", "No Shield Pickup1", ShieldPickup1 )
hook.Add( "AllowPlayerPickup", "No Shield Pickup2", ShieldPickup1 )
hook.Add( "GravGunPickupAllowed", "No Shield Pickup3", ShieldPickup1 )
hook.Add( "GravGunPunt", "No Shield Pickup4", ShieldPickup1 )

local function ShieldUnfreeze1( weapon, ply )
	local ent = ply:GetEyeTrace().Entity
	if IsValid(ent) then
		if ent:GetClass() == "shield" then
			return false
		end
	end
end
hook.Add("OnPhysgunReload", "No Shield Unfreeze1", ShieldUnfreeze1)

local function ShieldToolgun1( ply, trace, tool )
	if IsValid( trace.Entity ) then
		if trace.Entity:GetClass() == "shield" then
			return false
		end
   end
end
hook.Add( "CanTool", "No Shield Toolgun1", ShieldToolgun1 )
]]--
------------------------------------------------------------------------------------------------------------

--Think Function, control everything from here-----------------------------------------------------
function ENT:Think()
	if not IsValid(self.DummyShield) then
		print("no dummy")
		self:CreateDummy()
		self:SetDoScale(self.RealScale)
		--self.DummyShield = self.Entity
		self.Entity:NextThink(CurTime()+0.05)
		return true
	else
		local myColor = self.DummyShield:GetColor()
		if myColor.a > self.Alpha then
			myColor.a = myColor.a - 5
			if myColor.a < self.Alpha then myColor.a = self.Alpha end
			self.DummyShield:SetColor(myColor)
		end
	end
	
	if self.Damaged then
		self.Damaged = false
		self.Entity:EmitSound("ambient/energy/ion_cannon_shot1.wav", 400, math.random(150,200))
		self.Cleanup = true
		self.CleanupTimer = CurTime()+1
	end
	
	if self.Cleanup and self.CleanupTimer < CurTime() then
		self.Entity:RemoveAllDecals()
		self.DummyShield:RemoveAllDecals()
		self.Cleanup=false
	end
	
	if self.HaxCheck < CurTime() then
		local render = self.DummyShield:GetRenderMode()
		local myMat = self.DummyShield:GetMaterial()
		if myMat ~= self.Material then
			self.DummyShield:SetMaterial(self.Material)
		elseif render ~= 4 then
			self.DummyShield:SetRenderMode(4)
		end
		self.HaxCheck = CurTime()+1
	end

	--if WireAddon then self:UpdateWireOutput() end
	self.Entity:NextThink(CurTime()+0.05)
	return true
end
------------------------------------------------------------------------------------------------------------------------------------------------------

--Advanced Duplicator 1 and 2 Compatibility----------------------------------------------------------------------------------------
function ENT:PreEntityCopy()
	if WireAddon then duplicator.StoreEntityModifier(self,"WireDupeInfo",WireLib.BuildDupeInfo(self.Entity)) end
end

function ENT:PostEntityPaste(ply, ent, NewEnts)
	self.WhoMadeMe=ply
	local emods = ent.EntityMods
	if not emods then return end
	if WireAddon then WireLib.ApplyDupeInfo(ply, ent, emods.WireDupeInfo, function(id) return NewEnts[id] end) end
end
---------------------------------------------------------------------------------------------------------------------------------------------




end
AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_env_storage"

ENT.PrintName	= "Envx Modular storage"
ENT.Author		= "Ludsoe"
ENT.Purpose		= "Allows players more control what their storages actually stores"
ENT.Instructions	= ""

ENT.Spawnable		= false
ENT.AdminSpawnable	= false

ENT.StorageTypes = {}

ENT.NoEnvPanel = true
ENT.EnvxNetworked = true

if(SERVER)then
	
	function ENT:ModulateStorage()
		local Settings = self.envx_spawn_settings or {}

		local Smult = self:GetSizeMultiplier()
		local Loss = -2
		local Types = 0
		--Theres better ways to do this but, im tired and lazy right now ~ludsoe
		for sid,sval in pairs( Settings ) do if sval == true then Loss=Loss+2 Types=Types+1 end end

		self.StorageTypes={}
		self.WireDat = {}

		for sid,sval in pairs( Settings ) do
			if sval == true then
				local ResDat = EnvX.GetResourceData(sid)--Obtain the resource data.
				local MaxStorage = math.Round((ResDat.StorePerUnit*Smult)*(100-math.Clamp(Loss,0,100))/Types)
				self:AddResource(sid,MaxStorage/100)

				table.insert(self.StorageTypes,sid)

				table.insert(self.WireDat,sid)
				table.insert(self.WireDat,"Max "..sid)
			end
		end

		self.OverlayText = {HasOOO = false, stornames = self.StorageTypes}

		self.Outputs = Wire_CreateOutputs(self, self.WireDat)

		self:FlagForNetSync()
	end

	function ENT:NetworkData()
		local data = {}
	
		data.resources = self.resources
		data.maxresources = self.maxresources
		data.OverlayText = self.OverlayText

		return data
	end
	
	function ENT:ReloadEnvxSettings()
		local mynode = self.node
		--if the maxstorage changes while connected to a node things break drastically
		if IsValid(mynode) then self.node:Unlink(self) end

		self:ResetStorage()
		self:ModulateStorage()

		if mynode ~= nil and IsValid(mynode) then
			mynode:Link(self)
		end
	end

	--Called when resource containment is failed
	function ENT:ContainmentFailure()

	end

	--Calculate all the running costs for storing resources
	function ENT:MaintainContainment()
		for k,v in pairs(self.maxresources) do

		end
	end

	function ENT:Think()

		--self:MaintainContainment()

		if WireAddon then
			--print("AM Thinking WireCheck!")
			for k,v in pairs(self.maxresources) do
				Wire_TriggerOutput(self, k, self:GetResourceAmount(k))
				Wire_TriggerOutput(self, "Max "..k, self:GetNetworkCapacity(k))
			end
		end
		
		self:NextThink(CurTime() + 1)
		return true
	end

	function ENT:Initialize()
		self.BaseClass.Initialize(self)
		self:ModulateStorage()
	end

	function ENT:EnvxOnDupe() return {Data=1} end
	function ENT:EnvxOnPaste(Player,Ent,CreatedEntities) self:ReloadEnvxSettings() end

else

	function ENT:NetworkDataRecieve(Data)
		self.resources = Data.resources
		self.maxresources = Data.maxresources

		self.OverlayText = Data.OverlayText
	end

end		

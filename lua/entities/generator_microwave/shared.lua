ENT.Type 		= "anim"
ENT.Base 		= "base_env_entity"
ENT.PrintName 	= "Microwave Emitter"

ENT.OverlayText = {HasOOO = true, num = 2, resnames = {"energy"} }


local MC = EnvX.MenuCore

if(SERVER)then
	
	local T = {} --Create a empty Table
	
	T.Power = function(Device,ply,Data)
		Device:SetActive( nil, ply )
	end
	
	T.TransAngle = function(Device,ply,Data)
		Device:SetTransmitAngle( tonumber(Data) )
	end

	T.TransPower = function(Device,ply,Data)
		Device:SetTransmitPower( tonumber(Data) )
	end
	
	ENT.Panel=T --Set our panel functions to the table.
	
else 
	function ENT:ExtraData(Info)
		local Pow = math.Round(self:GetNetworkedInt("EnvTransPow") or 1)
		table.insert(Info,{Type="Percentage",Text="BroadCast Power: ".. Pow,Value=Pow/1000000})
		
		local Ang = math.Round(self:GetNetworkedInt("EnvTransAng") or 1)
		table.insert(Info,{Type="Percentage",Text="BroadCast Angle: ".. Ang,Value=Ang/360})

		return true 
	end
	
	function ENT:PanelFunc(entID)	
		self.DevicePanel = {
			function() return MC.CreateButton(Parent,{x=90,y=30},{x=0,y=0},"Toggle Power",function() RunConsoleCommand( "envsendpcommand",self:EntIndex(),"Power") end) end,
			function()
				local S = MC.CreateSlider(Parent,{x=150,y=30},{x=0,y=0},{Min=1,Max=1000000,Dec=0},"BroadCast Power",function(val) RunConsoleCommand( "envsendpcommand",self:EntIndex(),"TransPower",val) end)
				S:SetValue(self:GetNetworkedInt("EnvTransPow") or 1)
				return S
			end,
			function()
				local S = MC.CreateSlider(Parent,{x=150,y=30},{x=0,y=0},{Min=1,Max=360,Dec=0},"BroadCast Angle",function(val) RunConsoleCommand( "envsendpcommand",self:EntIndex(),"TransAngle",val) end)
				S:SetValue(self:GetNetworkedInt("EnvTransAng") or 1)
				return S
			end
		}
	end
end
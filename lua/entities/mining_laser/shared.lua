ENT.Type = "anim"
ENT.Base = "base_env_entity"
ENT.PrintName = "Mining Laser"

ENT.OverlayText = {HasOOO = true, resnames ={"energy","water"} }

function ENT:SetupDataTables()
	self:NetworkVar("Float",0,"LaserEfficiency")
	self:NetworkVar("Int",0,"LaserFlowrate")
	self:NetworkVar("Float",1,"LaserHeat")
	self:NetworkVar("Bool",0,"LaserLaserMine")
end
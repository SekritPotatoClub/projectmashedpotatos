include('shared.lua')

local OOO = {}
OOO[0] = "Off"
OOO[1] = "Firing"

function ENT:Initialize()
	self.BaseClass.Initialize(self)
	self.boxmax = self:OBBMaxs() - self:OBBMins()
	self.LastResource ="None"
end

function ENT:ExtraData(Info)

	local mode,status = self:GetOOO(),"Idle"
	if mode >= 0 or mode <2 then status = OOO[mode] end

	table.insert(Info,{Type="Label",Value="Status: "..status})
	
	if mode == 1 then
			
		local EMR = LDE.Anons.Resources[self:GetNetworkedString("MiningLaserResource")]
		local resname,resunit = "none",""
		if EMR and EMR.name then
			resname,resunit = EMR.name,EMR.unit
			self.LastResource = EMR.name
		end
		
		table.insert(Info,{Type="Label",Value="Efficiency: "..math.Round(self:GetLaserEfficiency(),2).." %"})
		table.insert(Info,{Type="Label",Value="Resource: [ "..resname.." ]"})
		table.insert(Info,{Type="Label",Value="Flowrate: "..self:GetLaserFlowrate().." "..resunit.."/sec"})
	else
		table.insert(Info,{Type="Label",Value="Last Resource: [ "..self.LastResource.." ]"})
	end
	
	table.insert(Info,{Type="Label",Value="Overheat: "..math.Round(self:GetLaserHeat(),1).." %"})
	
	return true 
end

function ENT:CustomDraw()
	--# I'm Firin Mah Lazor!!!
	if self:GetOOO() > 0 then
		local Pos,Fore = self:GetPos(), self:GetForward()
		local LaserStart = Pos + Fore * ( self.boxmax.x *0.62)
		local LaserOrigin = Pos + Fore * 768
		self:SetRenderBoundsWS(LaserStart,LaserOrigin)
		local tracedata = {}
		tracedata.start = LaserStart
		tracedata.endpos = LaserOrigin
		tracedata.filter = self
		local trace = util.TraceLine(tracedata)
		-- Draw laserbeam.
		local LaserHitPos,LaserTarget = trace.HitPos,trace.Entity
		local Ed = EffectData()
		Ed:SetOrigin(LaserHitPos)
		Ed:SetStart(LaserStart)
		util.Effect("eff_laserbeam",Ed)
		
		if self:GetLaserLaserMine() == true then -- Do rocky effect if we are getting resources
			local edata = EffectData()
			edata:SetOrigin(LaserHitPos )
			edata:SetNormal( -Fore )
			edata:SetMagnitude( trace.Fraction )
			util.Effect( "eff_laserhit" , edata )	
		end
	end
end

function ENT:Think()
	self:NextThink(CurTime() + 0.1)
end


AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

include('shared.lua')

ENT.IsLS = true
ENT.EnvxNetworked = false

local EnvX = EnvX --Localise the global table for speed.
local Utl = EnvX.Utl --Makes it easier to read the code.
local NDat = Utl.NetMan --Ease link to the netdata table.

function ENT:Initialize()
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetNetworkedInt( "OOO", 0 )
	
	--Failsafe against bad models check!
	if not util.IsValidProp( self:GetModel() ) then print("Model Invalid!") self:Remove() return end
	
	self.Active = 0
	self.Multiplier = 1
	self.DeviceSounds = {}
	self.Mute = 0
	
	self.resources = {}
	self.maxresources = {}
	self.client_updated = {}
			
	self.AnimTimer = CurTime() + 1
	self.Animate = 0
	
	self:SetupEnvxResBase()
end

--This is ment to be overwritten by entitys that support custom entitys
function ENT:ReloadEnvxSettings() end

function ENT:FlagForNetSync() self.client_updated = {} end

--This function is called when we want to know what to send to the clients for networking
function ENT:NetworkData()
	local data = {}

	data.resources = self.resources
	data.maxresources = self.maxresources

	return data
end


function ENT:SetupEnvxResBase()
	local data = self.envx_res_base_stats
	if data == nil or not type(data)=="table" then print(tostring(self).." What is this? Not valid data table! "..tostring(data)) return end
	local Storage = data.storage or {}
	
	local Smult = self:GetSizeMultiplier()
	
	--print("Setting up res base!")
	--print("Size Mult "..tostring(Smult))
	
	self:FlagForNetSync() --Clear our client updated table.
	
	if table.Count(Storage)>0 then
		--print("Doing storage!")
		for k,v in pairs(Storage) do
			---res={storage efficiency,percentage start}
			local ResDat = EnvX.GetResourceData(k)--Obtain the resource data.
			
			local MaxStorage = math.Round((ResDat.StorePerUnit*Smult)*(v[1]/100))
			
			self:AddResource(k,MaxStorage)
			self.resources[k]=MaxStorage*(v[2]/100)
		end
		
		--print("Printing info....")
		--PrintTable(self.maxresources)
		--print("Done!")
		
		if WireAddon then
			self:HandleWireStuff()
		end
	end
end

function ENT:SetActive( value, caller )
	--print("Setting Active State: "..tostring(value).." user: "..tostring(caller))
	--print("Active State is "..tostring(self.Active))
	if ((not(value == nil) and value ~= 0) or value == nil) and self.Active == 0 then
		--print("ON?")
		if self.TurnOn ~= nil then self:TurnOn( caller ) end
	elseif ((not(value == nil) and value == 0) or (value == nil)) and self.Active == 1 then
		--print("off?")
		if self.TurnOff ~= nil then self:TurnOff(caller ) end
	end
end

function ENT:GetActive()
	return self.Active>0
end

function ENT:OnTurnOn() end
function ENT:TurnOn(caller)
	--print("Turning on?")
	if not self:GetActive() then
		self.Active = 1
		self:SetOOO(1)
		self:OnTurnOn(caller)
	end
end

function ENT:OnTurnOff() end
function ENT:TurnOff(caller)
	--print("turning off")
	if self:GetActive() then
		self.Active = 0
		self:SetOOO(0)
		self:OnTurnOff(caller)
	end
end
		
function ENT:SetOOO(value)
	self:SetNetworkedInt( "OOO", value )
end

function ENT:GetMultiplier()
	return self.Multiplier or 1
end

function ENT:InitialSizeMultiplier()
	local base_volume = 40960 --Testing numbers to try and get near old levels
	local volume_mul = 1 --Change to be 0 by default later on
	
	local phys = self:GetPhysicsObject()
	if phys:IsValid() and phys.GetVolume then
		local vol = phys:GetVolume()
		vol = math.Round(vol)
		volume_mul = vol/base_volume
	end

	self:SetSizeMultiplier(volume_mul)
end

function ENT:SetSizeMultiplier(num)
	if num < 0.1 then num = 0.1 end
	self.SizeMultiplier = tonumber(num) or 1
end

function ENT:GetSizeMultiplier()	
	if not self.SizeMultiplier then self:InitialSizeMultiplier() end
	return self.SizeMultiplier or 1
end

function ENT:PlayDeviceSound(id,snd)
	if not self:GetDeviceMuted() then
		if not self.DeviceSounds[id] or not IsValid(self.DeviceSounds[id]) then
			local NID = table.Count(self.DeviceSounds)+1
			local sound = CreateSound( self, snd , self:EntIndex()+(NID/10))
			self.DeviceSounds[id] = {num=NID,snd=sound}
			sound:Play()
		else
			self.DeviceSounds[id].snd:Play()
		end
	end
end

function ENT:DeviceSoundPitch(id,pitch)
	if not self:GetDeviceMuted() then
		if self.DeviceSounds[id] then
			self.DeviceSounds[id].snd:ChangePitch( pitch ) 
		end
	end
end

function ENT:DeviceSoundVolume(id,volume)
	if not self:GetDeviceMuted() then
		if self.DeviceSounds[id] then
			self.DeviceSounds[id].snd:ChangeVolume( volume ) 
		end
	end
end

function ENT:StopDeviceSound(snd)
	local v = self.DeviceSounds[snd]
	if v~=nil then
		if v.snd~=nil then
			v.snd:Stop()
		else
			--MsgAll("Sound Isnt Valid! ID: "..tostring(snd).."  Snd: "..tostring(v).."\n")
		end
	end
end

function ENT:StopSounds()
	for k,v in pairs(self.DeviceSounds or {}) do
		self:StopDeviceSound(k)
	end
end

function ENT:SetMultiplier(num)
	num = num or 1
	if num < 1 then num = 1 end
	self.Multiplier = tonumber(num) or 1
	self:SetNetworkedInt( "EnvMultiplier", self.Multiplier )
end

function ENT:GetDeviceMuted()
	if self.Mute > 0 then 
		return true
	end
	return false
end

function ENT:SetDeviceMute(value)
	self.Mute = value
	self:SetNetworkedInt( "EnvDeviceMuted", value )
	
	if value > 0 then
		self:StopSounds()
	end
end

function ENT:AcceptInput(name,activator,caller)
	if name == "Use" and caller:IsPlayer() and caller:KeyDownLast(IN_USE) == false then
		EnvX:NetworkData("EnvxDevicePanel",{EntID=self:EntIndex(),Entity=self},caller)
	end
end

function ENT:OnRemove()
	if self.node and IsValid(self.node) then
		self.node:Unlink(self)
	end
	if WireLib then WireLib.Remove(self) end
	
	self:StopSounds()
end

function ENT:ConsumeResource( resource, amount)
	if self.node then
		return self.node:ConsumeResource(resource, amount)
	else
		return 0
	end
end

function ENT:SupplyResource(resource, amount)
	if self.node then
		return self.node:GenerateResource(resource, amount)
	end
end

function ENT:AddResource(name,amt)--adds to storage
	if not self.maxresources then self.maxresources = {} end
	if amt > 0 then
		self.maxresources[name] = (self.maxresources[name] or 0) + amt
	end
end

function ENT:ResetStorage()--This removes all the storage capacity of the entity use with care.
	for sid,sval in pairs( self.maxresources ) do self:RemoveResource(sid) end
	self.maxresources = {} --To ensure that the table is cleaned properly
	self:FlagForNetSync()
end

--Add some failsafes incase the storage isnt connected to a node or the node cant store the resources without this device
function ENT:RemoveResource(name)--Why would you remove resources!?!?!
	self.maxresources[name] = nil
	self:FlagForNetSync()
end

function ENT:Link(ent, delay)
	if self.node then
		if self.node == ent then return end
		self.node:Unlink(self)
	end
	
	if IsValid(ent) then
		self.node = ent
		self:FlagForNetSync()
		
		if delay then
			local func = function()
				local Nodes = {}
				Nodes[ent:EntIndex()]={self:EntIndex()}
				EnvX:NetworkData("EnvX_SetEntNode",{Nodes=Nodes})
			end
			timer.Simple(0.1, func)
		else
			local Nodes = {}
			Nodes[ent:EntIndex()]={self:EntIndex()}
			EnvX:NetworkData("EnvX_SetEntNode",{Nodes=Nodes})
		end
	end
end

function ENT:Unlink()
	if self.node then
		self.resources = {}
		for k,v in pairs(self.maxresources or {}) do
			--print("Resource: "..k, "Amount: "..v)
			local amt = self:GetResourceAmount(k)
			if amt > v then
				amt = v
			end
			if self.node.resources[k] then
				self.node.resources[k].value = self.node.resources[k].value - amt
			end
			self.resources[k] = amt
		end
		self.node.updated = true
		self.node:Unlink(self)
		self.node = nil
		self:FlagForNetSync()

		local Nodes = {}
		Nodes[0]={self:EntIndex()}
		EnvX:NetworkData("EnvX_SetEntNode",{Nodes=Nodes})
	end
end

function ENT:GetResourceAmount(resource)
	if self.node then
		if self.node.resources and self.node.resources[resource] then
			return self.node.resources[resource].value
		else
			return 0
		end
	else
		return 0
	end
end

function ENT:GetUnitCapacity(resource)
	return self.maxresources[resource] or 0
end

function ENT:GetNetworkCapacity(resource)
	if self.node then
		return self.node.maxresources[resource] or 0
	end
	return self:GetUnitCapacity(resource)
end

function ENT:GetStorageLeft(res)
	if self.node then
		if self.node.resources[res] then
			local max = self.node.maxresources[res]
			local cur = self.node.resources[res].value or 0
			if max then
				return max - cur
			end
		else
			local max = self.node.maxresources[res]
			if max then
				return max
			end
		end
	end
	return 0
end
	
function ENT:AnimateCorePlay(Animation)
	if not Animation then return end
	self.Animate = 1
	self.Animation = Animation
end

function ENT:AnimateThink()
	if self.Animate > 0 then
		if self.AnimTimer < CurTime() then
			local cycle,rate = 0,1
			self:SetSequence(self.Animation)
			self:ResetSequence(self.Animation)
			self:SetPlaybackRate(rate)
			self:SetCycle(cycle)
			self.AnimTimer=CurTime()+self:SequenceDuration(self.Animation)
			self.Animate=0
		end
	end
end
	
function ENT:OnRestore()
	if WireLib then WireLib.Restored(self) end
end

function ENT:PreEntityCopy()
	Environments.BuildDupeInfo(self)
	if self.EnvxOnDupe then
		local DI = self:EnvxOnDupe()
		if DI then
			duplicator.StoreEntityModifier( self, "EnvxDupeInfo", DI )
		end
	end
	if WireLib then
		local DupeInfo = WireLib.BuildDupeInfo(self)
		if DupeInfo then
			duplicator.StoreEntityModifier( self, "WireDupeInfo", DupeInfo )
		end
	end
end

function ENT:PostEntityPaste( Player, Ent, CreatedEntities )
	Environments.ApplyDupeInfo(Ent, CreatedEntities, Player)
	--print("Am paste")
	if Ent.EntityMods and Ent.EntityMods.EnvxDupeInfo then
		--print("Envx stuff")
		if self.EnvxOnPaste ~= nil then
			self:EnvxOnPaste(Player,Ent,CreatedEntities)
		end
		if self.resources then self.resources = {} end
		self:FlagForNetSync()
	else
		--print(tostring(Ent.EntityMods).." "..tostring(Ent.EntityMods.EnvxDupeInfo))
	end
	
	if WireLib and (Ent.EntityMods) and (Ent.EntityMods.WireDupeInfo) then
		WireLib.ApplyDupeInfo(Player, Ent, Ent.EntityMods.WireDupeInfo, function(id) return CreatedEntities[id] end)
	end
end

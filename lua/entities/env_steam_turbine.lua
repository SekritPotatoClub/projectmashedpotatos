AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_env_base"
ENT.PrintName 	= "Steam Turbine"

ENT.OverlayText = {HasOOO = true, genresnames = {"energy", "water"}, resnames={"steam"}}

if(SERVER)then

    function ENT:Generate()
        local needed = self:GetSizeMultiplier()*50
        local amt = self:ConsumeResource("steam", needed)
        self:SupplyResource("energy", amt)
        self:SupplyResource("water", amt*0.4)
    end
    
    function ENT:Think()
        if self.Active == 1 then
            self:Generate()
        end
        
        self:NextThink(CurTime() + 1)
        return true
    end
else

end
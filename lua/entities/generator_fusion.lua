AddCSLuaFile()


ENT.Type 		= "anim"
ENT.Base 		= "base_env_base"
ENT.PrintName 	= "Fusion Reactor"

ENT.Author		= "Ludsoe"

ENT.Spawnable		= false
ENT.AdminSpawnable	= false

ENT.OverlayText = {HasOOO = true, num = 2, resnames = {"water"}, genresnames={"energy"}}

local MC = EnvX.MenuCore

if(SERVER)then
    
    local Energy_Increment = 20
    local Coolant_Increment = 10 
    local HW_Increment = 1

    function ENT:Initialize()
        self.BaseClass.Initialize(self)
        self.Active = 0
        self.damaged = 0
        self.critical = 0
        self.hwcount = 0
        self.time = 0
        if WireLib then
            self.WireDebugName = self.PrintName
            self.Inputs = WireLib.CreateInputs(self, { "On" })
            self.Outputs = WireLib.CreateOutputs(self, { "On", "Output" })
        else
            self.Inputs = {{Name="On"}}
        end
    end

    function ENT:OnTurnOn()
        self:PlayDeviceSound( "Activate","ambient/machines/thumper_startup1.wav" )
        self:PlayDeviceSound( "Idle","k_lab.ambient_powergenerators" )
    end
    
    function ENT:OnTurnOff()
        self:StopDeviceSound( "Idle" )
        self:StopDeviceSound( "coast.siren_citizen" )
        WireLib.TriggerOutput(self, "On", 0)
        WireLib.TriggerOutput(self, "Output", 0)
    end

    function ENT:TriggerInput(iname, value)
        if (iname == "On") then
            self:SetActive(value)
        end
    end
    
    function ENT:Repair()
        self.BaseClass.Repair(self)
        self.damaged = 0
        self.critical = 0
        self:StopDeviceSound( "coast.siren_citizen" )
    end

    function ENT:Extract_Energy()
        local inc = Energy_Increment
        
        --[[
        if (self.critical == 1) then
            local ang = self:GetAngles()
            local pos = (self:GetPos() + (ang:Up() * self:BoundingRadius()))
            local test = math.random(1, 10)
            zapme = Environments.ZapMe
            if (test <= 2) then
                if zapme then
                    zapme((pos + (ang:Right() * 90)), 5)
                    zapme((pos - (ang:Right() * 90)), 5)
                end
                self:PlayDeviceSound( "Explode","ambient/levels/labs/electric_explosion3.wav" )
                inc = 0
            elseif (test <= 4) then
                if zapme then
                    zapme((pos + (ang:Right() * 90)), 3)
                    zapme((pos - (ang:Right() * 90)), 3)
                end
                self:PlayDeviceSound( "Explode","ambient/levels/labs/electric_explosion4.wav" )
                inc = math.ceil(inc / 4)
            elseif (test <= 6) then
                if zapme then
                    zapme((pos + (ang:Right() * 90)), 2)
                    zapme((pos - (ang:Right() * 90)), 2)
                end
                self:PlayDeviceSound( "Explode","ambient/levels/labs/electric_explosion1.wav" )
                inc = math.ceil(inc / 2)
            end
        end]]
        
        --Was 10
        local HeatAmount = 0.1 * self:GetSizeMultiplier()
        local required_water = math.ceil(Coolant_Increment * self:GetSizeMultiplier())
        if self:GetResourceAmount("water") < required_water then
            if (self.critical == 0) then
                if self.time > 3 then 
                    --self.critical = 1
                    self:PlayDeviceSound( "Warning","common/warning.wav" )
                    self.time = 0
                else
                    self.time = self.time + 1
                end
            else
                if self.time > 1 then 
                    self:StopDeviceSound( "Siren" )
                    self:PlayDeviceSound( "Siren","coast.siren_citizen" )
                    self.time = 0
                else
                    self.time = self.time + 1
                end
            end

            --only supply 5-25% of the normal amount
            if (inc > 0) then 
                inc = math.ceil(inc/math.random(12 - math.ceil(8 * ( self:GetResourceAmount("water")/math.ceil(Coolant_Increment * self:GetSizeMultiplier()))),20)) 
            end
        else
            local consumed = self:ConsumeResource("water", required_water)
            --self:SupplyResource("steam", math.ceil(consumed * 0.92))
            self:SupplyResource("water", math.ceil(consumed * 0.08))
            HeatAmount=HeatAmount/2 --Properly Cooled fusion reactors generate less excess heat.
        end
        
        LDE.HeatSim.ApplyHeat(self,HeatAmount)
        
        --the money shot!
        if (inc > 0) then 
            inc = math.ceil(inc * self:GetSizeMultiplier())
            self:SupplyResource("energy", inc)
        end
        if WireLib then WireLib.TriggerOutput(self, "Output", inc) end
    end

    function ENT:Leak() --leak cause this is like with storage, make be it could leak radation?
        if self:GetResourceAmount("energy") >= 500*self:GetSizeMultiplier() then
            if self.critical == 0 then
                self.critical = 1 
            end
        else
            if self.critical == 1 then
                self:StopDeviceSound( "Siren" )
                self.critical = 0
            end
        end
    end

    function ENT:Think()
        if self:GetActive() then
            self:Extract_Energy()
        end
        
        if (self.damaged == 1) then
            self:Leak()
        end

        self.BaseClass.Think(self)
        self:NextThink(CurTime() + 1)
        return true
    end

    --------------------------------------------------------------
    --Custom E menu that doesnt include the multiplier.
	local T = {} --Create a empty Table
	
	T.Power = function(Device,ply,Data)
		local toggle = 1
        if Device:GetActive() then toggle = 0 end
        print("Power Status: "..tostring(toggle))
		Device:SetActive( toggle , ply )
	end
	
	ENT.Panel=T --Set our panel functions to the table.
	
else
	function ENT:PanelFunc(entID)	
		self.DevicePanel = {
			function() return MC.CreateButton(Parent,{x=90,y=30},{x=0,y=0},"Toggle Power",function() RunConsoleCommand( "envsendpcommand",self:EntIndex(),"Power") end) end
		}
	end
end
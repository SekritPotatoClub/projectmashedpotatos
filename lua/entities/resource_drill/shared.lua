ENT.Type 		= "anim"
ENT.Base 		= "base_env_entity"
ENT.PrintName 	= "Resource Drill"

ENT.OverlayText = {HasOOO = true, resnames = {"energy"} }

function ENT:SetupDataTables()
	self:NetworkVar("Int",0,"DrillDepth")
	self:NetworkVar("Int",1,"DrillLocked")
	self:NetworkVar("Int",2,"DrillShaftspeed")
	self:NetworkVar("Int",3,"DrillPhase")
	self:NetworkVar("Float",0,"DrillExtractionRate")
	self:NetworkVar("Float",1,"DrillHeat")
end
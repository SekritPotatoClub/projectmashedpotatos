ENT.Type				= "anim"  
ENT.Base				= "base_anim"     
ENT.PrintName		= "shield"
ENT.Author			= "Mechanos"
ENT.Contact			= "http://steamcommunity.com/id/Mechanos/"
ENT.Purpose			= "Temporary Spawn Method"
ENT.Instructions		= "herpderpWUT"
ENT.Category		= "Temporary Spawn Method"
 
ENT.Spawnable		= true
ENT.AdminOnly	= true
--ENT.WhoMadeMe					= nil
ENT.WInfo								= ""
ENT.RenderGroup 					= RENDERGROUP_BOTH
ENT.AutomaticFrameAdvance	= true

function ENT:SetupDataTables()
	--self:DTVar("Int",0,"rawrLINKED");
	--self:DTVar("Int",1,"rawrCLIP");
end

if CLIENT then
	function ENT:Draw()
		self.Entity:DrawModel()
		Wire_Render(self.Entity)
	end
	function ENT:Initialize()
		self.WInfo = ""
	end
end

if SERVER then

AddCSLuaFile()

--Initialize-----------------------------------------------------------------------------
function ENT:Initialize()
	----------Initial Settings--------------------
	local model = self.Entity:GetModel()
	if model == "" or model == nil or model == "models/error.mdl" then model = "models/rawr/bore_cannon.mdl" end
	print(model)
	self.Entity:SetModel(model)
	self.Entity:PhysicsInit(SOLID_VPHYSICS)
	self.Entity:SetMoveType(MOVETYPE_VPHYSICS)
	self.Entity:SetSolid(SOLID_VPHYSICS)
	self.Entity:SetUseType(SIMPLE_USE)
	-----------------------------------------------
	----------Get Things Moving----------------
	local phys = self.Entity:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:Wake()
		phys:EnableGravity(true)
		phys:EnableDrag(true)
		phys:EnableCollisions(true)
		phys:EnableMotion(false)
	end
	-----------------------------------------------
	----------Wiremod Inputs and Outputs-----
	if WireAddon then
		--self.Inputs = WireLib.CreateInputs(self, {"Fire", "Reload"})
		--self.Outputs = WireLib.CreateOutputs(self, {"Shots Left", "Heat %"})
	end
	------------------------------------------------
	----------Declare Shared Variables---------
	self.ChassisType = self.ChassisType or 0
	self.ThinkTimer = CurTime()
	self.SetOnce = 0
	self.Elastic = nil
	self.Rope = nil
	------------------------------------------------
	
end
------------------------------------------------------------------------------------------

--SENT Spawn Menu-----------------------------------
function ENT:SpawnFunction(ply,tr)
	if not tr.Hit then return end
	local SpawnPos = tr.HitPos + tr.HitNormal * 60
	local ent = ents.Create("chassis_base")
	ent:SetPos(SpawnPos)
	ent:Spawn()
	ent:Activate()
	ent:GetPhysicsObject():EnableMotion(false)
	ent:SetColor(Color(255,255,255,255))
	ent.WhoMadeMe = ply
	ent.ChassisType = 1
	return ent
end
-----------------------------------------------------------

--Wiremod Functionality------------------------------------------------------------------------------------
function ENT:TriggerInput(iname, value)		
	--if (iname == "Fire" and value > 0) then self.Attack = true else self.Attack = false end
	--if (iname == "Reload" and value > 0) then self.Reload = true else self.Reload = false end
	--if (iname == "InputValue") then self.InputValue = value end
end

function ENT:UpdateWireOutput()
	--Wire_TriggerOutput(self.Entity, "Shots Left", self.ShotsLeft)
end
----------------------------------------------------------------------------------------------------------------

--Required funcs we can leave empty 'till needed--
function ENT:PhysicsUpdate()

end

function ENT:PhysicsCollide(data,physobj)
	
end

function ENT:OnTakeDamage(dmginfo)
	
end

function ENT:Use(activator,caller)

end
-----------------------------------------------------------

--Custom Functions--------------------------------------------------------------------------------------

--[[ Old RaWr reference functions
function ENT:ArcRifleFire()
	print("Starting ArcRifleFire")
	local start = self.Entity:GetPos() - self.Entity:GetForward() * 50
	local dir = -self.Entity:GetForward()
	local speed = 2000
	local spread = 2
	local obj = self.Entity:GetPhysicsObject()
	if rawr_lagless_shot(start, dir, speed, spread, self.Entity) then
		print("lagless_shot returned true in arcriflefire")
		self.Entity:ResetSequence(self.FireAnim) -- what sequence/animation to play
		self.Entity:SetCycle(0) -- Which frame to start out at (0) - first!
		self.Entity:SetPlaybackRate(2) -- Speed
		if (obj:IsValid()) then
			obj:ApplyForceCenter(self.Entity:GetForward() * 5000) -- recoil
		end
		self.Entity:EmitSound("weapons/ar1/ar1_dist1.wav", 400, 50)
		self.CoolDown = CurTime() + 0.3
		self.ShotsLeft = self.ShotsLeft - 1
		self.AmmoLeft = self.AmmoLeft - 1
	end
end

function ENT:ArcRifleShots()
	for i, v in ipairs (Shots) do
		if v ~= Vector(1,1,1) then
			local Trace = {}
			Trace.start = Shots[i]
			Trace.endpos = Shots[i] + Shotdirs[i]
			Trace.filter = self.Entity
			Shots[i] = Trace.endpos
			local Tr = util.TraceLine(Trace)
			local projectile = EffectData()
			projectile:SetStart(Trace.start)
			if Tr.Hit then projectile:SetOrigin(Tr.HitPos) else projectile:SetOrigin(Trace.endpos) end
			projectile:SetScale(20000)
			util.Effect("GaussTracer", projectile)
			if Tr.HitSky then
				Shots[i] = Vector(1,1,1)
				Shotdirs[i] = Vector(1,1,1)
			elseif Tr.Hit then
				local effectdata = EffectData()
				effectdata:SetOrigin(Tr.HitPos)
				effectdata:SetStart(Tr.HitPos)
				effectdata:SetScale(10)
				util.Effect( "cball_explode", effectdata )
				--util.BlastDamage(rawr_explosion, self.WhoMadeMe, Tr.HitPos, 20, 20)
				rawr_damage_radius(3,self.WhoMadeMe,Tr.HitPos, 75, 20)
				WorldSound("weapons/debris1.wav",Tr.HitPos,100,100)
				Shots[i] = Vector(1,1,1)
				Shotdirs[i] = Vector(1,1,1)
			end
		end
	end
end

function ENT:IonBolterFire()
	local start = self.Entity:GetPos() + self.Entity:GetUp() * 50 -- where to start the shot
	local finish = self.Entity:GetUp() * 3000 + VectorRand() * 200 -- direction, distance, and spread of shot
	
	local Trace = {}
	Trace.start = start
	Trace.endpos = start + finish
	Trace.filter = self.Entity
	--Shots.ShotA = TraceA.endpos
	local Tr = util.TraceLine(Trace)
	local projectile = EffectData()
	projectile:SetStart(Trace.start)
	if Tr.Hit then projectile:SetOrigin(Tr.HitPos) else projectile:SetOrigin(Trace.endpos) end
	projectile:SetEntity(Tr.Entity)
	projectile:SetScale(10000)
	projectile:SetMagnitude(50000)
	util.Effect("rawr_shock", projectile)
	if not Tr.HitSky and Tr.Hit then
		local effectdata = EffectData()
		effectdata:SetOrigin(Tr.HitPos)
		effectdata:SetStart(Tr.HitPos)
		effectdata:SetScale(500)
		util.Effect("Impact", effectdata)
		--util.BlastDamage(rawr_fire, self.WhoMadeMe, Tr.HitPos, 20, 5)
		rawr_damage_radius(8,self.WhoMadeMe,Tr.HitPos,200,8)
		WorldSound("weapons/physcannon/superphys_small_zap1.wav",Tr.HitPos,100,100)
	end

	self.Entity:EmitSound("ambient/energy/weld2.wav", 400, math.random(75,125))
	self.CoolDown = CurTime() + 0.05
	self.ShotsLeft = self.ShotsLeft - 1
	self.AmmoLeft = self.AmmoLeft - 0.5
end

function ENT:ParticleGunFire()
	local start = self.Entity:GetPos() + self.Entity:GetUp() * 50 -- where to start the shot
	local direction = self.Entity:GetUp() -- direction, duh
	local spread = Vector(0.1,0.1,0) -- spread thar BUTTAR
	local obj = self.Entity:GetPhysicsObject()
	
	rawr_damage_bullets(1, self.Entity, self.WhoMadeMe, start, direction, spread, 15, math.random(25,50))
	if (obj:IsValid()) then  		
		obj:ApplyForceCenter(self.Entity:GetUp() * -10000) -- recoil
	end 
	
	self.Entity:EmitSound("weapons/fx/nearmiss/bulletLtoR12.wav", 400, math.random(75,100))
	self.Entity:EmitSound("weapons/fx/nearmiss/bulletLtoR12.wav", 400, math.random(100,125))
	self.Entity:EmitSound("weapons/shotgun/shotgun_dbl_fire.wav", 300, 75)
	self.CoolDown = CurTime() + 0.05
	self.ShotsLeft = 0
	self.AmmoLeft = 0
end
]]--

------------------------------------------------------------------------------------------------------------------------------------------------------

--Think Function, control everything from here-----------------------------------------------------
function ENT:Think()
	--One Time Setup per spawn--
	if self.SetOnce == 0 and self.ChassisType ~= 0 then
		if self.ChassisType == 1 then
			--Example
			--Cache Sounds
			util.PrecacheSound("weapons/ar1/ar1_dist1.wav")
			--Check Model
			local model = self.Entity:GetModel()
			if model ~= "models/rawr/bore_cannon.mdl" then
				--Set Model
				self.Entity:SetModel("models/rawr/bore_cannon.mdl")
				--Set default Animation
				local idle = self.Entity:LookupSequence("idle")
				self.Entity:SetSequence(idle) 		-- what sequence/animation to play, leave idle if none
				self.Entity:SetCycle(0) 				-- Which frame to start out at (0) - first!
				self.Entity:SetPlaybackRate(0.5) 	-- speed
			end
			--Set Name
			self.Entity:SetName("dafuq is mah name BITCH")
		end
		self.SetOnce = 1
	elseif self.SetOnce == 0 and self.ChassisType == 0 then -- Something broke, probably dupe fail
		
	end
	-----
	
	
	if WireAddon then self:UpdateWireOutput() end
	self.Entity:NextThink(CurTime()+0.05)
	return true
end
------------------------------------------------------------------------------------------------------------------------------------------------------

--Advanced Duplicator 1 and 2 Compatibility----------------------------------------------------------------------------------------
function ENT:PreEntityCopy()
	--if self.Links then RaWrSaveLinks(self.Entity) end
	if WireAddon then duplicator.StoreEntityModifier(self,"WireDupeInfo",WireLib.BuildDupeInfo(self.Entity)) end
	--local RaWr_TyPe = {}
	--RaWr_TyPe.TyPe = self.RaWrTyPe
	--RaWr_TyPe.GuN = self.RaWrGuN
	--duplicator.StoreEntityModifier(self,"RaWr_TyPe",RaWr_TyPe)
end

function ENT:PostEntityPaste(ply, ent, NewEnts)
	self.WhoMadeMe=ply
	local emods = ent.EntityMods
	if not emods then return end
	--self.RaWrTyPe = emods.RaWr_TyPe.TyPe
	--self.RaWrGuN = emods.RaWr_TyPe.GuN
	--self.Links = {}
	--RaWrRebuildLinks(ent, NewEnts)
	if WireAddon then WireLib.ApplyDupeInfo(ply, ent, emods.WireDupeInfo, function(id) return NewEnts[id] end) end
end
---------------------------------------------------------------------------------------------------------------------------------------------




end
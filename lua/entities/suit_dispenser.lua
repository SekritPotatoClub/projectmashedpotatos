AddCSLuaFile()

ENT.Type 			= "anim"
ENT.Base 			= "base_env_base"
ENT.PrintName		= "Suit Dispenser"
ENT.Author			= "Ludsoe"
ENT.Category		= "EnvX"

ENT.Spawnable		= false
ENT.AdminSpawnable	= true

ENT.OverlayText = {HasOOO = true, resnames ={"energy","water","oxygen","hydrogen"}, genresnames={"Suit"}}

if(SERVER)then
	function ENT:Initialize()
		self.BaseClass.Initialize(self)
		
		self:AddResource("Suit",100)
		
		if WireAddon then
			self.WireDebugName = self.PrintName
		end
		self.Inputs = Wire_CreateInputs(self.Entity, { "Lock" })
		self.Locked = false
		self.Dispensing = false
		
		--self.env_extra_data
		local extra = self.env_extra_data
		if extra ~= nil then
			if extra.animated then
				self:SetSequence("idle")
				--self:SetSequence("empty")
				
				self:SetCycle(1)
				
				self.IsAnimated = true
				
				print("Am Animated!")
				--PrintTable(self.env_extra_data)
			end
		end
	end
	
	
	function ENT:AcceptInput(name,activator,caller)
		if name == "Use" and caller:IsPlayer() and caller:KeyDownLast(IN_USE) == false then
			self:SetActive( nil, caller )
		end
	end	
		
	function ENT:TriggerInput(iname, value)
		if (iname == "Lock") then
			if (value > 0) then
				self.Locked= true
			else
				self.Locked = false
			end	
		end
	end
	
	local function quiet_steam(ent)
		ent:StopSound( "ambient.steam01" )
	end

	local SuitDat = EnvX.DefaultSuitData
	local Multiplier = 1.5
	local Divider = 1/Multiplier
	
	function ENT:SetActive( value, caller )
		if not self.node then return end
		if self.Locked then return end
		
		local Res_needed = 100-((caller.suit.energy/SuitDat.maxenergy) * 100)
		local ResHas = self:GetResourceAmount("Suit")
		
		--print("N:"..Res_needed.." H:"..ResHas)
		
		if ResHas>=Res_needed then
			self:ConsumeResource("Suit", Res_needed)
			caller.suit.energy = SuitDat.maxenergy
		else
			self:ConsumeResource("Suit", ResHas)

			caller.suit.energy = caller.suit.energy + math.floor(((SuitDat.maxenergy*ResHas)/100))			
		end
		
		local fuel = self:GetResourceAmount("hydrogen")
		local fuel_needed = math.ceil(((SuitDat.maxfuel) - caller.suit.fuel) * Divider)
		if ( fuel_needed < fuel ) then
			self:ConsumeResource("hydrogen", fuel_needed)
			caller.suit.fuel = SuitDat.maxfuel
		elseif (fuel > 0) then
			caller.suit.fuel = caller.suit.fuel + math.floor(fuel * Multiplier)
			self:ConsumeResource("hydrogen", fuel)
		end
		
		if Res_needed > 0 or fuel_needed > 0 then
			caller:EmitSound( "ambient.steam01" )
			self:SetCycle(1-math.Clamp((self:GetResourceAmount("Suit")/100),0,1))
			self.Dispensing = true
			timer.Simple(1.2, function() self.Dispensing = false quiet_steam(caller) end)
		else
			self:PlayDeviceSound("Nope","hl1/fvox/fuzz.wav")
		end
	end
	
	function ENT:ConvertSuit()
		if self:GetStorageLeft("Suit")>0 and not self.Dispensing then
			local energy,water,oxygen = self:GetResourceAmount("energy"),self:GetResourceAmount("water"),self:GetResourceAmount("oxygen")
			
			if energy>80 and water>20 and oxygen>40 then
				self:SupplyResource("Suit",1)
				
				self:ConsumeResource("energy", 80)
				self:ConsumeResource("water", 20)
				self:ConsumeResource("oxygen", 40)
				
				if self.IsAnimated then
					self:SetCycle(1-math.Clamp((self:GetResourceAmount("Suit")/100),0,1))
				end
			end
		end
	end
	
	function ENT:Think()
		self.BaseClass.Think(self)
		
		self:ConvertSuit()
		
		self:NextThink( CurTime() + 0.1 )
		return true
	end
else

end		

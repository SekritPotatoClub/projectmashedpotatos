AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_env_base"
ENT.PrintName 	= "Hydrogen Fuel Cell"

ENT.OverlayText = {HasOOO = true ,resnames = {"oxygen","hydrogen"}, genresnames={"energy","water"}}


if(SERVER)then
    local Increment = 150

    function ENT:Initialize()
        self.BaseClass.Initialize(self)
        self.Active = 0
        self.overdrive = 0
        self.damaged = 0
        self.lastused = 0
        self.Mute = 0
        if WireAddon then
            self.WireDebugName = self.PrintName
            self.Inputs = Wire_CreateInputs(self, { "On", "Overdrive", "Mute", "Multiplier" })
        else
            self.Inputs = {{Name="On"},{Name="Overdrive"}}
        end
    end

    function ENT:OnTurnOn()
        self:PlayDeviceSound( "Activate","ambient/machines/thumper_startup1.wav" )
        self:PlayDeviceSound( "Idle","Airboat_engine_idle" )
    end
    
    function ENT:OnTurnOff()
        self:StopDeviceSound( "Idle" )
        self:StopDeviceSound( "coast.siren_citizen" )
        WireLib.TriggerOutput(self, "On", 0)
        WireLib.TriggerOutput(self, "Output", 0)
    end

    function ENT:TurnOnOverdrive()
        if ( self.Active == 1 ) then
            self:PlayDeviceSound( "OverDrive","apc_engine_start" )
            self:SetOOO(2)
            self.overdrive = 1
        end
    end
    
    function ENT:TurnOffOverdrive()
        if ( self.Active == 1 and self.overdrive == 1) then
            self:StopDeviceSound( "OverDrive" )
            self:SetOOO(1)
            self.overdrive = 0
        end	
    end

    function ENT:TriggerInput(iname, value)
        if (iname == "On") then
            self:SetActive(value)
        elseif (iname == "Overdrive") then
            if (value ~= 0) then
                self:TurnOnOverdrive()
            else
                self:TurnOffOverdrive()
            end
        end
        if (iname == "Mute") then
            if (value > 0) then
                self.Mute = 1
            else
                self.Mute = 0
            end
        end
        if (iname == "Multiplier") then
            self:SetMultiplier(value)
        end
    end
    function ENT:Proc_Water()
        local oxygen = self:GetResourceAmount("oxygen")
        local hydrogen = self:GetResourceAmount("hydrogen")
        local hinc = Increment + (self.overdrive*Increment)
        hinc = (math.Round(hinc * self:GetSizeMultiplier())) * self:GetMultiplier() * 2
        local oinc = Increment + (self.overdrive*Increment)
        oinc = (math.Round(oinc * self:GetSizeMultiplier())) * self:GetMultiplier()
        if (oxygen >= oinc and hydrogen >= hinc) then
            self:ConsumeResource("oxygen", oinc)
            self:ConsumeResource("hydrogen", hinc)
            
            local supplyEnergy = math.Round(hinc)
            self:SupplyResource("energy", supplyEnergy)
            
            local supplyWater = math.Round(hinc/2)
            self:SupplyResource("water",supplyWater)
        else
            self:TurnOff()
        end
    end
    
    function ENT:Think()
        self.BaseClass.Think(self)
        
        if ( self.Active == 1 ) then self:Proc_Water() end
        
        self:NextThink( CurTime() + 1 )
        return true
    end    
else

end
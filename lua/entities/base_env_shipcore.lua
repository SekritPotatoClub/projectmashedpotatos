AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_env_base"

ENT.PrintName	= "Environments Entity Core"
ENT.Author		= "Ludsoe"
ENT.Purpose		= "Base for all ShipCores"
ENT.Instructions	= ""

ENT.Spawnable		= false
ENT.AdminSpawnable	= false

ENT.IsLDEC = 1
ENT.IsCore = true
ENT.NoEnvPanel = true

if SERVER then
	function ENT:SetupCore()		
		local V,N,A,E = "VECTOR","NORMAL","ANGLE","ENTITY"
		self.Outputs = WireLib.CreateSpecialOutputs( self,
			 { "Health", "Total Health" ,"Shields" ,"Max Shields", "Mount Points", "Mount Capacity" , "Temperature","Freezing Point",  "Melting Point", "OverHeating", "Attacker" },
			{N,N,N,N,N,N,N,N,N,N,E}
			)
		self.Inputs = Wire_CreateInputs(self, { "SelfDestruct","Vent Shields","UnLink All" })
		
		--Lets Grab our data we got assigned!
		local Settings = self.envx_spawn_settings or {}
		--print("I Got") PrintTable(Settings)
		local ID = Settings["Core Type"] or "Basic Core"
		self.Data = LDE.CoreSys.CoreStats[ID]
		
		--Setup all of our variables.
		self.OverHeating= 0 self.Thinkz= 0
		self.LDE = {CorePoints=0,MaxCorePoints=1,CoreHealth=1,CoreMaxHealth=1,CoreShield=0,CoreMaxShield=0,ShieldCostMultiplier=1,TotalHealth=1,CanRecharge=1,Flashing=1,CoreTemp=0,CoreMinTemp=-1,CoreMaxTemp=1,Core=self}
		self.Props ={} self.Weapons ={} self.CoreLinked ={} self.PropHealth ={} self.Shielded ={} self.ShieldFlux = {}
		self.ShipClass = "Calculating"
		
		self:CoreLink(self)
		
		LDE.CoreSys.CoreHealth(self,self.Data)
		
		self.LDE.CoreHealth = self.LDE.CoreMaxHealth
		self.LDE.CorePoints = self.LDE.MaxCorePoints
		
		WireLib.TriggerOutput( self, "Health", self.LDE.CoreHealth or 0 )
		WireLib.TriggerOutput( self, "Total Health", self.LDE.CoreMaxHealth or 0 )
		WireLib.TriggerOutput( self, "Shields", self.LDE.CoreShield or 0 )
		WireLib.TriggerOutput( self, "Max Shields", self.LDE.CoreMaxShield or 0 )
		WireLib.TriggerOutput( self, "Temperature", self.LDE.CoreTemp)
		WireLib.TriggerOutput( self, "Freezing Point", self.LDE.CoreMinTemp or 0 )
		WireLib.TriggerOutput( self, "Melting Point", self.LDE.CoreMaxTemp or 0 )	
		WireLib.TriggerOutput( self, "Mount Points", self.LDE.CorePoints or 0 )
		WireLib.TriggerOutput( self, "Mount Capacity", self.LDE.MaxCorePoints or 0 )
		
		self:SetNWInt("LDECoreType", self.Data.name)
		self:SetNWInt("LDECoreClass", "Registering")	
	end

	function ENT:ReloadEnvxSettings()
		self:SetupCore()
	end

	function ENT:Initialize()   
		self:PhysicsInit( SOLID_VPHYSICS )  	
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS ) 

		self:SetupCore()
		
		self:NextThink( CurTime() + 1 )
		return true
	end
	
	function ENT:ClearProp( Entity )
		for key, ent in pairs( self.Props ) do
			if Entity == ent then
				table.remove( self.Props, key )
				self.Prophealth[ent:EntIndex()] = nil
				return --Stop the loop there.
			end
		end
	end

	function ENT:CoreLink(Entity)
		if(Entity.LDE.Core and not Entity.LDE.Core == self)then
			Entity.LDE.Core:CoreUnLink(Entity)
		end
		self.CoreLinked[Entity:EntIndex()]=Entity
		Entity.LDE.Core = self
		if Entity.IsLDEWeapon or Entity.PointCost then
			self.Weapons[Entity:EntIndex()] = Entity
			if Entity.HasPoints == false and Entity.PointCost <= self.LDE.CorePoints then
				self.LDE.CorePoints=self.LDE.CorePoints-Entity.PointCost
				Entity.HasPoints=true
			end
		end
	end

	function ENT:CoreUnLink( Entity )
		for key, ent in pairs( self.CoreLinked ) do
			if Entity == ent then
				table.remove( self.CoreLinked, key )
				Entity.LDE.Core = nil
				if Entity.IsLDEWeapon or Entity.PointCost then
					if Entity.HasPoints == true then
						self.Weapons[Entity:EntIndex()] = nil
						self.LDE.CorePoints=self.LDE.CorePoints+Entity.PointCost
						Entity.HasPoints=false
					end
				end
				return --Stop the loop there.
			end
		end
	end

	function ENT:UnLinkAll()
		if not self.CoreLinked then return end
		for _, ent in pairs( self.CoreLinked ) do
			if ent and IsValid(ent) then
				ent.LDE.Core = nil
				ent.Shield = nil
			end
		end
		self.CoreLinked={}
	end
	
	function ENT:TriggerInput(iname, value)
		if (iname == "SelfDestruct") then
			if (value > 0) then
				LDE:ExplodeCore(self)
			end	
		elseif(iname=="Vent Shields")then
			if (value > 0) then
				self.LDE.CoreShield=0
			end
		elseif(iname=="UnLink All")then
			if (value > 0) then
				self:UnLinkAll()
			end
		end
	end

	function ENT:Think() 
		if not self.Thinkz then return end--WOT
		self.Thinkz=self.Thinkz+1
		if self.Thinkz>=5 then
			self.Thinkz=0
			LDE.CoreSys.CoreHealth(self,self.Data)
			
			self.LDE.CorePoints = self.LDE.MaxCorePoints --Set the points to max.
			
			for key, ent in pairs( self.Weapons ) do
				if ent and IsValid(ent) then
					if not LDE:CheckUnlocked(LDE.GetPropOwner(ent),ent) then
						if ent.PointCost <= self.LDE.CorePoints then
							self.LDE.CorePoints=self.LDE.CorePoints-ent.PointCost
							ent.HasPoints=true
						else
							ent.HasPoints=false
						end
					end
				else
					table.remove( self.Weapons, key )
				end
			end
			
			if(self.Data.Think)then
				self.Data.Think(self)
			end
			
			LDE.CoreSys.CoreClass(self)
		end
					
		if self.LDE.CoreTemp > self.LDE.CoreMaxTemp then 
			self.OverHeating=1
			LDE:DealDamage(self, math.abs(self.LDE.CoreTemp-self.LDE.CoreMaxTemp)*3, self, self,true)
		else
			self.OverHeating=0
		end
			
		local Networked = {
			CoreMaxHealth 	= "LDEMaxHealth",
			CoreHealth		= "LDEHealth",
			CoreMaxShield	= "LDEMaxShield",
			CoreShield		= "LDEShield",
			CoreMaxTemp		= "LDEMaxTemp",
			CoreMinTemp		= "LDEMinTemp",
			CoreTemp		= "LDETemp",
			MaxCorePoints	= "MaxCorePoints",
			CorePoints		= "CorePoints"
		}
			
		-- Set NW ints
		for DV, NW in pairs(Networked) do
			local hp = self:GetNWInt(NW)
			if not hp or hp ~= self.LDE[DV] then
				--   print("Synced "..NW.." as "..DV.." for "..self.LDE[DV])
				self:SetNWInt(NW, self.LDE[DV])
			end				
		end

		self:NextThink( CurTime() + 1 )
		return true
	end
	
	function ENT:ShieldFluxCheck(ent,damage)		
		if not self.ShieldFlux[ent] then self.ShieldFlux[ent]={} end
		local Flux = self.ShieldFlux[ent]
		
		local TotalFlux = 0
		for ID, Flx in pairs(Flux) do
			if Flx.Time > CurTime() then
				TotalFlux = TotalFlux+Flx.DMG
				Flx.Time=CurTime()+3
			else
				table.remove(Flux,ID)
			end
		end
		
		table.insert(Flux,{Time=CurTime()+3,DMG=damage})
		
		--return TotalFlux+damage < LDE:CalcHealth(ent)*self.Data.ShieldRate
		return true
	end
	
	function ENT:ChangeTemp(Amount)
	--	print("Temperature Changing, "..Amount)
		if Amount==0 then return end
		self.LDE.CoreTemp=self.LDE.CoreTemp+Amount
		WireLib.TriggerOutput( self, "Temperature", self.LDE.CoreTemp)
	end
	
	function ENT:OnRemove()
		self:UnLinkAll()
	end

	function ENT:EnvxOnDupe() return {Data=1} end
	function ENT:EnvxOnPaste(Player,Ent,CreatedEntities) self:SetupCore() end

else --Lets do client side shit! :D
	function ENT:Draw()      
		self:DrawDisplayTip()
		self:DrawModel()
	end

	local TipColor = Color( 250, 250, 200, 255 )

	surface.CreateFont("GModWorldtip", {font = "coolvetica", size = 24, weight = 500})
		
	function ENT:DrawDisplayTip()		
		
		if LocalPlayer():GetEyeTrace().Entity == self and EyePos():Distance( self:GetPos() ) < 512 then
			EnvX.MenuCore.RenderWorldTip(self,function(self)
				return {
					{Type="Label",Value="Type: "..self:GetNWString("LDECoreType")},
					{Type="Label",Value="Class: "..self:GetNWString("LDECoreClass")},
					{Type="Percentage",
						Value=math.Round(self:GetNWInt("LDEHealth"))/math.Round(self:GetNWInt("LDEMaxHealth")),
						Text="Health: "..math.Round(self:GetNWInt("LDEHealth")).." / "..math.Round(self:GetNWInt("LDEMaxHealth"))
					},
					{Type="Percentage",
						Value=math.Round(self:GetNWInt("LDEShield"))/math.Round(self:GetNWInt("LDEMaxShield")),
						Text="Shields: "..math.Round(self:GetNWInt("LDEShield")).." / "..math.Round(self:GetNWInt("LDEMaxShield"))
					},
					{Type="Percentage",
						Value=math.Round(self:GetNWInt("CorePoints"))/math.Round(self:GetNWInt("MaxCorePoints")),
						Text="Processor: "..math.Round(self:GetNWInt("CorePoints")).." / "..math.Round(self:GetNWInt("MaxCorePoints"))
					},					
					{Type="Label",Value="Heat: "..math.Round(self:GetNWInt("LDEMinTemp")).." / ("..math.Round(self:GetNWInt("LDETemp"))..") / "..math.Round(self:GetNWInt("LDEMaxTemp"))}
				}
			end)
		end
	end
end
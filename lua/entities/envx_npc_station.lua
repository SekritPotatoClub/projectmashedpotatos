AddCSLuaFile()

ENT.Type 			= "anim"
ENT.Base 			= "base_gmodentity"
ENT.PrintName		= "Envx Npc Station"
ENT.Author			= "Ludsoe"
ENT.Category = "EnvX"

ENT.Spawnable		= false
ENT.AdminOnly	= false
ENT.IsEnvxNPCStation = true

if(SERVER)then
	function ENT:Initialize()
		self:PhysicsInit(SOLID_VPHYSICS)
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetSolid(SOLID_VPHYSICS)
		local phys = self:GetPhysicsObject()
		if IsValid(phys) then
			phys:Wake()
			phys:EnableMotion(false)
			phys:EnableGravity(false)
		end
				
		--Ship Core Mimicing....
		self.CoreLinked={self}
		self.NoGrav=true
		
		self.SelfDestructActive = false
		self.AttachedSpores = {}
		
		self.LDE={
			Core=self,
			CoreMaxShield=1000000,
			CoreShield=1000000,
			CoreMaxHealth=1000000,
			CoreHealth=1000000
		}
	end
	
	function ENT:OnSporeAttach(spore)
		table.insert(self.AttachedSpores,spore)
	end
	
	function ENT:RegenerateShields()
		--Add hull and shield regeneration.
	end
	
	function ENT:Think()
		self:RegenerateShields()
		
		if table.Count(self.AttachedSpores)>0 then
			for k, spore in pairs(self.AttachedSpores) do
				if not IsValid(spore) then
					table.remove(self.AttachedSpores,k)
				end
			end
		end
		self:NextThink(CurTime()+1)
	end
	
	function ENT:GravGunPunt()
		return false
	end

	function ENT:GravGunPickupAllowed()
		return false
	end
	
	function ENT:ChangeTemp(Amount) return end
	function ENT:ShieldFluxCheck(ent,damage) return true end 
else
	ENT.RenderGroup = RENDERGROUP_OPAQUE

	function ENT:Draw()
		self.Entity:DrawModel()
	end
end		

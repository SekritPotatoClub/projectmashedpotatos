AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_env_base"

ENT.PrintName	= "Environments Storage Core"
ENT.Author		= "CmdrMatthew,Ludsoe"
ENT.Purpose		= "Base for all RD Storages"
ENT.Instructions	= ""

ENT.Spawnable		= false
ENT.AdminSpawnable	= false

ENT.EnvxNetworked = true
ENT.NoEnvPanel = true

if(SERVER)then
	function ENT:MakeWireOutputs()
		local tab = {}
		local i = 1
		local resources = self.maxresources
		for k,res in pairs(resources) do
			tab[i] = k
			tab[i+1] = "Max "..k
			i = i + 2
		end
		--PrintTable(tab)
		self.Outputs = Wire_CreateOutputs(self, tab)		
	end
	
	function ENT:HandleWireStuff()
		self.WireDebugName = self.PrintName
		self.Inputs = WireLib.CreateInputs(self, { "Vent", "Vent Amount"})
		
		self:MakeWireOutputs()
	end

	function ENT:TriggerInput(iname, value)
		if iname == "Vent" then
			if value > 0 then
				self.Vent = 1
			else
				self.Vent = 0
			end
		elseif iname == "Vent Amount" then
			if value > 0 then
				self.ventamt = value
			else
				self.ventamt = 1000
			end
		elseif iname == "Test" then
			LDE:KillEnt(self)
		end
	end
	
	local Explosions = {
		"ambient/explosions/explode_1.wav",
		"ambient/explosions/explode_2.wav",
		"ambient/explosions/explode_3.wav",
		"ambient/explosions/explode_4.wav",
		"ambient/explosions/explode_5.wav",
		"ambient/explosions/explode_7.wav",
		"ambient/explosions/explode_8.wav",
		"ambient/explosions/explode_9.wav"
	}
	
	function ENT:ContainmentFailure() --Add support for Blackholium causing implosions, forming temporary black-holes.
		self:Unlink() --So we know whats in it.	
		local explosiveness = 0
		print("Calculating explosiveness!")
		for n,i in pairs( self.resources ) do 
			local dat = EnvX.GetResourceData(n)
			
			local exp = dat.Volatality*i
			
			print("resource: "..n.." amount: "..i.." explosiveness: "..exp)
		
			explosiveness = explosiveness+exp
		end
		
		explosiveness = math.Clamp(explosiveness,0,1000000)
		
		if explosiveness > 1000 then
			local Booms = table.Random(Explosions)
			self:EmitSound(Booms)
			
			local ExplosionScale = explosiveness/10000
			
			print("Explosion Scale: "..ExplosionScale)
			
			local Boom = { 
				Pos 					=		self:GetPos(),	--Required--		--Position of the Explosion, World vector
				ShrapDamage	=		100*ExplosionScale,										--Amount of Damage dealt by each Shrapnel that hits, if 0 or nil then other Shap vars are not required
				ShrapCount		=		4*ExplosionScale,										--Number of Shrapnel, 0 to not use Shrapnel
				ShrapDir			=		self:GetForward(),							--Direction of the Shrapnel, Direction vector, Example: Missile:GetForward()
				ShrapCone		=		180,										--Cone Angle the Shrapnel is randomly fired into, 0-180, 0 for all to be released directly forward, 180 to be released in a sphere
				ShrapRadius		=		120*ExplosionScale,										--How far the Shrapnel travels
				ShockDamage	=		explosiveness,				--Required--		--Amount of Shockwave Damage, if 0 or nil then other Shock vars are not required
				ShockRadius		=		100*ExplosionScale,										--How far the Shockwave travels in a sphere
				Ignore				=		self,									--Optional Entity that Shrapnel and Shockwaves ignore, Example: A missile entity so that Shrapnel doesn't hit it before it's removed
				Inflictor				=		self,			--Required--		--The weapon or player that is dealing the damage
				Owner				=		self:CPPIGetOwner()			--Required--		--The player that owns the weapon, or the Player if the Inflictor is a player
			}
			LDE:BlastDamage(Boom)	
			
			local effectdata = EffectData() effectdata:SetOrigin(self:GetPos()) effectdata:SetStart(self:GetPos()) effectdata:SetMagnitude(ExplosionScale)
			util.Effect( "WhomphSplode", effectdata )			
		end
		
		print("Total Explosiveness! "..explosiveness)
	end
	
	function ENT:Think()
		if self.Vent == 1 and self.environment then
			if self.node then
				for v,k in pairs(self.maxresources) do
					self:ConsumeResource(k, self.ventamt or 100)
				end
			end
		end
		
		if WireAddon then
			--print("AM Thinking WireCheck!")
			for k,v in pairs(self.maxresources) do
				--print(tostring(k).." : "..tostring(v))
				Wire_TriggerOutput(self, k, self:GetResourceAmount(k))
				Wire_TriggerOutput(self, "Max "..k, self:GetNetworkCapacity(k))
			end
		end
		
		self:NextThink(CurTime() + 1)
		return true
	end
else
	ENT.RenderGroup = RENDERGROUP_BOTH
end		

include("shared.lua")

local OOO = {}
OOO[0] = "Off"
OOO[1] = "Scanning"

function ENT:ExtraData(Info)
	if self:GetOOO() == 1 then
		table.insert(Info,{Type="Label",Value="Resource Density: "..math.Round( self:GetDensity(),2)})
		table.insert(Info,{Type="Label",Value="Resource Pool Volume: "..math.Round( ( ( ( self:GetFindSize() ) * 0.75 ) * 2.54 ) *1e-2,2).." cubic m"})
		table.insert(Info,{Type="Label",Value="Scanner Range: "..self:GetFindRange()})
		table.insert(Info,{Type="Label",Value="Scanner Beam Angle: "..tostring(self:GetScanAngle()).." deg."})
		table.insert(Info,{Type="Label",Value="Resource Depth: "..math.Round( ( ( ( self:GetDepth() ) * 0.75) * 2.54) * 1e-2,2).." m"})
		table.insert(Info,{Type="Label",Value="Resource Distance: "..tostring( math.Round(self:GetFindDistance(),2) ).." m"})
		table.insert(Info,{Type="Label",Value="Resource Type: "..self:GetFindResType()})
		table.insert(Info,{Type="Label",Value="Relative Angle: ("..tostring(self:GetFindTargetAngle())..")"})
		table.insert(Info,{Type="Label",Value="Resource Count: "..self:GetFindQuantity()})
		return true
	end
	return false 
end

ENT.Type 		= "anim"
ENT.Base 		= "base_env_entity"
ENT.PrintName 	= "Resource Scanner"

ENT.OverlayText = {HasOOO = true, resnames = {"energy"} }

function ENT:SetupDataTables()
	self:NetworkVar( "Float", 0, "Density") 
	self:NetworkVar("Int",0,"FindSize")
	self:NetworkVar("Int",1,"FindQuantity")
	self:NetworkVar("Int",2,"FindRange")
	self:NetworkVar("Int",3,"ScanAngle")
	self:NetworkVar("Float",1,"Depth")
	self:NetworkVar("Float",2,"FindDistance")
	self:NetworkVar("Angle",0,"FindTargetAngle")
	self:NetworkVar("String",0,"FindResType")
end

local MC = EnvX.MenuCore

if(SERVER)then
	local T = {} --Create a empty Table
	
	T.Power = function(Device,ply,Data)
		Device:SetActive( nil, ply )
	end
	
	T.Range = function(Device,ply,Data)
		Device:SetScanningRange(tonumber(Data))
	end
	
	T.Angle = function(Device,ply,Data)
		Device:SetScaningAngle(tonumber(Data))
	end
	
	ENT.Panel=T --Set our panel functions to the table.
else
	function ENT:PanelFunc(entID)	
		self.DevicePanel = {
			function() return MC.CreateButton(Parent,{x=90,y=30},{x=0,y=0},"Toggle Power",function() RunConsoleCommand( "envsendpcommand",self:EntIndex(),"Power") end) end,
			function()
				local S = MC.CreateSlider(Parent,{x=150,y=30},{x=0,y=0},{Min=50,Max=8000,Dec=0},"Scan Range",function(val) RunConsoleCommand( "envsendpcommand",self:EntIndex(),"Range",val) end)
				S:SetValue(self:GetFindRange())
				return S
			end,
			function()
				local S = MC.CreateSlider(Parent,{x=150,y=30},{x=0,y=0},{Min=5,Max=45,Dec=0},"Scan Angle",function(val) RunConsoleCommand( "envsendpcommand",self:EntIndex(),"Angle",val) end)
				S:SetValue(self:GetScanAngle())
				return S
			end
		}
	end
end		


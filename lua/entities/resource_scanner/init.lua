AddCSLuaFile("shared.lua")
AddCSLuaFile("cl_init.lua")
include("shared.lua")

--# Initialize
function ENT:Initialize()
	self.BaseClass.Initialize(self)

	self.Range = 1000
	self.MaxRange = 1000
	self.Angle = 15
	self.Energy_Increment = self.Range * 0.02
	self.Hologram = nil
	self.HoloVec = Vector(0,0,0)
	
	self:SetDensity(1)
	self:SetDepth(0)
	self:SetFindSize(0)
	self:SetFindRange(self.Range)
	self:SetFindQuantity(0)
	self:SetFindDistance(0)
	self:SetScanAngle(self.Angle)
	self:SetFindTargetAngle(Angle(0,0,0))
	self:SetFindResType("None")
	
	if WireAddon then
		self.WireDebugName = self.PrintName
		self.Inputs = Wire_CreateInputs(self, { "On", "Range","ScanAngle" })
		self.Outputs = WireLib.CreateSpecialOutputs(self, 
			{ "On","Density","Depth","Range","Size","Quantity","Distance","Type","ScanAngle","TargetAngle"},
			{"NORMAL","NORMAL","NORMAL","NORMAL","NORMAL","NORMAL","NORMAL","STRING","NORMAL","ANGLE"}
		)
	else
		self.Inputs = {{Name="On"}}
	end
	
	self.SetScaningAngle(self.Angle)
end

--# Set the maximum range of the scanner ( affects energy use )
function ENT:SetScanningRange(range)
	if not range or type(range) ~= "number" then return end

	if self.env_extra then self.MaxRange = self.env_extra
	else self.MaxRange = 1000
	end

	if range < 10 then range = 10
	elseif range > self.MaxRange then range = self.MaxRange
	end
	-- yeah redundant  -_-
	self.Range = range
	self:SetFindRange(self.Range)
	self.Energy_Increment = self.Range * (self.Angle * 0.00425)
	return self.Range
end

--# Set the angle of the scaning beam
function ENT:SetScaningAngle(angle)
	if not angle or type(angle) ~= "number" then return end

	if angle < 1 then angle = 1
	elseif angle > 45 then angle = 45
	end
	-- todo: ugh get rid of the redundancy
	self.Angle = angle
	self:SetScanAngle(angle)
	
	return self.Angle
end

--# Activate the Scanner
function ENT:TurnOn()
	self.Active = 1
	self:SetOOO(1)
	self:Scan()
	self:TriggerWireOutputs()
	self:EmitSound("/buttons/combine_button3.wav",100,100)
end

--# Shut down the scanner
function ENT:TurnOff()
	self.Active = 0
	self:SetOOO(0)
	
	-- update datatable	
	self:SetDensity(1)
	self:SetDepth(0)
	self:SetFindSize(0)
	self:SetFindQuantity(0)
	self:SetFindDistance(0)
	self:SetFindTargetAngle(Angle(0,0,0))
	self:SetFindResType("None")
	
	self:RemoveHolo()
	
	-- triger wire outputs
	self:TriggerWireOutputs()
end

--# Read inputs
function ENT:TriggerInput(iname,value)
	if iname == "On" then
		if value > 0 then
			self:TurnOn()
		else
			self:TurnOff()
		end
	end
	if iname == "Range" then
		self:SetScanningRange(value)
	end
	if iname == "ScanAngle" then
		self:SetScaningAngle(value)
	end
end

function ENT:TriggerWireOutputs()
	if WireAddon then
		Wire_TriggerOutput(self,"On",self.Active)
		Wire_TriggerOutput(self,"Density",self:GetDensity())
		Wire_TriggerOutput(self,"Depth",self:GetDepth())
		Wire_TriggerOutput(self,"Range",self:GetFindRange())
		Wire_TriggerOutput(self,"Size",self:GetFindSize())
		Wire_TriggerOutput(self,"Quantity",self:GetFindQuantity())
		Wire_TriggerOutput(self,"Distance",self:GetFindDistance())
		Wire_TriggerOutput(self,"Type",self:GetFindResType())
		Wire_TriggerOutput(self,"ScanAngle",self:GetScanAngle())
		Wire_TriggerOutput(self,"TargetAngle",self:GetFindTargetAngle())
	end
end

function ENT:OnRemove()	
	self:RemoveHolo()
end

function ENT:RemoveHolo()
	if IsValid(self.Hologram) then
		self.Hologram:Remove()
	end
end

function ENT:ProjectHolo()
	if self:GetActive() then
		if not IsValid(self.Hologram) then
			local holo = ents.Create("envx_miningholo")
			
			holo:SetModel( "models/slyfo/powercrystal.mdl" )
			holo:SetColor(Color(0,140,255,255))
			holo:SetMaterial("models/wireframe")
			
			holo.Bob = 0 holo.BobDir = 1
			holo:SetPos(self.HoloVec)
			holo.Rotation = 5
			holo:SetAngles(Angle(0,holo.Rotation,180))
			
			holo:Spawn()
			holo:Initialize()
			
			holo:SetModelScale( 3, 0 )
			
			self.Hologram = holo
		else
			local holo = self.Hologram
			holo.Rotation=holo.Rotation+5
			holo:SetAngles(Angle(0,holo.Rotation,180))
			
			holo.Bob=holo.Bob+holo.BobDir
			if holo.Bob>10 then holo.BobDir=-1 elseif holo.Bob<-10 then holo.BobDir=1 end
			holo:SetPos(self.HoloVec+Vector(0,0,holo.Bob))
		end
	else
		if IsValid(self.Hologram) then
			self:RemoveHolo()
		end
	end
end
	
local function angnorm(ang) -- normalize angle for direction finding.
	if not ang and type(ang) ~= "Angle" then return end
	return Angle( (ang.pitch+180)%360-180,(ang.yaw+180)%360-180,(ang.roll+180)%360-180)
end
	
--# Scan for the closest resource in the cone.
function ENT:Scan()
	if self:GetResourceAmount("energy") <= 0 then
		self:TurnOff()
		return
	end
	
	local res,Pos = {},self:GetPos()
	local restypes = {"resource_pool","resource_asteroid"}
	scandir = self:GetForward()
	scandir:Normalize()
	
	-- Fooking Hax damn ents.FindInCone is shait!
	local scanAng = math.cos( (math.pi / 180 ) * self.Angle )
	local function IsInCone(ent)
		local dir = (ent:GetPos() - Pos )
		dir:Normalize()
		local dot = scandir:Dot(dir)
		return dot >= scanAng
	end
	
	local ST = ents.FindInSphere(Pos,self.Range)
	for k,v in pairs(ST) do
		if table.HasValue(restypes,v:GetClass()) and IsInCone(v) then
			res[#res+1] = v
		end
	end
	
	local closest,dist = nil, self.Range
	if (#res > 0 ) then
		-- Find the closest target
		for k,v in pairs(res) do 
			local range = Pos:Distance( v:GetPos() )
			if  range < dist then  closest,dist = v,range end
		end
	end
	local quantity = 0
	local density = 1
	local depth = 0
	local size = 0
	local distance = 0
	local angle = Angle(0,0,0)
	local res = "None"
	
	if closest ~= nil then
		density = math.Round( 1+ (closest.density^2 / (dist*1e-1) ),2)
		depth = closest.depth or 0
		size = closest.base_volume
		quantity = #res or 0
		distance = math.Round( ( ( (dist-closest.radius) * 0.75) * 2.54) * 1e-2,2)
		res = closest.resource_type
		
		local fuckgarry = (closest:GetPos() - self:GetPos())
		fuckgarry:Normalize()
		angle = angnorm ( self:GetAngles() - fuckgarry:Angle() ) or Angle(0,0,0)
	end
	
	if IsValid(closest) and depth>0 then
		if self.LastFound~=closest then
			self.LastFound = closest
			self:EmitSound( "garrysmod/content_downloaded.wav")
		end
		self.HoloVec = closest:GetPos()+Vector(0,0,closest.depth+90)
		self:ProjectHolo()
	else
		self:RemoveHolo()
		self.LastFound = nil
	end
	
	-- update synced data
	self:SetDensity(density)
	self:SetDepth(depth or 0)
	self:SetFindSize(size)
	self:SetFindRange(self.Range)
	self:SetFindQuantity(quantity)
	self:SetFindDistance(distance)
	self:SetFindTargetAngle(angle or Angle(0,0,0))
	self:SetFindResType(res)
	
	-- triger wire outputs
	self:TriggerWireOutputs()
	-- consume energy
	self:ConsumeResource("energy",self.Energy_Increment)
end

--# Do what we gotta do.
function ENT:Think()
	self.BaseClass.Think(self)
	
	if self:GetActive() then self:Scan() end
	
	self:NextThink( CurTime() + 0.1 )
	return true
end

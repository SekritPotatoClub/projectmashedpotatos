AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_env_base"

ENT.PrintName	= "EnvironmentsX Npc"
ENT.Author		= "Ludsoe"
ENT.Purpose		= "A Basic NPC for Envx"
ENT.Instructions	= ""

ENT.Spawnable		= true
ENT.AdminOnly	= true
ENT.LDEC =1
ENT.Category = "EnvX"
ENT.NoEnvPanel = true
ENT.NoGrav = true
ENT.IsEnvxNPC = true

if SERVER then
	function ENT:Initialize()
		self:PhysicsInit(SOLID_VPHYSICS)
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetSolid(SOLID_VPHYSICS)
		local phys = self:GetPhysicsObject()
		if IsValid(phys) then
			phys:Wake()
			phys:EnableMotion(true)
			phys:EnableGravity(false)
		end
		self.CoreLinked={self}
		self.Data={}
		self.Memory={Bullets={},Trails={},Enemys={}}
		self.Weapons={}
		self.LDE={Core=self}
		self.LastShot=CurTime()
		self.DebugBeam=0
		self.HasTarget = false
		self.IsInCombat = false
	end
	
	function ENT:InitNPCData(Dat)
		self.Data = Dat
		self.LDE={
			Core=self,
			CoreMaxShield=Dat.Shields,
			CoreShield=Dat.Shields,
			CoreMaxHealth=Dat.Hull,
			CoreHealth=Dat.Hull
		}
	end
	
	function ENT:NewWeaponData()
		return {
			WepName = "Gun",
			Type = "Bullet",
			FireAngle = 15,AngElevOffset=0,AngBearOffset=0,
			Position=Vector(0,0,0),
			Range=4000,Speed=300,
			Damage = 100,Count=1,
			FireRate = 1,Spread=4,
			LastShot=0,
			FireSound="npc/turret_floor/shoot1.wav",
			TrailColor=Color(255,90,30),TrailLifeTime=0.8,TrailStartW=30,TrailTexture="trails/laser.vmt"
		}
	end
	
	function ENT:AddWeapon(Data)
		table.insert(self.Weapons,table.Merge(self:NewWeaponData(),Data))
	end
	
	function ENT:AddWeapons(Data)
		for I,Weapon in pairs(Data) do
			self:AddWeapon(Weapon)
		end
	end
	
	function ENT:AddTrail(Data)
		local DE = ents.Create("dummyent")
		DE:SetPos(self:LocalToWorld(Data.Offset or Vector(0,0,0)))
		DE:Spawn()
		DE:SetParent(self)

		local startWidth,endWidth = Data.StartWidth or 32,Data.EndWidth or 0
		util.SpriteTrail( DE, 0, Data.Color or Color(255,255,255), false, startWidth, endWidth, Data.LifeTime or 5, 1 / ( startWidth + endWidth ) * 0.5, Data.Material or "trails/laser.vmt" )
		
		table.insert(self.Memory.Trails,DE)
	end

	function ENT:OnRemove()
		if table.Count(self.Memory.Trails)>0 then
			for I,Trail in pairs(self.Memory.Trails) do
				if IsValid(Trail) then
					Trail:Remove()
				end
			end
		end
	end
	
	function ENT:SpawnFunction( ply, tr )
		local Data = {Speed=50,TurnRate=0.3,Hull=20000,Shields=10000,Model="models/af/aff/usn/ragnarok_tb.mdl"}

		local ent = ents.Create("envx_npc_ship")
		ent:SetPos( tr.HitPos + Vector(0, 0, 560))
		ent:SetModel( Data.Model )
		ent:Spawn()
		ent:InitNPCData(Data)
		
		--ent:SetModelScale( Data.Scale, 0 ) ent:Activate()

		ent:AddTrail({Offset=Vector(-2861,70,-320),StartWidth=800,LifeTime=15,Color=Color(255,190,0)})
		ent:AddTrail({Offset=Vector(-2861,-70,-320),StartWidth=800,LifeTime=15,Color=Color(255,190,0)})
		ent:AddTrail({Offset=Vector(-2861,-70,-160),StartWidth=500,LifeTime=15,Color=Color(255,190,0)})
		ent:AddTrail({Offset=Vector(-2861,70,-160),StartWidth=500,LifeTime=15,Color=Color(255,190,0)})
		ent:AddTrail({Offset=Vector(-2861,-70,-440),StartWidth=500,LifeTime=15,Color=Color(255,190,0)})
		ent:AddTrail({Offset=Vector(-2861,70,-440),StartWidth=500,LifeTime=15,Color=Color(255,190,0)})
		
		return ent
	end
	
	function ENT:VantagePoint(TargetPos)
		if(self.VPoint)then return self.VPoint end
		local Trys = 50
		while Trys > 0 do --Only try so many times in one go.
			local r = 2500+math.random(1000,1500)
			local Point = TargetPos+Vector(math.random(-r,r),math.random(-r,r),math.random(-r,r))
			local Distance = Point:Distance(TargetPos)
			--if ( Distance > 3000) then
				self.VPoint = Point
				return Point
			--end
		end
		
		if TargetPos.Z> self:GetPos().Z then
			return TargetPos+Vector(0,0,-2000)--We couldnt get a vantage point, lets come from below
		else
			return TargetPos+Vector(0,0,2000)--We couldnt get a vantage point, lets come from above
		end	
	end

	function ENT:FireGuns(Target)	
		if not self.Weapons or table.Count(self.Weapons)<1 then return end
				
		for I,Weapon in pairs(self.Weapons) do
			if(Weapon.LastShot+Weapon.FireRate<CurTime())then
				pos = self:WorldToLocal(Target)
				local rad2deg = 180 / math.pi		
				
				elevation=math.abs(self:GetElevation(rad2deg,pos))
				bearing = math.abs(self:GetBearing(rad2deg,pos))
				--print("Firing gun: "..Weapon.WepName.." E: "..elevation.." B: "..bearing)
				
				if(elevation-Weapon.AngElevOffset<Weapon.FireAngle and bearing-Weapon.AngBearOffset<Weapon.FireAngle)then
					--print("PEW")
					self.AIFunctions.Weapons[Weapon.Type](self,Weapon,Target)
					Weapon.LastShot=CurTime()
				end
			end
		end
	end
	
	--function ENT:GetObjective() return false end
	function ENT:CheckHostile(ent) return false end
	
	function ENT:FindPOI()
		--print("Finding POI!")
		local planet = environments[math.random(1,#environments)]
		if IsValid(planet) and self:HasLineOfSight(planet:GetPos()+Vector(0,0,planet.radius)) then
			--print("Found one!")
			if (planet.EnvZone or 0)<1 then
				return planet:GetPos()+Vector(0,0,planet.radius)
			end
			
		else
			if IsValid(self.environment) then
				--print("Inside a planet!")
				return self.environment:GetPos()+Vector(0,0,self.environment.radius)
			end
		end
		
		--print("Didnt find one...")
	end
	
	local Directions = {Vector(0,0,1),Vector(0,0,-1),Vector(1,0,0),Vector(-1,0,0),Vector(0,1,0),Vector(0,-1,0)}
	function ENT:FindOpenRoute(Pos)
		for I,dir in pairs(Directions) do
			local Check = Pos+(dir*Vector(1000,1000,1000))
			--print(tostring(Check))
			if self:HasLineOfSight(Check) then
				--print("New Path found!")
				return Check
			end
		end
	end
	
	--Todo add collision detection and avoidance code.
	function ENT:Approach(Pos,Dist)
		if not Pos then print("Nil!") self:Remove() return end
		local LOS,Res = self:HasLineOfSight(Pos)
		local FlyTo = Pos
		if LOS==false then
			FlyTo=self:FindOpenRoute(Res.HitPos)
		end
		
		self.TargetPos = FlyTo
		self.AIFunctions.Flight.FlyTowards(self,FlyTo)
		
		local Distance = Dist>self:GetPos():Distance(Pos)
		return Distance
	end
	
	function ENT:SlowDown()		
		local Phys=self:GetPhysicsObject()
		if IsValid(Phys) then
			Phys:SetVelocity(Phys:GetVelocity() * .5)
			Phys:AddAngleVelocity(-Phys:GetAngleVelocity()*Vector(0.3,0.6,0.6))
		end
	end
	
	--Default Ai Programming, Randomly wander planets.--
	--and retreat to station when attacked...
	
	function ENT:CombatRoutines()
		if self:Approach(LDE.NPCMain.StationFront(),1000) then
			self.IsInCombat = false
		end
	end
	
	function ENT:AiThink()
		if self.IsInCombat==true then self:CombatRoutines() return end
		if self.GetObjective and self:GetObjective() then return end
		
		if self.HasTarget==false then	
			local TargetPos = self:FindPOI()
			if TargetPos then
				self.TargetPos = TargetPos
				self.HasTarget = true
			end
			
			self:SlowDown()
		else
			if self:Approach(self.TargetPos,1000) then
				self.HasTarget = false
			end
		end
	end
	
	----------------------------------------------------------
	
	function ENT:AIDamageCheck(amount,attacker,inflictor)
		return false
	end
	
	--Make spores take over npc ships and use them to spread.
	function ENT:OnLDEDamage(amount,attacker,inflictor)
		if self:AIDamageCheck(amount,attacker,inflictor) == false then
			self.IsInCombat = true
			self.HasTarget = false
			
			if attacker then
				--print("Attacker ent: "..tostring(attacker))
				if not attacker:IsPlayer() then attacker=LDE.GetPropOwner(attacker) end
				if attacker and attacker:IsPlayer() then
					attacker:GiveLDEStat("Bounty",math.Clamp(math.Round(amount/10),1,1000))
					attacker:GiveLDEStat("Cash",math.Clamp(math.Round(amount/10),1,1000))
				end
			end
		end
	end
	
	function ENT:GetBearing(rad2deg,pos)
		return rad2deg*-math.atan2(pos.y, pos.x)
	end

	function ENT:GetElevation(rad2deg,pos)
		local len = pos:Length()
		if len < delta then return 0 end	
		return rad2deg*math.asin(pos.z / len)
	end

	function ENT:GetAngAim(Targ)
		if not self.TargetPos and not Targ then return end
		local Ang = self:GetAngles()
		
		pos = self:WorldToLocal(self.TargetPos)
		if Targ then pos = self:WorldToLocal(Targ-self:GetVelocity()) end
		local rad2deg = 180 / math.pi

		elevation=self:GetElevation(rad2deg,pos)
		bearing = self:GetBearing(rad2deg,pos)
		local phys = self:GetPhysicsObject()
		if not IsValid(phys) or not phys.GetAngleVelocity then return end
		return Vector(Ang.Roll,elevation,bearing)+phys:GetAngleVelocity()
	end
	
	function ENT:TraceDebug(Pos)
		local effectdata = EffectData()
		effectdata:SetOrigin(Pos)
		effectdata:SetStart(self:GetPos())
		effectdata:SetEntity( self )
		util.Effect( "lde_beameffect", effectdata )
	end
	
	function ENT:HasLineOfSight(Pos,Pos2)
		local tr = {} --self:TraceDebug(Pos)
		
		self.Connected = constraint.ShipCoreDetect(self)
		
		tr.start = Pos2 or self:GetPos()
		tr.endpos = Pos
		tr.filter = function(check) return (IsValid(self.Connected[check])==false) end
		
		local res = util.TraceLine( tr )
		
		return (not res.Hit and not res.HitWater and not res.HitSky and not res.HitWorld and not res.HitNoDraw),res
	end
	
	function ENT:ChangeTemp(Amount) return end
	function ENT:ShieldFluxCheck(ent,damage) return true end 
	
	function ENT:Think()
		if self.IsDead then return end
		
		self:AiThink()
		
		self:NextThink(CurTime()+0.2)
	end
	
	function ENT:CanTool()
		return false
	end

	function ENT:GravGunPunt()
		return false
	end

	function ENT:GravGunPickupAllowed()
		return false
	end
else
	function ENT:Draw( DrawModel )			
		if DrawModel then self:DrawModel() end	
		if Wire_Render then
			Wire_Render(self)
		end
	end
end

if SERVER then
	ENT.AIFunctions = {Flight={},Weapons={}}
	---------Flight Functions------

	local FLT = ENT.AIFunctions.Flight

	function FLT.PointAboveTarget(self,TargetPos,Data)
		if(self.VPoint)then return self.VPoint end
		local Trys = 20
		while Trys > 0 do --Only try so many times in one go.
			Trys=Trys-1
			local Min = Data.Min or 1000
			local Max = Data.Max or 1500
			local Rad = Data.Rad or 2500
			local Hgt = Data.Hgt or 3000
			local r = Rad+math.random(Min,Max)
			local x,y = math.random(-r,r),math.random(-r,r)
			local Point = TargetPos+Vector(x,y,math.random(-Hgt,Hgt))
			local Distance = Point:Distance(TargetPos)
			if Distance > (Data.MinDist or 2000) then
				if self:HasLineOfSight(Point) then
					local LOS,Res = self:HasLineOfSight(TargetPos,Point)
					if self:CheckHostile(Res.Entity) then
						self.VPoint = Point
						return Point
					end
				end
			end
		end
				
		return TargetPos+Vector(0,0,2000)--We couldnt get a vantage point, lets come from above
	end
	
	function FLT.ApplyMoveForce(self,For)
		local Phys=self:GetPhysicsObject()
		if IsValid(Phys) then
			Phys:SetVelocity(Phys:GetVelocity() * .5)
			Phys:ApplyForceCenter((For * self.Data.Speed)* Phys:GetMass())
		end
	end
	
	function FLT.ApplyTurnForce(self,Ang)
		local Phys=self:GetPhysicsObject()
		if IsValid(Phys) then	
			local TurnRate = self.Data.TurnRate
			Phys:AddAngleVelocity(Ang*Vector(TurnRate,TurnRate,TurnRate))
		end
	end		
	
	function FLT.HandleMovement(self,For,Ang)
		if not IsValid(self) then return end
		FLT.ApplyMoveForce(self,For)
		FLT.ApplyTurnForce(self,Ang)
	end
	
	function FLT.FlyTowards(self,TargetPos)
		if TargetPos then
			FLT.HandleMovement(self,self:GetForward(),-self:GetAngAim(TargetPos))
		end	
	end

	function FLT.DiveRoutine(self,TargetPos,Data)	
		local Distance = self:GetPos():Distance(TargetPos)
		local Data = Data or {}
		
		if Distance>(Data.Max or 3000) then--We gotta get closer...
			self.TargetPos=TargetPos
		elseif Distance<(Data.Min or 1800) then--Fall Back for another run!!!
			self.TargetPos=self:VantagePoint(TargetPos)
		end
		
		if(self.TargetPos)then
			FLT.HandleMovement(self,self:GetForward(),-self:GetAngAim())
		end
		
		if Distance<(Data.Max or 3000)*1.5 and self.TargetPos then
			self:FireGuns(TargetPos)
		end	
	end

	function FLT.HoverRoutine(self,TargetPos)
		local Distance = self:GetPos():Distance(TargetPos)

		if Distance>4000 then--We gotta get closer...
			self.TargetPos=TargetPos
			self.VPoint=nil
		elseif Distance<4000 then--Lets get into position to start gunning!
			self.TargetPos=FLT.PointAboveTarget(self,TargetPos,{Min=-1000,Max=1000,Rad=1500,Hgt=1500,MinDist=2000})
			
			--self:TraceDebug(TargetPos)
			local LOS,Res = self:HasLineOfSight(TargetPos)
			
			if self:CheckHostile(Res.Entity) then
				self:FireGuns(TargetPos)
			end
			
			if self.VPoint then
				local VDist = self:GetPos():Distance(self.VPoint)
				if(VDist<100)then
					self.Hover=self.Hover or 10
					if(self.Hover<=1)then
						self.VPoint=nil
						self.Hover=10
					else
						self.Hover=self.Hover-1
					end
				end
			end
		end
			
		if self.TargetPos then
			local Phys=self:GetPhysicsObject()
			if Phys:IsValid() then
				
				if self.VPoint then
					local Direction =self.VPoint-self:GetPos()
					Direction:Normalize()
					local VDist = self:GetPos():Distance(self.VPoint)
					local SpeedMult=0.8
					local Drag = 0.5
					if(VDist<100)then SpeedMult=0.2 Drag=0.8 end 
					Phys:SetVelocity(Phys:GetVelocity() * Drag)
					Phys:ApplyForceCenter(( Direction * (self.Data.Speed)*SpeedMult) * Phys:GetMass())
					Phys:AddAngleVelocity(-self:GetAngAim(TargetPos)*Vector(1,1,1))
				else
					Phys:SetVelocity(Phys:GetVelocity() * .5)
					Phys:ApplyForceCenter((self:GetForward() * self.Data.Speed) * Phys:GetMass())
					Phys:AddAngleVelocity(-self:GetAngAim()*Vector(0.3,0.3,0.3))
				end
			end
		end
	end
	-------------------------------
	--Weapon Functions--
	local WLT = ENT.AIFunctions.Weapons
	
	WLT["Bullet"]=function(self,Data,Target)

		local MyPos = self:GetPos()
		local Dir=(Target-MyPos)
		
		local Bullet = {}
		Bullet.Count = Data.Count or 1
		Bullet.ShootPos = self:LocalToWorld(Vector(Data.Position))
		Bullet.Direction = Dir
		Bullet.Spread = Data.Spread or 4
		Bullet.Attacker = self
		Bullet.ProjSpeed = Data.Speed or 150
		Bullet.Drop=0
		Bullet.HomingSpeed = Data.HomingSpeed or 10
		Bullet.Model = Data.Model or "models/Items/AR2_Grenade.mdl"
		Bullet.TrailColor = Data.TrailColor or Color(255,255,120)
		Bullet.TrailLifeTime = Data.TrailLength or 0.4
		Bullet.TrailStartW = Data.TrailStartW or 10
		Bullet.TrailTexture = Data.TrailTexture or "trails/laser.vmt"
		Bullet.Ignore = self
		Bullet.Data=Data
		Bullet.Inflictor = self
		Bullet.OnHit = function (tr, Bullet)
			if(Bullet.Data.BullHit)then
				Bullet.Data.BullHit(tr,Bullet)
			else
				LDE:DealDamage(tr.Entity,Bullet.Data.Damage,Bullet.Attacker,Bullet.Inflictor)
			end
		end
		--self:FireBullets(Bullet)
		if not self.Memory.Bullets[Data.WepName] then self.Memory.Bullets[Data.WepName] = {} end
		table.insert(self.Memory.Bullets[Data.WepName],LDE:FireProjectile(Bullet))--Add support for counts higher then 1
		self:EmitSound(Data.FireSound or "npc/turret_floor/shoot1.wav")
	end
	
	WLT["Laser"]=function(self,Data,Target)
		local Bullet = {
			Damage=Data.Damage or 100,
			heat=Data.Heat or 1,
			NoHeat=true,
			Effect=Data.Effect or {Beam="LDE_laserbeam",Hit="LDE_laserhiteffect"},
			FireSound=Data.FireSound or "weapons/bison_main_shot_01.wav",
			--ShootPos = self:LocalToWorld(Vector(Data.Position)),
		}
		self.TraceTarg = Target
		LDE.Weapons.FireLaser(self,Bullet)
	end
end





















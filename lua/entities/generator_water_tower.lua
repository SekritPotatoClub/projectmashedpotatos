AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_env_base"
ENT.PrintName 	= "Atmospheric Water Generator"
ENT.AutomaticFrameAdvance = true  

ENT.OverlayText = {HasOOO = true,resnames = {"nitrogen"}, genresnames={"water"}}

local Water_Increment = 10

if(SERVER)then

    function ENT:Extract_Energy()
        local inc = 0
        if self.environment then
            local planet = self.environment:IsOnPlanet()
            if planet and planet:GetAtmosphere() > 0.2 then
                inc = math.random(1, (4 * planet:GetAtmosphere()))
            end
        else
            inc = 1
        end
        if (inc > 0) then
            local einc = math.floor(inc * Water_Increment)
            local nitro = self:GetResourceAmount("nitrogen")
            if(nitro>=(einc*2))then return end
            einc = math.ceil(einc * self:GetSizeMultiplier())
            
            self:ConsumeResource("nitrogen", einc*2)
            self:SupplyResource("water", einc)
            if WireAddon then Wire_TriggerOutput(self, "Out", einc) end
        else
            if WireAddon then Wire_TriggerOutput(self, "Out", 0) end
        end
    end
    
    function ENT:GenEnergy()
        local waterlevel = self:WaterLevel() or 0
        if (waterlevel > 1) then
            self:TurnOff()
        --	self:Destruct()
        else
            self:Extract_Energy()
        end
    end
    
    function ENT:Think()
        self.BaseClass.Think(self)	
        if self.environment then		
            local planet = self.environment:IsOnPlanet()
            if (planet and planet:GetAtmosphere() > 0) then
        --		self:TurnOn()
            else
                self:TurnOff()
            end
        end
        
        if (self.Active == 1) then
            self:GenEnergy()
        end
        self:NextThink(CurTime() + 1)
        return true
    end
    
else
    language.Add("generator_water_tower", "Atmospheric Water Generator")
end
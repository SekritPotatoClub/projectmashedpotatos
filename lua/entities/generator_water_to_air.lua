AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_env_base"
ENT.PrintName 	= "Electrolysis Reactor"

ENT.OverlayText = {HasOOO = true ,resnames = {"energy","water"}, genresnames={"oxygen","hydrogen"}}

if(SERVER)then
    local Energy_Use = 50
    local Water_Use = 60
    local O2_Gen = 20
    local Hydro_Gen = 40

    function ENT:Initialize()
        self.BaseClass.Initialize(self)
        self.Active = 0
        self.overdrive = 0
        self.damaged = 0
        self.lastused = 0
        self.Mute = 0
        if WireAddon then
            self.WireDebugName = self.PrintName
            self.Inputs = Wire_CreateInputs(self, { "On", "Overdrive", "Mute", "Multiplier" })
        else
            self.Inputs = {{Name="On"},{Name="Overdrive"}}
        end
    end

    function ENT:OnTurnOn()
		self:PlayDeviceSound( "Start","buttons/button1.wav" )
    end

    function ENT:OnTurnOff()
		self:PlayDeviceSound( "Stop","buttons/button4.wav" )
		self:StopDeviceSound( "Start" )
    end

    function ENT:TurnOnOverdrive()
        if ( self.Active == 1 ) then
            self:SetOOO(2)
            self.overdrive = 1
        end
    end
    
    function ENT:TurnOffOverdrive()
        if ( self.Active == 1 and self.overdrive == 1) then
            self:SetOOO(1)
            self.overdrive = 0
        end	
    end

    function ENT:TriggerInput(iname, value)
        if (iname == "On") then
            self:SetActive(value)
        elseif (iname == "Overdrive") then
            if (value ~= 0) then
                self:TurnOnOverdrive()
            else
                self:TurnOffOverdrive()
            end
        end
        if (iname == "Mute") then
            if (value > 0) then
                self:SetDeviceMute(1)
            else
                self:SetDeviceMute(0)
            end
        end
        if (iname == "Multiplier") then
            self:SetMultiplier(value)
        end
    end
        
    function ENT:Proc_Water()
        local energy = self:GetResourceAmount("energy")
        local water = self:GetResourceAmount("water")

        local total_multiplier = self:GetSizeMultiplier()*self:GetMultiplier()

        local einc = Energy_Use + (self.overdrive*Energy_Use)
        einc = math.Round(einc * total_multiplier)
        local winc = Water_Use + (self.overdrive*Water_Use)
        winc = math.Round(winc * total_multiplier)

        if (energy >= einc and water >= winc) then
            self:ConsumeResource("energy", einc)
            self:ConsumeResource("water", winc)
                        
            self:SupplyResource("oxygen", O2_Gen*total_multiplier)
            self:SupplyResource("hydrogen",Hydro_Gen*total_multiplier)
            
            --Was 5
            LDE.HeatSim.ApplyHeat(self,0.5 * self:GetSizeMultiplier())
        else
            self:TurnOff()
        end
    end

    function ENT:Think()
        self.BaseClass.Think(self)
        
        if ( self.Active == 1 ) then self:Proc_Water() end
        
        self:NextThink( CurTime() + 1 )
        return true
    end

else

end
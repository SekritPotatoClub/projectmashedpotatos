AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_env_base"
ENT.PrintName 	= "Solar Panel"

ENT.NoEnvPanel = true
ENT.OverlayText = {HasOOO = true, genresnames={"energy"}}

ENT.SolarPer = 0
ENT.EnvxNetworked = true
ENT.OffUpdate = false

local Energy_Increment = 150


if(SERVER)then

    function ENT:Initialize()
        self.BaseClass.Initialize(self)
        self.damaged = 0
        if WireAddon then
            self.WireDebugName = self.PrintName
            self.Outputs = Wire_CreateOutputs(self, { "Out", "Efficiency" })
        end
    end
    
    function ENT:Extract_Energy(mul)
        mul = mul or 0
        
        self:SetNetworkedInt( 1, mul or 0 )
        
        if mul == 0 then return	end
        local inc = 0
    
        if not self.environment then return end
		local atmo = self.environment:GetAtmosphere()
		
		self.SolarPer = math.Round(((mul*100)/(atmo + 1))/(self.damaged + 1))
        inc = math.ceil(Energy_Increment / (atmo + 1))
    
        if (self.damaged == 1) then inc = math.ceil(inc / 2) end
        if (inc > 0) then
            inc = math.Round(inc * self:GetSizeMultiplier() * mul)
            self:SupplyResource("energy", inc)
        end
        if not (WireAddon == nil) then
			Wire_TriggerOutput(self, "Out", inc)
			Wire_TriggerOutput(self, "Efficiency", self.SolarPer)
		end
    end
    
	local pi = math.pi
	local rad2deg = 180 / pi
	local atan2 = math.atan2
	local asin = math.asin
	function ENT:SolarHeading(sunpos,panelangle)
		panelangle = panelangle or Angle(90,0,0)
		if type(panelangle) ~= "Angle" or type(sunpos) ~= "Vector" then return end
		
		--local angles = self:GetAngles():Rotate(panelangle)
		local angles = self:GetAngles()
		angles:RotateAroundAxis(self:GetRight(),panelangle.p)
		angles:RotateAroundAxis(self:GetUp(),panelangle.y) --+panelangle
		local pos = WorldToLocal(sunpos,Angle(0,0,0),self:GetPos(),angles)

		local bearing = rad2deg*-atan2(pos.y, pos.x)
		local len = pos:Length()
		if len < 0.0000001000000 then
			return Angle(0, bearing, 0)
		else
			return Angle(rad2deg*asin(pos.z / len), bearing, 0)
		end
	end
    
    function ENT:GenEnergy()
        local waterlevel = self:WaterLevel() or 0
		
		--Underwater, so no power
        if waterlevel > 1 then
            self:TurnOff()
		--No map suns or inside an atmosphere so default to up direction
		elseif not TrueSun or table.Count(TrueSun) == 0 or (IsValid(self.environment) and self.environment.name ~= "space") then
			local entpos = self:GetPos()
			local sunpos = entpos + Vector(0,0,4096)
			local trace = util.QuickTrace(sunpos, entpos-sunpos, nil)
			if trace.Hit then
				if trace.Entity == self then
					local heading = self:SolarHeading(sunpos)
					heading.p = math.abs(heading.p)
					heading.y = math.abs(heading.y)
					local output = math.min( (90-heading.p)/90,(90-heading.y)/90 )
					if output < 0 then output = 0 end
					if self.OffUpdate then self.OffUpdate=false end
					self:TurnOn()
                    self:Extract_Energy(output)
				end
			else
				--Janky Workaround to get the panel to update to 0 before turning off
				if not self.OffUpdate then
					self.OffUpdate=true
					self:TurnOn()
                    self:Extract_Energy(0.0001)
				else
					self:TurnOff()
				end
			end
		--In space and the map has one or more suns
        else
            local entpos = self:GetPos()
            local trace = {}
            --local lit = false
            --local SunAngle2 = SunAngle or Vector(0, 0 ,1)
            --local SunAngle = nil
            if TrueSun and table.Count(TrueSun) > 0 then
                local output = 0
				local n = 0
                for k, sunpos in pairs(TrueSun) do
				
					trace = util.QuickTrace(sunpos, entpos-sunpos, nil)
					if trace.Hit then
						if trace.Entity == self then
							local heading = self:SolarHeading(sunpos)
							heading.p = math.abs(heading.p)
							heading.y = math.abs(heading.y)
							n = ((90-heading.p)/90 + (90-heading.y)/90)*0.5
							if n < 0 then n = 0 end
							output = output + n
						end
					end
									
                    --[[SunAngle = (entpos - v)
                    SunAngle:Normalize()
                    local startpos = (entpos - (SunAngle * 4096))
                    trace.start = startpos
                    trace.endpos = entpos //+ Vector(0,0,30)
                    local tr = util.TraceLine( trace )
                    if (tr.Hit) then
                        if (tr.Entity == self) then
                            self:TurnOn()
                            self:Extract_Energy()
                            return
                        end
                    else
                        self:TurnOn()
                        self:Extract_Energy()
                        return
                    end]]
					--[[
                    trace = util.QuickTrace(SUN_POS, entpos-SUN_POS, nil)
                    if trace.Hit then 
                        if trace.Entity == self then
                            local v = self:GetUp() + trace.HitNormal
                            local n = v.x*v.y*v.z
                            if n > 0 then
                                output = output + n
                                --solar panel produces energy
                            end
                        else
                            local n = math.Clamp(1-SUN_POS:Distance(trace.HitPos)/SUN_POS:Distance(entpos),0,1)
                            output = output + n
                            --solar panel is being blocked
                        end
                    end
                    if output >= 1 then
                        break
                    end]]
                end
                --[[self.SolarPer = output
                if output > 1 then 
                    output = 1
                end
                if output > 0 then
                    self:TurnOn()
                    self:Extract_Energy(output)
                    return
                end]]
				if output > 0 then
					if self.OffUpdate then self.OffUpdate=false end
					self:TurnOn()
                    self:Extract_Energy(output)
				else
					--Janky Workaround to get the panel to update to 0 before turning off
					if not self.OffUpdate then
						self.OffUpdate=true
						self:TurnOn()
						self:Extract_Energy(0.0001)
					else
						self:TurnOff()
					end
				end
            end
			
            --local SUN_POS = (entpos - (SunAngle2 * 4096))
            --[[trace.start = startpos
            trace.endpos = entpos //+ Vector(0,0,30)
            local tr = util.TraceLine( trace )
            if (tr.Hit) then
                if (tr.Entity == self) then
                    self:TurnOn()
                    self:Extract_Energy(1)
                    return
                end
            else
                self:TurnOn()
                self:Extract_Energy()
                return
            end]]
            --[[trace = util.QuickTrace(SUN_POS, entpos-SUN_POS, nil)
            if trace.Hit then 
                if trace.Entity == self then
                    local v = self:GetUp() + trace.HitNormal
                    local n = v.x*v.y*v.z
                    if n > 0 then
                        self:TurnOn()
                        self:Extract_Energy(n)
                        self.SolarPer = n
                        return
                    end
                else
                    local n = math.Clamp(1-SUN_POS:Distance(trace.HitPos)/SUN_POS:Distance(entpos),0,1)
                    if n > 0 then
                        self:TurnOn()
                        self:Extract_Energy(n)
                        self.SolarPer = n
                        return
                    end
                    --solar panel is being blocked
                end
            end
            self:TurnOff() //No Sunbeams in sight so turn Off
			]]
        end
    end

	function ENT:NetworkData()
		local data = {}
	
		data.SolarPer = self.SolarPer
		data.Damaged = self.damaged
		data.Atmosphere = self.environment:GetAtmosphere()

        --print("Networking! "..tostring(data.SolarPer))

		return data
    end
    
    function ENT:Think()
        self.BaseClass.Think(self)
        self:GenEnergy()
        self:FlagForNetSync()
        self:NextThink(CurTime() + 1)
        return true
    end    
else
    language.Add("generator_energy_solar", "Solar Panel")

    function ENT:NetworkDataRecieve(Data)
		self.SolarPer = Data.SolarPer
		self.Damaged = Data.damaged
		self.Atmosphere = Data.Atmosphere
	end

    function ENT:GetStatusInfo(Info) --Entities overwrite this to show stuff above resources.
	
		local status = "Effiency: "..tostring(self.SolarPer).."%"
		if self.Damaged == 1 then status = status .. "\nDamage Reducing Efficiency" end
		if self.Atmosphere ~= 0 then status = status .. "\nAtmosphere Reducing Efficiency" end
    
        table.insert(Info,{Type="Label",Value=status})
        
        return true
    end 
end
AddCSLuaFile()

ENT.Type 			= "anim"
ENT.Base 			= "base_env_base"
ENT.PrintName		= "Effect Test"
ENT.Author			= "Ludsoe"
ENT.Category		= "Other"

ENT.Spawnable		= true
ENT.AdminOnly		= true

if(SERVER)then

	function ENT:Initialize()
		self:SetModel("models/slyfo_2/gunball.mdl")
		
		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		
		local effectdata = EffectData()
		effectdata:SetOrigin(self:GetPos())
		effectdata:SetEntity(self)
		util.Effect( "titan_jumpin", effectdata )
	end

	function ENT:GravGunPunt()
		return false
	end

	function ENT:GravGunPickupAllowed()
		return false
	end
else
	ENT.RenderGroup = RENDERGROUP_OPAQUE
end		

--[[Mechanos -CD-: Template = empty sent that can spawn, but does nothing.  Has wire inputs/outputs and any dupe code not included from base ent
Mechanos -CD-: And a standard empty think and stuffs
Mechanos -CD-: And functions that are used by practically all envx sents
Mechanos -CD-: Like the activate function
Mechanos -CD-: Or functions that access resource usage or spawning projectiles
Mechanos -CD-: Also including the table that sets up the pop up hud thingy, with a description of it's options
Mechanos -CD-: I've copy pasta'ed it from other ents and know it creates the readout, but dunno what all it can do
Mechanos -CD-: And..  that's pretty much it
Ludsoe: i can do that
Ludsoe: might take me bit to hunt down all the info
Mechanos -CD-: The important thing is that it has all the needed stuff, and examples of useful envX functions (commented out as needed)
Ludsoe: maybe ill make it a ongoing project and update it as we find stuff in envx/make it
Mechanos -CD-: The fact that it has all that, but doesn't have other code in the way from an existing sent, that I have no idea what does wha
Mechanos -CD-: *what, will help immensely
Mechanos -CD-: That would be a good idea
Mechanos -CD-: can make a sent named envx_template
Mechanos -CD-: And in comments, include all the tasty functions and documentation regarding them
]]

AddCSLuaFile()--Makes this lua automagically Sync to client.

ENT.Type 			= "anim" --Im not too sure on this one yet
ENT.Base 			= "base_env_base" --Baseclass you want, right now its using envx base.
ENT.PrintName		= "Template" --What the entity displays as inside the spawner tab
ENT.Author			= "Ludsoe" --Sets the author of the entity... I suggest putting your name here
ENT.Category		= "EnvX" --Sets which category the entity is listed in, inside entities tab

ENT.Spawnable		= false --Should the entity be spawnable at all?
ENT.AdminOnly		= true --If its spawnable... is it admin only?

--ENT.AutomaticFrameAdvance = true --If the entity is animated, automatically progress the animation?

--[[
--This is used to define the information displayed on the hover over gui when you look at this entity.

local GeneratedResources = {} --A table of resources that the device shows what it generates.
local RequiredResources = {"energy"} --A table full of the resources the device shows as required.
local HasDifferntStatus = false --If the entity should display "Status:" in its hover over gui.
ENT.OverlayText = {HasOOO = HasDifferntStatus, resnames = RequiredResources, genresnames = GeneratedResources }

]]

if(SERVER)then
	function ENT:Initialize() 
		self.BaseClass.Initialize(self) --Make sure you call this, if your using base_env_base as your baseclass
		
		if WireLib then
			self.WireDebugName = self.PrintName--Name of entity in wire debugger? Im not too sure.
			
			--Simple Wire Input setup, assumes all are numbers.
			self.Inputs = WireLib.CreateInputs(self, { "On" })
			--Simple wire output setup, assumes all are numbers
			self.Outputs = WireLib.CreateOutputs(self, { "On" })
			
			--Advanced wire input setup, allows you to define datatypes.	--Todo add all the possible datatypes
			--self.Inputs = WireLib.CreateSpecialInputs( self, { "Number", "String", "Vector", "Angle" }, { "NUMBER","STRING","VECTOR","ANGLE" } )
		
			--Advanced wire output setup, allows you to define datatypes.	--Todo add all the possible datatypes
			--self.Outputs = WireLib.CreateSpecialOutputs( self, { "Number", "String", "Vector", "Angle" }, { "NUMBER","STRING","VECTOR","ANGLE" } )
		end
	end
	
	function ENT:OnTurnOn() end --Put anything you want to happen when the device is activated.
	function ENT:OnTurnOff() end --Put anything you want to happen when the device is deactivated.
	
	--This is called when a player presses USE(E) on the device, overwriting this disables the env USE gui.
	--function ENT:AcceptInput(name,activator,caller) end
	
	function ENT:OnRemove()	
		self.BaseClass.OnRemove(self)
		
		--Put anything here you want called when the entity is removed...
	end
	
	function ENT:Think()
		
		--Add any think code you want here...
		
		--If you plan to use custom animations, uncomment the line below
		--self:AnimateThink()	self:NextThink( CurTime() + 0.001 )
		self:NextThink( CurTime() + 1 ) --How fast do we want to think?
		return true
	end
	
	--[[
	function ENT:EnvxOnDupe()--This function is being called WHILE the entity is being duped. Use it to save any data you wish to persist via dupes.
		return {} 
	end
	
	function ENT:EnvxOnPaste(Player,Ent,CreatedEntities) --This is called when your entity is being pasted...
		local MyData = Ent.EntityMods.EnvxDupeInfo --This should be the table of data you saved with the above function...
		
	end
	]]
	
	local T = {} --Create a empty Table
	
	T.Button = function(Device,ply,Data)
		--Device = the entity that was used
		--Ply = the player that used the entity
		--Data = the data that was sent by the player
	end
	
	T.Slider = function(Device,ply,Data) end
	T.CheckBox = function(Device,ply,Data) end
	
	ENT.Panel=T --Set our panel functions to the table.
else
	local MC = EnvX.MenuCore

	function ENT:PanelFunc(entID)	
		self.DevicePanel = {
			function() return MC.CreateButton(Parent,{x=90,y=30},{x=0,y=0},"Button",function() RunConsoleCommand( "envsendpcommand",self:EntIndex(),"Button") end) end,
			function()
				local S = MC.CreateSlider(Parent,{x=150,y=30},{x=0,y=0},{Min=1,Max=100,Dec=0},"Slider",function(val) RunConsoleCommand( "envsendpcommand",self:EntIndex(),"Slider",val) end)
				--S:SetValue(INSERT_NETWORKED_VALUE_HERE)
				return S
			end,
			function()
				local S = MC.CreateCheckbox(Parent,{x=0,y=0},"CheckBox",function(val) RunConsoleCommand( "envsendpcommand",self:EntIndex(),"CheckBox",val) end)
				--S:SetChecked(INSERT_NETWORKED_VALUE_HERE)
				return S
			end
		}
	end

	--function ENT:CustomDraw() end --If you wish to draw stuff, but dont want to overwrite the baseclass draw function.

	--[[ Envx Hover over UI Info...
		local Info = {}
		
		--Inserting this into the Info table adds an label line with no extra features.
		table.insert(Info,{Type="Label",Value="YOUR_INFO_HERE"}) 
		
		--Inserting this into the Info Table adds an percentage line which displays both a label longside a percentage bar.
		table.insert(Info,{Type="Percentage",Text="YOUR_INFO_HERE",Value=0.5}) --Percentage is scaled from 0 to 1
	]]
	
	function ENT:GetPreResInfo(Info) return false end --This renders under status info, but before resource counts
	function ENT:ExtraData(Info) return false end -- This Appears after the resource counts, but before player ownership.
	
end	

--[[ Down here Ill list a bunch of functions collected from all the entities, and some information about them.

Key: --Not saying you dont know these but, incase its a bit fuzzy.
Number			=	1111
Bool 			= 	true/false
Vector 			= 	Vector(0,0,0)
Angle 			= 	Angle(0,0,0)
Table 			= 	{}
String 			=	"String"
Entity			=	self
Functions		=	function() end
None			= 	nil

]]

--[[
------------------------------------------------
--------Resource related functions.---------
------------------------------------------------

------- self:ConsumeResource(ResourceName,Amount) = AmountConsumed -------
Info: Consumes the inputed amount of the inputed resource, and returns how much was actually consumed.
Inputs: String,Number		Outputs: Number

------- self:SupplyResource(ResourceName,Amount) = AmountWasted -------
Info: Generates the inputed amount of the inputed resource, and returns how much was wasted.
Inputs: String,Number		Outputs: Number

------- self:AddResource(ResourceName,StorageAmount) -------
Info: Adds a resource storage to the entity for the inputed resource.
Inputs: String,Number				Outputs: None

------- self:GetResourceAmount(ResourceName) = Amount -------
Info: Returns the amount of the inputed resource, in the currently linked network
Inputs: String				Outputs: Number

------- self:GetUnitCapacity(ResourceName) = MaxStorage -------
Info: Returns the maxmimum amount of the inputed resource, the device can store in itself.
Inputs: String				Outputs: Number

------- self:GetNetworkCapacity(ResourceName) = NetWorkCapacity -------
Info: Returns the maxmimum amount of the inputed resource the network can store.
Inputs: String				Outputs: Number

------- self:GetStorageLeft(ResourceName) = SpaceLeftInNetwork -------
Info: Returns the remaining space available in the networks storage for the inputted resource.
Inputs: String				Outputs: Number
]]


------------------------------------------------
-------[[Entity Information functions.]]--------
------------------------------------------------

-------[[ self:GetActive() = IsActive ]]-------
--Info: Returns if the device is active. 
--Inputs: None 				Outputs: Bool

-------[[ self:GetSizeMultiplier() = SizeMult ]]-------
--Info: Returns the devices size multiplier.
--Inputs: None				Outputs: Number

-------[[ self:GetMultiplier() = NormalMult ]]-------
--Info: Returns the devices multiplier, usualy set via wire or the USE menu.
--Inputs: None				Outputs: Number

-------[[ self:SetOOO(Number) ]]-------
--Info: Sets networked interger thats translated into the "Status: " message
--Inputs: Number				Outputs: None

------[[ LDE:CalcHealth( Entity ) = MaximumHealth ]]-------
--Info: Returns the maximum health a entity can have.
--Inputs: Entity				Outputs: Number

-------[[ LDE:GetHealth( Entity ) = CurrentHealth ]]-------
--Info: Returns the current health a entity has.
--Inputs: Entity				Outputs: Number




------------------------------------------------
----------[[Damage System functions.]]----------
------------------------------------------------

-------[[ LDE:BreakOff(Entity) ]]-------
--Info: Makes the entity break off, of what ever its constrainted to. (Basically it kills it.)
--Inputs: Entity				Outputs: None

-------[[ LDE:KillEnt(Entity) ]]-------
--Info: Instantly kills the entity, calling all death functions. And playing a effect/sound. Also spawns scrap if the entity is a resource cache.
--Inputs: Entity				Outputs: None

-------[[ LDE:DealAdvDamage(Entity,DamageData) ]]-------
--Info: Deals Advanced damage to a entity, useful for weapons that have specific effects for differnt entity types.
--Inputs: Entity,DamageType				Outputs: None

-------[[ LDE:DealDamage(Entity,DamageAmount,Attacker,Inflictor,IgnoresSafeZones) ]]-------
--Info: Deals damage to the entity, defining who/what is attacking and if it ignores safezones.
--Inputs: Entity,Number,Entity,Entity,Bool				Outputs: None

-------[[ LDE:RepairHealth(Entity,RepairAmount,IgnoreCore) ]]-------
--Info: Heals the entitys health by the inputed amount. If ignorecore is true it does not repair any other part of the entities shipcore.
--Inputs: Entity,Number,Bool				Outputs: None

-------[[ LDE:RechargeCoreShields(Entity,RechargeAmount) ]]-------
--Info: If the inputted entity has a shipcore, it recharges its shields by the inputted amount.
--Inputs: Entity,Number				Outputs: None

-------[[ LDE:IsInPirateZone(Entity) = IsInPirateZone ]]-------
--Info: Returns if the entity is inside a planet marked as a pirate zone.
--Inputs: Entity				Outputs: Bool

-------[[ LDE:IsInSafeZone(Entity) = IsInSafeZone ]]-------
--Info: Returns if the entity is inside a planet marked as a safe zone. (Theres no damage in safezones.)
--Inputs: Entity				Outputs: Bool

-------[[ LDE:CheckValid(Entity) = IsValidEntity ]]-------
--Info: Returns if a entity is a valid entity for other damage system functions.
--Inputs: Entity				Outputs: Bool

-------[[ LDE:IsImmune(Entity) = CanBeDamaged ]]-------
--Info: Returns if a entity is immune to being damaged.
--Inputs: Entity				Outputs: Bool


------------------------------------------------
----------[[Weapon System functions.]]----------
------------------------------------------------

--Todo List all the weapon firing functions here.

------------------------------------------------
----------[[Sound Related functions.]]----------
------------------------------------------------

-------[[ self:GetDeviceMuted() = IsMuted ]]-------
--Info: Returns if the device is muted.
--Inputs: None				Outputs: Bool

-------[[ self:SetDeviceMute(Value) ]]-------
--Info: Sets if the device is muted, or not. Input is then greater then 0 mute. Automatically stops sounds if device is being muted.
--Inputs: Number				Outputs: None

-------[[ self:PlayDeviceSound(SoundID,SoundPath) ]]-------
--Info: Plays the inputted sound, on the device. Using this function is preferable as it follows device muting and auto cleans up.
--Inputs: String,String				Outputs: None

-------[[ self:StopDeviceSound(SoundID) ]]-------
--Info: Stops the inputted sound, if its playing on the device.
--Inputs: String				Outputs: None

-------[[ self:StopSounds() ]]-------
--Info: Automatically stops ALL sounds currently playing on the device. (Only accounts for sounds started via PlayDeviceSound)
--Inputs: None				Outputs: None

------------------------------------------------
--------[[Animation Related functions.]]--------
------------------------------------------------

-------[[ self:AnimateCorePlay(AnimationName) ]]-------
--Info: Sets the currently playing animation, to the inputed animation. Requires AnimateThink to be called every tick to properly work.
--Inputs: String				Outputs: None

--[[
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~ Internal Functions ~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--You can call these, but Im unsure what will happen as a result of using them.

------- self:SetSizeMultiplier(Number) -------
Info: Sets the entitiers size multiplier
Inputs: Number				Outputs: None

------- self:example() -------
Info: Example
Inputs: None				Outputs: None

]]



















AddCSLuaFile( "shared.lua" )
AddCSLuaFile("core/base.lua")
------------------------------------------
--  Environments   --
--   CmdrMatthew   --
------------------------------------------
local Space = Space
local math = math
local util = util
local ents = ents
local table = table
local pairs = pairs
local CurTime = CurTime
local Vector = Vector
local Msg = Msg

PlayerGravity = true

local CompatibleEntities = {"func_precipitation", "env_smokestack", "func_dustcloud", "func_smokevolume"}

include("shared.lua")
include("core/base.lua")

--fixes stargate stuff
ENT.IgnoreStaff = true
ENT.IgnoreTouch = true
ENT.NotTeleportable = true
ENT.Debugging = false

function ENT:Initialize()
	self:SetModel( "models/combine_helicopter/helicopter_bomb01" ) --setup stuff
	self:SetMoveType( MOVETYPE_NONE )
	self:SetSolid( SOLID_NONE )
	self:PhysicsInitSphere(1)
	self:SetCollisionBounds(Vector(-1,-1,-1),Vector(1,1,1))
	self:SetTrigger( true )
    self:GetPhysicsObject():EnableMotion( false )
	self:DrawShadow(false)
--	self:SetAlpha(0) 
	self.gravity = 0
	self.IsEnvironment =  true
	
	self.Entities = {}
	
	local phys = self:GetPhysicsObject() --reset physics
	if phys:IsValid() then
		phys:Wake()
	end
	self:SetNotSolid( true )
	
	self:SetColor(Color(255,255,255,0)) --Make invis
end

local notouch = {}
notouch["func_door"] = 1

function ENT:StartTouch(ent)
	if not ent:GetPhysicsObject():IsValid() then return end	--only physics stuff 
	if notouch[ent:GetClass()] or ent:IsWorld() then return end --no world stuff
	
	if not self.Enabled then 
		if self.Debugging then MsgAll("Entity ", ent, " tried to enter but ", self.name, " wasn't on.\n") end
		
		return
	elseif self.Debugging then 
		MsgAll("Entity ", ent, " has started touching ", self.name, " in unusual places....\n")
	end
	
	table.insert(self.Entities,ent)
	
	ent.environment = self
	self:UpdateGravity(ent)
end

function ENT:EndTouch(ent)
	if ent:IsWorld() then return end
	
	if self.Debugging then
		MsgAll("Entity ", ent, " has stopped touching ", self.name, " in unusual places....\n")
	end
	if not IsValid(ent:GetPhysicsObject()) then return end

	self.Entities=self.Entities or {}
	
	if ent.environment == self then
		EnvX.SpaceEntity(ent)
	else
		if self.Debugging then MsgAll("...and has decided to not get spaced.\n") end
	end
end
	
function ENT:GravNDrag(ent,g,d)
	if ent:IsRagdoll() then
		for i = 0, ent:GetPhysicsObjectCount() do	
			local phys = ent:GetPhysicsObjectNum( i );
			if( phys and phys:IsValid() ) then
				phys:EnableGravity( g )
				phys:EnableDrag( d )
			end
		end
	else
		ent:GetPhysicsObject():EnableDrag( d )
		ent:GetPhysicsObject():EnableGravity( g )		
	end
end

function ENT:UpdateGravity(ent)
	ent.environment = self
	
	local Grav,Press = self.gravity,self.pressure
	if ent.NoGrav == true then 
		Grav,Press = 0,0
	end
	
	ent:SetGravity( self.gravity )

	if ent:IsPlayer()  then
		ent:SetNWBool( "inspace", false )
	end
	
	self:GravNDrag(ent,Grav > 0.01, Press> 0.1)
end

function ENT:UpdatePressure(ent) end

function ENT:Check()
	--local start = SysTime()
	local radius = self.radius
	for k,ent in pairs(self.Entities) do
		if IsValid(ent) then
			if IsValid(ent:GetPhysicsObject()) then
				--if ent.NoGrav then continue end
				if ent:GetPos():Distance(self:GetPos()) <= radius then
					self:UpdateGravity(ent)
				else --space
					--Set Space
					if not ent.environment == self then
						table.remove(self.Entities,k)
					end
					
				end
			end
		else
			table.remove(self.Entities,k)
		end
	end
	--print(self.name, SysTime()-start, table.Count(self.Entities))
end

function ENT:Think()
	if not self:GetPos() == self.position then
		self:SetPos(self.position)
	end
	
	if self.Entities then
		self:Check()
	end
	
	self:NextThink(CurTime() + 1)
	return true
end

function ENT:Configure(rad, gravity, name, env)
	self:PhysicsInitSphere(rad)
	self:SetCollisionBounds(Vector(-rad,-rad,-rad),Vector(rad,rad,rad))
	self:SetTrigger( true )
    self:GetPhysicsObject():EnableMotion( false )
	self:SetMoveType( MOVETYPE_NONE )
	
	local phys = self:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:Wake()
	end
	self:SetNotSolid( true )
	
	self.OldData = {}
	for k,v in pairs(env) do
		self[k] = v
		self.OldData[k] = v
	end
	
	self.radius = rad
	self.Enabled = true
	self.gravity = gravity
end

function ENT:CanTool()
	return false
end

function ENT:GravGunPunt()
	return false
end

function ENT:GravGunPickupAllowed()
	return false
end


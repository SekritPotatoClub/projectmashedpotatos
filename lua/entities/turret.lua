AddCSLuaFile()

ENT.Type 			= "anim"
ENT.Base 			= "base_env_base"
ENT.PrintName		= "Powered Turret"
ENT.Author			= "Mechanos"
ENT.Category		= "EnvX"

ENT.Spawnable		= false
ENT.AdminSpawnable	= false

ENT.OverlayText = {HasOOO = true, resnames ={"energy"}}

ENT.NoEnvPanel = true	--temporary 'till we play with the EnvX.MenuCore stuffs

if(SERVER)then
	function ENT:Initialize()
		self.Active = 0
		self.ActiveAttempt = 0
		self.Deactivate = 0
		self.Mute = 0
		
		self.RealThink = CurTime()
		self.Powered = CurTime()
		
		self.Weapons = self.Weapons or {}
		self.NoCollides = {}
		self.NoCollideCheck = 0
		self.AimPosition = Vector(0,0,0)
		self.TurretType = self.TurretType or 0
		self.TurretSettings = {}
		self.SetupFinished = false
		self.SoundPlaying = false
		self.SoundLevel = 0
		self.SoundCooldown = 0
		self.OurSound = CreateSound( self, "weapons/physgun_off.wav", self:EntIndex() ) 
		self.OurSound:Play()
		
		self.Bear = 0
		self.Elev = 0
		if self.TurretType == 0 then
			self.TurretSettings = {
				BaseModel		=	"models/Gibs/HGIBS.mdl"
			}
		elseif self.TurretType == 1 then
			self.TurretSettings = {
				BaseModel		=	"models/slyfo/smlturretbase.mdl",
				SwivelModel	=	"models/slyfo/smlturrettop.mdl",
				MoveSound	=	"vehicles/crane/crane_extend_loop1.wav",
				StopSound	=	"vehicles/crane/crane_extend_stop.wav",
				SoundPitch	=	200,
				SwivelPos		=	Vector(3.8,1.6,31),
				ParentPos		=	Vector(0,0,10),
				MountOffset1	=	Vector(0,0,0),
				MountOffset2	=	Vector(0,0,0),			--unused
				MountOffset3	=	Vector(0,0,0),			--unused
				AimSpeed		=	1,
				EnergyIdle	=	100,
				EnergyActive	=	200,
				MaxAngles		=	{80,-10,180,-180},	--Pitch Up, Pitch Down, Yaw Left, Yaw Right
				Guns				=	1,								--Number of Weapons to look for before activating
				GunList			=	{
					example = {
						pos=Vector(0,0,0),
						ang=Angle(0,0,0)
					},
					heavy_machine_weapon = {
						pos=Vector(60,0,10),
						ang=Angle(0,0,0)
					}
				}
			}
		elseif self.TurretType == 2 then
			self.TurretSettings = {
			}
		elseif self.TurretType == 3 then
			self.TurretSettings = {
			}
		elseif self.TurretType == 4 then
			self.TurretSettings = {
			}
		end
		
		if not self.TurretSettings.BaseModel then
			self:Remove()
			return
		end
		self:SetModel(self.TurretSettings.BaseModel)
		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		
		if self.TurretType == 0 then return end	--No turret data, it's potato time!
		
		if not (WireAddon == nil) then
			self.WireDebugName = self.PrintName
			if self.TurretType == 3 then self.Inputs = WireLib.CreateSpecialInputs(self, { "On", "Mute",  "AimPosition", "Weapon1", "Weapon2", "Weapon3" },{[3] = "VECTOR", [4] = "ENTITY", [5] = "ENTITY", [6] = "ENTITY"})
			else WireLib.CreateSpecialInputs(self, { "On", "Mute", "AimPosition", "Weapon" },{[3] = "VECTOR", [4] = "ENTITY"})
			end
			self.Outputs = Wire_CreateOutputs(self, {"On"})
		else
			self.Inputs = {{Name="On"}}
		end
		
		self:CallOnRemove( "Turret_StopBeingNoiseyPl0x", function()
			self.OurSound:Stop()
		end )
		
		timer.Simple(0.1, function()
			if IsValid(self) then
				self:DelayedSetup()
			end
		end)
		self.BaseClass.Initialize(self)
	end
	
	function ENT:DelayedSetup()
		if not self.SetupFinished then
			if not IsValid(self.Swivel) then
				self.Swivel = ents.Create("prop_physics")
				self.Swivel:SetModel( self.TurretSettings.SwivelModel )
				self.Swivel:SetAngles( self:GetAngles() )
				self.Swivel:SetPos( self:LocalToWorld( self.TurretSettings.SwivelPos ) )
				self.Swivel:SetSolid(SOLID_NONE)
				self.Swivel:SetMoveType(MOVETYPE_NONE)
				self.Swivel:Spawn()
				self.Swivel:SetParent(self)
				self.Swivel:SetNotSolid(true)
			end
		
			if IsValid(self.Swivel) and not IsValid(self.ParentChip) then
				self.ParentChip = ents.Create("prop_physics")
				self.ParentChip:SetModel( "models/cheeze/wires/cpu.mdl" )
				self.ParentChip:SetAngles( self:GetAngles() )
				self.ParentChip:SetPos( self.Swivel:LocalToWorld( self.TurretSettings.ParentPos ) )
				self.ParentChip:SetSolid(SOLID_NONE)
				self.ParentChip:SetMoveType(MOVETYPE_NONE)
				self.ParentChip:Spawn()
				self.ParentChip:SetParent(self.Swivel)
				self.ParentChip:SetNotSolid(true)
				
				local crayola = Color(255,255,255,0)
				self.ParentChip:SetColor(crayola)
				self.ParentChip:SetRenderMode( RENDERMODE_TRANSALPHA )
			end
			
			--STUFF HERE TO CHECK WEAPONS
			if self.Inputs.Weapon then
				if IsValid(self.Inputs.Weapon.Src) then
					self.Weapons[1] = nil
					self:WeaponCheck(self.Inputs.Weapon.Src,1)
					MsgAll("Weapon Found!")
				else
					MsgAll("Weapon Src Not Found!")
				end
			else
				MsgAll("Weapon Not Found!")
			end
			if self.Inputs.Weapon1 then
				if IsValid(self.Inputs.Weapon1.Src) then
					self.Weapons[1] = nil
					self:WeaponCheck(self.Inputs.Weapon1.Src,1)
				end
			end
			if self.Inputs.Weapon2 then
				if IsValid(self.Inputs.Weapon2.Src) then
					self.Weapons[2] = nil
					self:WeaponCheck(self.Inputs.Weapon2.Src,2)
				end
			end
			if self.Inputs.Weapon3 then
				if IsValid(self.Inputs.Weapon3.Src) then
					self.Weapons[3] = nil
					self:WeaponCheck(self.Inputs.Weapon3.Src,3)
				end
			end
			
			self.SetupFinished = true
		end
	end
	
	function ENT:SpawnFunction( ply, tr )
		--MsgAll("Attempting to spawn a Turret")
		local ent = ents.Create("turret")
		ent:SetPos( tr.HitPos + Vector(0, 0, 10))
		ent:SetModel( "models/Gibs/HGIBS.mdl" )
		ent:Spawn()
		return ent
	end
		
	function ENT:SetActive( value, caller )
		if not self.node then
			--MsgAll("Turret: No Node, not activating")
			self.Active=0
			return 
		end
		if self.TurretSettings.Guns == 1 and not IsValid(self.Weapons[1]) then
			--MsgAll("Turret: No Weapon, not activating")
			self.Active=0
			return
		elseif self.TurretSettings.Guns == 2 and (not IsValid(self.Weapons[1]) or not IsValid(self.Weapons[2])) then
			self.Active=0
			return
		elseif self.TurretSettings.Guns == 3 and (not IsValid(self.Weapons[1]) or not IsValid(self.Weapons[2]) or not IsValid(self.Weapons[3])) then
			self.Active=0
			return
		end
		if value then
			if value ~= 0 and self.Active == 0  then
				--MsgAll("Turret: Attempting to Activate")
				self.Active = 1
			elseif value == 0 and self.Active == 1  then
				self.Active = 0
				self.Deactivate = 1
			end
		else
			if self.Active == 0  then
				self.Active = 1
			else
				self.Active = 0
				self.Deactivate = 1
			end
		end
	end

	function ENT:TriggerInput(iname, value)
		if iname == "On" then
			--MsgAll("Turret: Triggered, setting active")
			self:SetActive(value)
			self.ActiveAttempt = value
		end
		if iname == "Mute" then
			if value > 0 then
				self:SetDeviceMute(1)
				self.Mute=1
			else
				self:SetDeviceMute(0)
				self.Mute=0
			end
		end
		if iname == "AimPosition" then
			self.AimPosition = value
		end
		
		if iname == "Weapon1" or iname == "Weapon" then
			self:WeaponCheck(value,1)
			--[[
			MsgAll("Contents of Inputs:")
			for K, V in pairs(self.Inputs) do
				MsgAll(tostring(K)..":"..tostring(V))
				if type(V) == "table" then
					for K2, V2 in pairs(V) do
						MsgAll("-->"..tostring(K2)..":"..tostring(V2))
					end
				end
			end
			]]
		end
		if iname == "Weapon2" then
			self:WeaponCheck(value,2)
		end
		if iname == "Weapon3" then
			self:WeaponCheck(value,3)
		end
	end
	
	function ENT:WeaponFreeze(num)
		local Wep = self.Weapons[num]
		if IsValid(Wep) then
			local phys = Wep:GetPhysicsObject()
			if IsValid(phys) then
				if phys:IsMoveable() then
					self:SetupWeapons()
				end
			end
		end
	end
	
	function ENT:WeaponCheck(value,num)
		if IsValid(value) then
			local class = value:GetClass()
			if self.Weapons[num] ~= value then
				if not self.TurretSettings.GunList[class] and value.MountType then
					--MsgAll(value.MountType)
					if self.TurretType == 1 and value.MountType == "Medium" then
						self.TurretSettings.GunList[class] = {}
						self.TurretSettings.GunList[class].pos=value.MountVectorOffSet
						self.TurretSettings.GunList[class].ang=value.MountAngleOffSet
						--MsgAll(tostring(self.TurretSettings.GunList[class]))
						--MsgAll(tostring(self.TurretSettings.GunList[class].pos))
						--MsgAll(tostring(self.TurretSettings.GunList[class].ang))
					end
				end
				if self.TurretSettings.GunList[class] then
					if IsValid(self.Weapons[num]) then self.Weapons[num]:Remove() end
					self.Weapons[num] = value
					self:SetupWeapons()
				end
			end
		end
	end
	
	function ENT:SetupWeapons()
		if not IsValid(self) then --[[FUCKING KILL YOURSELF CUZ U DED ALREADY err I mean..]] return end
		if not IsValid(self.ParentChip) then return end
		local Ready = false
		if self.TurretSettings.Guns == 1 and not IsValid(self.Weapons[1]) then
			return
		elseif self.TurretSettings.Guns == 2 and (not IsValid(self.Weapons[1]) or not IsValid(self.Weapons[2])) then
			return
		elseif self.TurretSettings.Guns == 3 and (not IsValid(self.Weapons[1]) or not IsValid(self.Weapons[2]) or not IsValid(self.Weapons[3])) then
			return
		end
		
		local Angs = self.ParentChip:GetAngles()
		
		local N = 1
		local Wep = self.Weapons[N]
		local Class = Wep:GetClass()
		
		local phys = Wep:GetPhysicsObject()
		if phys:IsMoveable() then
			phys:EnableMotion(false)
			phys:Wake()
		end
		local WepPos = self.ParentChip:LocalToWorld(self.TurretSettings.MountOffset1 + self.TurretSettings.GunList[Class].pos)
		constraint.RemoveAll(Wep)
		self.NoCollides[N] = constraint.NoCollide(Wep,self,0,0) --Wep:SetMoveType(MOVETYPE_NONE)
		Wep:SetAngles(Angs + self.TurretSettings.GunList[Class].ang) Wep:SetPos(WepPos) Wep:SetParent(self.ParentChip)
	end
	
	--[[local function quiet_steam(ent)
		ent:StopSound( "ambient.steam01" )
	end]]
	
	--[[function ENT:SetActive( value, caller )
		if not self.node then return end
		
		local Res_needed = 100-((caller.suit.energy/SuitDat.maxenergy) * 100)
		local ResHas = self:GetResourceAmount("Energy")
		
		--print("N:"..Res_needed.." H:"..ResHas)
		
		if ResHas>=Res_needed then
			self:ConsumeResource("Suit", Res_needed)
			caller.suit.energy = SuitDat.maxenergy
		else
			self:ConsumeResource("Suit", ResHas)
			
			caller.suit.energy = caller.suit.energy + math.floor(ResHas * Multiplier)
		end
		
		local fuel = self:GetResourceAmount("hydrogen")
		local fuel_needed = math.ceil(((SuitDat.maxfuel) - caller.suit.fuel) * Divider)
		if ( fuel_needed < fuel ) then
			self:ConsumeResource("hydrogen", fuel_needed)
			caller.suit.fuel = SuitDat.maxfuel
		elseif (fuel > 0) then
			caller.suit.fuel = caller.suit.fuel + math.floor(fuel * Multiplier)
			self:ConsumeResource("hydrogen", fuel)
		end
		
		caller:EmitSound( "ambient.steam01" )
		timer.Simple(1.2, function() quiet_steam(caller) end) 
	end]]
	
	local rad2deg = 180 / math.pi
	local asin = math.asin
	local atan2 = math.atan2
	local delta = 0.0000001000000
	
	function ENT:Bearing(ent, pos)
		pos = ent:WorldToLocal(Vector(pos[1],pos[2],pos[3]))

		return rad2deg*-atan2(pos.y, pos.x)
	end
	
	function ENT:Elevation(ent, pos)
		pos = ent:WorldToLocal(Vector(pos[1],pos[2],pos[3]))

		local len = pos:Length()
		if len < delta then return 0 end
		return rad2deg*asin(pos.z / len)
	end
	
	function ENT:Think()
		local curtime = CurTime()
		if not IsValid(self.Swivel) or not IsValid(self.ParentChip) then
			self:NextThink( curtime + 1 )
			return
		end
		self:NextThink( curtime + 0.01 )
	
		if self.RealThink <= curtime then
			self.RealThink = curtime + 0.05
			--self.BaseClass.Think(self)
			
			if self.ActiveAttempt ~= 0 and self.ActiveAttempt < curtime then
				self.ActiveAttempt = curtime + 2
				self:SetActive(1)
			end
			
			--Keep Weapons Frozen
			self:WeaponFreeze(1)
			if self.TurretSettings.Guns >= 2 then self:WeaponFreeze(2) end
			if self.TurretSettings.Guns >= 3 then self:WeaponFreeze(3) end
			
			--temporary nocollide/parent workaround
			if self.NoCollideCheck < curtime then
				self.NoCollideCheck = curtime + 1
				for K, V in pairs(self.NoCollides) do
					if not IsValid(V) and IsValid(self.Weapons[K]) then
						self.NoCollides[V] = constraint.NoCollide(self.Weapons[K],self,0,0)
					end
				end
				if table.Count(self.Weapons) > 0 then
					for K, V in pairs(self.Weapons) do
						if IsValid(V) then
							local children = V:GetChildren()
							if table.Count(children) > 0 then
								MsgAll("Turret Abuse Detected D: = ")
								for K2, V2 in pairs(children) do
									MsgAll("-->"..tostring(K2)..":"..tostring(V2))
									V2:EmitSound("ambient/alarms/klaxon1.wav")
									local pos = V2:GetPos()
									V2:SetParent()
									V2:SetPos(pos)
								end
							end
						end
					end
				end
			end
			
			local AimSpeed = self.TurretSettings.AimSpeed
			--MsgAll("I'm Thinking!")
			if self.Active == 1 then
				--MsgAll("Turret: I'm Active!")
				if not IsValid(self.Weapons[1]) and not IsValid(self.Weapons[2]) and not IsValid(self.Weapons[3]) then
					MsgAll("Turret: No Valid Weapons, shutting down")
					self:SetActive(0)
					self:NextThink( curtime + 0.1 )
					return
				end
				
				local IdleCheck = false
				local EnergyCost = 0
				
				self.Bear = self:Bearing(self.ParentChip, self.AimPosition)
				if self.Bear > 45 or self.Bear < -45 then
					self.Elev = self:Elevation(self.ParentChip, self.Swivel:GetPos() + self.Swivel:GetForward()*100)
				else
					self.Elev = self:Elevation(self.ParentChip, self.AimPosition)
				end
				
				if self.Bear > AimSpeed then
					self.Bear = AimSpeed
					IdleCheck = true
				elseif self.Bear < -AimSpeed then
					self.Bear = -AimSpeed
					IdleCheck = true
				end
				if self.Elev > AimSpeed then
					self.Elev = AimSpeed
					IdleCheck = true
				elseif self.Elev < -AimSpeed then
					self.Elev = -AimSpeed
					IdleCheck = true
				end
				if IdleCheck then EnergyCost = self.TurretSettings.EnergyActive else EnergyCost = self.TurretSettings.EnergyIdle end
				
				if self.Powered <= curtime then
					local Power = self:GetResourceAmount("energy")
					--MsgAll("Turret: Power = "..tostring(Power))
					if Power < EnergyCost then
						--MsgAll("Turret: Not Enough Energy, shutting down")
						self:SetActive(0)
					else
						NOMNOMNOM = self:ConsumeResource("energy", EnergyCost)
						if NOMNOMNOM == EnergyCost then
							--MsgAll("Turret: Power Consumed, commence spam!")
							self.Powered = curtime + 1
							self.Deactivate=0
						end
					end
				end
			end
			
			if (self.Active == 1 and self.Powered > curtime) or self.Deactivate == 1 then
			
				if self.Deactivate == 1 then
					AimSpeed = AimSpeed*0.5
					local BaseAngles = self:GetAngles()
					local ParentAngles = self.ParentChip:GetAngles()
					self.Bear = -(BaseAngles.y - ParentAngles.y)
					MsgAll(self.Bear)
					if self.Bear > 45 or self.Bear < -45 then
						self.Elev = self:Elevation(self.ParentChip, self.Swivel:GetPos() + self.Swivel:GetForward()*100)
					else
						self.Elev = -(BaseAngles.x - ParentAngles.x)
					end
					if self.Bear > AimSpeed then
						self.Bear = AimSpeed
					elseif self.Bear < -AimSpeed then
						self.Bear = -AimSpeed
					end
					if self.Elev > AimSpeed then
						self.Elev = AimSpeed
					elseif self.Elev < -AimSpeed then
						self.Elev = -AimSpeed
					end
					--MsgAll(self.Bear) MsgAll(self.Elev)
					if (self.Bear < 0.1 and self.Bear > -0.1) and (self.Elev < 0.1 and self.Elev > -0.1) then self.Deactivate = 0 end
				end
				
				ElevClamp = self:Elevation(self.Swivel,self.ParentChip:GetPos() + self.ParentChip:GetForward()*100)
				if ElevClamp > self.TurretSettings.MaxAngles[1] and self.Elev > 0 then
					self.Elev = 0
				elseif ElevClamp < self.TurretSettings.MaxAngles[2] and self.Elev < 0 then
					self.Elev = 0
				end
				
				BearClamp = self:Bearing(self,self.Swivel:GetPos() + self.Swivel:GetForward()*100)
				if BearClamp > self.TurretSettings.MaxAngles[3] and self.Bear > 0 then
					self.Bear = 0
				elseif BearClamp < self.TurretSettings.MaxAngles[4] and self.Bear < 0 then
					self.Bear = 0
				end
				
				local level = 0
				if self.Bear > AimSpeed*0.25 or self.Bear < -AimSpeed*0.25 then level=level+1 end
				if self.Elev > AimSpeed*0.25 or self.Elev < -AimSpeed*0.25 then level=level+1 end
				
				if level > 0 and self.SoundCooldown < curtime then
					self.SoundCooldown = curtime + 0.1
					local pitch = self.TurretSettings.SoundPitch
					if level == 1 then pitch = pitch*0.9 end
					if not self.SoundPlaying then
						self.OurSound = CreateSound( self, self.TurretSettings.MoveSound, self:EntIndex() )
						self.OurSound:Play()
						self.OurSound:ChangePitch(pitch,0)
						if self.Mute == 1 then
							self.OurSound:ChangeVolume(0.05)
						end
						self.SoundPlaying = true
					end
					if self.SoundLevel ~= level then
						self.OurSound:ChangePitch(pitch,0.5)
						self.SoundLevel = level
					end
				elseif level == 0 and self.SoundPlaying then
					self.OurSound:Stop()
					self.OurSound  = CreateSound( self, self.TurretSettings.StopSound, self:EntIndex() )
					self.OurSound:Play()
					self.OurSound:ChangePitch(self.TurretSettings.SoundPitch,0)
					if self.Mute == 1 then
						self.OurSound:ChangeVolume(0.05)
					end
					self.SoundPlaying = false
				end
				
				local SwivelAng = self.Swivel:LocalToWorldAngles(Angle(0,-self.Bear,0))
				self.Swivel:SetAngles(SwivelAng)
				
				local ParentAng = self.ParentChip:LocalToWorldAngles(Angle(-self.Elev,0,0))
				self.ParentChip:SetAngles(ParentAng)
				
			end
			
		end

		return true
	end
	
	function ENT:EnvxOnDupe()--This function is being called WHILE the entity is being duped. Use it to save any data you wish to persist via dupes.
		local info = {}
		info.TurretType = self.TurretType
		return info
	end
	
	function ENT:EnvxOnPaste(Player,Ent,CreatedEntities) --This is called when your entity is being pasted...
		local MyData = Ent.EntityMods.EnvxDupeInfo --This should be the table of data you saved with the above function...
		if MyData and MyData.TurretType then
			MsgAll("Data Found!")
			self.TurretType = MyData.TurretType
			self:Spawn()
		else
			MsgAll("No Data :(")
		end
		--self:SetupWeapons()
	end
	
	--function ENT:PreEntityCopy()
		--self.BaseClass.PreEntityCopy()
		--local TurretInfo = { TurretType=self.TurretType }
		--duplicator.StoreEntityModifier( self, "EnvxTurretInfo", TurretInfo )
	--end
	
	--function ENT:PostEntityPaste( Player, Ent, CreatedEntities )
		--self.BaseClass.PostEntityPaste( Player, Ent, CreatedEntities)
		--if Ent.EntityMods and Ent.EntityMods.TurretInfo then
		--	self.TurretType = Ent.EntityMods.TurretInfo.TurretType
		--end
	--end
	
else

end		

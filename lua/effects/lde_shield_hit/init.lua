

--local matRefract = Material( "models/spawn_effect" )
--local matRefract = Material( "models/alyx/emptool_glow" )
--local matLight	 = Material( "models/alyx/emptool_glow" )

--local matRefract = Material( "models/props_combine/stasisshield_sheet" )
--local matLight	 = Material( "models/props_combine/stasisshield_sheet" )
--Space_Combat/Shield/shield_01
local matRefract = Material( "Zup/Shields/teal_shield" )
local matLight	 = Material( "Zup/Shields/teal_shield" )

--[[---------------------------------------------------------
   Initializes the effect. The data is a table of data 
   which was passed from the server.
---------------------------------------------------------]]--
function EFFECT:Init( data )
	
	-- This is how long the spawn effect 
	-- takes from start to finish.
	self.Time = 1
	self.LifeTime = CurTime() + self.Time
	
	local ent = data:GetEntity()
	if ( ent == NULL ) then return end
	
	self.ParentEntity = ent
	
	self:SetModel( ent:GetModel() )
	self:SetModelScale(ent:GetModelScale())
	self:SetPos( ent:GetPos() )
	self:SetAngles( ent:GetAngles() )
	self:SetParent( ent )
	self:SetMaterial( matRefract )
end


/*---------------------------------------------------------
   THINK
   Returning false makes the entity die
---------------------------------------------------------*/
function EFFECT:Think( )

	if (!self.ParentEntity || !self.ParentEntity:IsValid()) then return false end

	return ( self.LifeTime > CurTime() ) 
	
end

/*---------------------------------------------------------
   Draw the effect
---------------------------------------------------------*/
function EFFECT:Render()
	
	-- What fraction towards finishing are we at
	local Fraction = (self.LifeTime - CurTime()) / self.Time
	Fraction = math.Clamp( Fraction, 0, 1 )
	local Alpha = 1 + math.sin( Fraction * math.pi ) * 255
	--print("F: "..Fraction.." A: "..Alpha)
	
	-- Change our model's alpha so the texture will fade out
	self:SetColor( Color(255, 255, 255, Alpha ))
	
	-- Place the camera a tiny bit closer to the entity.
	-- It will draw a bit bigger and we will skip any z buffer problems
	local EyeNormal = self:GetPos() - EyePos()
	local Distance = EyeNormal:Length()
	EyeNormal:Normalize()
	
	local Pos = EyePos() + EyeNormal * Distance * 0.01
	
	-- Start the new 3d camera position
	cam.Start3D( Pos, EyeAngles() )		
		
		-- If our card is DX8 or above draw the refraction effect
		
		if ( render.GetDXLevel() >= 80 ) then
		
			-- Update the refraction texture with whatever is drawn right now
			render.UpdateRefractTexture()
			
			matRefract:SetFloat( "$refractamount", Fraction )
		
			-- Draw model with refraction texture
			render.MaterialOverride( matRefract )
				self:DrawModel()
			render.MaterialOverride( 0 )
		else
			render.MaterialOverride( matLight )
				self:DrawModel()
			render.MaterialOverride( 0 )		
		end
		
	
	-- Set the camera back to how it was
	cam.End3D()

end




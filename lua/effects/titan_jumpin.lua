local matRefraction	= Material( "refract_ring" )
local pi = 3.141593 --incase pi changes lol

local function NORM(Vec)
	local Length = Vec:Length()
	return Vec/Length
end

function EFFECT:Init( data )
	self.Pos = data:GetOrigin()
	self.End = CurTime()+2.3
	
	self.emtr = ParticleEmitter(self.Pos)
	
	self.WarpSize = 1000
	self.Exploded = false
	
	self.Ent = data:GetEntity()
	self:SetParent( self.Ent )
	
	self:EmitSound("warpdrive/warp.wav",100,50,1)
end

function EFFECT:emit()
	local Mult = self.WarpSize
	local a = math.random(9999)
	local b = math.random(1,180)
	local X = math.sin(b)*math.sin(a)*Mult
	local Y = math.sin(b)*math.cos(a)*Mult
	local Z = math.cos(b)*Mult
	local Pos = Vector(X,Y,Z)

	local particle = self.emtr:Add("effects/combinemuzzle2",self.Pos + Pos)
	if particle then
		local Col = Color(0,0,255,255)
		particle:SetColor(0,0,255)
		particle:SetLifeTime(0)
		particle:SetDieTime(2.5)
		particle:SetStartAlpha(0)
		particle:SetEndAlpha(255)
		particle:SetStartSize(20)
		particle:SetEndSize(0.1)
		particle:SetGravity(NORM(self.Pos-(Pos+self.Pos))*(0.5*Mult))
	end
end

function EFFECT:Think()
	
	if self.End<CurTime() and not IsValid(self.Ent) or self.Ent and not IsValid(self.Ent) then
		self.emtr:Finish()
		return false 
	end
	
	if self.End-CurTime()< 0.5 and not self.Exploded == true then
		self:EmitSound("ambient/explosions/citadel_end_explosion2.wav")
		self.Exploded = true
	elseif self.End-CurTime()> 1 then
		self.Pos = self.Ent:GetPos()
		self:emit()
	elseif self.Exploded == true then
		--Explode Effect
	end

	return true
end

function EFFECT:Render()
	if self.Ent and not IsValid(self.Ent) then return end
	local tmp = math.abs(self.WarpSize*3)
	local Pos = self.Ent:GetPos() or self.Pos
	matRefraction:SetFloat("$refractamount", .1)
	render.SetMaterial(matRefraction)
	render.UpdateRefractTexture()
	render.DrawSprite(Pos,tmp,tmp)
end
local Utl = EnvX.Utl
if SERVER then
	local meta = FindMetaTable("Player")
	local ChatColors = setmetatable({},{
        __index = function(t,k)
            local x,y = string.find(k,"%b[]")
            if x ~= nil then
                k = k:sub(x+1,y-1)
                local RGBA = {}
                for i in string.gmatch(k, "%P+") do
                    RGBA[#RGBA+1] = tonumber(i)
                    if RGBA[#RGBA] == nil then return end
                end
                
                if #RGBA >= 3 then
                    if #RGBA == 3 then RGBA[#RGBA+1] = 255 end
                    return Color(unpack(RGBA))
                end
            end
        end
    })

	ChatColors["[r]"] = Color(255,0,0,255)
	ChatColors["[g]"] = Color(0,255,0,255)
	ChatColors["[b]"] = Color(0,0,255,255)
	ChatColors["[y]"] = Color(255,255,0,255)
	ChatColors["[p]"] = Color(132,96,255,255)
	ChatColors["[bl]"] = Color(0,0,0,255)
	ChatColors["[gg]"] = Color(0,220,90,255)

	function meta:SendColorChat(nam,col,msg)
		EnvX:NetworkData("envx_colorchat",{MSG={col,nam,Color(255,255,255,255),": "..msg}},self)
	end
	
	function meta:SendColorChatAdvanced(Dat)
		EnvX:NetworkData("envx_colorchat",{MSG=Dat},self)
	end

	EnvX:HookNet("envx_colorchat",function(Data,Ply)

        local chatStr = {team.GetColor(Ply:Team()),Ply:Nick(),Color(255,255,255,255),": "}

        for i in string.gmatch(Data.MSG, "%S+") do
            local str = i:lower()
            if ChatColors[str] ~= nil then
            	chatStr[#chatStr+1] = ChatColors[str]
            else
            	chatStr[#chatStr+1] = i.." "
        	end
        end
		local plys = player.GetAll()
		for k,v in pairs(plys) do
			v:SendColorChatAdvanced(chatStr)
		end

		timer.Simple(0.1,function() 
			hook.Call("PlayerSay",nil,Ply,Data.MSG,false)
		end)
	end)
else
	local ChatBox = {}

	ChatBox.ActiveMsg = false
	local NDat = EnvX.Utl.NetMan

	local H = ScrH()
	local W = ScrW()
	surface.CreateFont( "EnvXChatFont", {
		font = "Arial",
		size = 22,
		weight = 500,
		blursize = 0,
		scanlines = 0,
		antialias = true,
		underline = false,
		italic = false,
		strikeout = false,
		symbol = false,
		rotary = false,
		shadow = false,
		additive = false,
		outline = false,
	} )

	function ChatBox.FramePaint(self,w,h)
		draw.RoundedBox(16,10,10,w-20,h-20, EnvX.GuiThemeColor.BG)
		surface.SetTexture( EnvX.GradientTex )
		local GC = EnvX.GuiThemeColor.GC
		surface.SetDrawColor( GC.r, GC.g, GC.b, GC.a )
		surface.DrawTexturedRect( 10,10,w-20,h-20 )
		draw.NoTexture()
		draw.RoundedBox(16,0,0,w,h,EnvX.GuiThemeColor.FG)
		draw.NoTexture()
	end

	function ChatBox.Blank()

	end

	function ChatBox.Create()
		if IsValid(ChatBox.DFrame) then ChatBox.DFrame:Remove() end
		ChatBox.DFrame = vgui.Create("DFrame")
		ChatBox.DFrame:SetDeleteOnClose(false)
		ChatBox.DFrame:SetSize(W*0.35,H*0.3)
		ChatBox.DFrame:SetPos(W*0.01,H*0.5)
		ChatBox.DFrame:ShowCloseButton(false)
		ChatBox.DFrame:SetTitle("")
		ChatBox.DFrame.Close = ChatBox.Close 
		ChatBox.DFrame.Think = function()
			if input.IsKeyDown( KEY_ESCAPE ) then
				ChatBox.Close()
			end

			if ChatBox.ActiveMsg and (CurTime() - ChatBox.LastMsg) > 5 then
				ChatBox.DFrame:SetVisible(false)
				ChatBox.ActiveMsg = false
			end
		end
		ChatBox.DFrame.Paint = ChatBox.FramePaint

		ChatBox.RichText = vgui.Create("RichText",ChatBox.DFrame)
		ChatBox.RichText.PerformLayout = function( self )
			self:SetFontInternal("EnvXChatFont")
			self:SetFGColor( EnvX.GuiThemeColor.Text )
		end
		ChatBox.RichText:Dock( FILL )
		
		ChatBox.DTextEntry = vgui.Create("DTextEntry",ChatBox.DFrame)
		ChatBox.DTextPaint = ChatBox.DTextEntry.Paint
		ChatBox.DTextEntry:SetFont("EnvXChatFont")
		ChatBox.DTextEntry:SetTall(25)
		ChatBox.DTextEntry:Dock( BOTTOM )
		ChatBox.DTextEntry.OnEnter = function(_,str)
		    ChatBox.Close()
			if str == "" then return end
			EnvX:NetworkData("envx_colorchat",{MSG=str},LocalPlayer())
		    ChatBox.DTextEntry:SetText( "" )
		end
	end
	function ChatBox.Open()
		if not IsValid(ChatBox.DFrame) then
			ChatBox.Create()
		end
		ChatBox.ActiveMsg = false
		ChatBox.DFrame:SetVisible(true)
		ChatBox.DFrame.Paint = ChatBox.FramePaint
		ChatBox.DTextEntry.Paint = ChatBox.DTextPaint
		ChatBox.DFrame:ShowCloseButton(true)
		ChatBox.DFrame:MakePopup()
		ChatBox.DTextEntry:RequestFocus()
		ChatBox.RichText:SetVerticalScrollbarEnabled( true )
	end

	function ChatBox.Close()
		ChatBox.ActiveMsg = true
		ChatBox.LastMsg = CurTime()
		ChatBox.DFrame.Paint = ChatBox.Blank
		ChatBox.RichText.Paint = ChatBox.Blank
		ChatBox.DTextEntry.Paint = ChatBox.Blank
		ChatBox.DFrame:ShowCloseButton(false) 
		ChatBox.DFrame:SetMouseInputEnabled(false)
		ChatBox.DFrame:SetKeyboardInputEnabled(false)
		ChatBox.DTextEntry:KillFocus()
		ChatBox.RichText:SetVerticalScrollbarEnabled( false )
	end
	local old = old or chat.AddText
	function ChatBox.AddText(...)
		if not IsValid(ChatBox.DFrame) then
			return
		end
		ChatBox.DFrame:SetVisible(true)
		ChatBox.ActiveMsg = true
		ChatBox.LastMsg = CurTime()
		local Args = {...}
		for i=1,#Args do
			local typ = type(Args[i])
			if IsColor(Args[i]) or typ == "table" then
				ChatBox.RichText:InsertColorChange(Args[i].r,Args[i].g,Args[i].b,Args[i].a or 255)
			elseif typ == "Player" then
				local c = team.GetColor(Args[i]:Team())
				ChatBox.RichText:InsertColorChange(c.r,c.g,c.b,c.a)
				ChatBox.RichText:AppendText(Args[i]:Nick())
			elseif typ == "string" then
				ChatBox.RichText:AppendText(Args[i])
			end
		end

		ChatBox.RichText:AppendText("\n")
		timer.Simple(0.1,function()	ChatBox.RichText:GotoTextEnd() end)
		old(...)
	end

	chat.AddText = ChatBox.AddText

	hook.Add( "PlayerBindPress", "overrideChatbind", function( ply, bind, pressed )
	    if bind ~= "messagemode" or not pressed then
	        return
	    end

	    ChatBox.Open()

	    return true
	end )

	hook.Add( "HUDShouldDraw", "remChat", function( name )
		if name == "CHudChat" then
			return false
		end
	end )

	hook.Add( "InitPostEntity", "InitChat", function()
		timer.Simple(0.5,function()
			ChatBox.Create()
			ChatBox.Close()
		end)
	end )

	EnvX:HookNet("envx_colorchat",function(Data)
		chat.AddText(unpack(Data.MSG))
	end)
end
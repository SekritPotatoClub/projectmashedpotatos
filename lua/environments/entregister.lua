--[[                        Environments Life Support System SDK
Environments.RegisterLSStorage(name, class, res, basevolume, basehealth, basemass)
   This function creates a new storage entity and registers its multipliers
	-name: the name of the new storage
	-class: the entity class of the new storage
	-res: a table in the format of {[amt] = "resource"} of the resources stored inside
	-basevolume: the volume used to calculate the multiplier
	-basehealth: the health given to the device at base volume
	-basemass: the mass of the storages at base volume
   
Environments.RegisterTool(name, filename, category, description, cleanupgroup, limit)
   This function creates a new tool.
    -name: this is the name of the tool you are creating
	-filename: this is the technical name of the tool used by the toolgun system
	-category: the name of the subtab the tool is added to
	-description: a description of what the tool does
	-cleanupgroup: the cleanup group used by the tool
	-limit: the max number of devices a user can spawn from this tool/cleanupgroup
   
Environments.RegisterDevice(toolname, genname, devname, class, model, skin, extra)
   This function adds a device to the tool specified with the specified model.
    -toolname: the name of the tool to add the device to
	-genname: the name of the type of generator
	-devname: the actual name of the generator you are adding
	-class: the class of the generator's entity
	-model: the model of the generator
	-skin: a number for its skin (if needed)
	-extra: any extra variable you need to pass on to the ent as ent.env_extra, can be any value
]]
   
local list = list
local scripted_ents = scripted_ents
local table = table
local math = math
local cleanup = cleanup
local language = language
local util = util
local constraint = constraint
local pairs = pairs
local CurTime = CurTime

local Environments = Environments --yay speed boost!

local log10 = math.log(10) --yay for optimization
function math.PlaceRound(num, sigfigs)
	local pow = math.ceil(math.log(num)/log10) 
	return math.Round(num, -(pow - sigfigs)) 
end

function Environments.RegisterLSEntity(name,class,In,Out,generatefunc) --simple quick entity creation
	local ENT = {}
	ENT.Type = "anim"
	ENT.Base = "base_env_entity"
	ENT.PrintName = name
	
	ENT.OverlayText = {HasOOO = true, resnames = In, genresnames = Out}

	if SERVER then
		function ENT:Initialize()
			self.BaseClass.Initialize(self)
			self:PhysicsInit( SOLID_VPHYSICS )
			self:SetMoveType( MOVETYPE_VPHYSICS )
			self:SetSolid( SOLID_VPHYSICS )
			self.Active = 0
			self.multiplier = 1
			if WireAddon then
				self.WireDebugName = self.PrintName
				self.Inputs = WireLib.CreateInputs(self, { "On", "Multiplier" })
				self.Outputs = WireLib.CreateOutputs(self, { "On" })
			end
		end
		
		function ENT:TurnOn()
			if self.Active == 0 then
				self.Active = 1
				self:SetOOO(1)
				if WireLib then 
					WireLib.TriggerOutput(self, "On", 1)
				end
			end
		end
		
		function ENT:TriggerInput(iname, value)
			if iname == "On" then
				if value > 0 then
					if self.Active == 0 then
						self:TurnOn()
					end
				else
					if self.Active == 1 then
						self:TurnOff()
					end
				end
			elseif iname == "Multiplier" then
				if value > 0 then
					self:SetMultiplier(value)
				else
					self:SetMultiplier(1)
				end
			end
		end

		function ENT:TurnOff()
			if self.Active == 1 then
				self.Active = 0
				self:SetOOO(0)
				if WireLib then 
					WireLib.TriggerOutput(self, "On", 0)
				end
			end
		end

		function ENT:SetActive(value)
			if not (value == nil) then
				if (value ~= 0 and self.Active == 0 ) then
					self:TurnOn()
				elseif (value == 0 and self.Active == 1 ) then
					self:TurnOff()
				end
			else
				if ( self.Active == 0 ) then
					self:TurnOn()
				else
					self:TurnOff()
				end
			end
		end
		
		ENT.Generate = generatefunc

		function ENT:Think()
			if self.Active == 1 then
				self:Generate()
			end
			
			self:NextThink(CurTime() + 1)
			return true
		end
	else
		--client
	end
	
	scripted_ents.Register(ENT, class, true, true)
	print("Entity Registered: "..class)
end


Environments.FailSafeData = Environments.FailSafeData or {}

function Environments.RegisterLSStorage(name, class, res)
	local ENT = {}
	ENT.Type = "anim"
	ENT.Base = "base_env_storage"
	ENT.PrintName = name
	
	ENT.OverlayText = {HasOOO = false, stornames = res}

	Environments.FailSafeData[class] = {storage=res}
	
	scripted_ents.Register(ENT, class, true, true)
	print("Storage Created: "..class)
end

--[[
local data = {
	Path = {"gens","basic","pump"},

	DevName="pumper1",
	model="modelpath",
	class="classname",
	skin=0,
	extra={},
	description="",
	tooltip=""
}
]]
Environments.Tooldata = Environments.Tooldata or {}
Environments.EntityData = Environments.EntityData or {}

function Environments.ConstructDevicePath(data)
	local tab = Environments.Tooldata
	
	for i,v  in pairs( data ) do
		tab[v] = tab[v] or {}
		tab = tab[v]
	end
	
	tab.ModelList = true
	tab.Classes = tab.Classes or {}
	tab.Settings = tab.Settings or {}
	
	return tab
end

function Environments.RegisterClassTag(path,class)
	local tab = Environments.ConstructDevicePath(path)
	tab.Classes[class]=true
end

function Environments.RegisterToolTip(path,text)
	local tab = Environments.Tooldata	
	for i,v  in pairs( path ) do tab[v] = tab[v] or {} tab = tab[v] end
	tab.NodeDesc = text
end

function Environments.RegisterNodeIcon(path,icon)
	local tab = Environments.Tooldata
	for i,v  in pairs( path ) do tab[v] = tab[v] or {} tab = tab[v] end
	tab.NodeIcon = icon
end

function Environments.RegisterDevice(data)
	if not data.Path then print("Error! "..data.DevName.." Is using outdated register data. Please update them.") return end
	local DName = data.DevName
	
	duplicator.RegisterEntityClass(data.class, Environments.DupeFix, "Data" )--Allow Duplicators to dupe this class.
	

	local Dat = Environments.ConstructDevicePath(data.Path)
	
	Dat.DisplayInfo = data.displayinfo
	Dat.NodeDesc = data.nodetext
	Dat.NodeIcon = data.nodeicon
	Dat.Classes[data.class]=true

	Environments.EntityData[data.class] = Environments.EntityData[data.class] or {Ents={},Settings={}}
	local EntDat = Environments.EntityData[data.class]
	
	Dat.Settings = data.settings
	EntDat.Ents[DName] = {
		model = string.lower(data.model),
		class = data.class,
		skin = data.skin,
		extra = data.extra,
		description = data.description,
		tooltip = data.tooltip,	
		resources = {
			consumes=data.res_in,
			produces=data.res_out,
			storage=data.res_store
		},
		ToolSettings = data.toolsettings
	}
	
	if EnvX.MegaGui~=nil then
		EnvX.MegaGui.IsLoaded = false
	end
end
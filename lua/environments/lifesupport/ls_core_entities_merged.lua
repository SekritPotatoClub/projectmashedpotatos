--Core Environments LS Entities/Devices

--Register fail-safe tables so old entities dont break on dupe.
local Stores = {} Stores["steam"]={100,0} Environments.RegisterLSStorage("Steam Storage", "env_steam_storage", Stores)

local Stores = {} Stores["water"]={100,0} Environments.RegisterLSStorage("Water Storage", "env_water_storage", Stores)

local Stores = {} Stores["energy"]={100,0} Environments.RegisterLSStorage("Energy Storage", "env_energy_storage", Stores)

local Stores = {} Stores["oxygen"]={100,0} Environments.RegisterLSStorage("Oxygen Storage", "env_oxygen_storage", Stores)

local Stores = {} Stores["hydrogen"]={100,0} Environments.RegisterLSStorage("Hydrogen Storage", "env_hydrogen_storage", Stores)

local Stores = {} Stores["nitrogen"]={100,0} Environments.RegisterLSStorage("Nitrogen Storage", "env_nitrogen_storage", Stores)

local Stores = {} Stores["carbon dioxide"]={100,0} Environments.RegisterLSStorage("CO2 Storage", "env_co2_storage", Stores)

local Stores = {} local Per = 100/6 Stores["carbon dioxide"]={Per,0} Stores["hydrogen"]={Per,0} Stores["nitrogen"]={Per,0} Stores["oxygen"]={Per,0} Stores["energy"]={Per,0} Stores["water"]={Per,0} 
Environments.RegisterLSStorage("Resource Cache", "env_cache_storage", Stores)

Environments.RegisterLSEntity("Water Heater","env_water_heater",{"water","energy"},{"steam"},
function(self) 
	local mult = self:GetSizeMultiplier()*self.multiplier 
	local amt = self:ConsumeResource("water", 200) or 0 
	amt = self:ConsumeResource("energy",amt*1.5)  
	self:SupplyResource("steam", amt) 
end, 70000, 300, 300)

----Resource Nodes----
local Path={"Resource Node"}
Environments.RegisterDevice({Path=Path,DevName="Classic Node",model="models/props_wasteland/panel_leverBase001a.mdl",class="resource_node_env",toolsettings={Ang=Angle(),OffDir=Vector(0,1,0)}})
Environments.RegisterDevice({Path=Path,DevName="Round Node",model="models/SBEP_community/d12shieldemitter.mdl",class="resource_node_env"})
Environments.RegisterDevice({Path=Path,DevName="Crystiline Node",model="models/slyfo/powercrystal.mdl",class="resource_node_env"})
Environments.RegisterDevice({Path=Path,DevName="Micro Node",model="models/slyfo/util_tracker.mdl",class="resource_node_env"})
Environments.RegisterNodeIcon(Path,"icon16/drive_network.png")





----Alternitive Listings----
--Environments.RegisterClassTag({}},"")






----Advanced Generators----
--Environments.RegisterDevice("Advanced Generators", "Blackholium", "Reactor Core", "generator_blackholium_reactor", "models/hunter/blocks/cube2x2x2.mdl")






----Generator Tool----
Environments.RegisterNodeIcon({"Resource Production"},"icon16/drive.png")
--Autogen
local Path={"Resource Production","Resource Management"}
Environments.RegisterDevice({Path=Path,DevName="R01 Automatic Resource Manager",class="env_autogen",model="models/rawr/minispire.mdl"})

--Fusion Reactors
local Path={"Resource Production","LifeSupport","Energy","Fusion Reactor"}
Environments.RegisterDevice({Path=Path,DevName="Small SBEP Reactor",class="generator_fusion",model="models/punisher239/punisher239_reactor_small.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Large SBEP Reactor",class="generator_fusion",model="models/punisher239/punisher239_reactor_big.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Small Pallet Reactor",class="generator_fusion",model="models/slyfo/forklift_reactor.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Large Crate Reactor",class="generator_fusion",model="models/slyfo/crate_reactor.mdl"})
Environments.RegisterDevice({Path=Path,DevName= "Classic Reactor",class="generator_fusion",model="models/props_c17/substation_circuitbreaker01a.mdl"})
Environments.RegisterDevice({Path=Path,DevName= "Rotary Reactor",class="generator_fusion",model="models/cerus/modbridge/misc/ls/ls_gen11a.mdl"})
Environments.RegisterDevice({Path=Path,DevName= "Cubicle Reactor",class="generator_fusion",model="models/smallbridge/life support/sbclimatereg.mdl"})
Environments.RegisterDevice({Path=Path,DevName= "Test Reactor",class="generator_fusion",model="models/SmallBridge/Life Support/sbhullcache.mdl"})

--SolarPanels
local Path={"Resource Production","LifeSupport","Energy","Solar Panel"}
Environments.RegisterDevice({Path=Path,DevName="Mounted Solar Panels",class="generator_solar",model="models/Slyfo_2/miscequipmentsolar.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Small Mounted Solar Panel",class="generator_solar",model="models/Slyfo_2/acc_sci_spaneltanks.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Micro Solar Panel Plate",class="generator_solar",model="models/environmentsx/solarsmall.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Small Solar Panel Plate",class="generator_solar",model= "models/environmentsx/solarmedium.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Massive Solar Panel Plate",class="generator_solar",model= "models/environmentsx/solarlarge.mdl"})

--HydrogenFuel Cells
local Path={"Resource Production","LifeSupport","Energy","Hydrogen Fuel Cell"}
Environments.RegisterDevice({Path=Path,DevName="Large Fuel Cell",class="generator_hydrogen_fuel_cell",model= "models/slyfo/electrolysis_gen.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Small Fuel Cell",class="generator_hydrogen_fuel_cell",model= "models/slyfo/crate_watersmall.mdl"})

--Steam Turbine
local Path={"Resource Production","LifeSupport","Energy","Steam Turbine"}
Environments.RegisterDevice({Path=Path,DevName="Steam Turbine",class="env_steam_turbine",model= "models/sbep_community/d12siesmiccharge.mdl"})

--Microwave Emitters
local Path={"Resource Production","LifeSupport","Energy","Microwave Transmission"}
Environments.RegisterDevice({Path=Path,DevName="Massive Receiver",class="reciever_microwave",model="models/props_spytech/satellite_dish001.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Large Receiver",class="reciever_microwave",model="models/spacebuild/nova/recieverdish.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Small Receiver",class="reciever_microwave",model="models/slyfo_2/miscequipmentradiodish.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Emitter",class="generator_microwave",model="models/slyfo/finfunnel.mdl"})

--Fission Reactor
--Environments.RegisterDevice("Generators", "Fission Generator", "Basic Fission Reactor", "generator_fission", "models/SBEP_community/d12siesmiccharge.mdl")

--WaterPumps
local Path={"Resource Production","LifeSupport","Water","Water Pump"}
Environments.RegisterDevice({Path=Path,DevName="Small Water Pump",class="generator_water",model="models/maxofs2d/thruster_propeller.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Large Water Pump",class="generator_water",model="models/maxofs2d/hover_propeller.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Industrial Water Pump",class="generator_water",model="models/environmentsx/liquidpump.mdl"})

--Atmospheric Water Generator
local Path={"Resource Production","LifeSupport","Water","Atmospheric Water Generator"}
Environments.RegisterDevice({Path=Path,DevName="Atmospheric Water Generator",class="generator_water_tower",model="models/Slyfo/moisture_condenser.mdl"})

--Hydroponics
local Path={"Resource Production","LifeSupport","Oxygen","HydroPonics"}
Environments.RegisterDevice({Path=Path,DevName="Hydroponics Bush",class="generator_plant",model="models/props_foliage/tree_deciduous_03b.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Hydroponics Tree",class="generator_plant",model="models/props_foliage/tree_deciduous_03a.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Hydroponics Large Tree",class="generator_plant",model="models/props_foliage/tree_deciduous_01a.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Hydroponics Potted plant",class="generator_plant",model="models/props/cs_office/plant01.mdl"})

--WaterSplitters
local Path={"Resource Production","LifeSupport","Oxygen","Water Splitter"}
Environments.RegisterDevice({Path=Path,DevName= "Electrolysis Generator",class="generator_water_to_air",model="models/slyfo/electrolysis_gen.mdl"})
Environments.RegisterDevice({Path=Path,DevName= "Electrolysis Generator Compact",class="generator_water_to_air",model="models/sbep_community/d12airscrubber.mdl",toolsettings={Ang=Angle(),OffDir=Vector(0,1,0)}})







----Life Support Tool----
Environments.RegisterNodeIcon({"Life Support"},"icon16/group_link.png")
--Suit Dispener
local Path={"Life Support","Suit Dispensers"}
Environments.RegisterDevice({Path=Path,DevName="Suit Dispenser Compact",class="suit_dispenser",model="models/props_combine/combine_emitter01.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Suit Dispenser",class="suit_dispenser",model="models/props_combine/suit_charger001.mdl",extra={animated=true},toolsettings={Ang=Angle(),OffDir=Vector(0,1,0)}})

--Medical Dispenser
local Path={"Life Support","Medical Dispensers"}
Environments.RegisterDevice({Path=Path,DevName="Medical Dispenser Compact",class="env_health",model="models/Items/HealthKit.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Medical Dispenser",class="env_health",model="models/props_combine/health_charger001.mdl"})

--LS Cores
local Path={"Life Support","LS Core"}
Environments.RegisterDevice({Path=Path,DevName="LS Core",class="env_lscore",model="models/sbep_community/d12airscrubber.mdl"})
Environments.RegisterDevice({Path=Path,DevName="SmallBridge LS Core",class="env_lscore",model="models/smallbridge/life support/sbclimatereg.mdl"})

--AtmosProbe
local Path={"Life Support","Atmospheric Probe"}
Environments.RegisterDevice({Path=Path,DevName="Compact Atmospheric Probe",class="env_probe",model="models/environmentsx/atmosensor.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Atmospheric Probe",class="env_probe",model="models/props_combine/combine_mine01.mdl"})






----Mining Devices...----
--Environments.RegisterEnt("mining_laser", 800, 500, 12) Environments.RegisterEnt("resource_drill", 1500, 850, 14) Environments.RegisterEnt("resource_scanner", 1200, 350, 10)

Environments.RegisterNodeIcon({"Resource Production","Mining"},"icon16/cog_add.png")

--# Mining Laser
local Path={"Resource Production","Mining","Lasers"}
Environments.RegisterDevice({Path=Path,DevName="Standard Mining Laser",class="mining_laser",model="models/Slyfo_2/pss_missilepod.mdl"})

--# Surface Drills
local Path={"Resource Production","Mining","Drills"}
Environments.RegisterDevice({Path=Path,DevName="Offset Drill Rig",class="resource_drill",model="models/Slyfo/rover_drillbase.mdl",extra={env_extra=1}})
Environments.RegisterDevice({Path=Path,DevName="Standard Drill Platform",class="resource_drill",model="models/Slyfo/drillplatform.mdl",extra={env_extra=2}})
Environments.RegisterDevice({Path=Path,DevName="Basic Drill Rig",class="resource_drill",model="models/Slyfo/drillbase_basic.mdl",extra={env_extra=3}})

--# Resource Scanners
local Path={"Resource Production","Mining","Scanners"}
Environments.RegisterDevice({Path=Path,DevName="Huge Resource Scanner",class="resource_scanner",model="models/Slyfo_2/sattelite_doomray.mdl",extra={env_extra=12000}})
Environments.RegisterDevice({Path=Path,DevName="Large Resource Scanner",class="resource_scanner",model="models/Slyfo/sat_relay.mdl",extra={env_extra=8192}})
Environments.RegisterDevice({Path=Path,DevName="Medium Resource Scanner",class="resource_scanner",model="models/Slyfo/searchlight.mdl",extra={env_extra=2000}})
Environments.RegisterDevice({Path=Path,DevName="Small Resource Scanner",class="resource_scanner",model="models/Slyfo/rover1_spotlight.mdl",extra={env_extra=1000}})






----Storage Tool----
--Water
local Path={"Storage","Water"}  local Stores = {water={100,0}}
Environments.RegisterDevice({Path=Path,DevName="Massive Water Tank",class="env_water_storage",model="models/props/de_nuke/storagetank.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Water Shipping Crate",class="env_water_storage",model="models/slyfo/crate_resource_large.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Small Water Crate",class="env_water_storage",model="models/slyfo/crate_watersmall.mdl",res_store=Stores})

Environments.RegisterDevice({Path=Path,DevName="Small Water Tank",class="env_water_storage",model= "models/slyfo/sat_resourcetank.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Medium Water Tank",class="env_water_storage",model="models/mandrac/water_storage/water_storage_small.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Large Water Tank",class="env_water_storage",model="models/mandrac/water_storage/water_storage_large.mdl",res_store=Stores})

--Energy
local Path={"Storage","Energy"} local Stores = {energy={100,0}}
Environments.RegisterDevice({Path=Path,DevName="Substation Capacitor",class="env_energy_storage",model="models/props_c17/substation_stripebox01a.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Substation Backup Battery",class="env_energy_storage",model="models/props_c17/substation_transformer01a.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Large Capacitor",class="env_energy_storage",model="models/mandrac/energy_cell/large_cell.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Medium Capacitor",class="env_energy_storage",model="models/mandrac/energy_cell/medium_cell.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Small Capacitor",class="env_energy_storage",model="models/mandrac/energy_cell/small_cell.mdl",res_store=Stores})

--Oxygen 
local Path={"Storage","Oxygen"} local Stores = {oxygen={100,0}}
Environments.RegisterDevice({Path=Path,DevName="Large Oxygen Storage",class="env_oxygen_storage",model="models/props_wasteland/coolingtank02.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Oxygen Shipping Tank",class="env_oxygen_storage",model="models/slyfo/crate_resource_large.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Large Compressed Oxygen Crate",class="env_oxygen_storage",model="models/mandrac/oxygen_tank/oxygen_tank_large.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Medium Compressed Oxygen Crate",class="env_oxygen_storage",model="models/mandrac/oxygen_tank/oxygen_tank_medium.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Small Compressed Oxygen Crate",class="env_oxygen_storage",model="models/mandrac/oxygen_tank/oxygen_tank_small.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Compact Oxygen Tank",class="env_oxygen_storage",model="models/environmentsx/gastank.mdl",res_store=Stores})

--Nitrogen 
local Path={"Storage","Nitrogen"} local Stores = {nitrogen={100,0}}
Environments.RegisterDevice({Path=Path,DevName= "Large Nitrogen Storage",class="env_nitrogen_storage",model= "models/props_wasteland/coolingtank02.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName= "Nitrogen Shipping Tank",class="env_nitrogen_storage",model= "models/slyfo/crate_resource_large.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName= "Compact Nitrogen Tank",class="env_nitrogen_storage",model=  "models/environmentsx/gastank.mdl",res_store=Stores})

--Hydrogen 
local Path={"Storage","Hydrogen"} local Stores = {hydrogen={100,0}}
Environments.RegisterDevice({Path=Path,DevName="Large Hydrogen Storage",class="env_hydrogen_storage",model= "models/props_wasteland/coolingtank02.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Hydrogen Shipping Tank",class="env_hydrogen_storage",model="models/slyfo/crate_resource_large.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Compact Hydrogen Tank",class="env_hydrogen_storage",model="models/environmentsx/gastank.mdl",res_store=Stores})

--Co2
local Path={"Storage","CO2"} local Stores = {} Stores["carbon dioxide"]={100,0}
Environments.RegisterDevice({Path=Path,DevName="Large CO2 Storage",class="env_co2_storage",model="models/props_wasteland/coolingtank02.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Compact CO2 Storage",class="env_co2_storage",model="models/environmentsx/gastank.mdl",res_store=Stores})

--Resource Cache
local Path={"Storage","Resource Cache"} local Stores = {} local Per = 100/6
Stores["carbon dioxide"]={Per,0} Stores["hydrogen"]={Per,0} Stores["nitrogen"]={Per,0} Stores["oxygen"]={Per,0} Stores["energy"]={Per,0} Stores["water"]={Per,0}
Environments.RegisterDevice({Path=Path,DevName="Modular Unit X-01",class="env_cache_storage",model="models/Spacebuild/milcock4_multipod1.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Slyfo Tank 1",class="env_cache_storage",model="models/slyfo/t-eng.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Slyfo Power Crystal",class="env_cache_storage",model="models/Slyfo/powercrystal.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="SmallBridge Small Wall Cache",class="env_cache_storage",model="models/SmallBridge/Life Support/SBwallcacheS.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="SmallBridge Large Wall Cache",class="env_cache_storage",model="models/SmallBridge/Life Support/SBwallcacheL.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="SmallBridge External Wall Cache",class="env_cache_storage",model="models/SmallBridge/Life Support/SBwallcacheE.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="SmallBridge Small Wall Cache (half length)",class="env_cache_storage",model="models/SmallBridge/Life Support/SBwallcacheS05.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="SmallBridge Large Wall Cache (half length)",class="env_cache_storage",model="models/SmallBridge/Life Support/SBwallcacheL05.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="SmallBridge Hull Cache",class="env_cache_storage",model="models/SmallBridge/Life Support/sbhullcache.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Mandrac Cargo Cache",class="env_cache_storage",model="models/mandrac/resource_cache/cargo_cache.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Mandrac Huge Cache",class="env_cache_storage",model="models/mandrac/resource_cache/colossal_cache.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Mandrac Hanger Container",class="env_cache_storage",model="models/mandrac/resource_cache/hangar_container.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Mandrac Large Cache",class="env_cache_storage",model="models/mandrac/resource_cache/huge_cache.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Mandrac Medium Cache",class="env_cache_storage",model="models/mandrac/resource_cache/large_cache.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Mandrac Small Cache",class="env_cache_storage",model="models/mandrac/resource_cache/medium_cache.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Mandrac Tiny Cache",class="env_cache_storage",model="models/mandrac/resource_cache/small_cache.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Mandrac Levy Cache",class="env_cache_storage",model="models/mandrac/resource_cache/nitro_large.mdl",res_store=Stores})
Environments.RegisterDevice({Path=Path,DevName="Simple Cache",class="env_cache_storage",model="models/lt_c/sci_fi/box_crate.mdl",res_store=Stores})






----Ship Utilities----
--Extra
local Path={"Utilities","Extra"}
Environments.RegisterDevice({Path=Path,DevName="Vehicle Exit Point",class="EPoint",model="models/jaanus/wiretool/wiretool_range.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Matter Transporter",class="wep_transporter",model="models/SBEP_community/d12shieldemitter.mdl"})
Environments.RegisterDevice({Path=Path,DevName="WarpDrive",class="wep_transporter",model="models/Slyfo/ftl_drive.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Cloning Device",class="envx_clonetube",model="models/TwinbladeTM/cryotubemkii.mdl"})

--Trade Console
local Path={"Utilities","Trade Console"}
Environments.RegisterDevice({Path=Path,DevName="Deluxe TradeConsole",class="env_tradeconsole",model= "models/SBEP_community/errject_smbwallcons.mdl"})
Environments.RegisterDevice({Path=Path,DevName="Compact TradeConsole",class="env_tradeconsole",model= "models/props_lab/reciever_cart.mdl"})

--Item Fabricator
local Path={"Utilities","Fabricators"}
Environments.RegisterDevice({Path=Path,DevName="Item Materialiser",class="env_factory",model="models/slyfo/swordreconlauncher.mdl"})

local Path={"Combat","Weapon Mounts","Turrets"}
Environments.RegisterDevice({Path=Path,DevName="Medium Turret",class="turret",model="models/slyfo/smlturretbase.mdl",extra={TurretType=1}})


local DisplayFunc = function(setting_list,settings)
    local Loss = -2
    
    for sid,sval  in pairs( settings ) do if sval == true then Loss=Loss+2 end end

    Info = vgui.Create( "DLabel" ) 
    Info:SetText("Storage Effiency Loss: "..tostring(math.Clamp(Loss,0,100)).."%")
    Info:SetDark( true )
    setting_list:Add( Info ) table.insert(setting_list.Infos,Info)
end

local Settings = {
    Type = "Properties",
    Catagory = "mod_storage",
    settings = {}
}

local Basic_List = { energy="Energy", steam="Steam", water="Water", oxygen="Oxygen", nitrogen="Nitrogen", hydrogen="Hydrogen" } 
    Basic_List["carbon dioxide"]="Carbon Dioxide"

local Mining_List = {Carbon = "Carbon"}
    Mining_List["Raw Ore"]="Raw Ore"
    Mining_List["Refined Ore"]="Refined Ore"
    Mining_List["Hardened Ore"]="Hardened Ore"
    Mining_List["Crystalized Polylodarium"]="Crystalized Polylodarium"
    Mining_List["Liquid Polylodarium"]="Liquid Polylodarium"

local Combat_List = { Casings="Casings" }
    Combat_List["Basic Shells"]="Basic Shells"
    Combat_List["Basic Rounds"]="Basic Rounds"
    Combat_List["Heavy Shells"]="Heavy Shells"
    Combat_List["Missile Parts"]="Missile Parts"

local Exotic_list = { AntiMatter="AntiMatter", BlackHolium="BlackHolium" }

for sid,sval in pairs( { Lifesupport=Basic_List, Industrial=Mining_List, Military=Combat_List, Exotic=Exotic_list } ) do 
    Settings.settings[sid]={} 
    for soid,soval  in pairs( sval ) do
        Settings.settings[sid][soid]={soval,"Boolean",false}
    end
end

local Basic_Models = {}
    Basic_Models["Hull Storage"] = "models/SmallBridge/Life Support/sbhullcache.mdl"
    Basic_Models["Small Wall Storage"] = "models/SmallBridge/Life Support/SBwallcacheS.mdl"
    Basic_Models["Large Wall Storage"] = "models/SmallBridge/Life Support/SBwallcacheL.mdl"
    Basic_Models["External Storage"] = "models/SmallBridge/Life Support/SBwallcacheE.mdl"
    Basic_Models["Small Wall Storage HL"] = "models/SmallBridge/Life Support/SBwallcacheS05.mdl"
    Basic_Models["Large Wall Storage HL"] = "models/SmallBridge/Life Support/SBwallcacheL05.mdl"
    Basic_Models["Mandrac Cargo Storage"]="models/mandrac/resource_cache/cargo_cache.mdl"
    Basic_Models["Mandrac Huge Storage"]="models/mandrac/resource_cache/colossal_cache.mdl"
    Basic_Models["Mandrac Hanger Container"]="models/mandrac/resource_cache/hangar_container.mdl"
    Basic_Models["Mandrac Large Storage"]="models/mandrac/resource_cache/huge_cache.mdl"
    Basic_Models["Mandrac Medium Storage"]="models/mandrac/resource_cache/large_cache.mdl"
    Basic_Models["Mandrac Small Storage"]="models/mandrac/resource_cache/medium_cache.mdl"
    Basic_Models["Mandrac Tiny Storage"]="models/mandrac/resource_cache/small_cache.mdl"
    Basic_Models["Mandrac Levy Storage"]="models/mandrac/nitrogen_tank/nitro_large.mdl"
    Basic_Models["Simple Storage"]="models/lt_c/sci_fi/box_crate.mdl"

    Basic_Models["Wait what"]="models/props_junk/garbage_plasticbottle003a.mdl"
    
local Mining_Models = {}
    Mining_Models["Small Wall Storage"] = "models/SmallBridge/Life Support/SBwallcacheS.mdl"
    Mining_Models["Large Wall Storage"] = "models/SmallBridge/Life Support/SBwallcacheL.mdl"
    Mining_Models["Huge Mandrac Storage"] = "models/mandrac/ore_container/ore_large.mdl"
    Mining_Models["Medium Mandrac Storage"] = "models/mandrac/ore_container/ore_medium.mdl"

local Combat_Models = {}
    Combat_Models["Thin Wall Storage"]="models/SmallBridge/Life Support/sbwallcaches05.mdl"
    Combat_Models["Barrel Storage"]="models/Slyfo/barrel_refined.mdl"
    Combat_Models["Thick Wall Storage"]="models/smallbridge/life support/sbwallcachel05.mdl"
    Combat_Models["Wide Thin Wall Storage"]="models/smallbridge/life support/sbwallcaches.mdl"
    Combat_Models["Wide Thick Wall Storage"]="models/smallbridge/life support/sbwallcachel.mdl"
    Combat_Models["Exterior Storage"]="models/SmallBridge/Life Support/sbwallcaches05.mdl"
    Combat_Models["Ammo Box"]="models/Items/BoxMRounds.mdl"

local Storages = {
    Basic = Basic_Models,
    Mining = Mining_Models,
    Combat = Combat_Models
}

local Path={"Resource Storage"}
for sid,sval  in pairs( Storages ) do 
    
    for mid,mval  in pairs( sval ) do
        Environments.RegisterDevice({
            Path=Path,
            settings={Settings},
            displayinfo=DisplayFunc,
            DevName="Modular "..tostring(mid),
            class="env_modular_storage",
            model=mval,
            nodeicon="icon16/folder_wrench.png"
        })      
    end
end

Environments.RegisterNodeIcon(Path,"icon16/box.png")
Environments.RegisterToolTip(Path,"Devices used for storing resources, customisable in what they can store.")
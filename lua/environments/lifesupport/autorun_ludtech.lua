AddCSLuaFile("environments/lifesupport/autorun_ludtech.lua") 

LDE = LDE or {}
local LDE = LDE

print("=========================")
print("LDE Starting up.")
print("=========================")

local LoadFile = EnvX.LoadFile --Lel Speed.
local P = "lde/"

LoadFile(P.."lde_variables.lua",2)
LoadFile(P.."sh_constraints.lua",1)

--LoadFile(P.."sh_debug.lua",1)
--LoadFile(P.."sh_utility.lua",1)
--LoadFile(P.."sh_networking.lua",1)
LoadFile(P.."lde_ownership.lua",1)

LoadFile(P.."lde_core.lua",1)
LoadFile(P.."lde_unlocksystem.lua",1)
LoadFile(P.."lde_mechadds.lua",2)
LoadFile(P.."lde_weaponcore.lua",1)
LoadFile(P.."lde_shipcore.lua",1)
LoadFile(P.."lde_lifecore.lua",1)
LoadFile(P.."lde_playerpersist.lua",1)
LoadFile(P.."lde_statsystem.lua",1)
LoadFile(P.."lde_variables.lua",1)
LoadFile(P.."lde_spaceanons.lua",1)
LoadFile(P.."lde_sporeai.lua",1)
LoadFile(P.."lde_npccontrol.lua",1)
LoadFile(P.."lde_overrides.lua",1)
LoadFile(P.."lde_effectcore.lua",1)
LoadFile(P.."lde_factioncore.lua",1)
LoadFile(P.."class/faction.lua",1)

LoadFile(P.."lde_userinterface.lua",1)

LoadFile(P.."lde_damagecontrol.lua",2)
LoadFile(P.."lde_sporeai.lua",2)
LoadFile(P.."lde_envheat.lua",2)
LoadFile(P.."lde_ludos.lua",2)

LoadFile(P.."sh_scaling.lua",1)

if(SERVER)then

	resource.AddFile("materials/Space_Combat/Shield/shield_01.vmt")
	resource.AddFile("sound/tech/sga_impact_01.wav")
	resource.AddFile("sound/tech/sga_impact_02.wav")
	resource.AddFile("sound/tech/sga_impact_03.wav")
	resource.AddFile("sound/tech/sga_impact_04.wav")

	LDE.Installed = true

else
	language.Add( "env_explosion", "Meteor" )
end

print("=========================")
print("LDE Installed, Have fun. :)")
print("=========================")
local EnvX = EnvX --Localise the global table for speed.
local Utl = EnvX.Utl --Makes it easier to read the code.
local NDat = Utl.NetMan --Ease link to the netdata table.

EnvX.MegaGui =  EnvX.MegaGui or {}
local MG = EnvX.MegaGui
MG.ToolSettings = MG.ToolSettings or {Tool={Frozen=true,Weld=true,NoCollide=true},Settings={},Cache={}}
local ToolSettings = MG.ToolSettings
	
function MG.UpdateTable(Old,New,over)
	for n,b in pairs( New ) do
		if type(b)=="table" then
			if over then Old[n] = {} else Old[n] = Old[n] or {} end
			MG.UpdateTable(Old[n],b)
		else
			Old[n]=b
		end
	end
end


if(SERVER)then
	--Incase theres something serverside we need to do...
	
	function MG.GetToolSettings(Ply)
		if Ply.ToolSettings~=nil then return Ply.ToolSettings end
		Ply.ToolSettings = table.Copy(ToolSettings) 
		return Ply.ToolSettings
	end
	
	local function ReceiveSettings(Dat,Ply)
		
		--print("Recieved Settings!") PrintTable(Dat)

		MG.UpdateTable(MG.GetToolSettings(Ply),Dat[1],Dat[2])
	end
	EnvX:HookNet("SpawnerData",ReceiveSettings)
	
else
	MG.IsLoaded = false

	--ToolGun Root Nodes Order
	local RootOrder = {
		"Resource Node",
		"Ship Core",
		"Combat",
		"Resource Production",
		"Resource Storage",
		"Life Support",
		"Heat Management",
		"Utilities",
		--"Generators",
		--"Mining",
		--"Ammunition",
		--"Storage",
	}
	
	function MG.SendSettings(overwrite,mini)
		EnvX:NetworkData("SpawnerData",{mini or ToolSettings,overwrite or false})
	end
	
	function MG.CheckForSetting(table,settingcat,settingid)
		if table[settingcat] == nil then return end
		return table[settingcat][settingid]
	end

	local CFS = MG.CheckForSetting
	function MG.GetSetting(settingcat,settingid,default)
		local SettingsExist = CFS(ToolSettings,"Settings",settingid)
		if SettingsExist ~= nil then
			--print("Found setting "..tostring(settingid))
			return SettingsExist
		else
			local Cached = CFS(ToolSettings.Cache,settingcat,settingid)
			if Cached ~= nil then
				--print("Setting was cached! "..tostring(settingid))
				MG.SetSetting(settingcat,settingid,Cached)
				return Cached
			end
		end
		--print("Using default! "..tostring(settingid))
		return default
	end
	
	function MG.SetSetting(settingcat,settingid,value)
		local Sets = {}
		Sets[settingid] = value

		local Caching = {}
		Caching[settingcat] = {}
		Caching[settingcat][settingid] = value

		MG.UpdateTable(ToolSettings.Settings,Sets)
		MG.UpdateTable(ToolSettings.Cache,Caching)
		MG.SendSettings(false,{Settings=Sets})
	end

	function MG.SetModelDat(settingcat,values)
		local Sets = {}
		Sets[settingcat] = values
		MG.UpdateTable(ToolSettings,Sets)
		MG.SendSettings(false,Sets)
	end

	function MG.GetToolSetting(settingid,default)
		local SettingsExist = CFS(ToolSettings,"Tool",settingid)
		if SettingsExist ~= nil then
			return SettingsExist
		else
			local Cached = CFS(ToolSettings.Cache,"Tool",settingid)
			if Cached ~= nil then
				MG.GetToolSetting(settingid,Cached)
				return Cached
			end
		end
		return default
	end

	function MG.SetToolSetting(settingid,value)
		local Sets = {}
		Sets[settingid] = value

		Sets = {Tool=Sets}

		MG.UpdateTable(ToolSettings,Sets) 
		MG.UpdateTable(ToolSettings.Cache,Sets)
		MG.SendSettings(false,Sets)
	end

	function MG.GenerateNode(name,data,parent,tree,path)
		local node = tree:AddNode( name )
		local path = table.Copy(path) table.insert(path,name)
		
		if data.NodeDesc~=nil then
			node:SetTooltip(data.NodeDesc)
		end
		
		if data.ModelList then
			node.ModelList = data
			node.Path = path
							
			function node:DoClick()
				parent.Grid:SetupList(self.Path,self.ModelList)
				parent.ModelCat:DoExpansion(true)
			end

			data.NodeIcon = data.NodeIcon or "icon16/folder_brick.png"
		else
			MG.ParseTable(data,parent,node,path)
		end

		if data.NodeIcon~=nil then
			node.Icon:SetImage(data.NodeIcon)
		end
	end

	function MG.ParseTable(data,parent,tree,path)--Setup system to allow unique icons for each node.
		for n,b  in pairs( data ) do
			if type(b)=="table" then
				MG.GenerateNode(n,b,parent,tree,path)
			else
				if (n ~= "NodeDesc") and (n ~= "NodeIcon") then
					print("Error: "..tostring(n).." is typed wrong! "..type(b))
					PrintTable(path)
				end
			end
		end
	end

	function MG.SetupNodeRoots(parent,tree)
		local ToolData = Environments.Tooldata

		--PrintTableSlice(ToolData)

		for ind,name  in pairs( RootOrder ) do
			MG.GenerateNode(name,ToolData[name],parent,tree,{})		
		end
	end

	function MG.InitGUI(parent,self)
		local tree = vgui.Create( "DTree", parent )
		tree:SetSize( parent:GetWide(), 250 )
		
		MG.SetupNodeRoots(parent,tree)
		--MG.ParseTable(Environments.Tooldata,parent,tree,{})
		
		MG.SendSettings()
						
		parent:Add(tree)
		
		local ModelCat = vgui.Create( "DCollapsibleCategory" )
		ModelCat:SetLabel( "Model Selection" )
		ModelCat:SetExpanded( false )
		
		parent:Add(ModelCat) parent.ModelCat = ModelCat
		
		local ModelPanel = vgui.Create( "DPanel", ModelCat )
		ModelPanel:SetSize(parent:GetWide(),250)	
		function ModelPanel:Paint(w,h) draw.RoundedBox( 3, 0, 0, w, h, Color(200,200,200,255) ) end
		
		ModelCat:SetContents( ModelPanel )	
		
		
		local scroll = vgui.Create( "DScrollPanel", ModelPanel )
		scroll:SetSize( parent:GetWide(), 250 )
		
		local grid = vgui.Create( "DIconLayout",scroll )
		grid:SetSize( parent:GetWide(), 250 )
		grid:SetSpaceY( 5 ) --Sets the space in between the panels on the X Axis by 5
		grid:SetSpaceX( 5 ) --Sets the space in between the panels on the Y Axis by 5
		
		grid.Icons = {}
		grid.Tool = self
		
		parent.Grid = grid
		
		function grid:SetupList(ID,Path)			
			if table.Count(self.Icons)>0 then
				for i,v in pairs( self.Icons ) do v:Remove() end
				self.Icons = {}
			end
			
			--Handle the settings bit.	
			local SetList = self.ESettings
			if table.Count(SetList.Settings)>0 then
				for i,v in pairs( SetList.Settings ) do v:Remove() end
				SetList.Settings = {}
			end
			
			--PrintTable(Path)
			
			for class,ign in pairs( Path.Classes ) do
				local EntDat = Environments.EntityData[class]
				for i,v in pairs( EntDat.Ents ) do
					if type(v)=="table" then
						local BackGround = vgui.Create( "DPanel" ) 
						local icon = vgui.Create( "SpawnIcon",BackGround ) util.PrecacheModel(v.model)
						BackGround:SizeToContents() BackGround:SetHeight(BackGround:GetWide())
						
						icon:SetModel(v.model, v.skin or 0)
										
						icon.tool = self.Tool
						icon.model = v.model
						icon.class = v.class
						icon.skin = v.skin
						icon.devname = i
						icon.devid = ID
						icon.description = v.description
						if v.tooltip then icon:SetTooltip(v.tooltip) else icon:SetTooltip(i) end
						icon.DoClick = function( self )
							self.tool.Model = self.model

							MG.SetModelDat("Tool",{
								Path=util.TableToJSON(self.devid),
								Class=self.class,
								SType=self.devname,
								Model=self.model
							})
						end
						self:Add( BackGround )
						
						table.insert(self.Icons,BackGround)
					end
				end
			end
			
			local function UpdateInfo()
				local InfList = self.Einfo
				if table.Count(InfList.Infos)>0 then
					for i,v in pairs( InfList.Infos ) do v:Remove() end
					InfList.Infos = {}
				end	
			
				local InfoDisplay = Path.DisplayInfo
				if InfoDisplay~=nil then
					InfoDisplay(InfList,ToolSettings.Settings)
				end
			end
			UpdateInfo()
			
			ToolSettings.Settings = {}
			MG.SendSettings(true)

			--Now the settings.
			local settings = Path.Settings
			--PrintTable(Path)
			if settings~=nil and table.Count(settings)>0 then
				self.ESetCat:DoExpansion(true)

				for sid,sval  in pairs( settings ) do
					local Type = sval.Type
			
					if Type == "DropDown" then
						local Set = vgui.Create( "DComboBox" )
						Set:SetSize( 100, 20 )
						Set:SetValue(MG.GetSetting(sval.Catagory,sid,sid..": Select One"))
						
						Set.Grid = self
						
						for soid,soval  in pairs( sval.Options ) do
							Set:AddChoice(soval)
							Set.OnSelect = function( panel, index, value )
								MG.SetSetting(sval.Catagory,sid,value)
								UpdateInfo()								
							end
						end
						
						SetList:Add(Set)
						table.insert(SetList.Settings,Set)
					elseif Type == "Properties" then
						--print("Properties")
						local DProp = vgui.Create("DProperties")
						SetList:Add(DProp)
						DProp:Dock( FILL )
						table.insert(SetList.Settings,DProp)

						for catid,catagory  in pairs( sval.settings ) do
							for setid,setval  in pairs( catagory ) do

								local Set = DProp:CreateRow( catid , setval[1] )
								Set:Setup(setval[2]) Set.Type = setval[2]

								--print("Value for "..tostring(setid).." "..tostring(MG.GetSetting(sval.Catagory,setid,setval[3])))
								Set:SetValue(MG.GetSetting(sval.Catagory,setid,setval[3]))

								function Set:DataChanged(val,val2)
									if self.Type == "Boolean" then
										MG.SetSetting(sval.Catagory,setid,val>0)
									else
										--Need to add the other types of variables
									end
									UpdateInfo()
								end
							end
						end
					end
				end
			else
				self.ESetCat:DoExpansion(false)
				MG.SendSettings()
			end
		end

		local ESettings = vgui.Create( "DCollapsibleCategory" ) parent:Add(ESettings)
		ESettings:SetLabel( "Entity Settings" ) ESettings:SetExpanded( false ) ESettings:SetSize(parent:GetWide(),300)	
		local sets = vgui.Create( "DListLayout" ) sets:SetSize( parent:GetWide(), 100 ) ESettings:SetContents( sets )
		local info = vgui.Create( "DListLayout" ) info:SetSize( parent:GetWide(), 100 ) sets:Add(info)-- ESettings:SetContents( info )
		grid.ESettings = sets sets.Settings = {} grid.ESetCat = ESettings grid.Einfo = info info.Infos = {}
		
		local ToolSetCat = vgui.Create( "DCollapsibleCategory" ) ToolSetCat:SetLabel( "Tool Settings" ) parent:Add(ToolSetCat) 
		local layout = vgui.Create( "DListLayout" ) layout:SetSize( parent:GetWide(), 100 ) ToolSetCat:SetContents( layout )	
		
		local FreezeCheck = vgui.Create( "DCheckBoxLabel" ) layout:Add(FreezeCheck)
		FreezeCheck:SetText( "Spawn Frozen" ) FreezeCheck:SetValue(MG.GetToolSetting("Frozen",true))
		FreezeCheck:SetTextColor(Color(0,0,0))
		FreezeCheck:SizeToContents() function FreezeCheck:OnChange( val ) MG.SetToolSetting("Frozen",val) end
		
		local WeldCheck = vgui.Create( "DCheckBoxLabel" ) layout:Add(WeldCheck)
		WeldCheck:SetText( "Weld Entities" ) WeldCheck:SetValue(MG.GetToolSetting("Weld",true))
		WeldCheck:SetTextColor(Color(0,0,0))
		WeldCheck:SizeToContents() function WeldCheck:OnChange( val ) MG.SetToolSetting("Weld",val) end
		
		local NoCollideCheck = vgui.Create( "DCheckBoxLabel" ) layout:Add(NoCollideCheck)
		NoCollideCheck:SetText( "Nocollide Entities" ) NoCollideCheck:SetValue(MG.GetToolSetting("NoCollide",true))
		NoCollideCheck:SetTextColor(Color(0,0,0))
		NoCollideCheck:SizeToContents() function NoCollideCheck:OnChange( val ) MG.SetToolSetting("NoCollide",val) end
		
	end
end		




TOOL.Category = "Tools"
TOOL.Name = "EnvX Spawner"
TOOL.Command = nil
TOOL.ConfigName = ""
TOOL.Tab = "EnvX"

TOOL.Mode = "envx_megaspawner"

local cleanupgroup = "EnvX Device"
TOOL.CleanupGroup = cleanupgroup
TOOL.Language = {Undone=cleanupgroup.." Removed",Cleanup=cleanupgroup,Cleaned="Removed all "..cleanupgroup,SBoxLimit="Hit the "..cleanupgroup.." limit"}

local self = TOOL
local name = TOOL.Mode

cleanup.Register(cleanupgroup)
if CLIENT then
    language.Add("Tool."..TOOL.Mode..".name", "EnvironmentsX Spawner")
    language.Add("Tool."..TOOL.Mode..".desc", "Spawn Ship Entities")
    language.Add("Tool."..TOOL.Mode..".0", "Left click to perform an action.")
	
	for k,v in pairs(TOOL.Language) do
		language.Add(k.."_"..cleanupgroup,v)
	end
else
	CreateConVar("sbox_max"..cleanupgroup,100)
end

function TOOL:CreateDevice(ply, trace, class)
	if not ply:CheckLimit(self.CleanupGroup) then return false end
	if not class then return false end
	local ent = ents.Create(class)
	if not IsValid(ent) then return false end
	
	local info = self:GetDeviceInfo()
	
	local offsets = info.ToolSettings or {}
	
	--[[ old spawn code
	local PosOff = ent:OBBMins().z if offsets.OffDir then PosOff = ent:OBBMins()*offsets.OffDir end
	local AngOff = Angle(90,0,0) if offsets.Ang then AngOff = offsets.Ang end
	
	--print("Settings?")
	--PrintTable(offsets)
	
	-- Pos/Model/Angle
	ent:SetModel( Model )
	ent:SetPos( trace.HitPos - trace.HitNormal * PosOff )
	ent:SetAngles( trace.HitNormal:Angle() + AngOff )
	]]

	-- Get the model
	local Model = self:GetDeviceModel()
	if not Model then return false end

	--Do this first so we can get proper dimensions
	ent:SetModel( Model )
	
	--Rotate before setpos, since rotations happen around origin and can change position
	local AngOff = Angle(90,0,0) if offsets.Ang then AngOff = offsets.Ang end
	ent:SetAngles( trace.HitNormal:Angle() + AngOff )
	
	--Automagically create an offset based on bounding box and rotation
	local HeightOffset = Vector(0,0,0)

	if AngOff == Angle(90,0,0) or AngOff == Angle(-90,0,0) then HeightOffset = (ent:OBBMaxs().z - ent:OBBMins().z)*0.5
	elseif AngOff == Angle(0,90,0) or AngOff == Angle(0,-90,0) then HeightOffset = (ent:OBBMaxs().y - ent:OBBMins().y)*0.5
	else HeightOffset = (ent:OBBMaxs().x - ent:OBBMins().x)*0.5
	end
	
	--Correct for the difference between origin(coordinate center) and bounding box center, then set the position
	local CoordinateOffset = ent:LocalToWorld(ent:OBBCenter()) - ent:GetPos()
	ent:SetPos( trace.HitPos - CoordinateOffset + trace.HitNormal * HeightOffset )

	ent:SetPlayer(ply)
	ent:CPPISetOwner(ply) -- Just adding this here
	
	if info.skin then
		ent:SetSkin(info.skin)
	end
	
	ent.env_extra_data = info.extra

	ent.envx_spawn_settings = table.Copy(self:GetSettings().Settings)
	--print("Tool Settings") PrintTable(self:GetSettings()) print("Done")

	ent.envx_res_base_stats = info.resources --Copy the base resource stats to the entity.
	
	if info.extra then
		for i, ext in pairs( info.extra ) do
			ent[i] = ext
		end
	end
	
	--ent:SetupEnvxResBase()
	ent:Spawn()
	ent:Activate()	
	
	----------------------------------------------
	--This stuff used to be in TOOL:LeftClick()
	--	LDE.UnlockCreateCheck(ply,ent) --Check if unlocked!

	if not IsValid(ent) then return false end
	
	if ent.AdminOnly then
		if not ply:IsAdmin() then
			ent:Remove()
			ply:ChatPrint("This device is admin only!")
		end
	end
	
	--effect :D
	if DoPropSpawnedEffect then
		DoPropSpawnedEffect(ent)
	end
	
	--constraints
	local weld = nil
	local nocollide = nil
	local phys = ent:GetPhysicsObject()
	local traceent = trace.Entity
	if not traceent:IsWorld() and not traceent:IsPlayer() then
		if self:GetToolSettings().Weld then
			weld = constraint.Weld( ent, trace.Entity, 0, trace.PhysicsBone, 0 )
		end
		if self:GetToolSettings().NoCollide then
			nocollide = constraint.NoCollide( ent, trace.Entity, 0, trace.PhysicsBone )
		end
	end

	if self:GetToolSettings().Frozen then
		phys:EnableMotion( false ) 
		ply:AddFrozenPhysicsObject( ent, phys )
	end
	
	--Counts and undos
	ply:AddCount( self.CleanupGroup, ent)
	ply:AddCleanup( self.CleanupGroup, ent )

	self:AddUndo(ply, ent, weld, nocollide)

	return true
end

function TOOL:LeftClick( trace )
	if CLIENT then return true end

	if not trace then return end
	local traceent = trace.Entity
	local ply = self:GetOwner()
	local DeviceClass = self:GetDeviceClass()
	
	if IsValid(traceent) and traceent:GetClass() == DeviceClass then
		if traceent.envx_spawn_settings ~= nil then 
			traceent.envx_spawn_settings = table.Copy(self:GetSettings().Settings)
			traceent:ReloadEnvxSettings()
			return true
		end
	end

	--If we got this far it means we are ment to spawn a entity
	self:CreateDevice( ply, trace, DeviceClass )
	return true
end

function TOOL:RightClick(trace)
    if CLIENT then return end
	
	return
end

function TOOL:Reload(trace)
	if CLIENT then return end
	
	return
end

function TOOL:AddUndo(p,...)
	undo.Create(self.CleanupGroup)
	for k,v in pairs({...}) do
		if k ~= "n" then
			undo.AddEntity(v)
		end
	end
	undo.SetPlayer(p)
	local name = self:GetDeviceClass()
	undo.SetCustomUndoText("Undone "..name)
	undo.Finish(name)
end

function TOOL:GetSettings()
	if SERVER then
		if self:GetOwner().ToolSettings~=nil then return self:GetOwner().ToolSettings end
		self:GetOwner().ToolSettings = {} return self:GetOwner().ToolSettings
	else
		return table.Copy(EnvX.MegaGui.ToolSettings)
	end
end

function TOOL:GetToolSettings() return self:GetSettings().Tool end
function TOOL:GetDeviceModel() return self:GetToolSettings().Model end --self:GetClientInfo("model")
function TOOL:GetDeviceClass() return self:GetDeviceInfo().class end

--return Environments.Tooldata[self:GetClientInfo("root")][self:GetClientInfo("category")][self:GetClientInfo("type")][self:GetClientInfo("sub_type")].class
function TOOL:GetDeviceInfo()
	local Settings = self:GetToolSettings()

	local EntDat = Environments.EntityData[Settings.Class]
	if EntDat~=nil then
		EntDat = EntDat.Ents[Settings.SType]
	end
	return EntDat or {}
end

if game.SinglePlayer() and SERVER or not game.SinglePlayer() and CLIENT then
	-- Ghosts, scary
	function TOOL:UpdateGhostEntity( ent, player )
		if not IsValid(ent) then return end
		local trace = player:GetEyeTrace()
		
		if trace.HitNonWorld then
			if trace.Entity:IsPlayer() or trace.Entity:IsNPC() then
				ent:SetNoDraw( true )
				return
			end
		end
		
		local Info = self:GetDeviceInfo().ToolSettings or {}
	
		local PosOff = ent:OBBMins().z if Info.OffDir then PosOff = ent:OBBMins()*Info.OffDir end
		local AngOff = Angle(90,0,0) if Info.Ang then AngOff = Info.Ang end

		ent:SetAngles( trace.HitNormal:Angle()+AngOff )
		ent:SetPos( trace.HitPos - trace.HitNormal * PosOff )
		
		ent:SetNoDraw( false )
	end
		
	function TOOL:Think()
		local model = self:GetDeviceModel()
		--PrintTable(self:GetSettings())
		if model then
			if not self.GhostEntity or not IsValid(self.GhostEntity) or self.GhostEntity:GetModel() ~= model then
				local trace = self:GetOwner():GetEyeTrace()
				self:MakeGhostEntity( model, trace.HitPos, trace.HitNormal:Angle()+Angle(90,0,0) , self:GetDeviceInfo().skin )
			end
		end
		self:UpdateGhostEntity( self.GhostEntity, self:GetOwner() )
	end
	
	function TOOL:MakeGhostEntity( model, pos, angle, skin ) --why do you spam so?
		-- Don't allow ragdolls/effects to be ghosts
		if model == nil or model == " " or model == "" then print("None existant model.") return end
		
		-- Release the old ghost entity
		self:ReleaseGhostEntity()
		
		if CLIENT and ents.CreateClientProp then
			self.GhostEntity = ents.CreateClientProp( model )
		else
			self.GhostEntity = ents.Create( "prop_physics" )
		end
		
		-- If there's too many entities we might not spawn..
		if not IsValid(self.GhostEntity) then
			self.GhostEntity = nil
			return
		end
		
		self.GhostEntity:SetModel( model )
		self.GhostEntity:SetPos( pos )
		self.GhostEntity:SetAngles( angle )
		self.GhostEntity:Spawn()
		if skin then self.GhostEntity:SetSkin(skin) end
		
		--self.GhostEntity:SetSolid( SOLID_VPHYSICS )
		self.GhostEntity:SetMoveType( MOVETYPE_NONE )
		self.GhostEntity:SetNotSolid( true )
		self.GhostEntity:SetRenderMode( RENDERMODE_TRANSALPHA )
		self.GhostEntity:SetColor( Color( 255, 255, 255, 150 ))
	end
end
	
local LoadFile = EnvX.LoadFile --Lel Speed.
LoadFile("weapons/gmod_tool/envx_megagui.lua",1)

function TOOL.BuildCPanel(panel)
    panel:SetSpacing(10)
    panel:SetName("Environments X Spawning Tool")

	local list = vgui.Create( "DListLayout",panel )
	list:SetSize( 280,400 )	
	
	function list:LoadData(tool)		
		if IsValid(self.sheet) then self.sheet:Remove() end
		local sheet = vgui.Create( "DListLayout", panel )
		sheet:SetSize( self:GetWide(), 400 )

		EnvX.MegaGui.InitGUI(sheet,tool)
		
		self:Add(sheet)
		self.sheet = sheet
	end
	
	panel:AddPanel(list)
	list:LoadData(self)
	
	panel.NextCheck = CurTime()
	function panel:Think()
		if self.NextCheck<CurTime() then
			if EnvX.MegaGui.IsLoaded == false then
				list:LoadData(self)
				EnvX.MegaGui.IsLoaded=true
			end
			self.NextCheck = CurTime()+1
		end
	end
end
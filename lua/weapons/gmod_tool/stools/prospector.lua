TOOL.Category = 'Mining'
TOOL.Name = '#Handheld Prospector'
TOOL.Command = nil
TOOL.ConfigName = ''
TOOL.Tab = "EnvX"

if CLIENT  then
	language.Add( "Tool.prospector.name", "Handheld Prospector" )
	language.Add( "Tool.prospector.desc", "Used to find raw resources for mining" )
	language.Add( "Tool.prospector.0", "Use the readings on tool screen to detect mining resources." )
	language.Add( "Tool.prospector.1", "As you walk around near resources the readings should change")
end

if SERVER then
	local NextThink = CurTime()
	local rstypes = {"resource_pool","resource_asteroid"}
	local function prospect(ply,radius)
		if not radius then radius = 1100 end
		-- where are we?
		local Pos = ply:GetPos()
		-- take a look about.
		local e = ents.FindInSphere(Pos,radius)
		-- filter out unwanted crap
		local g = {}
		for k,v in pairs(e) do if table.HasValue(rstypes,v:GetClass()) then g[#g+1] = v end end
		local dist = 1e5
		local closest
		if (#g > 0 ) then
			-- Find the closest target
			for k,v in pairs(g) do 
				local range = Pos:Distance( v:GetPos() )
				if  range < dist then  closest,dist = v,range end
				--print(tostring(closest).." <? - >"..tostring(dist))
			end
		end
		-- Send to client
		if closest ~= nil then
			--Todo: Fix density calculations to give better readings/averages :(
			density = 1+ (closest.density^2 / (dist*1e-1) ) 
		else
			density,dist = 1,0
		end
			
		umsg.Start("env_prospecttooldata",ply)
			umsg.Short(math.Round(dist,0))
			umsg.Float(density)
		umsg.End()
		
	end
	
	function TOOL:Think()
		if CurTime() >= NextThink then
			prospect(self:GetOwner())
			NextThink = CurTime() + 0.5
		end
	end
end

if CLIENT then
	local status = "unknown"
	local distance,density = 0,1
	-- Fonts
	--surface.CreateFont("Arial", 38, 1000, true, false, "EnvProspectingToolFont",false,true)
	--surface.CreateFont("Arial", 32, 1000, true, false, "EnvProspectingToolSmallFont",false,true)
	local Font = {font="Arial",size=38,weight=1000}
	surface.CreateFont("EnvProspectingToolFont",Font)
	Font.size = 32
	surface.CreateFont("EnvProspectingToolSmallFont",Font)
	
	-- Random Title text
	local titletext
	local function SetTitle()
		rtitle = {"DeepTech Industries Prospecting","SciTech Mining Corp.","Mechanos Mining Machines.",
			"Vengeful Mining Exploration Devices","Environments Mining Inc.","Salmontek Scanning Systems."}
		titletext = table.Random(rtitle)
	end
	SetTitle()

	-- sensor data
	local function GetSensorData(um)
		distance = um:ReadShort()
		density = um:ReadFloat()
	end
	usermessage.Hook("env_prospecttooldata",GetSensorData)
	
	-- Scrolly text fanciness
	local function scrolltitle(text,y,width)
		w,h = surface.GetTextSize(text)
		local w = w + 64
		local x = math.fmod( CurTime() * 30, w) * -1
		while x < width do
			surface.SetTextColor(255,255,255,255)
			surface.SetTextPos(x,y)
			surface.DrawText(text)
			x = x + w
		end
	end
		
	-- Draw Tool screen
	local function RenderScreen()
		cam.Start2D()
		
		surface.SetDrawColor(120,120,120,255)
		surface.DrawRect(0,0,256,256)
		

		surface.SetFont("EnvProspectingToolFont")
		scrolltitle(titletext,6,256)
		
		surface.SetDrawColor(50,50,50,255)
		surface.DrawRect(0,50,250,250)
		
		local pct,barx = 0,1
		if distance > 0 then 
			pct = math.Round(100-((distance/1300)*100),0)
			barx = math.Round(230 * (pct*0.01),0)
		end
		surface.SetDrawColor(120+pct,50,50,255)
		surface.DrawRect(5,220,barx,235)
		if barx > 35 then bartext = barx -35 else bartext = barx+12 end
		surface.SetFont("EnvProspectingToolSmallFont")
		surface.SetTextPos(bartext,221)
		surface.DrawText(pct.."%")
		
		-- convert source units to meters
		local meters = math.Round( ((distance * 0.75) * 2.54) * 1e-2,2)
		local feet = math.Round ( (distance * 0.75) / 12 ,2)
		surface.SetTextColor(0,200,0,255)
		surface.SetTextPos(10,60)
		surface.DrawText("Dist: "..meters.." m ")
		
		surface.SetTextColor(240,200,0,255)
		surface.SetTextPos(10,90)
		surface.DrawText("Density:"..math.Round(density,2) )
		
		if pct < 1 then status = "none"
		elseif pct >= 1 and pct <= 20 then status = "poor"
		elseif pct > 20 and pct < 49 then status = "moderate"
		elseif pct > 49 and pct < 80 then status = "good"
		elseif pct > 80 then status = "excellent"
		end
		surface.SetTextColor(255,255,255,255)
		surface.SetTextPos(10,125)
		surface.DrawText("Prospect status:")
		surface.SetTextPos(10,160)
		surface.DrawText(status)
		
		cam.End2D()
	end
	
	function TOOL:DrawToolScreen()
		RenderScreen()
	end
	
end
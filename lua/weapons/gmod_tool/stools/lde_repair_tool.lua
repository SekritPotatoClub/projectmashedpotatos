-- Repair Tool
-- This tool repairs stuff

TOOL.Category = "Ship Cores"
TOOL.Name = "Health Reader"
TOOL.Tab = "EnvX"
TOOL.ent = {}
TOOL.Timer = CurTime()	

local function IsReallyValid(trace)
	if not trace.Entity:IsValid() then return false end
	if trace.Entity:IsPlayer() then return false end
	if SERVER and not trace.Entity:GetPhysicsObject():IsValid() then return false end
	return true
end
		
if (SERVER) then
	AddCSLuaFile("lde_repair_tool.lua")
	function TOOL:Think()
		--[[if (CurTime() > self.Timer) then
			local ply = self:GetOwner()
			if (ply:KeyDown( IN_ATTACK )) then
				local trace = ply:GetEyeTrace()
				if (!trace.Hit) then return end
				if (trace.HitPos:Distance(ply:GetShootPos()) < 125 and trace.Entity and LDE:CheckValid( trace.Entity )) then
					LDE:RepairHealth( trace.Entity, 10 )
					-- Run slower!
					self.Timer = CurTime() + 0.1
				end
			end
		end]]--
		local ply = self:GetOwner()	
		local wep = ply:GetActiveWeapon()
		if not wep:IsValid() or wep:GetClass() ~= "gmod_tool" or ply:GetInfo("gmod_toolmode") ~= "lde_repair_tool" then return end
		local trace = ply:GetEyeTrace()
		if not IsReallyValid(trace) then return end
		ply:SetNetworkedFloat("ToolHealth", LDE:GetHealth(trace.Entity) or 0)
		ply:SetNetworkedFloat("ToolMaxHealth", LDE:CalcHealth(trace.Entity) or 0)
		ply:SetNetworkedFloat("ToolTemp", trace.Entity.LDE.Temperature or 0)
		ply:SetNetworkedFloat("ToolMaxTemp", trace.Entity.LDE.MeltingPoint or 0)
	end
end

if CLIENT then
	language.Add( "Tool.lde_repair_tool.name", "LDE Health Detector" )
	language.Add( "Tool.lde_repair_tool.desc", "Used to see a entities Health." )
	language.Add( "Tool.lde_repair_tool.0", "Primary: Look at a entity to see its health." )
	
	function TOOL:Reload()
	end
	
	function TOOL.BuildCPanel( CPanel )
		local label = vgui.Create("DLabel", LDE_weaponframe)
		label:SetText("Aim at a entity to see its health.")
		label:SizeToContents()
		
		CPanel:AddItem(label)
	end
	
	local TipColor = Color( 250, 250, 200, 255 )

	surface.CreateFont("GModWorldtip", {font = "coolvetica", size = 24, weight = 500})
	
	local function DrawRepairTip()
		local pl = LocalPlayer()
		local wep = pl:GetActiveWeapon()
		if not wep:IsValid() or wep:GetClass() ~= "gmod_tool" or pl:GetInfo("gmod_toolmode") ~= "lde_repair_tool" then return end
		local trace = pl:GetEyeTrace()
		if not IsReallyValid(trace) then return end
		
		EnvX.MenuCore.RenderWorldTip(trace.Entity,function(self)
			return {
				{Type="Percentage",
					Value=math.Round(pl:GetNWInt( "ToolHealth" ))/math.Round(pl:GetNWInt( "ToolMaxHealth" )),
					Text="Health: "..math.Round(pl:GetNWInt( "ToolHealth" )).." / "..math.Round(pl:GetNWInt( "ToolMaxHealth" ))
				},
				{Type="Percentage",
					Value=math.Round(pl:GetNWInt( "ToolTemp" ))/math.Round(pl:GetNWInt( "ToolMaxTemp" )),
					Text="Temperature: "..math.Round(pl:GetNWInt( "ToolTemp" )).." / "..math.Round(pl:GetNWInt( "ToolMaxTemp" ))
				}
			}
		end)
	end
	hook.Add("HUDPaint", "RepairWorldTip", DrawRepairTip)
	
end

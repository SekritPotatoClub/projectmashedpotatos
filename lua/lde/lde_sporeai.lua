LDE.SporeAI = LDE.SporeAI or {}

local EmptyPool = {Attach = {},Touch = {},Death = {},Damage = {}}

LDE.SporeAI.GeneSplices = {Organic=table.Copy(EmptyPool),Mechanical=table.Copy(EmptyPool)}

LDE.SporeAI.Spores = LDE.SporeAI.Spores or {}

--Function to create new gene splices for spores.
function LDE.SporeAI.MakeGeneSlice(Pool,Type,Name,Col,Gene)
	LDE.SporeAI.GeneSplices[Pool][Type][Name]={Id=Name,Dna=Gene,Col=Col}
end

--Picks a random gene out of a table.
function LDE.SporeAI.PickRandGene(Type,Splice)
	local Gene = table.Random(LDE.SporeAI.GeneSplices[Type][Splice])
	if(not Gene)then Gene ={Id="Error",Dna=function()end,Col=Color(255,255,255,255)} end
	return Gene
end

function LDE.SporeAI.CombineColor(Col1,Col2)
	local Col3 = Color(0,0,0,255)
	Col3.r = math.floor((Col1.r+Col2.r)/2)
	Col3.g = math.floor((Col1.g+Col2.g)/2)
	Col3.b = math.floor((Col1.b+Col2.b)/2)
	return Col3
end

--Creates a new random genetics table for spores to use.
function LDE.SporeAI.GetNewGenes(Type)
	local Tab={}
	local Gene = LDE.SporeAI.PickRandGene(Type,"Attach")
	Tab.Think,BID,C1 	= Gene.Dna,Gene.Id,Gene.Col
	
	local Gene = LDE.SporeAI.PickRandGene(Type,"Touch")
	Tab.Touch,TID,C2 	= Gene.Dna,Gene.Id,Gene.Col
	
	local Gene = LDE.SporeAI.PickRandGene(Type,"Death")
	Tab.Death,DID,C3 	= Gene.Dna,Gene.Id,Gene.Col
	
	local Gene = LDE.SporeAI.PickRandGene(Type,"Damage")
	Tab.OnDmg,OID,C4	= Gene.Dna,Gene.Id,Gene.Col
	
	NC1=LDE.SporeAI.CombineColor(C1,C2)
	NC2=LDE.SporeAI.CombineColor(C3,C4)
	NewColor=LDE.SporeAI.CombineColor(NC1,NC2)
	
	Tab.Color 	= NewColor--Color(math.random(150,255),math.random(150,255),math.random(150,255),255)
	Tab.Genes={BID,TID,DID,OID}
	
	local DNA = BID.." "..TID.." "..DID.." "..OID
	--MsgAll("Spore Dna: "..DNA)
	return Tab
end

if(SERVER)then
	local Utl = EnvX.Utl --Makes it easier to read the code.

	function SporeThink() --This allows all the spores to get a chance at thinking.
		local status, error = pcall(function()
			local I = 0
			
			for key, spore in pairs( LDE.SporeAI.Spores ) do
				if IsValid(spore) and spore.Brain then 
					spore:Brain()
				else
					table.remove(LDE.SporeAI.Spores,key)
				end 

				if I>=10 then
					I=1
					coroutine.yield()
				else
					I=I+1
				end
			end
			
			coroutine.yield()
		end)
		if error then EnvX.Debug(error,0,"SporeAI") MsgAll("SporeAi Error: "..error) end
		SporeThink()
	end

	function ManageSporeCoroutine()
		--MsgAll("Running Spores! \n")
		local Plys =  player.GetAll()	
		if table.Count(Plys)<1 then return end --No players, dont run spore ai. (Remove spores also?)
		
		if not SporeMaster then
			SporeMaster = coroutine.create(SporeThink)
		else
			local Status = coroutine.status(SporeMaster)
			--MsgAll(tostring(Status).." \n")
			if Status=="dead" then
				SporeMaster = coroutine.create(SporeThink)
			else
				coroutine.resume(SporeMaster)
			end
		end
		
		--MsgAll("Spores Ran! \n")
	end
	
	Utl:SetupThinkHook("SporeAIThread",0.1,0,function() ManageSporeCoroutine() end)
end

function SporeDebug(string)
	--MsgAll(string)
end

function LDE.SporeAI.Evolve(self)
	if not self.FullyEvolved or self.FullyEvolved==false then
		self.Data.OnEvolve(self)
	end
end

function LDE.SporeAI.GetNearby(pos)
	local nearby,flowers = 0,0
	for k,e in pairs(ents.FindInSphere(pos,1000)) do
		if(e.IsSpore)then
			nearby=nearby+1
			if e.SporeStage==5 then
				flowers=flowers+1
			end
		end
	end
	return nearby,flowers
end

function LDE.SporeAI.Attach(self,ent)
	if not IsValid(ent) then return end
	if not IsValid(self.Attached) then
		self.Attached = ent
		self.Host = ent
		--constraint.Weld(self, ent, 0, 0, 0, true)
		self:SetParent(ent)
		self.NextEvolve=CurTime()+20
		
		if ent.OnSporeAttach then ent:OnSporeAttach(self) end

		local phys = self:GetPhysicsObject()
		if (IsValid(phys)) then
			phys:EnableMotion(false)
		end
	end
end

function LDE.SporeAI.Stickto(self,ent,hitnorm,hitpos)
	if not IsValid(ent) then return end
	if not IsValid(self.Attached) then
		
		local Norm = hitnorm
		local RA = Norm:Angle()+Angle(-90,0,0)
		RA:RotateAroundAxis(Norm,math.Rand(0,360))
		self:SetAngles(RA)
		
		self:SetPos(hitpos+(-Norm*2))
		
		if ent.OnSporeAttach then ent:OnSporeAttach(self) end
		
		self.Attached = ent
		self.Host = ent
		self.CanFlower = true
		--constraint.Weld(self, ent, 0, 0, 0, true)
		self:SetParent(ent)
		self.NextEvolve=CurTime()+5
	end
end

function ZoneCheck(self,Pos,Size)  
    local XB, YB, ZB = Size * 0.8, Size * 0.8, Size * 0.8
    local Results = {}
    local Clear = true
    for k,e in pairs(ents.FindInSphere(Pos,Size)) do
		if(e.IsSpore)then
			local EP = e:GetPos()
					   
			local EPL = WorldToLocal( EP, Angle(0,0,0), Pos, Angle(0,0,0))
			local X,Y,Z = EPL.x, EPL.y, EPL.z
					   
			if X <= XB and X >= -XB and Y <= YB and Y >= -YB and Z <= ZB and Z >= -ZB then
				Clear = false
				break
			end
		end
    end
    return Clear
end

function LDE.SporeAI.SpreadTrace(self,OldTr)
	local n = math.Rand(0,360)
	local d = math.Rand(10,140)
	local SPos = self:GetPos()
	if(OldTr)then SPos=OldTr.HitPos end
	local trace = {}
	trace.start = SPos
	trace.endpos = SPos + (self:GetForward() * 50) + (self:GetRight() * (math.cos(n) * d)) + (self:GetUp() * (math.sin(n) * d))
	trace.filter = self

	return util.TraceLine( trace )
end

function LDE.SporeAI.GenerateSpore(self)
	SporeDebug("Reproducing")
	local tr = LDE.SporeAI.SpreadTrace(self)
	local SpreadDist = 80--70
	if tr.Hit and not tr.HitSky and not tr.HitWorld then
		if not ZoneCheck(self,tr.HitPos,SpreadDist) then SporeDebug("Spores are too close!") return end
		SporeDebug("First trace is success!")
		LDE.SporeAI.GenerateSporeFinal(self,tr.HitPos,tr.HitNormal,tr.Entity)
	else
		SporeDebug("First trace failed")
		local tr2 = LDE.SporeAI.SpreadTrace(self,tr)
		if tr2.Hit and not tr2.HitSky and not tr2.HitWorld then
			if not ZoneCheck(self,tr2.HitPos,SpreadDist) then SporeDebug("Spores are too close!") return end
			SporeDebug("SecondTrace Success!")
			LDE.SporeAI.GenerateSporeFinal(self,tr2.HitPos,tr2.HitNormal,tr2.Entity)
		end
	end
end

function LDE.SporeAI.GenerateSporeFinal(self,Pos,Norm,HEnt)
	if HEnt.IsSpore or HEnt:GetClass() == "player" then return end
	SporeDebug("Generating Spore now!")
    local P1 = Pos + Norm
    local P2 = Pos - Norm
    local ent = ents.Create( self:GetClass() )
    local RA = Norm:Angle()+Angle(90,0,0)
    RA:RotateAroundAxis(Norm,math.Rand(0,360))
    ent:SetAngles(RA)
    ent:SetPos( Pos + Norm * 2 )
    ent:Spawn()
    ent:Initialize()
    ent:Activate()
	LDE.SporeAI.Attach(ent,HEnt)
	ent.Genetics = self.Genetics
	ent:SetColor(self.Genetics.Color)
	
	if self.Data.OnSpread then self.Data.OnSpread(self,ent) end
	
	ent:CPPISetOwnerless(true)
	
	SporeDebug("made a spore!")
end

function LDE.SporeAI.TakeDam(self,dmg,attacker,inflictor)
	if(self.LDEHealth>dmg)then
		self.LDEHealth=self.LDEHealth-dmg
	else
		if self.Genetics.Death then self.Genetics.Death(self,dmg,attacker,inflictor) end
		self:EmitSound( "npc/antlion_grub/squashed.wav",100,math.Rand(90,110) )
		self:Remove()
	end
	
	if not self.Genetics.OnDmg then return end
	self.Genetics.OnDmg(self,dmg,attacker,inflictor)
end

--Base Code for spores.
function LDE.SporeAI.MakeSpore(Data)
	local ENT = {}
	ENT.Type = "anim"
	ENT.Base = "base_gmodentity"
	ENT.PrintName = Data.name
	ENT.Spawnable			= true
	ENT.AdminOnly		= true
	ENT.Category = "EnvX"

	ENT.ldedamageinsafe 	= true
	ENT.IsSpore				= true
	
	if(Data.Shared)then
		Data.Shared(ENT)
	end
		
	if SERVER then
		if(Data.Server)then
			Data.Server(ENT)
		end
		
		function ENT:ReadGenetics()
			return self.Genetics
		end
		
		function ENT:SpawnFunction( ply, tr )
			if ( not tr.Hit ) then return end
			local SpawnPos = tr.HitPos + tr.HitNormal * 16 + Vector(0,0,30)
			local ent = ents.Create(Data.class)
			ent:SetPos(SpawnPos);
			ent:Spawn();
			ent:Activate();
			local Gene = LDE.SporeAI.GetNewGenes(Data.GenePool)
			ent.Genetics=Gene
			ent:SetColor(Gene.Color)
			return ent
		end
		
		function ENT:Initialize()
			if not Data then MsgAll("HALP! NO DATA! \n") return end --NoData!
			self.Data = Data
			
			self.GenePool = Data.GenePool
			self.EvoModels = Data.EvoModels
			
			self.Attached = false
			self.Genetics = LDE.SporeAI.GetNewGenes(Data.GenePool)
			self.SporeStage	= 1
			self.LDEHealth = 200
			self.LDEMaxHealth = 200
			self.Attached = false
			self.CanFlower = false
			self.NextEvolve = 0
			
			self:SetModel(self.EvoModels[self.SporeStage])--NewModel
			self:PhysicsInit(SOLID_VPHYSICS)
			self:SetMoveType(MOVETYPE_VPHYSICS)
			self:SetSolid(SOLID_VPHYSICS)
			self:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)--Prevents them from self colliding.
			self:CPPISetOwnerless(true)
			
			if Data.OnInit then Data.OnInit(self) end
			
			if IsValid(self:GetPhysicsObject()) then
				self:GetPhysicsObject():Wake()
				self:GetPhysicsObject():EnableGravity(false)
			end

			if(math.random(1,20)<3)then self.CanFlower = true end
			
			table.insert(LDE.SporeAI.Spores,self)
		end
		
		function ENT:OnLDEDamage(dmg,attacker,inflictor)
			LDE.SporeAI.TakeDam(self,dmg,attacker,inflictor)
		end	
		
		function ENT:PhysicsCollide(colData,physobj)
			local activator = colData.HitEntity
			if activator==self.Attached then return end
			if LDE:IsImmune(activator) then return end
			if self.Attached then
				if self.Genetics.Touch then self.Genetics.Touch(self,activator) end
			else
				if not activator.IsSpore and not activator:IsPlayer() then
					LDE.SporeAI.Stickto(self,activator,colData.HitNormal,colData.HitPos)--self:GetPos()+(pos*50))
				end 
			end		
		end

		function ENT:Brain()
			self:SetColor(self.Genetics.Color)
			if self.Attached then
				if(self.SporeStage>=4)then
					if self.Genetics.Think then self.Genetics.Think(self,self.Attached) end
					LDE.SporeAI.GenerateSpore(self)
				end
				if(self.NextEvolve<CurTime())then
					LDE.SporeAI.Evolve(self)
				end
			end 
		end
		
	else
		if(Data.Client)then
			Data.Client(ENT)
		end
	end
	scripted_ents.Register(ENT, Data.class, true, false)
	print("Spore Class Registered: "..Data.class)
end

local LoadFile = EnvX.LoadFile --Lel Speed.
local P = "lde/sporetypes/"
LoadFile(P.."organic.lua",1)
LoadFile(P.."mechanical.lua",1)



local Gene = function(self,Attached)
	self.NextEvolve = self.NextEvolve-1
end
LDE.SporeAI.MakeGeneSlice("Mechanical","Attach","Fast Replication",Color(127,255,255,255),Gene)

LDE.SporeAI.MakeGeneSlice("Mechanical","Death","None",Color(0,0,0,255),function()end)

LDE.SporeAI.MakeGeneSlice("Mechanical","Touch","None",Color(255,255,255,255),function()end)

LDE.SporeAI.MakeGeneSlice("Mechanical","Damage","None",Color(0,0,0,255),function()end)

local OnEvolve = function(self)
	self.NextEvolve=CurTime()+(10*self.SporeStage)
	if(self.SporeStage<4)then
		SporeDebug("Next Evolve in "..self.NextEvolve)
		self.SporeStage=self.SporeStage+1
		self:SetModelScale( 0.2*self.SporeStage, 0 ) self:Activate()
		self.LDEHealth=(200*self.SporeStage)
		self.LDEMaxHealth=self.LDEHealth
		local Snds = {"npc/scanner/combat_scan1.wav","npc/scanner/combat_scan2.wav","npc/scanner/combat_scan3.wav","npc/scanner/combat_scan4.wav","npc/scanner/combat_scan5.wav"}
		self:EmitSound(Snds[math.random(1,#Snds)],100,math.Rand(90,110),0.3)
	elseif(self.SporeStage==4)then
		local Nearby,Flowers=LDE.SporeAI.GetNearby(self:GetPos())
		if(self.CanFlower or Flowers<4)then
			self.SporeStage=5
			self.FullyEvolved=true			
			self:SetModelScale( 0.2*self.SporeStage, 0 ) self:Activate()
			self.LDEHealth=4000
			self.LDEMaxHealth=self.LDEHealth
			self:EmitSound( "npc/roller/mine/rmine_predetonate.wav",100,math.Rand(90,110),0.3 )
		end
	end
end

function OnInit(self)
	self:SetModelScale( 0.2*self.SporeStage, 0 ) self:Activate()
end

local OnSpread = function(self,ent)
	local Snds = {"npc/roller/mine/rmine_blip1.wav","npc/roller/mine/rmine_chirp_quest1.wav","npc/roller/mine/rmine_predetonate.wav"}
	ent:EmitSound( Snds[math.random(1,#Snds)],100,math.Rand(90,110),0.3 )
end

local Data={name="Mecha Space Spore",class="lde_mechaspore",OnEvolve=OnEvolve,EvoModels={"models/mandrac/bug.mdl"},OnInit=OnInit,OnSpread=OnSpread,GenePool="Mechanical"}
LDE.SporeAI.MakeSpore(Data)











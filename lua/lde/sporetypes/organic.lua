
local Gene = function(self,Attached)
	if(Attached.LDE)then
		LDE:DealDamage(Attached,10,self,self)
	end
end
LDE.SporeAI.MakeGeneSlice("Organic","Attach","Acidic",Color(0,150,0,255),Gene)

local Gene = function(self,Attached)
	if(Attached.LDE)then
		local Core = Attached.LDE.Core
		if(Core and Core:IsValid())then
			LDE.HeatSim.ApplyHeat(Core,400)
		else
			LDE.HeatSim.ApplyHeat(Attached,20)
		end
	end
end
LDE.SporeAI.MakeGeneSlice("Organic","Attach","Combustion",Color(255,180,0,255),Gene)

local Gene = function(self,Attached)
	if(Attached.LDE)then
		local Core = Attached.LDE.Core
		if(Core and Core:IsValid())then
			LDE.HeatSim.ApplyHeat(Core,-1000)
		else
			LDE.HeatSim.ApplyHeat(Attached,-1000)
		end
	end
end
LDE.SporeAI.MakeGeneSlice("Organic","Attach","FrostBite",Color(0,255,230,255),Gene)

local Gene = function(self,Attached)
	self.NextEvolve = self.NextEvolve-1
end
LDE.SporeAI.MakeGeneSlice("Organic","Attach","Darwins Whore",Color(255,100,100,255),Gene)

local Gene = function(self,Attached)
	if self.LDEHealth < self.LDEMaxHealth then
		self.LDEHealth = self.LDEHealth+50
	else
		self.LDEHealth = self.LDEMaxHealth
	end
end
LDE.SporeAI.MakeGeneSlice("Organic","Attach","Regenerative",Color(150,255,150,255),Gene)

--OnDeath Genes.
--Add gene that causes resources to drop.
--Add a gene that causes E5 spores to spew out spores.

local Gene = function(self)	

	self.NoLDEDamage = true
	
	local NewData = { 
		Pos 					=		self:GetPos(),							--Required--		--Position of the Explosion, World vector																--How far the Shrapnel travels
		ShockDamage				=		2000,		--Optional--		--Amount of Shockwave Damage, if 0 or nil then other Shock vars are not required
		ShockRadius				=		500,							--How far the Shockwave travels in a sphere
		Ignore					=		self,								--Optional--		--Entity that Shrapnel and Shockwaves ignore, Example: A missile entity so that Shrapnel doesn't hit it before it's removed
		Inflictor				=		self,							--Required--		--The weapon or player that is dealing the damage
		Owner					=		self					--Required--		--The player that owns the weapon, or the Player if the Inflictor is a player
	}
	LDE:BlastDamage(NewData)
	
	local effectdata = EffectData()
	effectdata:SetOrigin(self:GetPos())
	effectdata:SetStart(self:GetPos())
	util.Effect( "Explosion", effectdata )
end
LDE.SporeAI.MakeGeneSlice("Organic","Death","Explosive",Color(255,0,0,255),Gene)

LDE.SporeAI.MakeGeneSlice("Organic","Death","None",Color(0,0,0,255),function()end)
LDE.SporeAI.MakeGeneSlice("Organic","Death","None2",Color(0,0,0,255),function()end)

--OnTouch Genes.
--Add a gene that causes the spore to stick to everything.

local Gene = function(self,Activator)
	LDE:DealDamage(Activator,2,self,self)
end
LDE.SporeAI.MakeGeneSlice("Organic","Touch","Spiny",Color(160,255,0,255),Gene)

local Gene = function(self,Activator)
	if(Activator:IsPlayer())then
		local Debuff = {
			Tick 		= function(ply) LDE:DealDamage(ply,5,ply,ply,true) end,
			OnDeath 	= function(ply,Ext) end, --Add exploding hook here.
			OnStart 	= function(ply) ply:SendColorChat("Alert",{r=255,g=0,b=0},"You've Been Sporefected!") end, --Add notification that players got infected.
			OnTimeEnd 	= function(ply) end,
			OnRemove 	= function(ply) ply:SendColorChat("Alert",{r=0,g=255,b=0},"You are no longer Sporefected!") end, --Add notification that its done.
			OnDamage 	= function(ply,Ext) end,
			OnKill 		= function(ply,Ext) end
		}
		Activator:GiveMutation("Sporefection",60,Debuff,false,true)
	end
end
LDE.SporeAI.MakeGeneSlice("Organic","Touch","Infectious",Color(220,0,255,255),Gene)

LDE.SporeAI.MakeGeneSlice("Organic","Touch","None",Color(0,0,0,255),function()end)
LDE.SporeAI.MakeGeneSlice("Organic","Touch","None2",Color(0,0,0,255),function()end)

--OnDamage genes.
--Add a gene that causes spores to fight back.
--Add a gene that causes a protective shield to form if the attacker is a player

LDE.SporeAI.MakeGeneSlice("Organic","Damage","None",Color(0,0,0,255),function()end)
LDE.SporeAI.MakeGeneSlice("Organic","Damage","None2",Color(0,0,0,255),function()end)


----------------------------------------------------------------------------------------------------------------------
------------------------------------Lets Register the spore here------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

local Spawn = function() 	
	rock = ents.Create("lde_spore_cloud")
	local Point = LDE.Anons:PointInOrbit(rock.Data) if(not Point)then return end	
	rock:SetPos(Point) rock:Spawn()
end

local Models = {"models/env_spore/spore_ev1.mdl","models/env_spore/spore_ev2.mdl","models/env_spore/spore_ev3.mdl","models/env_spore/spore_ev4.mdl","models/env_spore/spore_ev5.mdl"}

local OnEvolve = function(self)
	self.NextEvolve=CurTime()+(10*self.SporeStage)
	if(self.SporeStage<4)then
		SporeDebug("Next Evolve in "..self.NextEvolve)
		self.SporeStage=self.SporeStage+1
		self:SetModel(self.EvoModels[self.SporeStage])--NewModel
		self:PhysicsInit(SOLID_VPHYSICS)
		self.LDEHealth=(200*self.SporeStage)
		self.LDEMaxHealth=self.LDEHealth
		local Snds = {"weapons/bugbait/bugbait_squeeze1.wav","weapons/bugbait/bugbait_squeeze2.wav","weapons/bugbait/bugbait_squeeze3.wav"}
		self:EmitSound( Snds[math.random(1,#Snds)],100,math.Rand(90,110),0.3 )				
	elseif(self.SporeStage==4)then
		local Nearby,Flowers=LDE.SporeAI.GetNearby(self:GetPos())
		if(self.CanFlower or Flowers<4)then
			self.SporeStage=5
			self.FullyEvolved=true			
			self:SetModel(self.EvoModels[self.SporeStage])--NewModel
			self:PhysicsInit(SOLID_VPHYSICS)
			self:EmitSound( "npc/antlion_grub/squashed.wav",100,math.Rand(90,110),0.3 )
			self.LDEHealth=4000
			self.LDEMaxHealth=self.LDEHealth
			if(math.Rand(1,15)==3)then
				self.Genetics = LDE.SporeAI.GetNewGenes()
			end
		end
	end
end

local OnSpread = function(self,ent)
	local Snds = {"weapons/bugbait/bugbait_impact1.wav","weapons/bugbait/bugbait_impact3.wav"}
	ent:EmitSound( Snds[math.random(1,#Snds)],100,math.Rand(90,110),0.3 )
end

local Data={name="Basic Space Spore",class="lde_spore",Type="Orbit",SpawnMe=Spawn,minimal=40,EvoModels=Models,OnEvolve=OnEvolve,OnSpread=OnSpread,GenePool="Organic"}
LDE.SporeAI.MakeSpore(Data)
LDE.Anons:RegisterAnomaly(Data)





















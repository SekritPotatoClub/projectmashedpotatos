
----------------------------------------------
------------------Quest Lib-------------------
----------------------------------------------

local LDE = LDE --Localise the global table for speed.
local Utl = EnvX.Utl --Makes it easier to read the code.
local PlayerData = LDE.PlayerData

PlayerData.GoalData= PlayerData.GoalData or {}
	
local GData = PlayerData.GoalData
function PlayerData.GenerateGoalData(GoalDat)
	GData[GoalDat.Quest] = GData[GoalDat.Quest] or {}
	GData[GoalDat.Quest][GoalDat.Name] = GoalDat
end
	
if SERVER then
	function PlayerData.ManageQuestSystem()
		local players = player.GetAll()
		
		for _, ply in ipairs( players ) do
			if ply and ply:IsConnected() then
				local Quests = ply:GetQuests()
				for qi, quest in pairs( Quests ) do
					for gi, goal in pairs( quest.Goals ) do
						local State = goal.State
						if State == "InActive" or State == "Active" then
							if State == "InActive" then
								goal.State = "Active"
								GData[qi][goal.Dat].Init(ply)
							elseif State == "Active" then
								local Return = GData[qi][goal.Dat].Think(ply)
								if Return == false then
									goal.State = "Failed"
									GData[qi][goal.Dat].Failed(ply)
								elseif Return == true then
									goal.State = "Completed"
									GData[qi][goal.Dat].Completed(ply)
								end
							end
							break
						end
					end
				end
			end
		end
	end	
	Utl:SetupThinkHook("EnvxQuestLibThink",1,0,function() PlayerData.ManageQuestSystem() end)

	local meta = FindMetaTable( "Player" )
	if not meta then return end
	
	function meta:GiveQuest(Name,Goals)
		local GoalDat = {}
		
		for i, goal in pairs( Goals ) do
			GoalDat[i]={State="InActive",Dat=goal}
		end
		
		self:GetQuests()[Name]={Goals=GoalDat}
	end
	
	function meta:AddQuestGoal(Name,Goal)
		local Quest = self:GetQuests()[Name]
		if Quest~=nil then
			table.insert(Quest.Goals,{State="InActive",Dat = Goal})
		end
	end
	
	function meta:MoveQuestToLog(Name) end
else

end

--[[
local GoalDat = {
	Quest = "Example", --Name of the quest this goal was made for, helps sort the data table
	Name = "Goal1", --The name of the goal, should be unique for each goal
	Desc = "Descriptive Text here", --Text that is shown to the player to help them complete the goal.
	
	Init = function(ply) end, --Called when the goal is first started, use this to setup what needs to be done.
	Think = function(ply) return end, --Called once every second, returning true causes the goal to complete, while returned false fails the goal. Return nothing to continue waiting.
	Completed = function(ply) end, --Called when the goal is completed.
	Failed = function(ply) end --Called when the goal is failed.
}

PlayerData.GenerateGoalData(GoalDat)



player:GiveQuest("Example",{"Goal1"})
]]




















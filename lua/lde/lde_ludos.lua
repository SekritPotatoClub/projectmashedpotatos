
----------LudOS Core Logic-------------
--Created by ludsoe.


local low = string.lower

--Create the Core.
LudCore = {Names={["server"]=true,["lud"]=true,["ludos"]=true}}
local LudCore = LudCore

function LudCore.Initialize()
	LudCore.LudOS = {Version="Potato",Commands={},Functions={},ClassAlias={}}
	LudCore.LoadFunctions()
end

function LudCore.IsAdmin(ply)
	return ply:IsAdmin() or ply:IsSuperAdmin()
end

--Response system. Delayed so its after the players chat.
function LudCore.Response(Text)
	timer.Simple(0.01,function() LDE:NotifyPlayers("LUD-OS",Text,{r=0,g=180,b=180}) end)
end

function LudCore.IsForLudOS(str)
	return LudCore.Names[low(str)]
end

function LudCore.FindPlayerByName(name)
	if not name then return end
	
	for k,v in pairs(player.GetAll()) do 
		local ply = low(v:Name())
		if ply == name then
			return v
		else
			if string.find(ply,name) then
				return v
			end
		end
	end
end

function LudCore.RegisterClassAlias(Alias,Class) end

function LudCore.RegisterCommand(Name,KeyWords,Function)
	table.insert(LudCore.LudOS.Commands,{Name=Name,KeyWords=KeyWords,Function=Function})
end

function LudCore.ParseString(str,key,args)
	--print(tostring(key))
	if type(key) == "string" then
		--print("Comparing.. "..str.." and "..key)
		if key == "(varg)" then
			table.insert(args,str)
			return true
		else
			return str==low(key)
		end
	else
		for _, alias in ipairs( key ) do
			if LudCore.ParseString(str,alias,args) then
				return true
			end
		end
	end
	
	return false
end

function LudCore.ProcessCommand(ply,cmd,teamchat)
	--print("Chat Detected!")
	--Don't return anything it will override the players text.
	local explode = string.Explode(" ",cmd)
	if not LudCore.IsForLudOS(explode[1]) then return end --chat wasnt directed at ludos
	
	for i, exstr in pairs( explode ) do
		explode[i]=low(exstr)
	end
	--print("Chat is for LudOS")
	
	for _, cmd in ipairs( LudCore.LudOS.Commands ) do
		local Args = {}
		local IsCommand = true
		--PrintTable(cmd)
		--print("checking command: "..tostring(cmd.Name))
		for i, exstr in ipairs( explode ) do
			--print(tostring(exstr))
			if i == 1 then continue end
			
			--print("true")
			
			if not LudCore.ParseString(exstr,cmd.KeyWords[i-1],Args) then
				--print("not a command")
				IsCommand = false
				break
			end
		end
		
		if IsCommand then
			cmd.Function(ply,Args)
			return
		end
	end
	--LudCore.Response("I don't know what you want..")
end
EnvX.Utl:HookHook("PlayerSay","ludoscommandcheck",LudCore.ProcessCommand,1)

function LudCore.LoadFunctions()
	LudCore.RegisterCommand("PropCount",{{"how","count"},{"many","the"},{"props","entities","entitys"},{"is","are","in"},{"this","in","that"},{"this","that"}},function(ply,args)
		local looking = ply:GetEyeTrace()
		if looking.Hit then
			if looking.HitNonWorld then
				if looking.Entity then
					local Cons = constraint.GetAllConstrainedEntities(looking.Entity)
					LudCore.Response("That is made up of "..table.Count(Cons).." Props/Entities.")
				end
			else
				LudCore.Response("That's the world.")
			end
		else
			LudCore.Response("You're not looking at anything!")
		end
	end)
	
	LudCore.RegisterCommand("SporeCount",{"how","many","spores","are","there"},function(ply,args)
		local Spores = ents.FindByClass("lde_spore")
		LudCore.Response("Theres "..table.Count(Spores).." spores on the map.")
	end)
	
	LudCore.RegisterCommand("DetectStationHP",{"what","is","the","stations","health"},function(ply,args)
		local Station = LDE.NPCMain.Station
		if IsValid(Station) then
			LudCore.Response("Hull:"..Station.LDE.CoreHealth.." Shields:"..Station.LDE.CoreShield)
		else
			LudCore.Response("The station was destroyed....")
		end
	end)
	
	LudCore.RegisterCommand("RemoveAis",{{"cleanup","remove","destroy"},"all","npcs"},function(ply,args)
		if LudCore.IsAdmin(ply) then
			LudCore.Response("Removing NPC Ships!")
			for k, role in pairs( LDE.NPCMain.RoleData ) do
				for key, spore in pairs( ents.FindByClass(role) ) do
					if IsValid(spore) then
						spore:Remove()
					end
				end
			end
		else
			LudCore.Response("I cannot do that for you.")
		end
	end)
	
	LudCore.RegisterCommand("RemoveAsteroids",{{"cleanup","remove","destroy"},"all","roids"},function(ply,args)
		if LudCore.IsAdmin(ply) then
			LudCore.Response("Removing Asteroids!")
			for key, spore in pairs( ents.FindByClass("resource_asteroid") ) do
				if IsValid(spore) then
					spore:Remove()
				end
			end
		else
			LudCore.Response("I cannot do that for you.")
		end
	end)
		
	LudCore.RegisterCommand("RemoveSpores",{{"cleanup","remove","destroy"},"all","spores"},function(ply,args)
		if LudCore.IsAdmin(ply) then
			LudCore.Response("Removing Spores!")
			for key, spore in pairs( ents.FindByClass("lde_spore") ) do
				if IsValid(spore) then
					spore:Remove()
				end
			end
		else
			LudCore.Response("I cannot do that for you.")
		end
	end)
	
	LudCore.RegisterCommand("RemoveProps",{{"cleanup","remove","destroy"},"(varg)",{"props","stuff","entitys","entities","things","junk"}},function(ply,args)
		if args[1] == "my" then
			LudCore.Response("Cleaning up your things!")
			
			for i, ent in ipairs( ents.GetAll() ) do
				if ent and IsValid(ent) then
					if ent:CPPIGetOwner() == ply then
						ent:Remove()
					end
				end
			end
		end	
	end)
	
	LudCore.RegisterCommand("DataBaseCheck",{{"stats","database"}},function(ply,args)
		LudCore.Response("There are "..tostring(table.Count(LDE.PlayerData.PlayerData)).." players saved and "..tostring(table.Count(LDE.Factions.Factions)-1).." factions saved.")
	end)
	
	LudCore.RegisterCommand("VersionCheck",{{"version","ver","vers"}},function(ply,args)
		LudCore.Response("I'm Running on Version: "..LudCore.LudOS.Version.." Envx Version: "..EnvX.Version)
	end)
	
	LudCore.RegisterCommand("TimeCheck",{{"uptime",}},function(ply,args)
		LudCore.Response("The Server has been up for  "..string.FormattedTime( RealTime(), "%02i Hours %02i Minutes and %02i Seconds." ))
	end)
end

LudCore.Initialize()















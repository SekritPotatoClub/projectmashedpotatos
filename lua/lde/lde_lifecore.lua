LDE.LifeSupport = {}

--Needed resource check
LDE.LifeSupport.HasNeeded = function(self, List)
	for I,b in pairs(List) do  
		if self:GetResourceAmount(b)<((self.Data.InUse[I]*self:GetSizeMultiplier())*self:GetMultiplier())then
			if self.TurnOff then
				self:TurnOff()
			end
			return false
		end
	end
	return true
end

--Maxed Resource check
LDE.LifeSupport.MaxedResources = function(self,List)
	if not List then return false end
	for I,b in pairs(List) do
		if self:GetResourceAmount(b)>=self:GetNetworkCapacity(b) then
			return true --Whoa stop the line we have too much junk
		end
	end
	return false --Its good carry on.
end

--Use Resource loop
LDE.LifeSupport.UseResources = function(self, List)
	for I,b in pairs(List) do
		self:ConsumeResource(b, (self.Data.InUse[I]*self:GetSizeMultiplier())*self:GetMultiplier())
	end
end

--Make Resource loop
LDE.LifeSupport.MakeResources = function(self, List)
	for I,b in pairs(List) do
		self:SupplyResource(b, (self.Data.OutMake[I]*self:GetSizeMultiplier())*self:GetMultiplier())
	end
end

--Function that manages all the resources to a device.
LDE.LifeSupport.ManageResources = function(self,Override)
	if(LDE.LifeSupport.HasNeeded(self,self.Data.In))then --Do we have the needed resources?
		if(LDE.LifeSupport.MaxedResources(self,self.Data.Out) and not Override)then return end --Dont run the device if were maxed out.
		if(self.Data.In)then --Check if we have required resources.
			for I,b in pairs(self.Data.In) do --For all the required resources...
				self:ConsumeResource(b, (self.Data.InUse[I]*self:GetSizeMultiplier())*self:GetMultiplier()) --Nom dem.
			end
		end
		if(self.Data.Out)then --Check if we make stuff.
			for I,b in pairs(self.Data.Out) do --For all the outputed resources
				self:SupplyResource(b, (self.Data.OutMake[I]*self:GetSizeMultiplier())*self:GetMultiplier())--Pump dat shit out
			end
		end
		return true --Everything went perfectly...
	end
	return false --Didnt have the needed resources :(
end

--Heat Application function
function LDE.LifeSupport.ApplyHeat(ent,Data)
	heatchange = Data.heat or 0
	LDE.HeatSim.ApplyHeat(ent,heatchange*ent:GetSizeMultiplier())
	return true
end

--Manage Light
function LDE.LifeSupport.RunLight(self,Data)
	if self.RunLight == 1 and not self.flashlight then
		--self:SetOn(true)
		local angForward = self:GetAngles() +- Angle( 0, 0, 0 )

		self.flashlight = ents.Create( "env_projectedtexture" )
		self.flashlight:SetParent( self )

		-- The local positions are the offsets from parent..
		self.flashlight:SetLocalPos( Vector( 0, 0, 0 ) )
		self.flashlight:SetLocalAngles( Angle(0,0,0)+(Data.LightAngle or Angle(0,0,0)) )

		-- Looks like only one flashlight can have shadows enabled!
		self.flashlight:SetKeyValue( "enableshadows", 0 )
		self.flashlight:SetKeyValue( "farz", 8096 )
		self.flashlight:SetKeyValue( "nearz", 8 )

		--the size of the light
		self.flashlight:SetKeyValue( "lightfov", Data.FOV or 30 )

		-- Color.. white is default
		--print(Data.LightRed.." "..Data.LightGreen.." "..Data.LightBlue)
		self.flashlight:SetKeyValue( "lightcolor", (Data.LightRed or 255).." "..(Data.LightGreen or 255).." "..(Data.LightBlue or 255) )
		self.flashlight:Spawn()
		self.flashlight:Input( "SpotlightTexture", NULL, NULL, "effects/flashlight001" )
	elseif self.RunLight == 0 and self.flashlight then
		SafeRemoveEntity( self.flashlight )
		self.flashlight = nil
	end
end


local function AddLine(Text,Line) Text=Text.."\n"..Line return Text end
--Lets attempt to make some helpful descriptions for the spawning tool.
function LDE.LifeSupport.CreateDescription(Data,name)
	local Desc = name

	if Data.Desc then
		Desc=Desc.."\n".."\n"..Data.Desc.."\n"
	end
	
	if Data.storage ~= nil then
		local Takes = "" for I,b in pairs(Data.storage) do Takes=Takes..EnvX.GetDisplayName(b)..", " end
		Desc=AddLine(Desc,"Stores: "..Takes)		
	end
	if Data.In ~= nil then
		local Takes = "" for I,b in pairs(Data.In) do Takes=Takes..EnvX.GetDisplayName(b)..", " end
		Desc=AddLine(Desc,"Consumes: "..Takes)
	end
	if Data.Out ~= nil then
		local Takes = "" for I,b in pairs(Data.Out) do Takes=Takes..EnvX.GetDisplayName(b)..", " end
		Desc=AddLine(Desc,"Produces: "..Takes)		
	end

	
	return Desc
end

--Device compiling function.
function LDE.LifeSupport.CompileDevice(Data,Inner)
	
	for k,v in pairs(Inner.model) do
		local ToolInfo = {Path=Inner.Path,DevName=Inner.name[k],class=Inner.class,model=v,tooltip=LDE.LifeSupport.CreateDescription(Data,Inner.name[k])}
		Environments.RegisterDevice(ToolInfo)
	end
	LDE.LifeSupport.RegisterDevice(Data)
end

--Base Device Code we will inject the functions into.
function LDE.LifeSupport.RegisterDevice(Data)
	local ENT = {}
	ENT.Type = "anim"
	ENT.Base = "base_env_entity"
	ENT.PrintName = Data.name
	ENT.Data = Data
	ENT.CanRun=1
	
	if(Data.RunSound)then
		util.PrecacheSound(Data.RunSound)
	end
	
	ENT.OverlayText = {HasOOO = true, resnames = Data.In, genresnames = Data.Out}

	if SERVER then
		function ENT:Initialize()
			self.BaseClass.Initialize(self)
			self:PhysicsInit( SOLID_VPHYSICS )
			self:SetMoveType( MOVETYPE_VPHYSICS )
			self:SetSolid( SOLID_VPHYSICS )
			self.Active = 0
			self.multiplier = 1
			self.LastTime = 0
			if WireAddon then
				self.WireDebugName = self.PrintName
				if Data.WireIn then
					self.Inputs = WireLib.CreateInputs(self, Data.WireIn)
				else
					self.Inputs = WireLib.CreateInputs(self, { "On","Multiplier" })
				end
				if Data.WireOut then
					self.Outputs = WireLib.CreateOutputs(self, Data.WireOut)
				else
					self.Outputs = WireLib.CreateOutputs(self, {"On"})
				end
			end
			if self.Data.Initialize then
				self.Data.Initialize(self)
			end
		end
		
		function ENT:OnTurnOn() 
			WireLib.TriggerOutput(self, "On", 1)
		end
		
		function ENT:OnTurnOff()
			WireLib.TriggerOutput(self, "On", 0)
		end
		
		function ENT:TriggerInput(iname, value)
			if iname == "On" then
				if value > 0 then
					if self.Active <= 0 then
						self:TurnOn()
					end
				else
					if self.Active >= 1 then
						self:TurnOff()
					end
				end
			elseif iname == "Multiplier" then
				if(value<=1)then
					self:SetMultiplier(1)
				else
					self:SetMultiplier(value)
				end
			end
			--self.Inputs[iname]=value
		end

		ENT.Generate = Data.shootfunc

		function ENT:Think()
			if CurTime()>=self.LastTime+1 then
				self.LastTime=CurTime()
				self:Generate()
			end
			self:NextThink(CurTime() + 1)
			return true
		end
	else
		--client
	end
	
	scripted_ents.Register(ENT, Data.class, true, true)
	print("Device Registered: "..Data.class)
end

--Storage compiling function.
function LDE.LifeSupport.CompileStorage(Data,Inner)
	for k,v in pairs(Inner.model) do
		local ToolInfo = {Path=Inner.Path,DevName=Inner.name[k],class=Inner.class,model=v,tooltip=LDE.LifeSupport.CreateDescription(Data,Inner.name[k]),res_store=Data.Rates}
		Environments.RegisterDevice(ToolInfo)
	end
	Environments.RegisterLSStorage(Data.name, Data.class, Data.Rates)
	LDE.LifeSupport.RegisterStorage(Data)
end

--Base storage Code we will inject the functions into.
function LDE.LifeSupport.RegisterStorage(Data)
	local ENT = {}
	ENT.Type = "anim"
	ENT.Base = "base_env_storage"
	ENT.PrintName = Data.name
	ENT.Data = Data

	ENT.OverlayText = {HasOOO = false, resnames = Data.storage}

	scripted_ents.Register(ENT, Data.class, true, true)
	print("Storage Registered: "..Data.class)
end

local LoadFile = EnvX.LoadFile --Lel Speed.
local Path =  "lde/lifesupport/"

--LoadFile(Path.."lde_basemodules.lua",1)
LoadFile(Path.."lde_core_upgrades.lua",1)
LoadFile(Path.."lde_extraents.lua",1)
LoadFile(Path.."lde_factorys.lua",1)
LoadFile(Path.."lde_generators.lua",1)
LoadFile(Path.."lde_mining.lua",1)
LoadFile(Path.."lde_storage.lua",1)

Msg("LDE Space Anomaly System Loaded: Successfully\n")




-------------------------------------
----------Drone Systems--------------
-------------------------------------
local Path = {"Weapons","Exotic","Drones"}

local Func = function(self,CanFire)
	self.CanLaunch = CanFire
	return false
end

local OnThink = function(self) 
	local NPCM = LDE.NPCMain
	local DroneList = NPCM.ShipData.PlyDrone

	if self.CanLaunch then
		if not IsValid(self.Drone) then
			if self.ProduceDrone then
				if LDE.LifeSupport.ManageResources(self,1) then
					local MyDrone = NPCM.SpawnShipAtPosition(self:GetDockPosition(),DroneList[self.Data.DockingBay.Drone])
					self.Drone = MyDrone
					
					MyDrone.Hanger = self
					MyDrone.Docked = false
					MyDrone.Active = false
					MyDrone.Attack = false
					
					MyDrone.CombatMode = self.Data.DockingBay.CombatType
					
					WireLib.TriggerOutput(self, "Drone Entity", MyDrone)
					WireLib.TriggerOutput(self, "Drone Max Hull", MyDrone.LDE.CoreMaxHealth)
					WireLib.TriggerOutput(self, "Drone Max Shields", MyDrone.LDE.CoreMaxShield)
					
					timer.Simple(0.2,function() LDE.GivePlyProp(LDE.GetPropOwner(self),MyDrone) end)
				end
			end
		else
			local MyDrone = self.Drone
			
			MyDrone.Active = self.DroneData.Active
			MyDrone.Attack = self.DroneData.Attack
			MyDrone.TargetPos = self.DroneData.Targ
			
			WireLib.TriggerOutput(self, "Drone Hull", MyDrone.LDE.CoreHealth)
			WireLib.TriggerOutput(self, "Drone Shields", MyDrone.LDE.CoreShield)
			WireLib.TriggerOutput(self, "Drone Position", MyDrone:GetPos())
		end
	end	
end

local Intial = function(self)
	function self:GetDockPosition() return self:LocalToWorld(self.Data.DockingBay.Position) end
	function self:GetDockAngle() return self:LocalToWorldAngles(self.Data.DockingBay.Angle) end
	
	self.ProduceDrone = false
	self.CanLaunch = false
	
	self.DroneData = {Active=false,Targ=Vector(0,0,0),Attack=false}
end

local WireFunc = function(self,iname,value)
	if iname == "Active" then
			self.DroneData.Active = value > 0
	elseif iname == "Attack" then
			self.DroneData.Attack = value > 0
	elseif iname == "ConstructDrone" then
			self.ProduceDrone = value > 0
	elseif iname == "TargetPosition" then
		self.DroneData.Targ = value
	end
end

local WireSetup = function(self)
	local V,N,A,E = "VECTOR","NORMAL","ANGLE","ENTITY"
	self.Inputs = WireLib.CreateSpecialInputs( self,{"TargetPosition","Active","Attack","ConstructDrone"},{V,N,N,N})
	self.Outputs = WireLib.CreateSpecialOutputs( self, { "CanFire", "Drone Hull", "Drone Max Hull", "Drone Shields",  "Drone Max Shields","Drone Entity","Drone Position" },{N,N,N,N,N,E,V})
end

local DockingBay = {Angle=Angle(0,-90,0),Position=Vector(0,0,35),Drone="DakaDrone",CombatType="Hover"}
local Data={name="Micro Drone Platform",class="micro_drone_platform_weapon",In={"Refined Ore","energy"},WireSpecial=WireSetup,WireFunc=WireFunc,Intial=Intial,OnThink=OnThink,shootfunc=Func,Points=500,heat=50,firespeed=1,InUse={1000,5000},DockingBay=DockingBay}
local Makeup = {name={"Micro Drone Platform"},model={"models/mandrac/energy/heavy_pulse_body.mdl"},Path=Path,class=Data.class}
LDE.Weapons.CompileWeapon(Data,Makeup)


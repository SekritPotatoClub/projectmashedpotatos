
-------------------------------------
-----------RailGun-------------------
-------------------------------------

local Client = function(self) return LDE.Weapons.ShowCharge(self,self.Data.Bullet) end --Clientside
local ClientSetup = function(ENT) ENT.LDETools.Charge = Client end
local Func = function(self,CanFire) 
	LDE.Weapons.ManageCharge(self,self.Data.Bullet)
	return true
end --Charge function
local Path = {"Weapons","Exotic","RailGuns"}
local Effects = {Beam="LDE_laserbeam_long",Hit="LDE_laserhiteffect"}

--Basic RailGun
local Fire = function(self) LDE.Weapons.SingleBurst(self,self.Data.Bullet) end
local Bullet = {Recoil=200,CoolDown=2,ChargeRate=5,Radius=1,Single=true,MinCharge=50,MaxCharge=150,Dpc=4,ChargeType=true,ChargeShoot=Fire,Effect=Effects}
local Data={name="Basic RailGun",class="basic_railgun_weapon",WireOut={"Charge","CanFire"},In={"energy"},shootfunc=Func,ClientSetup=ClientSetup,Points=220,heat=5,firespeed=0.5,InUse={100},Bullet=Bullet}
local Makeup = {name={"Small RailGun"},model={"models/Slyfo_2/drone_railgun.mdl"},Path=Path,class=Data.class,Unlock=true,UnlockCost=8000,UnlockType="Exotic"}
LDE.Weapons.CompileWeapon(Data,Makeup)

--Large RailGun
local Fire = function(self) LDE.Weapons.SingleBurst(self,self.Data.Bullet) end
local Bullet = {Recoil=2000,CoolDown=20,ChargeRate=50,Radius=0.5,Single=true,MinCharge=500,MaxCharge=1500,Dpc=6,ShootDir=Vector(0,0,1),ShootPos=Vector(0,0,120),ChargeType=true,ChargeShoot=Fire,Effect=Effects}
local Data={name="Large RailGun",class="large_railgun_weapon",WireOut={"Charge","CanFire"},In={"energy"},MountType="CapRailGun",shootfunc=Func,ClientSetup=ClientSetup,Points=2000,heat=1000,firespeed=0.8,InUse={10000},Bullet=Bullet}
local Makeup = {name={"Large RailGun"},model={"models/mandrac/hybride/cap_railgun_gun1.mdl"},Path=Path,class=Data.class,Unlock=true,UnlockCost=16000,UnlockType="Exotic"}
LDE.Weapons.CompileWeapon(Data,Makeup)


-------------------------------------
---------Special Cannons-------------
-------------------------------------

local BulletFunc=function(self,Data,attacker,tr)
	local NewData = { 
		Pos 					=		tr.HitPos,							--Required--		--Position of the Explosion, World vector
		ShrapDamage	=		Data.ShrapDamage,			--Optional--		--Amount of Damage dealt by each Shrapnel that hits, if 0 or nil then other Shap vars are not required
		ShrapCount		=		Data.ShrapCount,										--Number of Shrapnel, 0 to not use Shrapnel
		ShrapDir			=		Vector(0,0,0),											--Direction of the Shrapnel, Direction vector, Example: Missile:GetForward()
		ShrapCone		=		180,															--Cone Angle the Shrapnel is randomly fired into, 0-180, 0 for all to be released directly forward, 180 to be released in a sphere
		ShrapRadius		=		Data.Radius/2,											--How far the Shrapnel travels
		ShockDamage	=		Data.Damage,					--Optional--		--Amount of Shockwave Damage, if 0 or nil then other Shock vars are not required
		ShockRadius		=		Data.Radius,												--How far the Shockwave travels in a sphere
		--Ignore			=		self,									--Optional--		--Entity that Shrapnel and Shockwaves ignore, Example: A missile entity so that Shrapnel doesn't hit it before it's removed
		Inflictor				=		self,							--Required--		--The weapon or player that is dealing the damage
		Owner				=		self:CPPIGetOwner()					--Required--		--The player that owns the weapon, or the Player if the Inflictor is a player
	}
	LDE:BlastDamage(NewData)
	
	local effectdata = EffectData()
		effectdata:SetOrigin(tr.HitPos)
		effectdata:SetScale(Data.Radius*2)
	util.Effect( Data.Effect or "prop_death", effectdata )
end

local Func = function(self,CanFire)
	if self.Active == 1 and CanFire then
		if not self.Moving then
			if(LDE.LifeSupport.ManageResources(self,1))then
				local Data = self.Data.Bullet
				Data.Ignore = {self,self.Barrel}
				LDE.Weapons.ShootShell(self,Data)
				self.Moving = true
				return true
			end
		end
	end
	return false
end

local ResetBarrel = function(self,Barrel)
	constraint.RemoveAll( Barrel )
	Barrel:SetPos(self:LocalToWorld(Vector(0,-75,0)))
	Barrel:SetAngles(self:GetAngles())
	Barrel:SetParent(self.ParentChip)
	Barrel:GetPhysicsObject():EnableMotion(false)
end

local OnThink = function(self) 	
	local Barrel = self.Barrel
	
	if not IsValid(Barrel) then return end
	
	if self.Moving then
		if self.BarrelAng<60 then
			self.BarrelAng=self.BarrelAng+5
			Barrel:SetAngles(Barrel:LocalToWorldAngles(Angle(5,0,0)))
		else
			self.BarrelAng=0
			self.Moving = false
		end
	end
	
	--Barrel checks >.< STAWP BREAKING MAH BARREL!
	if Barrel:GetParent()~= self.ParentChip then ResetBarrel(self,Barrel) end
	if Barrel:GetPhysicsObject():IsMoveable() then ResetBarrel(self,Barrel) end
	if constraint.HasConstraints( Barrel ) then ResetBarrel(self,Barrel) end
	
end

local Path = {"Weapons","Projectile","Cannons"}

local Intial = function(self)
	local Parent = ents.Create("prop_physics")
	Parent:SetModel( "models/cheeze/wires/cpu.mdl " )
	Parent:SetPos(self:GetPos())
	Parent:Spawn()
	Parent:SetParent(self)
	
	self.ParentChip = Parent
	
	local ent = ents.Create("prop_physics")
	ent:SetModel( "models/mandrac/projectile/cap_autocannon_gun.mdl" )
	ent:SetPos(self:LocalToWorld(Vector(0,-75,0)))
	ent:SetAngles(self:GetAngles())
	ent:Spawn()
	ent:SetParent(Parent)
	ent:GetPhysicsObject():EnableMotion(false)
	--ent:SetNotSolid( true )
	
	self.Moving = false
	self.BarrelAng = 0
	self.Barrel = ent
	self.LastShot = 0
		
	timer.Simple(0.2,function() LDE.GivePlyProp(LDE.GetPropOwner(self),ent) end)
end

--Gatling cannon
local Bullet = {ShrapCount=10,ShrapDamage=40,Spread=3,Radius=800,Damage=5000,Recoil=5000,Speed=100,BulletFunc=BulletFunc,FireSound="ambient/explosions/explode_4.wav",ShootDir=Vector(0,-1,0),ShootPos=Vector(0,-70-120,32),MuzzleFlash=3}
local Data={name="Gatling Cannon",class="gatling_cannon_weapon",In={"Shells"},Intial=Intial,OnThink=OnThink,shootfunc=Func,Points=1400,heat=50,firespeed=1,InUse={3},Bullet=Bullet}
local Makeup = {name={"Gatling Cannon"},model={"models/mandrac/projectile/cap_autocannon_gunbase.mdl"},Path=Path,class=Data.class}
LDE.Weapons.CompileWeapon(Data,Makeup)
--1400
-------------------------------------
-----------Plasma--------------------
-------------------------------------
local Path = {"Weapons","Exotic","Plasma Beam"}

--Hyper Rift Beam
local wire = function(self)
	self.Inputs = Wire_CreateInputs( self, { "Fire", "Multiplier" } ) 
	self.Outputs = Wire_CreateOutputs( self, { "Multiplier", "Energy/Sec", "DPS" } )
end

local Intial = function(self)
	self.multiplier = 1 
	Wire_TriggerOutput(self,"Multiplier",self.multiplier) 	
	
	self.playedcharge,self.playedfire,self.playedfiring,self.wirefire,self.chargetime 	= false,false,false,false,CurTime()	
	
	self.Sound = CreateSound( self, Sound("ambient/atmosphere/noise2.wav") )

	self:SetNWFloat( "multiplier", self.multiplier )  --Need this set or in some cases.. like advanced dupes, it can be nil and draw no effect (until the multiplier is changed)
end

local wirefunc = function(self,iname,value)
	if iname == "Fire" then
		if value == 1 then
			self.chargetime = CurTime() + 6			
			self.Active,self.wirefire = 1,true
		else 			
			self.Active,self.wirefire = 0,false

			self:StopSound(Sound("ambient.whoosh_huge_incoming1"))
			self:StopSound(Sound("explode_7"))
			self:StopSound(Sound("ambient/atmosphere/noise2.wav"))
			self:StopLoopingSound(1,Sound("ambient/atmosphere/noise2.wav"))
			
			if self.playedfiring == true then
				self.Sound:Stop()
				self.playedfiring = false
			end
   			self.playedcharge 	= false
			self.playedfire 	= false

			self:StopSound(Sound( "npc/strider/fire.wav" ))
			self:SetNWBool( "charging", false )
		end
	elseif (iname == "Multiplier") then
		if value < 1 then
			self.multiplier = 1
			self:SetNWFloat( "multiplier", self.multiplier )
		else
			if value > 3 then
				self.multiplier = 3
				self:SetNWFloat( "multiplier", self.multiplier )
			else 			
				self.multiplier = value
				self:SetNWFloat( "multiplier", self.multiplier )
			end 	  		
		end
		Wire_TriggerOutput(self,"Multiplier",self.multiplier)
	end
end

local Func = function(self,CanFire)
	if self.Active == 1 and CanFire then
		if self.wirefire == false then self.wirefire=true self.chargetime=CurTime()+6 end
		self:DoRes(self.multiplier)
		
		if self.playedcharge == false and self.energytofire then
			self:EmitSound(Sound("ambient.whoosh_huge_incoming1"))
			self:SetNWBool( "charging", true )
			self.playedcharge = true 				
		elseif not self.energytofire then
			self:StopSound(Sound("ambient.whoosh_huge_incoming1"))
			self.Sound:Stop()
			self.playedcharge = false
		end	
		
		if not self.energytofire then  --stop it from holding charge when energy breaks
			self:SetNWBool( "charging", false )
			self.chargetime = CurTime() + 6	
		end	
				
		if self.energytofire and CurTime() > self.chargetime  then
			if self.playedfire == false and self.energytofire then
				self:EmitSound(Sound("explode_7"))
				self:EmitSound(Sound( "npc/strider/fire.wav" ))
				self.playedfire = true 				
			elseif not self.energytofire then  				
				self:StopSound(Sound("explode_7"))
				self:StopSound(Sound( "npc/strider/fire.wav" ))
				self.playedfire = false 			
			end 
			if self.playedfiring == false and self.energytofire then
				--self:EmitSound(Sound("ambient/atmosphere/noise2.wav"))
				self.Sound:Play()
				self.playedfiring = true 				
			elseif not self.energytofire then   				
				self:StopSound(Sound("ambient/atmosphere/noise2.wav"))
				self.playedfiring = false 			
			end  			
			self:WeaponFiring()	
		else
			self:WeaponIdle()	
		end		
	else
		self.wirefire =  false
		self:StopSound(Sound("ambient.whoosh_huge_incoming1"))
		self:StopSound(Sound("explode_7"))
		self:StopSound(Sound("ambient/atmosphere/noise2.wav"))
		self:StopSound(Sound( "npc/strider/fire.wav" ))
		if self.playedfiring == true then
			self.Sound:Stop()
			self.playedfiring 	= false
		end
		self:WeaponIdle()
		self:SetNWBool( "charging", false )
	end
	
	self:SetNWFloat( "multiplier", self.multiplier )
	local energyneedsec = (self.energybase * self.multiplier)*10
	local damagesec = (self.damagebase * self.multiplier)*10
	Wire_TriggerOutput(self,"Plasma/Sec",energyneedsec)
	Wire_TriggerOutput(self,"DPS",damagesec)
	return true
end --Charge function

local shared = function(ENT)

	ENT.damagebase	= 200
	ENT.energybase 	= 2 --was 25 (10x for 1 second)
	ENT.energytofire = true
	ENT.chargetime = CurTime()

	if(SERVER)then
	
		function ENT:OnRemove()
			self.BaseClass.OnRemove(self)
			Wire_Remove(self)
			self:StopSound(Sound("ambient.whoosh_huge_incoming1"))
			self:StopSound(Sound("explode_7"))
			self:StopSound(Sound("ambient/atmosphere/noise2.wav"))
			self:StopSound(Sound( "npc/strider/fire.wav" ))
			self.Sound:Stop()
		end
		
		function ENT:WeaponFiring()
			local trace = {}
				trace.start = self:GetPos() + (self:GetForward() * 147.5) + (self:GetUp() * 9.25)
				trace.endpos = self:GetPos() + self:GetForward() * 100000
				trace.filter = self
					 
				tr = util.TraceLine( trace )

			self:SetNWBool( "drawbeam", true )
			self:SetNWBool( "charging", false )
			
			LDE.HeatSim.ApplyHeat(self,(1000*self.multiplier),false)
			
			if tr.Entity and IsValid(tr.Entity) then
				LDE.AdvDamage:BudderEffect(tr.Entity,(self.damagebase * self.multiplier),10,self,self)
			end
		end
		
		function ENT:DoRes(multi)
			local energy = self:GetResourceAmount("Plasma")
			local energyneed = self.energybase * multi
		   
			if energy >= energyneed then
				self.energytofire = true
				self:ConsumeResource("Plasma",energyneed)
				return true            
			else
				self.energytofire = false
				return false
			end
		end

		function ENT:WeaponIdle()
			self:SetNWBool( "drawbeam", false )
			
			if self.playedfiring == true then
				self.Sound:Stop()
				self.playedfiring 	= false
			end
		end
	else

	end
end

local client = function(self)
	local multi = self:GetNWFloat( "multiplier")
	local muzzel = self:GetPos() + (self:GetForward() * 147.5) + (self:GetUp() * 9.25) 
	
	if self:GetNWBool("charging") then
   		local effectdata = EffectData()
			effectdata:SetOrigin(muzzel)
			effectdata:SetMagnitude(multi)
			util.Effect( "ion_charge", effectdata )  	
			
		local effectdata = EffectData()
			effectdata:SetOrigin(muzzel)
			effectdata:SetNormal(self:GetForward())
			util.Effect( "ion_refract", effectdata )
   	end 	
	if self:GetNWBool("drawbeam") then
	
		local trace = {}
				trace.start = self:GetPos() + (self:GetForward() * 147.5) + (self:GetUp() * 9.25)
				trace.endpos = self:GetPos() + self:GetForward() * 100000
				trace.filter = self
				
				tr = util.TraceLine( trace )
				
		local dist = (tr.HitPos - trace.start):Length()
		local effectdata = EffectData()
				effectdata:SetOrigin(tr.HitPos)
				effectdata:SetNormal(tr.HitNormal)
				util.Effect( "ion_refract", effectdata )
				
		local effectdata = EffectData()
				effectdata:SetOrigin(tr.HitPos)
				effectdata:SetStart(trace.start)
				effectdata:SetMagnitude(multi)
				effectdata:SetScale(dist)
				util.Effect( "ion_beam", effectdata )
				
		util.Effect( "ion_impact", effectdata )		
   	end
end

local Data={name="Hyper Rift Beam",class="hyper_rift_laser_weapon",Client=client,Shared=shared,Intial=Intial,WireFunc=wirefunc,WireSpecial=wire,In={"Plasma"},MountType="Titan",shootfunc=Func,Points=3000,heat=0,firespeed=0.01,InUse={0}}
local Makeup = {name={"Hyper Rift Beam"},model={"models/Spacebuild/Nova/machuge.mdl"},Path=Path,class=Data.class,Unlock=true,UnlockCost=1000000,UnlockType="Exotic"}
LDE.Weapons.CompileWeapon(Data,Makeup)

--Winch
local Path = {"Weapons","Exotic","Winchs"}

local Func = function(self,CanFire)
	if(self.Active==1 and CanFire)then
		if (CurTime() >= self.MCDown) then
			if(LDE.LifeSupport.ManageResources(self,1))then
				local NewShell = ents.Create( "SF-GrappleH" )
				if ( not NewShell:IsValid() ) then return end
				NewShell:SetPos( self:GetPos() + (self:GetForward() * 50) )
				NewShell:SetAngles( self:GetAngles() )
				NewShell.SPL = self.SPL
				NewShell:Spawn()
				NewShell:Initialize()
				NewShell:Activate()
				local NC = constraint.NoCollide(self, NewShell, 0, 0)
				NC.Type = ""
				NewShell.PhysObj:SetVelocity(self:GetForward() * 5000)
				NewShell.Active = true
				NewShell.ATime = 0
				NewShell:Fire("kill", "", 120)
				NewShell.ParL = self
				NewShell:Think()
				NewShell:CPPISetOwnerless(true)
				
				self.MCDown = CurTime() + 1
				self:TurnOff()
				return true
			end
		end
	end
	return false
end

local WireFunc = function(self,iname,value)
	if (iname == "Launch") then
		if value > 0 then
			if self.Active == 0 then
				self:TurnOn()
			end
		else
			if self.Active == 1 then
				self:TurnOff()
			end
		end
	elseif (iname == "Length") then
		self.DLength = math.Clamp(value, 100, 5000)
		self.LChange = CurTime()
		
	elseif (iname == "Disengage") then
		if value > 0 then
			self.Disengaging = true
		else
			self.Disengaging = false
		end
		
	elseif (iname == "Speed") then
		self.ReelRate = math.Clamp(value, 0.01, 20)
		
	end
end

local WireSetup = function(self)
	self.Inputs = Wire_CreateInputs( self, { "Launch", "Length", "Disengage", "Speed" } )
	self.Outputs = Wire_CreateOutputs( self, { "CanLaunch", "CurrentLength","CanFire" })
	self.DLength = 0
	self.LChange = 0
	self.ReelRate = 5
	self.MCDown = 0
end

local Data={name="Winch",class="winch_weapon",In={"energy"},SafeAllow=true,shootfunc=Func,WireSpecial=WireSetup,WireFunc=WireFunc,Points=350,heat=5,firespeed=1,InUse={500}}
local Makeup = {name={"Winch"},model={"models/Slyfo/sat_grappler.mdl"},Path=Path,class=Data.class}
LDE.Weapons.CompileWeapon(Data,Makeup)


--------------------------------------
---------Blackhole Emitter------------
--------------------------------------
--[[
local BulletFunc=function(self,Data,attacker,tr)
	--SpawnBlackholeHere.
	local ent = ents.Create("hypermass")
	ent:SetPos(tr.HitPos)
	ent:Spawn()
	ent.Fuel = 10000
	ent.Drain = 10
	ent.Power = 500
end

local Func = function(self,CanFire) 
	LDE.Weapons.ShootShell(self,self.Data.Bullet)
	self.Charge=0
end

local Client = function(self) return LDE.Weapons.ShowCharge(self,self.Data.Bullet) end --Clientside
local ClientSetup = function(ENT) ENT.LDETools.Charge = Client end
local FireCannon = Func
local ChargeUp = function(self,CanFire) --Charge function
	LDE.Weapons.ManageCharge(self,self.Data.Bullet) 
	return true
end 

local Path = {"Exotic","Singularity"}
local Desc = "A weapon so powerful and feared, it has become illegal to own and fire."

--Large Pulse Laser
local Fire = function(self) self.Data.FireCannon(self) end
local Bullet = {ChargeShoot=Fire,BulletFunc=BulletFunc,Damage=1,Spread=0.1,Recoil=0,Speed=75,CoolDown=1,ChargeRate=1,MinCharge=95,MaxCharge=100,FireSound="sb/railgunlight.wav",TrailStartW=30,TrailColor=Color(127,255,255),TrailLifeTime=1,Model="models/cerus/weapons/projectiles/pc_proj.mdl"}
local Data={name="Singularity Emitter",class="sing_emit_weapon",In={"energy"},FireCannon=FireCannon,shootfunc=ChargeUp,ClientSetup=ClientSetup,Desc=Desc,Points=100,heat=67,firespeed=0.5,InUse={1500},Bullet=Bullet}
local Makeup = {name={"Singularity Emitter"},model={"models/slyfo/sat_laser.mdl"},Path=Path,class=Data.class,Unlock=true,UnlockCost=1000000000,UnlockType="Exotic"}
LDE.Weapons.CompileWeapon(Data,Makeup)
]]



Environments = Environments or {}
Environments.MapScaleLibrary = Environments.MapScaleLibrary or {}
local MSL = Environments.MapScaleLibrary

local LDE = LDE --Localise the global table for speed.
local Utl = EnvX.Utl --Makes it easier to read the code.
local NDat = Utl.NetMan --Ease link to the netdata table.

local MapScale = 10 --The rate at which we scale props.

if(SERVER)then
	function MSL.ScaleContraption(center)
		local Scaling = 1/MapScale
		local Props = constraint.ShipCoreDetect(center)
		
		center:SetModelScale( Scaling, 0 ) center:Activate() 
		for _, ent in pairs( Props ) do
			if ent ~= center then
				ent:SetPos(center:LocalToWorld(center:WorldToLocal(ent:GetPos())*Scaling))
				ent:SetModelScale( Scaling, 0 ) ent:Activate()
				
				--ent:SetParent(center)
			end
		end
		
		--ent:SetModelScale( Scaling, 0 ) ent:Activate() 
	end
	
	EnvX:HookNet("envx_scalethis_test",function(Data,ply)
		local MSG="Scaling Thing Triggered! "..tostring(ply:Nick()).." was the cause! \n"
		
		if ply:IsSuperAdmin() then
			local ent = ply:GetEyeTrace().Entity
			
			MSG=MSG.."TraceFound: "..tostring(ent).."\n"
			
			if not ent:IsWorld() then 
				MSL.ScaleContraption(ent)
			end
		end
		
		MsgAll(MSG)
	end)
else
	function MSL.TriggerScale()
		EnvX:NetworkData("envx_scalethis_test",{})
		print("Shoulda Triggered Scaling thing.")
	end
	concommand.Add("envx_scalethis",MSL.TriggerScale)
end		

print("Loaded!")




















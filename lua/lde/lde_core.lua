local LDE = LDE
local Utl = EnvX.Utl
local NDat = Utl.NetMan --Ease link to the netdata table.

--Random chat adverts
if(SERVER)then

	--Workshop bits...
	resource.AddWorkshop( "174935590" ) --Spore Models
	resource.AddWorkshop( "160250458" ) --Wire Models
	resource.AddWorkshop( "148070174" ) --Mandrac Models
	resource.AddWorkshop( "182803531" ) --SBEP Models
	resource.AddWorkshop( "247007332" ) --Envx
	resource.AddWorkshop( "231698363" ) --Npc Models
	
	function LDE:HealPlayer(ply,amount)
		if(not ply or not ply:IsValid() or not ply:IsPlayer() or not ply:Alive())then return end -- Error Checking.
		local health = ply:Health()		
		
		if health+amount < 250 then
			ply:SetHealth( health + amount )		
		elseif health+amount > 250 then
			ply:SetHealth( 250 )
		end	
	end
	
	
	function LDE.RoughThink()
		local plys = player.GetAll()
		for k,v in pairs(plys) do
			local Bount = v:GetLDEStat("Bounty")
			if Bount>0 then
				if v:GetLDEStat("Bounty")>5 then
					v:SetLDEStat( "Bounty", v:GetLDEStat("Bounty")-5)
				else
					v:SetLDEStat( "Bounty", 0)
				end
			end
			
			LDEFigureRole(v)
		end
	end
	Utl:SetupThinkHook("LDECoreThink",20,0,LDE.RoughThink)
	
	function LDE:NotifyPlayers(Source,String,Color)
		local plys = player.GetAll()
		for k,v in pairs(plys) do
			v:SendColorChat(Source,Color,String)
		end
	end
	
	function LDE:NotifyPlayer(Ply,Source,String,Color)
		Ply:SendColorChat(Source,Color,String)
	end
	
	function LDEFigureRole(ply)
		local oldrole = ply:GetLDERole()
		local Stats = ply:GetStats()
		
		local Roles = LDE.Rolesys.Roles
		local Role = {}
		
		for name,role in pairs(Roles) do
			if(LDE.Rolesys.CanFillRole(ply,role))then
				if(Role.name)then
					if(role.Priority>Role.Priority)then
						Role=role
					end
				else
					Role = role
				end
			end
		end

		if(Role.name~=oldrole)then
			local Text = ply:GetName().." is now a "..Role.name.."."
			LDE:NotifyPlayers("Stats",Text,{r=0,g=100,b=255})
			ply:SetLDERole(Role.name)
			ply:SetLDEStat("Moral",Role.Moral)
			
			EnvX.Debug(Text,2,"Player Related")
		end

		--LDE.Cash:UpdatePerson(ply)
	end
	
	function LDEPlayDeath(victim, weapon, killer)
		if not IsValid(victim) then return end --Idk This is wierd

		LDE.PlayerData.HandleMutations(victim,"OnDeath",{weapon=weapon,attacker=killer}) --Call the ondeath hook for mutations.
		LDE.PlayerData.HandleMutations(killer,"OnKill",{weapon=weapon,victim=victim}) --Call the ondeath hook for mutations.
		victim:ClearMutations()	--Clear all mutations.
		
		if not killer:IsPlayer() then killer = killer:CPPIGetOwner() end
		if victim==killer or not killer or not IsValid(killer) or not killer.GiveLDEStat then 
			local Text = victim:GetName().." has died through their own means."
			--LDE.Logger.LogEvent( Text )
			return 
		end
		
		killer:GiveLDEStat("Kills",1)
		LDEFigureRole(killer)
		local Text = victim:GetName().." was killed by "..killer:GetName()
		--LDE.Logger.LogEvent( Text )
		LDE:NotifyPlayers("Stats",Text,{r=255,g=0,b=0})
	end
	hook.Add("PlayerDeath","ldeplayerdeath",LDEPlayDeath)
	
	function LDEFirstSpawn(ply)
		ply.dbReady = false
		ply.Stats = {} --Our Stats Table
		ply.SStrings = {} --Our Strings Table
		--LDE.Cash.getstats( ply )
		local Text = ply:GetName().." has spawned."
		
		--TellPlayers(Text)
		LDE:NotifyPlayers("Server",Text,{r=150,g=150,b=150})

		EnvX.Debug(Text,3,"Player Related")
	end
	Utl:HookHook("PlayerInitialSpawn","ldeplayerispawn",LDEFirstSpawn,1)
	
	function LDELeftServ(ply)
		local Text = ply:GetName().." has disconnected from the server. (SteamID: "..ply:SteamID().." )"
		--TellPlayers(Text) --Fixed
		LDE:NotifyPlayers("Server",Text,{r=150,g=150,b=150})
		EnvX.Debug(Text,3,"Player Related")
	end
	Utl:HookHook("PlayerDisconnected","ldedisconnected",LDELeftServ,1)
	
	function LDEPlayConnect( name, address )
		local Text = name .. " has connected!"
		--TellPlayers( Text )
		LDE:NotifyPlayers("Server",Text,{r=150,g=150,b=150})
		EnvX.Debug(Text,3,"Player Related")
	end
	Utl:HookHook("PlayerConnect","ldeconnected",LDEPlayConnect,1)
		
	--[[----------------------------------------------------
	Serverside Chat Functions.
	----------------------------------------------------]]--
	
	function LDECoreChat( sender, text, teamChat  )
		--MsgAll("Chat Thest \n")
		--Utl:NotifyPlayers(sender:GetLDERole().." "..sender:Nick(),text,team.GetColor(sender:Team()))
		--return ""
	end
	--Utl:HookHook("PlayerSay","LDECoreChat",LDECoreChat,50)
	
	--Temporary until utl:hookhook works.

	function Utl:NotifyPlayers(Source,String,Color)
		local plys = player.GetAll()
		for k,v in pairs(plys) do
			v:SendColorChat(Source,Color,String)
		end
	end
	
	function Utl:NotifyPlayersAdv(ply,...)
		local plys = player.GetAll()
		local Dat = { ... }
		
		for k,v in pairs(plys) do
			v:SendColorChatAdvanced(Dat)
		end
	end
	
	local meta = FindMetaTable("Player")

	function meta:SendColorChat(nam,col,msg)
		EnvX:NetworkData("envx_colorchat",{MSG={col,nam,Color(255,255,255,255),": "..msg}},self)
	end
	
	function meta:SendColorChatAdvanced(Dat)
		EnvX:NetworkData("envx_colorchat",{MSG=Dat},self)
	end
	
	EnvX:HookNet("EnvXFChat",function(Data,ply)
		local Players = player.GetAll()
		for i=1,#Players do
			if ply ~= Players[i] and Players[i]:GetLDEString("Faction") == ply:GetLDEString("Faction") then
				Players[i]:SendColorChatAdvanced({Color(180,0,0),"[FACTION CHAT]",team.GetColor(ply:Team()),"["..ply:GetLDERole().."] "..ply:Nick(),Color(255,255,255,255),": ",Data.FCMsg})
			end
		end
	end)
else
	local MC = EnvX.MenuCore
	EnvX.ChatBox = false
	
	--[[----------------------------------------------------
	ClientSide Chat Handling.
	----------------------------------------------------]]--
	EnvX:HookNet("envx_colorchat",function(Data)
		chat.AddText(unpack(Data.MSG))
		if EnvX.ChatBox then

		end
	end)
	EnvX.LoadFile("vgui/chat.lua",1)
	local function loadchatbox()
		EnvX.ChatBox = not EnvX.ChatBox
	end
	concommand.Add("envx_load_chatbox", loadchatbox)
end

function LDE:IsLifeSupport(ent)
	if(not ent or not IsValid(ent))then return end
	if(ent.IsLS and not ent.IsNode)then
		return true
	else
		return false
	end
end

function LDE_EntCreated(ent)--Entity Spawn hook.
	if(LDE:CheckBanned(ent))then --LDE:Debug("Illegal Entity spawned. Removing it.") 
		ent:Remove() 
		return 
	end
	
	if IsValid(ent) and not ent:IsWeapon() and CurTime() > 5 then
		timer.Simple( 0.25, function()  
			if not ent or not IsValid(ent) then 
				return 
			end 
			LDE_Filter( ent ) 
		end)  --Need the timer or the ent will be detect as the base class and with no model.
		
		--Generate an base LDE table if one does not exist.
		if not ent.LDE then
			ent.LDE = {
				Temperature=0,
				HeatCapacity=0,
				MeltingPoint=0,
				FreezingPoint=0,
				Core=NULL
			}
		end
	end
end
Utl:HookHook("OnEntityCreated", "LDE_EntCreated", LDE_EntCreated,1)


function LDE_Filter(ent) --Because the hook finds EVERYTHING, lets filter out some usless junk 	
	if not IsValid(ent) then return false end
    if ent:GetClass() == "gmod_ghost" then return false end	
    if ent:GetSolid() == SOLID_NONE then return false end
    if ent:IsNPC() then return false end
	
	LDE:Spawned( ent ) --Anything not filtered goes to the spawned function,
end 

hook.Add( "CanDrive","FUCKCANDRIVE", function( ply, ent ) return false end)

function LDE:CheckBanned(ent)
	if ent == nil or not IsValid(ent) then print("Null LDE CheckBanned Ent") return false end
	local str = ent:GetClass()
	for _,v in pairs(LDE.BannedClasses) do
		if(string.find(str,v))then
			return true
		end
	end
	return false
end

function LDE:Spawned( ent )
	if not ent or not IsValid(ent) then print("LDE: Error Invalid Entity Spawn Report!") return end
	ent.LDE = ent.LDE or {} 
	
	if(SERVER)then
	
		if ent:IsPlayer() then return end
		
		local Health = LDE:CalcHealth( ent )
		
		--HeatSimulation Variables
		ent.LDE.Temperature = 0
		ent.LDE.MeltingPoint=Health/10
		ent.LDE.FreezingPoint=(Health/20)*-1
		ent.LDE.OverHeating = false
		
		--Networking....
		ent:SetNWInt("LDEEntTemp", ent.LDE.Temperature)
		ent:SetNWInt("LDEMinTemp", ent.LDE.FreezingPoint)
		ent:SetNWInt("LDEMaxTemp", ent.LDE.MeltingPoint)
		
		--Damage Control Spawn function
		local MaxHealth = LDE:MaxHealth()
		local MinHealth = LDE:MinHealth()
		
		if Health < MaxHealth and Health > MinHealth then
			ent.LDEHealth = Health
			ent.LDEMaxHealth = Health
			--Msg("I am: "..tostring(ent).."	My Health is: "..tostring(Health))
		elseif Health >= MaxHealth then
			ent.LDEHealth = MaxHealth
			ent.LDEMaxHealth = MaxHealth
			--Msg("I am: "..tostring(ent).."	My Health is Max at: "..tostring(MaxHealth))
		elseif Health <= MinHealth then
			ent.LDEHealth = MinHealth
			ent.LDEMaxHealth = MinHealth
			--Msg("I am: "..tostring(ent).."	My Health is Min at: "..tostring(MinHealth).." from: "..tostring(Health))
		else
			--Msg("You Broke Somthing... in the part where health is figured out")
		end
	end
end

-- This function basically deals with stuff that happens when a player hops out of a vehicle
function SetExPoint(player, vehicle)
	if vehicle.ExitPoint and vehicle.ExitPoint:IsValid() then
		local EPP = vehicle.ExitPoint:GetPos()
		local VP = vehicle:GetPos()
		local Dist = EPP:Distance(VP)
		if Dist <= 500 then
			player:SetPos(vehicle.ExitPoint:GetPos() + vehicle.ExitPoint:GetUp() * 10)
			vehicle.ExitPoint.CDown = CurTime() + 0.5
		end
	end
	
	if player.CamCon then
		player.CamCon = false
		player:SetViewEntity()
	end
end

Utl:HookHook("PlayerLeaveVehicle", "PlayerRepositioning", SetExPoint,1)
LDE.SetExPoint = SetExPoint




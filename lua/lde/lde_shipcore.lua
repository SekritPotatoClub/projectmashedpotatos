
--Get new sounds!
local sounds = {Sound("tech/sga_impact_01.wav"),Sound("tech/sga_impact_02.wav"),Sound("tech/sga_impact_03.wav"),Sound("tech/sga_impact_04.wav")}
for k,v in pairs(sounds) do util.PrecacheSound(v) end

LDE.CoreSys = {}

LDE.CoreSys.Cores = {}
LDE.CoreSys.Shields = {}

--Over Time effects on shields.
LDE.CoreSys.ShieldAge = function(self,Data)
	if(not Data)then return false end
	if(Data.ShieldAge<0)then
		if(self.LDE.CanRecharge==1)then
			if(self.LDE.CoreShield>self.LDE.CoreMaxShield)then
				self.LDE.CoreShield=self.LDE.CoreMaxShield
			elseif(self.LDE.CoreShield>self.LDE.CoreMaxShield*Data.ShieldAge)then
				self.LDE.CoreShield = self.LDE.CoreShield - self.LDE.CoreMaxShield*Data.ShieldAge
			elseif(self.LDE.CoreShield<self.LDE.CoreMaxShield*Data.ShieldAge)then
				self.LDE.CoreShield = 0
			end			
		end
	else
		if(self.LDE.CoreShield>self.LDE.CoreMaxShield)then
			self.LDE.CoreShield=self.LDE.CoreMaxShield
		elseif(self.LDE.CoreShield>self.LDE.CoreMaxShield*Data.ShieldAge)then
			self.LDE.CoreShield = self.LDE.CoreShield - self.LDE.CoreMaxShield*Data.ShieldAge
		elseif(self.LDE.CoreShield<self.LDE.CoreMaxShield*Data.ShieldAge)then
			self.LDE.CoreShield = 0
		end
	end
	
	if(self.LDE.CoreShield>self.LDE.CoreMaxShield)then
		self.LDE.CoreShield=self.LDE.CoreMaxShield
	end
end

--Temperature Change over time.
LDE.CoreSys.Radiate = function(self,Data)
	if(not self.LDE or not self.LDE.CoreTemp)then return false end
	if(self.LDE.CoreTemp>0)then
		local Resist = self.LDE.CoreMaxTemp*(0.1*Data.CoolBonus)
		if(self.LDE.CoreTemp>Resist)then
			self.LDE.CoreTemp = self.LDE.CoreTemp - Resist
		elseif(self.LDE.CoreTemp<Resist)then
			self.LDE.CoreTemp = 0
		end
	else
		--Add Heating logic here.
	end
end

local ShipClasses = {
	"Heavy - Fighter / Bomber / Interceptor",
	"Corvette",
	"Frigate",
	"Heavy Frigate",
	"Destroyer",
	"Cruiser",
	"Battle-Cruiser",
	"Battleship",
	"Dreadnought",
	"Super Battleship",
	"Class 1 Leviathan",
	"Class 2 Leviathan",
	"Class 3 Leviathan",
	"Class 1 Titan",
	"Class 2 Titan",
	"Battle-Barge",
	"Super Dreadnought",
	"Leviathan Destroyer",
	"Mega Titan",
	"Super Leviathan",
	"Titan Destroyer",
	"Eversor Regalis",
	"That's No Moon!",
	"Mechanical Planet",
	"Galactic Vengeance",
	"HOLY FUCKING JESUS"
}

--Determines the cores class
LDE.CoreSys.CoreClass = function(self)
	local T = self.LDE.CoreMaxShield+self.LDE.CoreMaxHealth
	self.LDE.TotalHealth = T
	local Classification = "Fighter / Bomber / Interceptor"
	
	for i, cls in pairs( ShipClasses ) do
		local Scale = 20000*(i*((i/5)+(i/10)))
		if T > Scale then
			Classification = cls
		else
			break
		end
	end

	self.ShipClass = Classification
	self:SetNWInt("LDECoreClass", Classification)
end

--Calculates the health of a core.
LDE.CoreSys.CoreHealth = function(self,Data)
	-- Get all constrained props
	self.Props = constraint.ShipCoreDetect(self.Entity)

	local hp = self.LDE.CoreHealth
	local sd = self.LDE.CoreShield
	local healthscalething = 1
	local maxhp = 1
	local maxsd = 1
	local sinkers = 0
	local meltp	= 1
	local freezep=-1
	local temp = 0
	local CPS = 0
	
	for _, ent in pairs( self.Props ) do
		if ent and LDE:CheckValid( ent ) then
			if not ent.LDE then ent.LDE = {} end

			if not self.PropHealth then self.PropHealth={} end --Make sure we have the prop health table.		
			if not ent.LDEHealth then LDE:CalcHealth( ent ) end
			local entcore = ent.LDE.Core
			local capacity = ent.LDE.HeatCapacity or 0
			local maxheat = ent.LDE.MeltingPoint or 0
			local minheat = ent.LDE.FreezingPoint or 0
			local temper = ent.LDE.Temperature or 0
			local health = self.PropHealth[ent:EntIndex()] or 0
			local enthealth = LDE:GetHealth(ent)*Data.HealthRate
			
			local Calcedhealth = LDE:CalcHealth(ent)
			local maxhealth = (Calcedhealth)*Data.HealthRate
			local entshield = (Calcedhealth)*Data.ShieldRate
			local entpoints = Calcedhealth*Data.CPSRate
			
			LDE.HeatSim.SetTemperature(ent,0)
			
			if string.find(ent:GetClass(),"spore") then 
				continue
				--health,enthealth,entshield,entpoints=0,0,0,0  --Spores dont get any treatment
			else
				if not entcore or not IsValid(entcore) then -- if the entity has no core
					ent.LDE.Core = self
					ent.Shield = self --Environments Damage Override Compatability
					self.PropHealth[ent:EntIndex()] = enthealth
					self:CoreLink(ent) --Link it to our core :)
					hp = hp + enthealth
				elseif entcore and entcore == self and enthealth ~= health then -- if the entity's health has changed
					hp = hp - health -- subtract the old health
					hp = hp + enthealth -- add the new health
					self.PropHealth[ent:EntIndex()] = enthealth
				elseif entcore and entcore ~= self then -- if the entity already has a core
					continue --Guess we dont get that prop :(
				end
			end
			maxhp=(maxhp+maxhealth)
			maxsd=maxsd+entshield
			sinkers=sinkers+capacity
			meltp=meltp+maxheat
			freezep=freezep+minheat
			temp=temp+temper
			CPS=CPS+entpoints
			healthscalething=healthscalething+Calcedhealth
		end
	end
	
	-- Set health
	self.LDE.CoreHealth = hp
	self.LDE.CoreMaxHealth = maxhp
	self.LDE.CoreMaxShield = maxsd
	self.LDE.CoreTemp = self.LDE.CoreTemp+temp
	self.LDE.CoreMaxTemp = meltp*Data.TempResist
	self.LDE.CoreMinTemp =  freezep*Data.TempResist
	self.LDE.MaxCorePoints=CPS
	
	self.LDE.ShieldCostMultiplier = healthscalething
	
	if (self.LDE.CoreHealth > self.LDE.CoreMaxHealth) then 
		self.LDE.CoreHealth = self.LDE.CoreMaxHealth
	end
	
	-- Wire Output
	WireLib.TriggerOutput( self, "Health", self.LDE.CoreHealth or 0 )
	WireLib.TriggerOutput( self, "Total Health", self.LDE.CoreMaxHealth or 0 )
	WireLib.TriggerOutput( self, "Shields", self.LDE.CoreShield or 0 )
	WireLib.TriggerOutput( self, "Max Shields", self.LDE.CoreMaxShield or 0 )
	WireLib.TriggerOutput( self, "Temperature", self.LDE.CoreTemp)
	WireLib.TriggerOutput( self, "Freezing Point", self.LDE.CoreMinTemp or 0 )
	WireLib.TriggerOutput( self, "Melting Point", self.LDE.CoreMaxTemp or 0 )	
	WireLib.TriggerOutput( self, "OverHeating", self.OverHeating or 0 )
	WireLib.TriggerOutput( self, "Mount Points", self.LDE.CorePoints or 0)	
	WireLib.TriggerOutput( self, "Mount Capacity", self.LDE.MaxCorePoints or 0 )	
end

LDE.CoreSys.CoreModels = {
	"models/props_wasteland/panel_leverBase001a.mdl",
	"models/Slyfo_2/miscequipmentfieldgen.mdl",
	"models/Cerus/Modbridge/Misc/LS/ls_gen11a.mdl",
	"models/SmallBridge/Life Support/sbfusiongen.mdl",
	"models/Slyfo_2/rocketpod_bigrockethalf.mdl",
	"models/Slyfo_2/miscequipmentmount.mdl",
	"models/props_lab/reciever01b.mdl",
	"models/SBEP_community/d12shieldemitter.mdl"
}

LDE.CoreSys.CoreStats = {}

LDE.CoreSys.Settings = {}
local Settings = LDE.CoreSys.Settings
LDE.CoreSys.Settings["Core Type"] = {Type="DropDown",Options={},Catagory = "shipcore",}

--Base Device Code we will inject the functions into.
function LDE.CoreSys.RegisterCore(Data)	
	LDE.CoreSys.CoreStats[Data.name] = Data
	LDE.CoreSys.Settings["Core Type"].Options[Data.name]=Data.name
	
	scripted_ents.Alias(Data.class, "base_env_shipcore")--This makes it so old dupes still work.
	
	print("Core Type Registered: "..Data.name)
end

local LoadFile = EnvX.LoadFile --Lel Speed.
local Path =  "lde/cores/"

LoadFile(Path.."lde_maincores.lua",1)

local DisplayInfos = {}
DisplayInfos["Health Multiplier"]="HealthRate"
DisplayInfos["Armour Multiplier"]="ArmorRate"
DisplayInfos["Shield Multiplier"]="ShieldRate"
DisplayInfos["Shield Decay Rate"]="ShieldAge"
DisplayInfos["Temperature Resistance"]="TempResist"
DisplayInfos["Coolant Bonus"]="CoolBonus"
DisplayInfos["Processor Multiplier"]="CPSRate"


local DisplayFunc = function(setting_list,settings)
	local ID = settings["Core Type"] or "Basic Core"
	local Data = LDE.CoreSys.CoreStats[ID]
	
	for i,v  in pairs( DisplayInfos ) do
		Info = vgui.Create( "DLabel" ) 
		Info:SetText(i..": "..Data[v])
		Info:SetDark( true )
		setting_list:Add( Info ) table.insert(setting_list.Infos,Info)
	end
end

local NodeText = "Essential component to spacecraft. \nAllows you to attach weaponry and makes your ship more resilliant."

for k,v in pairs(LDE.CoreSys.CoreModels) do
	local ToolInfo = {Path={"Ship Core"},DevName=v,class="base_env_shipcore",model=v,settings=Settings,displayinfo=DisplayFunc,nodetext=NodeText}
	Environments.RegisterDevice(ToolInfo)
end
	
Msg("LDE Core System Loaded: Successfully\n")




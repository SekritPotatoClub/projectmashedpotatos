local Utl = EnvX.Utl --Makes it easier to read the code.
local NDat = Utl.NetMan --Ease link to the netdata table.

if(SERVER)then
	
	function GetResourceData(Name)
		for t, r in pairs(LDE.Market.Resources) do
			if r.O == Name or r.name == Name then
				return r
			end
		end
	end
	
	EnvX:HookNet("envx_tradeconsole_price_request",function(D,P)
		--print("Market Request from: "..P:Name().." for "..D.Res)
		local Resource = GetResourceData(D.Res)
		if not Resource then Resource=GetResourceData(string.lower(D.Res))  end
		if not Resource then print("Couldnt Find Resource.. "..D.Res) return end
		EnvX:NetworkData("envx_tradeconsole_price_update",{Cost=Resource.C},P)
		--print("Update sent out.")	
	end)	
	
	function findvalue(find)
		for k,v in pairs(LDE.Market.Resources) do
			if find==v.name then
				return v.C
			end
		end
	end

	EnvX:HookNet("Envx_MarketTransaction",function(D,P)
		local ent,res,amt = D.Ent,D.Res,D.Amt
		if D.Mode == "Sell" then
			--Msg("Attempting to sell "..res.." A: "..amt.."\n")
			if ent:GetResourceAmount(res)>=amt then
				local value = findvalue(res)
				ent:ConsumeResource(res, amt)
				--Msg(amt*value.."\n")
				P:GiveCash(amt*value)
				P:GiveLDEStat("Trades",amt)
			end
		else
			local value = findvalue(res)
			--Msg(profit.."\n")
			if(profit==0)then return end
			P:TakeCash(amt*value)
			ent:SupplyResource(res,amt)
		end
	end)

else
	local MC = EnvX.MenuCore

	local function Smallifynumber(Number)
		Number=tonumber(Number)
		if(Number>1000000)then return math.floor(Number/1000000).."M"
		elseif(Number>1000)then return math.floor(Number/1000).."K"
		else return math.floor(Number)
		end
	end
	
	local function GetTradeAmount()
		return math.Clamp(GetConVarNumber( "tradeamount" ),0,10000)
	end
	
	local VGUI = {}
	function VGUI:Init()
		
		local TradeList = LDE.Market.Resources
		local curselected = {}	

		local MarketMenu = MC.CreateFrame({x=700,y=400},true,true,false,true)
		MarketMenu:Center()
		MarketMenu:SetTitle( "Trade Market" )
		MarketMenu:MakePopup()

		local schematicBox = MC.CreateList(MarketMenu,{x=150,y=350},{x=10,y=35},false)
		schematicBox:SetParent(MarketMenu)
		schematicBox:AddColumn("Resources") -- Add column
		
		--Schematic Items--
		for k,v in pairs(TradeList) do
			schematicBox:AddLine(v.name)
		end
			
		local infoBox = vgui.Create( "DPanel", DermaFrame ) 
		infoBox:SetPos( 165, 35 )
		infoBox:SetSize( 350, 350)
		infoBox:SetParent(MarketMenu)
		infoBox.Paint = function(self,w,h)   
			draw.RoundedBox( 16, 0, 0, w, h, EnvX.GuiThemeColor.FG )
			
			if schematicBox:GetSelected() and schematicBox:GetSelected()[1] then 
				local selectedValue = schematicBox:GetSelected()[1]:GetValue(1) 
				-- Get description data ----------------------
				for k,v in pairs(TradeList) do
					if(selectedValue==v.name)then
						curselected = v
						itemDesc=v.desc
						break
					end
				end
				if not self.Cur then self.Cur = 0 self.Source = curselected end
				if CurTime()>=self.Cur or self.Source.name ~= curselected.name then
					self.Source = curselected
					self.Cur = CurTime()+2
					EnvX:NetworkData("envx_tradeconsole_price_request",{Res=schematicBox:GetSelected()[1]:GetValue(1)})
				end
				
				local TC = EnvX.GuiThemeColor.Text
				surface.SetTextColor( TC.r, TC.g, TC.b, TC.a )
				posy = 10
				surface.SetTextPos( 15, posy )
				surface.DrawText(curselected.name)
				posy = posy + 10
				surface.SetTextPos( 15, posy )
				if(cost)then
					surface.DrawText("Cost Per Unit: "..(cost))
				end
				posy = posy + 10
				surface.SetTextPos( 15, posy )
				surface.DrawText("-----------------")
				posy = posy + 10
				
				local explode = string.Explode(" ",itemDesc or "")
				local NewLines = {}
				
				local line = ""
				for _, textLine in pairs(explode) do
					local text = line..textLine.." "
					tw,th = surface.GetTextSize(text)
					if tw < w-20 then
						line = text
					else
						table.insert(NewLines,line)
						line = textLine.." " 
					end
				end
				table.insert(NewLines,line)
				
				for _, textLine in pairs (NewLines) do
					surface.SetTextPos( 15, posy )
					surface.DrawText(textLine)
					posy = posy + 10
				end
			end						
		end

		if(not Mode)then Mode = "Buy" end

		CreateClientConVar( "tradeamount", "0",false,false)

		
		local Slide = MC.CreateSlider(MarketMenu,{x=170,y=30},{x=520,y=205},{Min=0,Max=10000,Dec=0},"Amount")
		Slide:SetConVar( "tradeamount" )
		
		local change = MC.CreateButton(MarketMenu,{x=180,y=40},{x=520,y=165})
		change.DoClick = function() end
		change.Paint = function() 
			local cost = tonumber(cost) or 0
			if(Mode=="Buy")then
				change:SetText( "Taus: "..Smallifynumber(cost*GetTradeAmount()) )
			else
				change:SetText( "Taus: "..Smallifynumber((cost*GetTradeAmount())*0.9) )
			end
		end
		
		local confirm = MC.CreateButton(MarketMenu,{x=180,y=40},{x=520,y=280})
		confirm:SetText( "Mode: "..Mode.." Confirm" )
		confirm.DoClick = function ()
			if schematicBox:GetSelected() and schematicBox:GetSelected()[1] then
				if(Mode=="Sell" and curselected.E)then return end
				print("Performing transaction!")
				EnvX:NetworkData("Envx_MarketTransaction",{Mode=Mode,Res=curselected.name,Amt=GetTradeAmount(),Ent=self.Ent})
				print("Updating prices!")
				EnvX:NetworkData("envx_tradeconsole_price_request",{Res=curselected.O})
			end
		end
				
		local buyButton = MC.CreateButton(MarketMenu,{x=90,y=30},{x=520,y=245})
		buyButton:SetText( "Buy" )
		buyButton.DoClick = function ()
			Mode = "Buy"
			confirm:SetText( "Mode: "..Mode.." Confirm" )
		end
				
		local sellButton = MC.CreateButton(MarketMenu,{x=90,y=30},{x=610,y=245})
		sellButton:SetText( "Sell" )
		sellButton.DoClick = function ()
			if(not curselected.E)then
				Mode = "Sell"
				confirm:SetText( "Mode: "..Mode.." Confirm" )
			end
		end
				
		local cancelButton = MC.CreateButton(MarketMenu,{x=180,y=60},{x=520,y=325})
		cancelButton:SetText( "Cancel" )
		cancelButton.DoClick = function ()
			MarketMenu:Remove()
		end
				
	end
	 
	vgui.Register( "MarketMenu", VGUI )
	
	EnvX:HookNet("envx_tradeconsole_open",function(D,P)
		if MC.CheckOpenFrame(false) then return end

		local Window = vgui.Create( "MarketMenu")
		Window:SetMouseInputEnabled( true )
		Window:SetVisible( true )
			
		Window.Ent = D.Ent
	end)
	
	EnvX:HookNet("envx_tradeconsole_price_update",function(D,P)
		cost = tonumber(D.Cost)
		--print("Recieved market update.")
	end)	
end		
		
		
		
		
		
		
		

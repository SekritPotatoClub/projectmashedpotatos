local LDE = LDE --Localise the global table for speed.
local Utl = EnvX.Utl --Makes it easier to read the code.
local NDat = Utl.NetMan --Ease link to the netdata table.

--Dont touch this. At all. Or I will murder you.
if(SERVER)then
	local ServerIp = tostring(GetConVarString("ip"))

	EnvX:HookNet("RequestOfficalStatus",function(Data,ply)
		EnvX:NetworkData("ReplyOfficalStatus",{IP = ServerIp},ply)
	end)

else
	local MC = EnvX.MenuCore

	LDE.ServerLists = {}
	
	LDE.OfficalStatus = "Click to Check Server Offical Status"
	LDE.StatusColor = Color(255,0,0,255)
	
	local function OnSelect(B,N)
		local ply = B.Players[N]
		B.PI:Clear() B.SL:Clear() B.PI.Lines = {} B.SL.Lines = {}
		
		B.PI:AddLine("Name",N)
		B.PI:AddLine("Team",team.GetName(ply:Team()))
		B.PI:AddLine("IsAdmin",tostring(ply:IsAdmin() or ply:IsSuperAdmin()))
		
		B.PI:DoList(ply)
		B.SL:DoList(ply)

		B.Selected = ply
	end
	
	hook.Add("LDEFillCatagorys","Stats", function()
		local PDA = MC.PDA.Menu.Catagorys
		
		--Player Stats
		
		local base = vgui.Create( "DPanel", PDA )
		base:SizeToContents()
		base.Paint = function() end
		PDA:AddSheet( "Stats", base, "icon16/chart_bar.png", false, false, "View your stats." ) 
		base.Players = {}
		
		local PL = MC.CreateList(base,{x=160,y=525},{x=0,y=0},false,function(V) OnSelect(base,V) end)
		PL:AddColumn("Player") -- Add column
		for k,v in pairs(player.GetAll()) do base.Players[v:Name()]=v PL:AddLine(v:Name()) end
		
		base.PL = PL
		
		local PI = MC.CreateList(base,{x=250,y=140},{x=170,y=0},false,function() end)
		PI:AddColumn("Info") -- Add column
		PI:AddColumn("Value") -- Add colum		
		PI.Lines = {}
		
		function PI:DoList(ply)
			for k,v in pairs(ply:GetStrings()) do
				if not self.Lines[k] then
					self.Lines[k]=self:AddLine(k,v)
				else
					self.Lines[k]:SetColumnText(2,v)
				end
			end
		end
		
		base.PI = PI
		
		local SL = MC.CreateList(base,{x=250,y=375},{x=170,y=150},false,function() end)
		SL:AddColumn("Stat") -- Add column
		SL:AddColumn("Amount") -- Add column
		SL.Lines = {}
		
		function SL:DoList(ply)	
			for k,v in pairs(ply:GetStats()) do 
				if not self.Lines[k] then
					self.Lines[k]=self:AddLine(k,v)
				else
					self.Lines[k]:SetColumnText(2,v)
				end
			end
		end
		
		base.SL = SL
		
		OnSelect(base,LocalPlayer():Name())
		
		base.NextUpdate = CurTime()
		
		function base:Think()
			if self.NextUpdate<CurTime() then
				if IsValid(self.Selected) then
					self.PI:DoList(self.Selected)
					self.SL:DoList(self.Selected)			
				end
				self.NextUpdate = CurTime()+1
			end
		end
		
		--Server Stats
		local Office = MC.CreateButton(base,{x=340,y=40},{x=430,y=0},LDE.OfficalStatus,function() 
			EnvX:NetworkData("RequestOfficalStatus",{})
		end)
		Office.Think = function(self)
			self:SetText(LDE.OfficalStatus)
			self:SetColor(LDE.StatusColor)
		end
		
		--Add connect dialogue on selection.
		local ServerListOffical = MC.CreateList(base,{x=340,y=230},{x=430,y=50},false,function() end)
		ServerListOffical:AddColumn("Official Servers") -- Add column
		ServerListOffical:AddColumn("IP") -- Add column	
		ServerListOffical:AddColumn("Port") -- Add column	
				
		function ServerListOffical:PopulateList()
			--print("Populating List: ServerListOffical")
			self:Clear()
			for k,v in pairs(LDE.ServerLists.Official or {}) do 
				self:AddLine(k,v.IP,v.Port) 
			end
		end
		
		ServerListOffical:PopulateList()
		LDE.ServerListOffical = ServerListOffical
		
		--Add connect dialogue on selection.
		local ServerListCommunity = MC.CreateList(base,{x=340,y=235},{x=430,y=290},false,function() end)
		ServerListCommunity:AddColumn("Community Servers") -- Add column
		ServerListCommunity:AddColumn("IP") -- Add column
		ServerListCommunity:AddColumn("Port") -- Add column	
		
		function ServerListCommunity:PopulateList()
			--print("Populating List: ServerListCommunity")
			self:Clear()
			for k,v in pairs(LDE.ServerLists.Community or {}) do 
				self:AddLine(k,v.IP,v.Port) 
			end
		end
		
		ServerListCommunity:PopulateList()
		LDE.ServerListCommunity = ServerListCommunity
	end)
	
	EnvX:HookNet("ReplyOfficalStatus",function(Data)
		local IP = Data.IP
		print(IP)
		local TheReturnedHTML = ""; -- Blankness
		http.Fetch( "http://www.mediafire.com/download/6z9wylcew4cn1vz/OfficalServersList.txt",
			function( body, len, headers, code )
				-- The first argument is the HTML we asked for.
				TheReturnedHTML = body;
				
				local Servers = util.JSONToTable(TheReturnedHTML)
				PrintTable(Servers)
				LDE.ServerLists = Servers
				
				local Authenticated = false
				
				for k,v in pairs(Servers.Official or {}) do
					if IP==v.IP then
						LDE.OfficalStatus = "Official Server: "..k
						LDE.StatusColor = Color(0,0,255,255)
						Authenticated = true
						break
					end
				end
				
				for k,v in pairs(Servers.Community or {}) do
					if IP==v.IP then
						LDE.OfficalStatus = "Community Server: "..k
						LDE.StatusColor = Color(0,255,0,255)
						Authenticated = true
						break
					end
				end
				
				if not Authenticated then
					LDE.OfficalStatus = "Unofficial Server"
					LDE.StatusColor = Color(255,0,0,255)
				end
				
				if LDE.ServerListCommunity and IsValid(LDE.ServerListCommunity) then
					LDE.ServerListCommunity:PopulateList()
				end
				
				if LDE.ServerListOffical and IsValid(LDE.ServerListOffical) then
					LDE.ServerListOffical:PopulateList()
				end	
			end,
			function( error )
				LDE.OfficalStatus = "Error Authenticating!"
				LDE.StatusColor = Color(255,0,0,255)
				print("Error: "..tostring(error))
			end
		)	
	end)
end		
	
local LDE = LDE --Localise the global table for speed.
local Utl = EnvX.Utl --Makes it easier to read the code.
local NDat = Utl.NetMan --Ease link to the netdata table.
local PlayerData = LDE.PlayerData

if(SERVER)then
	EnvX:HookNet("EnvXPCChangeStat",function(Data,ply)
		if not ply:IsSuperAdmin() then print("Not a super admin!") return end
		
		local SID,Stat,Val = Data.SID,Data.Stat,Data.Val
		
		if not Val or not Stat or not SID then MsgAll("You cant send nil values >.<") return end
		
		local Type = type(Val)
		if Type == "string" then
			Type = "Strings"
		else
			Type = "Stats"
		end
		
		--MsgAll("Received! "..tostring(SID).." S: "..tostring(Stat).." V: "..tostring(Val).." T: "..Type.."\n")
		
		local Data = PlayerData.GetDataSID(SID).Stats
		Data[Type][Stat]=Val
		PlayerData.SetDataSID(SID,"Stats",Data)
		
		PlayerData.SyncDataAll() 
	end)
	
	EnvX:HookNet("EnvXPCChangeUnlock",function(Data,ply)
		if not ply:IsSuperAdmin() then MsgAll("Not a super admin! \n") return end
		
		local SID,Stat,Val = Data.SID,Data.Stat,Data.Val
		--MsgAll("Received! "..tostring(SID).." S: "..tostring(Stat).." V: "..tostring(Val))
		
		if Val == nil or not Stat or not SID then MsgAll("You cant send nil values >.< \n") return end
		
		local Data = PlayerData.GetDataSID(SID).Unlocks
		Data[Stat]=Val
		PlayerData.SetDataSID(SID,"Unlocks",Data)
	
		PlayerData.SyncDataAll() 
	end)
	
	EnvX:HookNet("EnvXPCResetStat",function(Data,ply)
		if not ply:IsSuperAdmin() then print("Not a super admin!") return end
		
		local SID,Stat,Type = Data.SID,Data.Stat,Data.Type
		
		--MsgAll("Received! Reset Request: "..tostring(SID).." S: "..tostring(Stat).." T: "..Type.."\n")
		
		if not Stat or not SID or not Type then MsgAll("You cant send nil values >.<") return end
		
		local DefaultStats = PlayerData.GetNewPlayerTable().Stats
		local Data = PlayerData.GetDataSID(SID).Stats
		
		if Type == "string" then Type = "Strings" else Type = "Stats" end
		
		if DefaultStats[Type][Stat] then
			Data[Type][Stat]=DefaultStats[Type][Stat]
		else
			--MsgAll("That stat doesnt have a reset value... Delete? \n")
		end
		
		PlayerData.SetDataSID(SID,"Stats",Data)
		
		PlayerData.SyncDataAll() 
	end)	
	
	EnvX:HookNet("RequestFullSync",function(Data,ply)
		PlayerData.FullData(ply)
	end)
else
	local MC = EnvX.MenuCore
	
	--The actual panel generation....
	hook.Add("LDEFillCatagorys","PersistControl", function()
		if not LocalPlayer():IsSuperAdmin() then 
			--print("Not a super admin!") 
			return 
		end
		local PDA = MC.PDA.Menu.Catagorys

		local base = vgui.Create( "DPanel", PDA ) base.Paint = function() end
		base:SizeToContents()
		PDA:AddSheet( "Persist Control", base, "icon16/book_key.png", false, false, "Manage Players Persistant Data" ) 
		
		local x,y = PDA:GetWide(),PDA:GetTall()
		local Sheet = EnvX.MenuCore.CreatePSheet(base,{x=x-25,y=y-50 },{x=5,y=5})	
		
		--------------------------------------------------
		------------------Player Persist------------------
		--------------------------------------------------
		
		local panel = vgui.Create( "DPanel", PDA ) panel.Paint = function() end
		Sheet:AddSheet( "Player Persist", panel, "icon16/folder_user.png", false, false, "Manage Player Data" )
		panel.Players = {}
		
		function panel:OnSelect(N)
			self.PI:Clear() self.PI.Lines = {}
			self.PI:DoList(N)
			
			self.PU:Clear() self.PU.Lines = {}
			self.PU:DoList(N)
			self.Selected = N
		end
		
		function panel:OnSelectForEditStats(N,V)
			if IsValid(self.EditPanel) then self.EditPanel:Remove() end
			local Stat,Value = V[1]:GetValue(1),V[1]:GetValue(2) --Just grabbing the select stat/value.
			
			local StatContainer = vgui.Create( "DPanel", self )
			StatContainer:SetSize(160,160) StatContainer:SetPos(580,0)
			StatContainer.Selected = self.Selected StatContainer.Stat = {Stat=Stat,Value=Value}
			
			--print(type(Value))
			local Text = MC.CreateText(StatContainer,{x=10,y=10},tostring(Stat)..": "..tostring(Value),Color(0,0,0,255))
			local NewValue = MC.CreateTextBar(StatContainer,{x=150,y=30},{x=5,y=30},tostring(Value),function()end)
			
			local Save = MC.CreateButton(StatContainer,{x=150,y=40},{x=5,y=70},"Save Changes",function() 
				local Type,Change = type(Value),NewValue:GetValue()
				if Type == "string" then Change = tostring(Change) else Change = tonumber(Change) end
				local SID,Stat = StatContainer.Selected,tostring(StatContainer.Stat.Stat)
				EnvX:NetworkData("EnvXPCChangeStat",{SID=SID,Stat=Stat,Val=Change})
				StatContainer:Remove()
			end)
			
			local Reset = MC.CreateButton(StatContainer,{x=150,y=40},{x=5,y=115},"Reset",function() 
				local SID,Stat = StatContainer.Selected,tostring(StatContainer.Stat.Stat)
				EnvX:NetworkData("EnvXPCResetStat",{SID=SID,Stat=Stat,Type=type(Value)})
				StatContainer:Remove()
			end)
			
			self.EditPanel = StatContainer
		end
		
		function panel:OnSelectForEditUnlocks(N,V)
			if IsValid(self.EditPanel) then self.EditPanel:Remove() end
			local Stat,Value = V[1]:GetValue(1),V[1]:GetValue(2) --Just grabbing the select stat/value.
			
			local StatContainer = vgui.Create( "DPanel", self )
			StatContainer:SetSize(160,120) StatContainer:SetPos(580,0)
			StatContainer.Selected = self.Selected StatContainer.Stat = {Stat=Stat,Value=Value}
			
			--print(type(Value))
			local Text = MC.CreateText(StatContainer,{x=10,y=10},tostring(Stat),Color(0,0,0,255))
			
			local NewValue = MC.CreateButton(StatContainer,{x=150,y=40},{x=5,y=30},"Unlocked: "..tostring(Value),function(self) 
				self.IsUnlocked = not self.IsUnlocked
				self:SetText("Unlocked: "..tostring(self.IsUnlocked))
			end)
			NewValue.IsUnlocked = tobool(Value)
			
			local Save = MC.CreateButton(StatContainer,{x=150,y=40},{x=5,y=75},"Save Changes",function() 
				local Type,Change = type(Value),NewValue.IsUnlocked
				local SID,Stat = StatContainer.Selected,tostring(StatContainer.Stat.Stat)
				EnvX:NetworkData("EnvXPCChangeUnlock",{SID=SID,Stat=Stat,Val=Change})
				StatContainer:Remove()
			end)
			
			self.EditPanel = StatContainer
		end
		
		local PL = MC.CreateList(panel,{x=280,y=420},{x=0,y=0},false,function(V) panel:OnSelect(V) end)
		PL:AddColumn("SteamID") -- Add column
		PL:AddColumn("Nick") -- Add column
		
		function PL:DoList(ply)
			self:Clear()
			for k,v in pairs(PlayerData.PlayerData) do
				--print(tostring(k)..": "..tostring(v))
				if not v.ID then v.ID = {Nick="Error",SteamID=k} end
				PL:AddLine(k,v.ID.Nick)
			end
		end
		PL:DoList(ply)
		
		MC.CreateButton(panel,{x=280,y=50},{x=5,y=425},"Update List",function() 
			EnvX:NetworkData("RequestFullSync",{})			
		end)
		
		panel.PL = PL
		
		------------------------------
		---------Player Stats---------
		------------------------------
		
		local PI = MC.CreateList(panel,{x=250,y=150},{x=290,y=0},false,function(E,V) panel:OnSelectForEditStats(E,V)  end)
		PI:AddColumn("Stats") -- Add column
		PI:AddColumn("Value") -- Add colum		
		PI.Lines = {} panel.PI = PI
		
		function PI:DoList(ply)
			local Stats = PlayerData.GetDataSID(ply)
			local Tab = table.Merge({},Stats.Stats.Stats)
			Tab = table.Merge(Tab,Stats.Stats.Strings)
			for k,v in pairs(Tab) do
				if not self.Lines[k] then
					self.Lines[k]=self:AddLine(k,v)
				else
					self.Lines[k]:SetColumnText(2,v)
				end
			end
		end
				
		--------------------------------
		---------Player Unlocks---------
		--------------------------------
		
		local PU = MC.CreateList(panel,{x=250,y=150},{x=290,y=155},false,function(E,V) panel:OnSelectForEditUnlocks(E,V) end)
		PU:AddColumn("Unlocks") -- Add column
		PU:AddColumn("Unlocked") -- Add colum		
		PU.Lines = {} panel.PU = PU		
		
		function PU:DoList(ply)
			local Stats = PlayerData.GetDataSID(ply)
			local Tab = Stats.Unlocks
			
			for k,v in pairs(LDE.Unlocks) do
				for e,d in pairs(v) do
					if not Tab[e] then
						Tab[e]=false
					end
				end
			end
			
			for k,v in pairs(Tab) do
				if not self.Lines[k] then
					self.Lines[k]=self:AddLine(k,tostring(v))
				else
					self.Lines[k]:SetColumnText(2,tostring(v))
				end
			end
		end		
		
		--------------------------------
		---------Player Mutators--------
		--------------------------------
		
		local PM = MC.CreateList(panel,{x=250,y=150},{x=290,y=310},false,function(E,V) end)
		PM:AddColumn("Mutator") -- Add column
		PM:AddColumn("Value") -- Add colum		
		PM.Lines = {} panel.PM = PM		
		
		panel:OnSelect(LocalPlayer():SteamID())
		panel.NextUpdate = CurTime()
		panel.TotalCount = table.Count(PlayerData.PlayerData)
		function panel:Think()
			if self.NextUpdate<CurTime() then
				if self.Selected then
					self.PI:DoList(self.Selected)
					self.PU:DoList(self.Selected)
				end
				
				if panel.TotalCount ~= table.Count(PlayerData.PlayerData) then
					panel.TotalCount = table.Count(PlayerData.PlayerData)
					
					self.PL:DoList(ply)
					self.PU:DoList(ply)
				end
				self.NextUpdate = CurTime()+1
			end
		end
		
		--------------------------------------------------
		--------------------------------------------------
		--------------------------------------------------
	end)
end		
	
local Utl = EnvX.Utl --Makes it easier to read the code.

if(SERVER)then
	function SendDebugTypes(ply)
		for k, log in pairs(EnvX.DebugLogs) do
			Utl.NetMan.AddData({Name="pda_debugger_msg",Val=0.1,Dat={Type="Types",A=k}},ply)
		end
	end

	function SendDebugLogs(ply,Type)
		for k, log in pairs(EnvX.DebugLogs[Type] or {}) do
			Utl.NetMan.AddData({Name="pda_debugger_msg",Val=0.1,Dat={Type="Logs",A=log.C,B=log.M,C=Type}},ply)	
		end
	end
		
	EnvX:HookNet("pda_debugger_msg",function(Data,ply)
		MsgAll("Recieved Debugger Request! From: "..tostring(ply))
		local Type = Data.Type
		if Type == "Types" then
			SendDebugTypes(ply)
		elseif Type == "Logs" then
			SendDebugLogs(ply,Data.A)
		end	
	end)
else
	local MC = EnvX.MenuCore

	local RecievedLogs = 0
	local Logs = {}
	local Super = {}
	
	function SelectType(Type)
		Utl.NetMan.AddData({Name="pda_debugger_msg",Val=1,Dat={Type="Logs",A=Type}})
		
		Super.Selected = Type
		Super.LogDisplay:Clear()
	end
	
	function AddType(Type)
		if Super.LogTypes and IsValid(Super.LogTypes) then
			Super.LogTypes:AddLine(Type)
		end
	end
	
	function AddLog(Time,MSG,Type)
		if Super.Selected == Type then
			if Super.LogDisplay and IsValid(Super.LogDisplay) then
				Super.LogDisplay:AddLine(string.FormattedTime( Time, "%02i:%02i:%02i" ),MSG)
			end
		end
	end
	
	EnvX:HookNet("pda_debugger_msg",function(Data)
		local Type = Data.Type
		if Type == "Types" then
			AddType(Data.A)
		elseif Type == "Logs" then
			AddLog(Data.A,Data.B,Data.C)
		end		
	end)

	--The actual panel generation....
	hook.Add("LDEFillCatagorys","Debug", function()
		if not LocalPlayer():IsAdmin() then print("Not a admin!") return end
		local PDA = MC.PDA.Menu.Catagorys

		local base = vgui.Create( "DPanel", PDA )
		base:SizeToContents()
		base.Paint = function() end
		PDA:AddSheet( "Logging", base, "icon16/application_view_list.png", false, false, "Check the Debug Logs!" ) 
		
		local menupage = MC.CreateList(base,{x=150,y=520},{x=5,y=5},false,SelectType)
		menupage:AddColumn("LogType") -- Add column
		PDA.LogTypes = menupage
		Super.LogTypes = menupage
		
		local menupage = MC.CreateList(base,{x=600,y=520},{x=160,y=5},false,function() end)
		menupage:AddColumn("Time") -- Add column
		menupage:AddColumn("Logs") -- Add column
		PDA.LogDisplay = menupage
		Super.LogDisplay = menupage
		
		Utl.NetMan.AddData({Name="pda_debugger_msg",Val=0.1,Dat={Type="Types"}})
	end)
end		
	
local LDE = LDE --Localise the global table for speed.
local Utl = EnvX.Utl --Makes it easier to read the code.
local NDat = Utl.NetMan --Ease link to the netdata table.
local Factions = LDE.Factions

if(SERVER)then
	EnvX:HookNet("EnvxFactionsJoinRequest",function(Data,ply)
		if ply:GetLDEString( "Faction" ) ~= "No Faction" then 
			Factions.Factions[ply:GetLDEString( "Faction" )]:RemoveMember(ply) 
		end
		Factions.Factions[Data.Faction]:AddMember(ply,"Member")
		Factions.SyncToClients()
	end)
	
	EnvX:HookNet("EnvXFactionLeave",function(Data,ply)
		Factions.Factions[Data.Faction]:RemoveMember(ply)
		Factions.SyncToClients()
	end)
	
	EnvX:HookNet("EnvXFactionsCreateRequest",function(Data,ply)
		local Faction = Factions.CreateFaction(Data.Faction)
		Faction:AddMember(ply,"Owner")
		Factions.SyncToClients()
		LudCore.Response(ply:Name().." created a faction called "..Data.Faction.Name)
	end)
else
	local MC = EnvX.MenuCore

	hook.Add("LDEFillCatagorys","Factions", function()
		local PDA = MC.PDA.Menu.Catagorys
		
		local base = vgui.Create( "DPanel", PDA ) base.Paint = function() end
		base:SizeToContents()
		PDA:AddSheet( "Factions", base, "icon16/group.png", false, false, "View/Manage Factions" )
		
		Factions.PDAPage = base --Tell the faction module where we are.
		
		function base:OnFactionSync()
			local FL = base.FactionList
			FL:Clear()--Remove old data.
						
			for k,v in pairs(Factions.Factions) do
				if not v.Settings.Hidden then
					FL:AddLine(v.Info.Name)
				end
			end
		end
		
		local x,y = PDA:GetWide(),PDA:GetTall()
		local Sheet = EnvX.MenuCore.CreatePSheet(base,{x=x-25,y=y-50 },{x=5,y=5})		
		
		EnvX:NetworkData("EnvxFactionsSyncRequest",{})
		local panel = vgui.Create( "DPanel", PDA ) panel.Paint = function() end
		Sheet:AddSheet( "View Factions", panel, "icon16/eye.png", false, false, "View Factions" )
		
		local FL = MC.CreateList(panel,{x=240,y=y-90},{x=0,y=0},false,function(V)
			local Faction = Factions.Factions[V]
			
			base.FactionName:SetText("Name: "..Faction.Info.Name)
			base.FactionDesc:SetText("Description: "..Faction.Info.Desc)
						
			if Faction.Members[LocalPlayer():SteamID()] or game.SinglePlayer() then
				base.JoinButton:SetText("Leave: "..Faction.Info.Name)
			else
				base.JoinButton:SetText("Join: "..Faction.Info.Name)
			end

			base.JoinButton.SelectedFaction = Faction


			base.FactionInfo:DisplayFactionInfo(Faction)
		end)
		FL:AddColumn("Faction") -- Add column
		
		base.FactionList = FL
		base.FactionName = MC.CreateText(panel,{x=250,y=5},"Name: Select an faction!")		
		base.FactionDesc = MC.CreateAdvText(panel,{x=500,y=160},{x=245,y=30},"Description: Select an faction!")		
		
		base.JoinButton = MC.CreateButton(panel,{x=500,y=20},{x=245,y=200},"Join: Select an faction!",function(self)
			if self.SelectedFaction ~= nil then
				if self.SelectedFaction.Info.Password ~= "" and not self.SelectedFaction.Members[LocalPlayer():SteamID()] then
					local FJ = {}
					FJ.PopUP = MC.CreateFrame({x=250,y=100},true,true,true,true)
					FJ.PopUP:SetPos( 100, 100 )
					FJ.PopUP:SetTitle( "Password" )
					FJ.PopUP:MakePopup()
					
					FJ.SelectedFaction =  self.SelectedFaction

					FJ.Password = MC.CreateTextBar(FJ.PopUP,{x=95,y=30},{x=25,y=25},"",function()end)
					
					base.JoinB = MC.CreateButton(FJ.PopUP,{x=75,y=20},{x=100,y=75},"Join",function()
						if FJ.Password:GetValue() ~= FJ.SelectedFaction.Info.Password then
							base.JoinB:SetText("Incorrect!")
							return
						end
						EnvX:NetworkData("EnvxFactionsJoinRequest",{Faction=FJ.SelectedFaction.Info.Name})
						FJ:Remove()
					end)
				else
					if not self.SelectedFaction.Members[LocalPlayer():SteamID()] then
						EnvX:NetworkData("EnvxFactionsJoinRequest",{Faction=self.SelectedFaction.Info.Name})
					elseif self.SelectedFaction.Members[LocalPlayer():SteamID()] then 
						EnvX:NetworkData("EnvXFactionLeave",{Faction=self.SelectedFaction.Info.Name})
					end
				end
			end
		end)
		
		if LocalPlayer():GetLDEString( "Faction" ) == "No Faction" then 
			base.CreateButton = MC.CreateButton(panel,{x=500,y=20},{x=245,y=180},"Create a faction!",function(self)
				local FC = {}
				--Make pop up for creating faction
				FC.PopUP = MC.CreateFrame({x=300,y=475},true,true,true,true)
				FC.PopUP:SetPos( 100, 100 )
				FC.PopUP:SetTitle( "Faction Creation" )
				FC.PopUP:MakePopup()
	
				FC.Name = MC.CreateTextBar(FC.PopUP,{x=240,y=25},{x=25,y=30},"FactionName",function()
			
				end)
				FC.Name:SetToolTip("Faction Name")
				
				FC.Desc = MC.CreateTextBar(FC.PopUP,{x=240,y=60},{x=25,y=55},"FactionDesc",function()
					
				end)
				FC.Desc:SetMultiline(true)
				FC.Desc:SetToolTip("Faction Description")
				
				FC.Pass = MC.CreateTextBar(FC.PopUP,{x=240,y=25},{x=25,y=115},"password",function()
					
				end)
				FC.Pass:SetToolTip("Faction Password, Blank for none")
				
				FC.AC = MC.CreateTextBar(FC.PopUP,{x=240,y=25},{x=25,y=140},"ACYRON",function()
					
				end)
				FC.AC:SetToolTip("Faction Acronym, 5 Characters")
				
				FC.Mixer = vgui.Create("DColorMixer",FC.PopUP)
				FC.Mixer:SetColor( Color( 30, 100, 160 ) )
				FC.Mixer:SetPos(25,175)
				
				base.CreateB = MC.CreateButton(FC.PopUP,{x=160,y=20},{x=25,y=425},"Create",function(self)
					local Faction = {}
					Faction.Name = FC.Name:GetValue()
					Faction.Desc = FC.Desc:GetValue()
					Faction.Password = FC.Pass:GetValue()
					Faction.Col = FC.Mixer:GetColor()
					Faction.AC = FC.AC:GetValue()
					if string.len(Faction.Name) > 25 then 
						base.CreateB:SetText("Character Limit! 25") 
						return 
					elseif string.len(Faction.AC) > 5 then
						base.CreateB:SetText("Character Limit! 5, Acronym") 
						return 
					elseif Factions.Factions[Faction.Name] ~= nil then
						base.CreateB:SetText("Name Taken!")
						return
					end
					FC.PopUP:Remove()
					EnvX:NetworkData("EnvXFactionsCreateRequest",{Faction=Faction})
				end)
			end)
		end	

		local FSheet = EnvX.MenuCore.CreatePSheet(panel,{x=500,y=250},{x=245,y=225})		
		base.FactionInfo = FSheet
		
		function FSheet:DisplayFactionInfo(Faction)
			local ML,AL,SL,RL = self.MemberList,self.AllianceList,self.StatisticList,self.RankList
			ML:Clear() RL:Clear()
			
			for k,v in pairs(Faction.Members) do
				ML:AddLine(v.Nick,v.Rank)
			end
		
			for k,v in pairs(Faction.Ranks) do
				RL:AddLine(v.Name)
			end
		end
		
		local panel = vgui.Create( "DPanel", FSheet ) panel.Paint = function() end
		FSheet:AddSheet( "Members", panel, "icon16/group.png", false, false, "View Members" )
		
		local L = MC.CreateList(panel,{x=500,y=200},{x=0,y=0},false,function(V) end)
		L:AddColumn("Player") L:AddColumn("Rank")
		FSheet.MemberList = L
		
		local panel = vgui.Create( "DPanel", FSheet ) panel.Paint = function() end
		FSheet:AddSheet( "Alliances", panel, "icon16/calendar.png", false, false, "View Alliances" )
		
		local L = MC.CreateList(panel,{x=500,y=200},{x=0,y=0},false,function(V) end)
		L:AddColumn("Faction") L:AddColumn("Relation")
		FSheet.AllianceList = L
		
		local panel = vgui.Create( "DPanel", FSheet ) panel.Paint = function() end
		FSheet:AddSheet( "Statistics", panel, "icon16/chart_curve.png", false, false, "View Statistics" )
		
		local L = MC.CreateList(panel,{x=500,y=200},{x=0,y=0},false,function(V) end)
		L:AddColumn("Statistic") L:AddColumn("Value")
		FSheet.StatisticList = L
		
		local panel = vgui.Create( "DPanel", FSheet ) panel.Paint = function() end
		FSheet:AddSheet( "Ranks", panel, "icon16/layout.png", false, false, "View Ranks" )
		
		local L = MC.CreateList(panel,{x=500,y=200},{x=0,y=0},false,function(V) end)
		L:AddColumn("Rank")
		FSheet.RankList = L
		
		local panel = vgui.Create( "DPanel", PDA ) panel.Paint = function() end
		Sheet:AddSheet( "Manage My Faction", panel, "icon16/bullet_wrench.png", false, false, "Manage your Factions" )
		
		local FSheet = EnvX.MenuCore.CreatePSheet(panel,{x=740,y=465},{x=5,y=5})
		
		local panel = vgui.Create( "DPanel", FSheet ) panel.Paint = function() end
		FSheet:AddSheet( "Members", panel, "icon16/group.png", false, false, "Manage Members" )
		
		local L = MC.CreateList(panel,{x=740,y=465},{x=0,y=0},false,function(V) end)
		L:AddColumn("Player") L:AddColumn("Rank")
		FSheet.MemberList = L
		
		local panel = vgui.Create( "DPanel", FSheet ) panel.Paint = function() end
		FSheet:AddSheet( "Settings", panel, "icon16/bullet_wrench.png", false, false, "Manage Settings" )
		
		local panel = vgui.Create( "DPanel", FSheet ) panel.Paint = function() end
		FSheet:AddSheet( "Ranks", panel, "icon16/layout.png", false, false, "Manage Ranks" )
	end)
end		
	


local Path = {"Resource Production","LifeSupport","Nitrogen","Nitrogen Fuser"}

--Nitrogen Fuser
local Func = function(self) if(self.Active==1)then LDE.LifeSupport.ManageResources(self) end end
local Data={name="Nitrogen Fuser",class="generator_nitrogen_fuser",In={"carbon dioxide","hydrogen"},Out={"nitrogen"},shootfunc=Func,InUse={70,70},OutMake={50}}
local Makeup = {name={"Nitrogen Fuser Small","Nitrogen Fuser Large"},model={"models/sbep_community/d12shieldemitter.mdl","models/sbep_community/d12siesmiccharge.mdl"},Path=Path,class=Data.class}
LDE.LifeSupport.CompileDevice(Data,Makeup)

----------------------------------------------------------------------------------------------------------------------------

local Path = {"Resource Production","LifeSupport","Energy","Angular Power Conversion"}

--Alternater
local Func = function(self)
	local Phys = self:GetPhysicsObject()
	local Speed = Phys:GetAngleVelocity():Length()/10
	if self.LastSpeed==nil then self.LastSpeed=0 end
	
	local Change = math.abs(self.LastSpeed-Speed)
	self.LastSpeed = Speed

	if Change>50 then
		LDE:DamageHealth(self,Change-50,true,self)
	end
	
	--Phys:AddAngleVelocity(-Phys:GetAngleVelocity()*Vector(0.3,0.6,0.6))
	
	local supplyO2 = math.Round(Speed)
	self:SupplyResource("energy", supplyO2)
end
local Desc = "Device that generates energy from angular momentum."
local Data={name="Alternater",class="lde_ang_power",Out={"energy"},ThinkSpeed=0.01,shootfunc=Func,OutMake={0},Desc=Desc}
local Makeup = {name={"Alternater"},model={"models/Slyfo/sat_sat1.mdl"},Path=Path,class=Data.class}
LDE.LifeSupport.CompileDevice(Data,Makeup)

----------------------------------------------------------------------------------------------------------------------------

local Path = {"Resource Production","LifeSupport","Energy","AntiMatter Reactor"}

--AntiMatter Reactor
local Func = function(self) if(self.Active==1)then LDE.LifeSupport.ManageResources(self) end end
local Data={name="AntiMatter Reactor",class="generator_antimatter_reactor",In={"AntiMatter"},Out={"energy"},shootfunc=Func,InUse={1},OutMake={1000000}}
local Makeup = {name={"Medium Antimatter Reactor"},model={"models/environmentsx/mediumreactor.mdl"},Path=Path,class=Data.class}
LDE.LifeSupport.CompileDevice(Data,Makeup)
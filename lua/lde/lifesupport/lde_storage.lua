
--Raw Ore Storage
local Path = {"Mining","Storage","Raw Ore"} local Rates = {} Rates["Raw Ore"]={100,0}
local Names = {"Large Raw Ore Crate","Raw Ore Barrel","Raw Ore Outer Cache","Raw Ore Huge Mandrac","Raw Ore Medium Mandrac"}
local Models = {"models/Slyfo/nacshuttleleft.mdl","models/Slyfo/barrel_unrefined.mdl","models/SmallBridge/Life Support/sbwallcachee.mdl","models/mandrac/ore_container/ore_large.mdl","models/mandrac/ore_container/ore_medium.mdl"}
local Data={name="Raw Ore Storage",class="lde_ore_storage",storage={"Raw Ore"},Rates=Rates}
local Makeup = {name=Names,model=Models,Path=Path,class=Data.class}
LDE.LifeSupport.CompileStorage(Data,Makeup)

--Refined Ore Storage
local Path = {"Mining","Storage","Refined Ore"} local Rates = {} Rates["Refined Ore"]={100,0}
local Names = {"Refined Ore Outer Cache","Refined Ore Large Mandrac","Refined Ore Small Mandrac"}
local Models = {"models/SmallBridge/Life Support/sbwallcachee.mdl","models/mandrac/hw_tank/hw_tank_large.mdl","models/mandrac/hw_tank/hw_tank_small.mdl"}
local Data={name="Refined Ore Storage",class="lde_refined_ore_storage",storage={"Refined Ore"},Rates=Rates}
local Makeup = {name=Names,model=Models,Path=Path,class=Data.class}
LDE.LifeSupport.CompileStorage(Data,Makeup)

--Hardend Ore Storage
local Path = {"Mining","Storage","Hardend Ore"} local Rates = {} Rates["Hardened Ore"]={100,0}
local Names = {"Hardened Ore Outer Cache","Hardened Ore Crate","Hardened Ore Huge Mandrac","Hardened Ore Medium Mandrac","Hardened Ore Small Mandrac"}
local Models = {"models/SmallBridge/Life Support/sbwallcachee.mdl","models/Slyfo/nacshuttleleft.mdl","models/mandrac/oxygen_tank/oxygen_tank_large.mdl","models/mandrac/oxygen_tank/oxygen_tank_medium.mdl","models/mandrac/oxygen_tank/oxygen_tank_small.mdl"}
local Data={name="Hardened Ore Storage",class="lde_hardend_ore_storage",storage={"Hardened Ore"},Rates=Rates}
local Makeup = {name=Names,model=Models,Path=Path,class=Data.class}
LDE.LifeSupport.CompileStorage(Data,Makeup)

--Carbon Storage
local Path = {"Mining","Storage","Carbon"} local Rates = {} Rates["Carbon"]={100,0}
local Names = {"Large Carbon Mandrac","Small Carbon Mandrac"} 
local Models = {"models/mandrac/hydrogen_tank/hydro_tank_large.mdl","models/mandrac/hydrogen_tank/hydro_tank_small.mdl"}
local Data={name="Carbon Storage",class="lde_carbon_storage",storage={"Carbon"},Rates=Rates}
local Makeup = {name=Names,model=Models,Path=Path,class=Data.class}
LDE.LifeSupport.CompileStorage(Data,Makeup)

--Crystalized Polylodarium Storage
local Path = {"Mining","Storage","Polylodarium"} local Rates = {} Rates["Crystalized Polylodarium"]={100,0}
local Names = {"Crystalized Polylodarium Cache","Crystalized Polylodarium Huge Mandrac","Crystalized Polylodarium Medium Mandrac"}
local Models = {"models/SmallBridge/Life Support/sbwallcachee.mdl","models/mandrac/ore_container/ore_large.mdl","models/mandrac/ore_container/ore_medium.mdl"}
local Data={name="Crystalized Polylodarium Storage",class="lde_crys_poly_storage",storage={"Crystalized Polylodarium"},Rates=Rates}
local Makeup = {name=Names,model=Models,Path=Path,class=Data.class}
LDE.LifeSupport.CompileStorage(Data,Makeup)

--Liquid Polylodarium Storage
local Names = {"Liquid Polylodarium Barrel","Liquid Polylodarium Huge Mandrac"} local Rates = {} Rates["Liquid Polylodarium"]={100,0}
local Models = {"models/Slyfo/barrel_unrefined.mdl","models/mandrac/hydrogen_tank/hydro_tank_small.mdl"}
local Data={name="Liquid Polylodarium Storage",class="lde_liqu_poly_storage",storage={"Liquid Polylodarium"},Rates=Rates}
local Makeup = {name=Names,model=Models,Path=Path,class=Data.class}
LDE.LifeSupport.CompileStorage(Data,Makeup)

--Antimatter Storage
local Path = {"Mining","Storage","AntiMatter"} local Rates = {} Rates["AntiMatter"]={100,0}
local Names = {"AntiMatter Container","Small AntiMatter Container","AntiMatter Cell","Large AntiMatter Container"}
local Models = {"models/environmentsx/tritank.mdl","models/lt_c/sci_fi/dm_container_small.mdl","models/lt_c/sci_fi/am_container.mdl","models/lt_c/sci_fi/dm_container.mdl"}
local Data={name="AntiMatter Storage",class="lde_antimatter_storage",storage={"AntiMatter"},Rates=Rates}
local Makeup = {name=Names,model=Models,Path=Path,class=Data.class}
LDE.LifeSupport.CompileStorage(Data,Makeup)

--[[
--Electromium Storage
local Base = {Cat1="Storage",Cat2="Mining",Type="Electromium"}
local Names = {"Electromium Storage Huge","Electromium Storage Large","Electromium Storage Medium","Electromium Storage Small"}
local Models = {"models/ce_ls3additional/energy_cells/energy_cell_huge.mdl","models/ce_ls3additional/energy_cells/energy_cell_large.mdl","models/ce_ls3additional/energy_cells/energy_cell_medium.mdl","models/ce_ls3additional/energy_cells/energy_cell_small.mdl"}
local Data={name="Electromium Storage",class="lde_electrom_storage",storage={"Electromium"},Rates={[10] = "Electromium"}}
local Makeup = {name=Names,model=Models,Path=Path,class=Data.class}
LDE.LifeSupport.CompileStorage(Data,Makeup)
]]

--Casings Storage
local Path = {"Ammunition","Casings","Storage"} local Rates = {} Rates["Casings"]={100,0}
local Names = {"Thin Wall Storage","Casing Barrel","Thick Wall Storage","Wide Thin Wall Storage","Wide Thick Wall Storage","Exterior Storage"}
local Models = {"models/SmallBridge/Life Support/sbwallcaches05.mdl","models/Slyfo/barrel_refined.mdl","models/smallbridge/life support/sbwallcachel05.mdl","models/smallbridge/life support/sbwallcaches.mdl","models/smallbridge/life support/sbwallcachel.mdl","models/smallbridge/life support/sbwallcachee.mdl"}
local Data={name="Casings Storage",class="lde_casings_storage",storage={"Casings"},Rates=Rates}
local Makeup = {name=Names,model=Models,Path=Path,class=Data.class}
LDE.LifeSupport.CompileStorage(Data,Makeup)

--Basic Shells Storage
local Path = {"Ammunition","Basic Shells","Storage"} local Rates = {} Rates["Basic Shells"]={100,50}
local Names = {"Thin Wall Storage","Small Shell Storage","Thick Wall Storage","Wide Thin Wall Storage","Wide Thick Wall Storage","Exterior Storage"}
local Models = {"models/SmallBridge/Life Support/sbwallcaches05.mdl","models/slyfo_2/rocketpod_turbo_full.mdl","models/smallbridge/life support/sbwallcachel05.mdl","models/smallbridge/life support/sbwallcaches.mdl","models/smallbridge/life support/sbwallcachel.mdl","models/smallbridge/life support/sbwallcachee.mdl"}
local Data={name="Basic Shells Storage",class="lde_shells_storage",storage={"Basic Shells"},Rates=Rates}
local Makeup = {name=Names,model=Models,Path=Path,class=Data.class}
LDE.LifeSupport.CompileStorage(Data,Makeup)

--Basic Rounds Storage
local Path = {"Ammunition","Basic Rounds","Storage"} local Rates = {} Rates["Basic Rounds"]={100,50}
local Names = {"Thin Wall Storage","AmmoBox","Thick Wall Storage","Wide Thin Wall Storage","Wide Thick Wall Storage","Exterior Storage"}
local Models = {"models/SmallBridge/Life Support/sbwallcaches05.mdl","models/Items/BoxMRounds.mdl","models/smallbridge/life support/sbwallcachel05.mdl","models/smallbridge/life support/sbwallcaches.mdl","models/smallbridge/life support/sbwallcachel.mdl","models/smallbridge/life support/sbwallcachee.mdl"}
local Data={name="Basic Rounds Storage",class="lde_brounds_storage",storage={"Basic Rounds"},Rates=Rates}
local Makeup = {name=Names,model=Models,Path=Path,class=Data.class}
LDE.LifeSupport.CompileStorage(Data,Makeup)

--Heavy Shells Storage
local Path = {"Ammunition","Heavy Shells","Storage"} local Rates = {} Rates["Heavy Shells"]={100,50}
local Names = {"Thin Wall Storage","Turret Stockpile","Thick Wall Storage","Wide Thin Wall Storage","Wide Thick Wall Storage","Exterior Storage"}
local Models = {"models/SmallBridge/Life Support/sbwallcaches05.mdl","models/mandrac/projectile/cap_autocannon_gunbase.mdl","models/smallbridge/life support/sbwallcachel05.mdl","models/smallbridge/life support/sbwallcaches.mdl","models/smallbridge/life support/sbwallcachel.mdl","models/smallbridge/life support/sbwallcachee.mdl"}
local Data={name="Heavy shells Storage",class="lde_hshells_storage",storage={"Heavy Shells"},Rates=Rates}
local Makeup = {name=Names,model=Models,Path=Path,class=Data.class}
LDE.LifeSupport.CompileStorage(Data,Makeup)

--Missile Parts Storage
local Path = {"Ammunition","Missile Parts","Storage"} local Rates = {} Rates["Missile Parts"]={100,50}
local Names = {"Thin Wall Storage","Thick Wall Storage","Wide Thin Wall Storage","Wide Thick Wall Storage","Exterior Storage"}
local Models = {"models/SmallBridge/Life Support/sbwallcaches05.mdl","models/smallbridge/life support/sbwallcachel05.mdl","models/smallbridge/life support/sbwallcaches.mdl","models/smallbridge/life support/sbwallcachel.mdl","models/smallbridge/life support/sbwallcachee.mdl"}
local Data={name="Missile Parts Storage",class="lde_mparts_storage",storage={"Missile Parts"},Rates=Rates}
local Makeup = {name=Names,model=Models,Path=Path,class=Data.class}
LDE.LifeSupport.CompileStorage(Data,Makeup)

--Plasma Storage
local Path = {"Ammunition","Plasma","Storage"} local Rates = {} Rates["Plasma"]={100,0}
local Names = {"Basic Plasma Storage","Quantom Plasma Storage"}
local Models = {"models/SmallBridge/Life Support/sbfusiongen.mdl","models/Slyfo/powercrystal.mdl"}
local Data={name="Plasma Storage",class="lde_plasma_storage",storage={"Plasma"},Rates=Rates}
local Makeup = {name=Names,model=Models,Path=Path,class=Data.class}
LDE.LifeSupport.CompileStorage(Data,Makeup)

--BlackHolium
local Path = {"Storage","BlackHolium"} local Rates = {} Rates["BlackHolium"]={100,0}
local Names = {"BlackHolium Storage"}
local Models = {"models/SBEP_community/d12siesmiccharge.mdl"}
local Data={name="BlackHolium Storage",class="lde_blackhole_storage",storage={"BlackHolium"},Rates=Rates}
local Makeup = {name=Names,model=Models,Path=Path,class=Data.class}
LDE.LifeSupport.CompileStorage(Data,Makeup)

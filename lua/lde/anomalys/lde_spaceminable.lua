
local GetRes = function()	
	local Res,Rare = "Raw Ore",0
	for res,dat in pairs(LDE.Anons.Resources) do
		if math.random(1,dat.rarity)<4 then
			if dat.rarity>Rare then
				Res = dat.name
			end
		end
	end
	
	return Res
end

--Asteroid Field
local Int = function(self) 
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	
	if self:GetPhysicsObject():IsValid() then
		self:GetPhysicsObject():Wake()
	end
	
	local rocks = math.random(5,10)
	local i = 0
	for i=1, rocks do
		local pos = self:GetPos()+Vector(math.random(-768,768),math.random(-768,768),math.random(-768,768))
		rock = ents.Create("resource_asteroid")
		rock:SetPos(pos)
		rock:SetAngles(Angle(math.random(0,360),math.random(0,360),math.random(0,360)))

		rock:SetResource(GetRes())
		rock:Spawn()
		
		rock:SetVolume()
		rock:CalcResource()
	end
	self:Remove()
end
local Data={name="Asteroid field",class="asteroid_field",Type="Spawner",Startup=Int}
LDE.Anons.GenerateAnomaly(Data)

local Spawn = function() 		
	rock = ents.Create("asteroid_field")
	rock:SetPos(LDE.Anons:PointInSpace(rock.Data))
	rock:Spawn()
end

LDE.Anons.Monitor["resource_asteroid"]={
	class = "resource_asteroid",
	SpawnMe = Spawn,
	minimal=40
}



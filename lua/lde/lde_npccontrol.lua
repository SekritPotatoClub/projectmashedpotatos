local LDE = LDE
local Utl = EnvX.Utl

--ENVX NPC Control
LDE.NPCMain = LDE.NPCMain or {LastSpawned=0,SporeCheck=0}
local NPCM = LDE.NPCMain

local BaseTab = {Miner={},Police={},Trader={},Military={},Drone={},PlyDrone={}}

NPCM.Ships = NPCM.Ships or table.Copy(BaseTab)
NPCM.Targets = NPCM.Targets or {Hostiles={},Spores={}}

NPCM.ShipData = table.Copy(BaseTab)
NPCM.RoleData = {}

--More like minimum spawns...
NPCM.MaximumShips = {
	Miner=5,
	Trader=3,
	Police=4,
	Drone=5,
	Military=0,
	PlyDrone=0
}

NPCM.MaximumSpores = 200
NPCM.MinimumSpores = 50

NPCM.SporePanicMode = NPCM.SporePanicMode or false

NPCM.ShipsOnStandby = NPCM.ShipsOnStandby or 0
NPCM.MaxShipsOnStandby = NPCM.MaxShipsOnStandby or 2
NPCM.StandbyRestock = NPCM.StandbyRestock or 0

if(SERVER)then
	local Dirs = {Vector(1,0,0),Vector(-1,0,0),Vector(0,1,0),Vector(0,-1,0),Vector(0,0,1),Vector(0,0,-1)}
	local TraceDist = 10000
	
	function NPCM.CheckStationSpawn(Pos,Rescan)
		if Rescan > 10 then return false else Rescan=Rescan+1 end
		local TraceHit = false
		for ID, data in pairs(Dirs) do
			local TraceEnd = Pos+(Vector(TraceDist,TraceDist,TraceDist)*data)
			
			local tr = {}
			
			tr.start = Pos
			tr.endpos = TraceEnd
			
			local res = util.TraceLine( tr )
			
			local Dist = TraceDist*res.Fraction
			
			Pos = Pos-(Vector(Dist)*data)
		end	
		
		if TraceHit then
			return NPCM.CheckStationSpawn(Pos)
		else
			return Pos
		end
	end
	
	local Spawn = function()
		local Point = NPCM.CheckStationSpawn(LDE.Anons:PointInSpace({}),1)
		
		if Point == false then MsgAll("Station couldnt find a good spawn... \n") return end --Position was bad.....
		
		rock = ents.Create("envx_npc_station")
		rock:SetModel( "models/af/aff/aisn/naquda.mdl" )
		rock:SetPos(Point)
		rock:SetAngles(Angle(0,math.random(-180,180),0))
		rock:Spawn()
		rock:CPPISetOwnerless(true)
		
		NPCM.Station = rock--Save the station entity for later use
	end

	local Data={name="NPC SpaceStation",class="envx_npc_station",SpawnMe=Spawn,Type="Space",Initial=true,Mega=true,minimal=1}
	LDE.Anons:RegisterAnomaly(Data)
	
	function NPCM.StationFront()
		--Return point at station for ship spawning.
		local Station,X = NPCM.Station,3000
		if not IsValid(Station) then return Vector(0,0,0) end
		return Station:GetPos()+(Station:GetRight()*Vector(X,X,X))
	end
	
	function NPCM.SpawnShipAtPosition(Point,Data)	
		local Data = Data or {Speed=350,Hull=8000,Shields=6000,Model="models/af/aff/usn/heimdal.mdl",Scale=1}
		local ent = ents.Create(NPCM.RoleData[Data.AI] or "envx_npc_ship")
		if not IsValid(ent) then return end
		ent:SetPos(Point)
		ent:SetModel( Data.Model )
		ent:Spawn()
		ent:InitNPCData(Data)
		
		if Data.Scale then ent:SetModelScale( Data.Scale, 0 ) ent:Activate() end
		
		if Data.weapons then ent:AddWeapons(Data.weapons) end
		if Data.trails then
			for ID, data in pairs(Data.trails) do
				ent:AddTrail(data)
			end
		end
		
		return ent
	end
	
	function NPCM.SpawnShipAtStation(Data,Table)
		local Point = NPCM.StationFront()
		if not util.IsInWorld(Point) then NPCM.Station:Remove() return end
	
		local Data = Data or {Speed=350,Hull=8000,Shields=6000,Model="models/af/aff/usn/heimdal.mdl",Scale=1}
		local ent = ents.Create(NPCM.RoleData[Data.AI] or "envx_npc_ship")
		if not IsValid(ent) then return end table.insert(Table,ent)
		ent:SetPos(Point)
		ent:SetModel( Data.Model )
		ent:Spawn()
		ent:InitNPCData(Data)
		
		ent:CPPISetOwnerless(true)
		
		if Data.Scale then
			ent:SetModelScale( Data.Scale, 0 )
			ent:Activate()
		end
		
		if Data.weapons then ent:AddWeapons(Data.weapons) end
		if Data.trails then
			for ID, data in pairs(Data.trails) do
				ent:AddTrail(data)
			end
		end
		
		NPCM.LastSpawned=CurTime()
	end
	
	function NPCM.RemoveShips()
		--PrintTable(NPCM.Ships)
	
		for RID, Role in pairs(NPCM.Ships) do
			for ID, Ship in pairs(Role) do
				if IsValid(Ship) then Ship:Remove() end
				table.remove(Role,ID)
			end
		end
	end
	
	function NPCM.AddEnemyToTargetList(enemy,List) NPCM.Targets[List][enemy]=enemy end
	function NPCM.GetTargetList(List) return NPCM.Targets[List] end
	
	function NPCM.IsOnTargetList(enemy,List)
		if NPCM.Targets[List][enemy] then
			return true
		end
		return false
	end
	
	function NPCM.ManageTargetingData()
		for TID, Type in pairs(NPCM.Targets) do
			for ID, Target in pairs(Type) do
				if not IsValid(Target) or (Target:IsPlayer() and not Target:Alive()) or LDE:IsInSafeZone(Target) then
					Type[ID]=nil
				end
			end
		end
	
		local players = player.GetAll()
		for _, ply in ipairs( players ) do
			if IsValid(ply) and ply:IsConnected() and ply:Alive() then
				if tonumber(ply:GetLDEStat("Bounty"))>500 and not LDE:IsInSafeZone(ply) then
					if not NPCM.IsOnTargetList(ply,"Hostiles") then
						NPCM.AddEnemyToTargetList(ply,"Hostiles")
						--MsgAll(ply:Nick().." is wanted! Adding to targeting list. \n")
					end
				end
			end
		end
		
		if NPCM.SporeCheck<CurTime() then
			NPCM.SporeCheck=CurTime()+5
			local Spores = ents.FindByClass("lde_spore")
			for _, spore in ipairs( Spores ) do
				if IsValid(spore) then
					if IsValid(spore.Attached) then
						if not NPCM.IsOnTargetList(spore,"Spores") then
							NPCM.AddEnemyToTargetList(spore,"Spores")
						end
					end
				end
			end
		end
	end
	
	function NPCM.ManageShips()
		local Plys =  player.GetAll()	
		if table.Count(Plys)<1 then NPCM.RemoveShips() return end --No players, just remove all the npcs.
	
		for RID, Role in pairs(NPCM.Ships) do
			for ID, Ship in pairs(Role) do
				if not IsValid(Ship) then
					table.remove(Role,ID)--Purge none existant or dead ships out of the table.
				end
			end
		end
		
		if NPCM.StandbyRestock<CurTime() then
			if NPCM.ShipsOnStandby<NPCM.MaxShipsOnStandby then
				NPCM.ShipsOnStandby=NPCM.ShipsOnStandby+1
			end
			NPCM.StandbyRestock=CurTime()+300
		end
		
		
		if IsValid(NPCM.Station) then
			local Check = ents.FindInSphere( NPCM.StationFront(), 100 ) 
			if table.Count(Check)<1 then
				for ID, Role in pairs(NPCM.MaximumShips) do
					if ID~="Drone" and ID~="Military" and ID~="Police" then
						if table.Count(NPCM.Ships[ID])<Role then
							NPCM.SpawnShipAtStation(table.Random(NPCM.ShipData[ID]),NPCM.Ships[ID])
							break
						end
					elseif ID=="Drone" then
						if NPCM.SporePanicMode == true then
							if table.Count(NPCM.Ships.Drone)<NPCM.MaximumShips.Drone then
								NPCM.SpawnShipAtStation(NPCM.ShipData["Drone"]["SporeHunter"],NPCM.Ships.Drone)							
								break
							end
						end
					elseif ID=="Police" then
						if NPCM.ShipsOnStandby>0 then
							if table.Count(NPCM.Ships.Police)<NPCM.MaximumShips.Police then
								NPCM.SpawnShipAtStation(NPCM.ShipData["Police"]["PoliceFighter"],NPCM.Ships.Police)
								NPCM.ShipsOnStandby=NPCM.ShipsOnStandby-1
								break
							end
						end
					end
				end
			end
			
			local Spores = NPCM.GetTargetList("Spores")
			local SporesCount = table.Count(Spores)
			local MaxCheck = SporesCount>NPCM.MaximumSpores or table.Count(NPCM.Station.AttachedSpores)>NPCM.MaximumSpores/2
			if NPCM.SporePanicMode == false and MaxCheck then
				NPCM.SporePanicMode = true
				LDE:NotifyPlayers("Station","Attention, Spore Massing Detected! Deploying CounterMeasures.",{r=0,g=161,b=255})
			elseif NPCM.SporePanicMode and SporesCount<NPCM.MinimumSpores and table.Count(NPCM.Station.AttachedSpores)<1 then
				NPCM.SporePanicMode = false
				MsgAll("Panic Mode Disabled! \n")
			elseif NPCM.SporePanicMode and MaxCheck then
				local Station = NPCM.Station
				 if table.Count(Station.AttachedSpores)>150 then
					if not Station.SelfDestructActive or Station.SelfDestructActive == false then
						Station.SelfDestructActive = true
						LDE:NotifyPlayers("Station","Attention, Spore Mass Critical! Reactor Failing!",{r=255,g=0,b=0})
						timer.Simple(5,function() LDE:ExplodeCore(Station) end)
					end
				 end
			end
		end
		
		NPCM.ManageTargetingData()
	end
	
	Utl:SetupThinkHook("NPCShipControl",1,0,NPCM.ManageShips)

	function NPCM.SetupShipData(Role,Name,Data) NPCM.ShipData[Role][Name]=Data end
	
	--NPCM.SetupShipData("Other","Basic",{Speed=350,Hull=8000,Shields=6000,Model=""})
	NPCM.SetupShipData("Miner","KhamunMiner",{AI="Miner",Speed=200,TurnRate=0.3,Hull=8000,Shields=6000,Model="models/af/aff/aisn/khamun.mdl",Scale=2,
		trails = {
			{Offset=Vector(-250,0,100),Color=Color(150,255,255),StartWidth=100,Material="cable/xbeam.vmt"}
		}
	})	
	
	NPCM.SetupShipData("Trader","WhaleTrader",{AI="Trader",Speed=100,TurnRate=0.3,Hull=12000,Shields=8000,Model="models/af/aff/usn/whale.mdl",
		trails = {
			{Offset=Vector(-675,40,60),Color=Color(150,255,255),StartWidth=40,Material="cable/xbeam.vmt"},
			{Offset=Vector(-675,-40,60),Color=Color(150,255,255),StartWidth=40,Material="cable/xbeam.vmt"},
			
			{Offset=Vector(-420,100,-140),Color=Color(150,255,255),StartWidth=100,Material="cable/xbeam.vmt"},
			{Offset=Vector(-420,-100,-140),Color=Color(150,255,255),StartWidth=100,Material="cable/xbeam.vmt"}
		}	
	})
	
	NPCM.SetupShipData("Police","PoliceFighter",{AI="Police",Speed=500,TurnRate=0.3,Hull=8000,Shields=6000,Model="models/af/aff/usn/sword.mdl",Scale=4,
		weapons={
			{FireAngle=30,Spread=1,Damage=300,FireRate=0.3,Position=Vector(360,0,0)},
			{WepName="Missile",TrailColor=Color(180,180,30),HomingSpeed=40,Speed=100,FireAngle=40,Spread=1,Damage=300,FireRate=3,Position=Vector(0,180,0),Model="models/sbep_community/d12guidedbomb.mdl"},
			{WepName="Missile",TrailColor=Color(180,180,30),HomingSpeed=40,Speed=100,FireAngle=40,Spread=1,Damage=300,FireRate=3,Position=Vector(0,-180,0),Model="models/sbep_community/d12guidedbomb.mdl"}	
		},
		trails={
			{Offset=Vector(-200,75,12),Color=Color(255,190,0),StartWidth=200},
			{Offset=Vector(-200,-75,12),Color=Color(255,190,0),StartWidth=200}
		}
	})
	
	NPCM.SetupShipData("Drone","SporeHunter",{AI="SporeDrone",Speed=500,TurnRate=0.3,Hull=2000,Shields=1000,Model="models/af/aff/aisn/questes.mdl",
		trails = {
			{Offset=Vector(-35,-15,0)},
			{Offset=Vector(-35,15,0)}
		},
		weapons={
			{Type="Laser",Damage=200,FireAngle=5,FireRate=0.2,Position=Vector(360,0,0),FireSound="weapons/bison_main_shot_01.wav"}
		}
	})
	
	NPCM.SetupShipData("PlyDrone","DakaDrone",{AI="PlayerDrone",Speed=500,TurnRate=0.3,Hull=100,Shields=500,Model="models/spacebuild/nova/dronegun.mdl",
		trails = {
			{Offset=Vector(-13,0,22)}
		},
		weapons={
			{FireRate=0.3,Spread=0.8,Damage=50,Position=Vector(28,13,13)},
			{FireRate=0.3,Spread=0.8,Damage=50,Position=Vector(28,-13,13)}
		}
	})	
end

function NPCM.MakeNPCType(Data)
	local ENT = {}
	ENT.Type = "anim"
	ENT.Base = "envx_npc_ship"
	ENT.PrintName = Data.name
	ENT.Spawnable = false
	ENT.AdminOnly = true
	ENT.Category = "EnvX"
	
	if SERVER then Data.Func(ENT) end
	
	local Class = "envx_npc_ship_"..Data.Type
	scripted_ents.Register(ENT, Class, true, false)
	print("NPC AI Personality Registered: "..Class)
	
	return Class
end

function NPCM.SetupRoleData(Role,Data) NPCM.RoleData[Role]=NPCM.MakeNPCType({Type=string.lower(Role),Func=Data}) end

NPCM.SetupRoleData("Miner",function(ent)
	function ent:GetAsteroid()
		local Fs = ents.FindByClass("resource_asteroid")
		local roid = Fs[math.random(1,#Fs)]
		if self:HasLineOfSight(roid:GetPos()+Vector(0,0,200)) and roid:GetVelocity():Length()<40 then
			return roid
		end
	end
	
	function ent:GetObjective()
		if self.Memory.ReturnToStation == false then
			if not IsValid(self.Roid) then 
				local AsteroidCheck = self:GetAsteroid() 
				self.Roid = AsteroidCheck
				self.Memory.Mined = 0
				if not IsValid(AsteroidCheck) then
					return false
				end
			end
			local Pos = self.Roid:GetPos()+Vector(0,0,200)
			local Distance = self:GetPos():Distance(Pos)
			if Distance > 1000 then
				self:Approach(Pos,1000)
			else
				if self.Memory.Mined<100 then
					self.Memory.Mined = self.Memory.Mined+1
					self:SlowDown()
				else
					self.Memory.ReturnToStation = true
				end				
			end
		else
			if self:Approach(LDE.NPCMain.StationFront(),1000) then
				self.Memory.ReturnToStation = false
				self.Memory.Mined = 0
			end
		end
		return true
	end
end)

NPCM.SetupRoleData("SporeDrone",function(ent)
	function ent:AITargetingSystem()
		--MsgAll("Spore Drone Targeting Check! \n")
		local Spores = NPCM.GetTargetList("Spores")
		local Spore,Dist = nil,1000000
		for _, spore in pairs( Spores ) do
			if IsValid(spore) then
				if IsValid(spore.Attached) then
					local Distance = self:GetPos():Distance(spore:GetPos())
					if Distance<Dist then
						Spore = spore
						Dist = Distance
					end
				end
			end
		end
		
		if IsValid(Spore) then
			return Spore
		end
		
		if not NPCM.SporePanicMode then LDE:ExplodeCore(self) end
	end
	
	function ent:CombatRoutines()
		local CurTarg = self.Memory.Target
		if not IsValid(CurTarg) then
			local NewTarg = self:AITargetingSystem()
			if not IsValid(NewTarg) then return false end
			self.Memory.Target = NewTarg
		else
			self.AIFunctions.Flight.HoverRoutine(self,CurTarg:GetPos())
			return true
		end
	end		
	
	function ent:CheckHostile(ent)
		if IsValid(ent) then
			return ent:GetClass()=="lde_spore"
		end
	end
	
	function ent:GetObjective()
		
		return self:CombatRoutines()
	end
end)

NPCM.SetupRoleData("Police",function(ent)		
	function ent:AIDamageCheck(amount,attacker,inflictor)
		if attacker:GetClass()=="lde_spore" or attacker.IsEnvxNPC==true or attacker == self then return false end
		self.IsInCombat = true
		
		local Enemy = attacker
		if attacker:IsPlayer()==false then
			if IsValid(attacker.LDE.Core) then
				Enemy=attacker.LDE.Core
			end
		end
		
		--MsgAll("Adding new enemy! "..tostring(Enemy).."\n")
		
		NPCM.AddEnemyToTargetList(Enemy,"Hostiles")
		return false
	end
	
	function ent:AITargetingSystem()
		local Hostiles = NPCM.GetTargetList("Hostiles")
		
		--if table.Count(Hostiles)<1 and not NPCM.SporePanicMode then self.IsInCombat = false end
		if table.Count(Hostiles)<1 then self.IsInCombat = false end
	
		--if NPCM.SporePanicMode then
		--	return table.Random(NPCM.GetTargetList("Spores"))
		--else
			return table.Random(Hostiles)
		--end
	end
	
	function ent:ManageMissiles(Position)
		if not self.Memory.Bullets.Missile then self.Memory.Bullets.Missile = {} end
		if table.Count(self.Memory.Bullets.Missile)<1 then return end
		for I,miss in pairs(self.Memory.Bullets.Missile) do
			if IsValid(miss.Projectile) then
				miss.Data.HomingPos = Position
			else
				table.remove(self.Memory.Bullets.Missile,I)
			end
		end
	end
	
	function ent:CombatRoutines()
		local CurTarg = self.Memory.Target
		if not IsValid(CurTarg) or (IsValid(CurTarg) and CurTarg:IsPlayer() and not CurTarg:Alive()) or LDE:IsInSafeZone(CurTarg) then
			local NewTarg = self:AITargetingSystem()
			if not IsValid(NewTarg) then return false end
			self.Memory.Target = NewTarg
		else
			self:ManageMissiles(CurTarg:GetPos())
			self.AIFunctions.Flight.DiveRoutine(self,CurTarg:GetPos(),{Max=4000,Min=3000})
			--self.AIFunctions.Flight.HoverRoutine(self,CurTarg:GetPos())
			return true
		end
	end

	function ent:GetObjective()
		if table.Count(NPCM.GetTargetList("Hostiles"))>0 then self.IsInCombat = true end
		--if NPCM.SporePanicMode and table.Count(NPCM.GetTargetList("Spores"))>0 then self.IsInCombat = true end
		
		return false
	end
end)

NPCM.SetupRoleData("PlayerDrone",function(ent)	
	function ent:CheckHostile(ent)
		return true --Add Proper check here.
	end
	
	ent.IsEnvxDrone = true
	
	ent.CombatTypes = {}
	ent.CombatTypes["Hover"]=function(s,p) s.AIFunctions.Flight.HoverRoutine(s,p) end
	
	function ent:CombatRoutines()
		if not IsValid(self.Hanger) then LDE:ExplodeCore(self) end
		
		if self.Docked == false then
			if self.Active == true and self.TargetPos then
				if self.Attack == true then
					self.CombatTypes[self.CombatMode](self,self.TargetPos)
				else
					self:Approach(self.TargetPos,50)
				end
			else
				if self:Approach(self.Hanger:GetDockPosition(),200) then
					self.Docked = true
					self:SetPos(self.Hanger:GetDockPosition())
					self:SetAngles(self.Hanger:GetDockAngle())
					self:SetParent(self.Hanger)
				end
			end
		else
			if self.Active == true and self.TargetPos then
				self:SetParent(nil)
				self:SetPos(self.Hanger:GetDockPosition())
				self:SetAngles(self.Hanger:GetDockAngle())
				self.Docked = false
			end
		end
		
		return true		
	end
	
	function ent:GetObjective()
		return self:CombatRoutines()
	end
end)

hook.Add("PhysgunPickup", "Envx Enpc STAWPTOUCHING ME", function(ply,ent)
	if ent.IsEnvxNPC == true or ent.IsEnvxNPCStation == true or ent.IsSpore == true then
		if not ent.IsEnvxDrone == true then
			return ply:IsAdmin()
		end
	end
end)











